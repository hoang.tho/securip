from dataclasses import dataclass
from datetime import datetime
import os


@dataclass
class QSRIPProject:
    id: str = ''
    _id: str = ""
    project: str = ""
    client: str = ""
    sro: str = ""
    type: str = "APD"
    ver: int = 1
    create_date: datetime = datetime.now()
    last_modified: datetime = datetime.now()
    user_id: str = ""
    qgis_path: str = ""
    project_path: str = ""
    sor_path: str = ""
    jpg_path: str = ""
    boite_layer: str = ""
    cable_layer: str = ""
    pt_layer: str = ""
    error: int = 0
    warn: int = 0
    ok: int = 0
    tested: int = 0
    scripted: int = 0
    pbo = []
    cable = []
    files = []
    issues = []
    pts = []
    nro = ""
    sor_quality = []
    sor_error: int = 0
    sor_warn: int = 0
    sor_tested: int = 0
    sor_ok: int = 0
    count_error = 0
    vqse1 = 0
    vqse2 = 0
    vqse3 = 0
    info = {}
    images = []

    vqse_error: int = 0
    vqse_warn: int = 0
    vqse_tested: int = 0
    vqse_ok: int = 0
    code = {}

    def set_project_path(self, project_path):
        if len(project_path) > 0:
            if len(self.project_path) == 0:
                self.project_path = os.path.join(project_path, self.client, self.project, self.sro,
                                                 "{} V{}".format(self.type, self.ver))
            if len(self.qgis_path) == 0:
                self.qgis_path = os.path.join(self.project_path, "qgis")
                if not os.path.exists(self.qgis_path):
                    os.makedirs(self.qgis_path)
            if len(self.sor_path) == 0:
                self.sor_path = os.path.join(self.project_path, "sor")
            if len(self.jpg_path) == 0:
                self.jpg_path = os.path.join(self.project_path, "jpg")


@dataclass
class QSRIPIssue:
    id: str = ""
    level: int = 1
    client: str = ""
    project: str = ""
    apd: str = "APD"
    ver: int = 1
    create_date: datetime = datetime.now()
    last_update = []
    nro: str = ""
    type: str = ""
    user_id: str = ""
    assignee_id: str = ""
    assignee_name: str = ""
    username: str = ""
    code: str = ""
    pbo: str = ""
    issue_type: str = "script"
    status: int = 0
    text: str = ""
    content = {}

    def fromProject(self, p, u):
        self.client = p.client
        self.project = p.project
        self.apd = p.type
        self.ver = p.ver
        self.nro = p.sro
        self.user_id = u.userId
        self.username = u.displayName
        self.assignee_id = u.userId
        self.assignee_name = u.displayName

@dataclass
class PBO:
    proj_id: str = ""
    id: str = ""
    sro: str = ""
    ver: str = ""
    name: str = ""
    cable: str = ""
    pbo_id: int = 0
    reference: str = ""
    function: str = ""
    support: str = ""
    type: str = ""
    model: str = ""
    pt: str = ""
    x: float = 0.0
    y: float = 0.0
    error: int = 0
    vsqe: int = 0
    thdCodeExt = ""
    images = []
    comments = {}

@dataclass
class Chat:
    date_create: int = 0
    user_name: str = ''
    user_id: str = ''
    proj_id: str = ''
    pbo: str = ''
    text: str = ''
    file: str = ''
    user_read = []

@dataclass
class PointTechnique:
    sro: str = ""
    ver: str = ""
    name: str = ""
    code: str = ""
    x: float = 0.0
    y: float = 0.0
    type: str = ""


@dataclass
class Cable:
    id: str = ""
    sro: str = ""
    ver: str = ""
    name: str = ""
    src: str = ""
    dest: str = ""
    type: str = ""
    length: float = 0.0
    section: str = ""
    capacity: int = 0
    error: int = 0
    vsqe: int = 0
