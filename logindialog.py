# LoginDialog
from PyQt5 import QtWidgets, uic, QtCore, QtGui
from PyQt5.QtCore import Qt, QSettings
from PyQt5.QtGui import QMovie
from PyQt5.QtWidgets import QVBoxLayout, QLabel, QWidget
import requests
import json
import os
from user import QSRIPUser
import const
import qsriputils
import hashlib
import platform
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
f = open("lang.txt", 'r')
from multilang import english, french
if f.read() == 'en':
    ll = english
else:
    ll = french
f.close()

import env

current_dir = os.path.dirname(os.path.abspath(__file__))
Form, Base = uic.loadUiType(os.path.join(current_dir, "ui/login.ui"))


class LoginFrame(QtWidgets.QFrame):
    def __init__(self, parent=None):
        super(LoginFrame, self).__init__(parent)
        uic.loadUi("ui/login.ui", self)

        self.label.setText(ll['Label_login'])
        self.remBox.setText(ll['Remember_Password'])
        self.buttonLogin.setText(ll['log_in'])
        self.buttonCancel.setText(ll['Cancel'])

        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | Qt.SplashScreen)
        settings = QSettings("settings.ini", QSettings.IniFormat)
        self.rem = settings.value("rem", True, type=bool)
        qsriputils.print_rip(f"rem:{self.rem}")
        self.remBox.setChecked(self.rem)
        if self.rem:
            email = settings.value("email")
            pw = settings.value("pw")
            self.lineEmail.setText(email)
            self.linePassword.setText(pw)

    def updateText(self, lang):
        global ll
        ll = lang
        self.label.setText(ll['Label_login'])
        self.remBox.setText(ll['Remember_Password'])
        self.buttonLogin.setText(ll['log_in'])
        self.buttonCancel.setText(ll['Cancel'])


class LoginMacOS(QtWidgets.QDialog):
    loginFrame: LoginFrame = None
    def __init__(self, parent=None):
        super(LoginMacOS, self).__init__(parent)
        self.user = QSRIPUser()
        self.loginFrame = LoginFrame()
        layout = QVBoxLayout()
        self.setLayout(layout)
        layout.addWidget(self.loginFrame)
        self.loginFrame.buttonLogin.clicked.connect(self.__login)

    def __login(self):
        email = self.loginFrame.lineEmail.text().strip()
        password = self.loginFrame.linePassword.text()
        if not email or not password:
            self.loginFrame.errLabel.setText(ll['Enter_email'])
        else:
            self.__handleLogin(email, password)

    def __handleLogin(self, email, password):
        qsriputils.print_rip("{}/{}".format(email, password))
        pw_md5 = hashlib.md5(password.encode("utf-8")).hexdigest()
        user_ref = qsriputils.db.collection('users')
        users = dict()
        for user in user_ref.get():
            k = user.to_dict()
            users[k['email']] = [k['password'], k['userId'], k['displayName'], k['role'], k['project_ids'],
                                 k['isOnline']]
        if email in users and pw_md5 in users[email]:
            if users[email][5] and not users[email][5]:
                self.loginFrame.errLabel.setText(ll['Account_online'])
                self.loginFrame.buttonLogin.setChecked(False)
            else:
                qsriputils.print_rip('logged in')
                settings = QSettings("settings.ini", QSettings.IniFormat)
                rem = self.loginFrame.remBox.isChecked()
                settings.setValue("rem", rem)
                if rem:
                    settings.setValue("email", email)
                    settings.setValue("pw", password)
                else:
                    settings.remove("email")
                    settings.remove("pw")
                self.user.email = email
                self.user.displayName = users[email][2]
                self.user.role = users[email][3]
                self.user.password = password
                self.user.userId = users[email][1]
                self.user.project_ids = users[email][4]
                user_ref.document(users[email][1]).set({'isOnline': True}, merge=True)
                self.accept()
        else:
            self.loginFrame.errLabel.setText(ll['Login_Error'])
            qsriputils.print_rip('Email or password does not exist')
            # qsriputils.print_rip(result.get("error_description"))
            # qsriputils.print_rip(result.get("correlation_id"))  # You may need this when reporting a bug
            self.loginFrame.buttonLogin.setChecked(False)


class LoginDialog(QtWidgets.QDialog):
    loginFrame: LoginFrame = None

    def __init__(self, ll, parent=None):
        super(LoginDialog, self).__init__(parent)
        uic.loadUi("ui/logindialog.ui", self)
        self.user = QSRIPUser()
        layout = QtWidgets.QHBoxLayout(self.mainFrame)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.mediaPlayer = QMovie(os.path.join(env.PATH["root_path"], "opening.gif"))
        self.mediaPlayer.setScaledSize(self.mainFrame.rect().size())
        self.mediaLabel = QLabel()
        self.mediaLabel.setMovie(self.mediaPlayer)
        layout.addWidget(self.mediaLabel)
        self.mediaPlayer.finished.connect(self.gifFinished)
        # self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.error.connect(self.handleError)
        self.mediaPlayer.start()
        radius = 10.0
        path = QtGui.QPainterPath()
        path.addRoundedRect(QtCore.QRectF(self.rect()), radius, radius)
        mask = QtGui.QRegion(path.toFillPolygon().toPolygon())
        self.setMask(mask)
        self.ll = ll


    def mediaStateChanged(self, state):
        if state == QMovie.Paused:
            self.loginFrame = LoginFrame(self.mainFrame)
            self.loginFrame.adjustSize()
            self.loginFrame.show()
            self.loginFrame.buttonLogin.clicked.connect(self.__login)
            self.loginFrame.buttonCancel.clicked.connect(self.reject)
            self.loginFrame.setStyleSheet("background-color:transparent")
            self.loginFrame.errLabel.setText("")
            qsriputils.print_rip("login frame: ", self.loginFrame.rect())
            qsriputils.print_rip("main frame: ", self.mainFrame.rect())
            dx = (self.width() - self.loginFrame.width()) / 2
            dy = (self.height() - self.loginFrame.height()) / 2
            qsriputils.print_rip("dx, dy: ", dx, dy)
            self.loginFrame.updateText(self.ll)
            # self.loginFrame.move(dx, dy)


    def gifFinished(self):
        self.loginFrame = LoginFrame(self.mainFrame)
        self.loginFrame.adjustSize()
        self.loginFrame.show()
        self.loginFrame.buttonLogin.clicked.connect(self.__login)
        self.loginFrame.buttonCancel.clicked.connect(self.reject)
        self.loginFrame.setStyleSheet("background-color:transparent")
        self.loginFrame.errLabel.setText("")
        qsriputils.print_rip("login frame: ", self.loginFrame.rect())
        qsriputils.print_rip("main frame: ", self.mainFrame.rect())
        dx = (self.width() - self.loginFrame.width()) / 2
        dy = (self.height() - self.loginFrame.height()) / 2
        qsriputils.print_rip("dx, dy: ", dx, dy)
        self.loginFrame.updateText(self.ll)


    def handleError(self, e):
        qsriputils.print_rip("error", self.mediaPlayer.errorString())

    def accept(self):
        self.loginFrame.setParent(None)
        super(LoginDialog, self).accept()

    def reject(self):
        self.loginFrame.setParent(None)
        super(LoginDialog, self).reject()

    def __login(self):
        email = self.loginFrame.lineEmail.text().strip()
        password = self.loginFrame.linePassword.text()
        if not email or not password:
            self.loginFrame.errLabel.setText(ll['Enter_email'])
        else:
            self.__handleLogin(email, password)

    def __handleLogin(self, email, password):
        qsriputils.print_rip("{}/{}".format(email, password))
        pw_md5 = hashlib.md5(password.encode("utf-8")).hexdigest()
        user_ref = qsriputils.db.collection('users')
        users = dict()
        for user in user_ref.get():
            k = user.to_dict()
            users[k['email']] = [k['password'], k['userId'], k['displayName'], k['role'], k['project_ids'],
                                 k['isOnline']]
            if 'uuid' in k:
                users[k['email']].append(k['uuid'])
        if email in users and pw_md5 in users[email]:
            if users[email][5] and len(users[email]) == 7 and users[email][6] != env.UUID:
                self.loginFrame.errLabel.setText(ll['Account_online'])
                self.loginFrame.buttonLogin.setChecked(False)
            else:
                qsriputils.print_rip('logged in')
                settings = QSettings("settings.ini", QSettings.IniFormat)
                rem = self.loginFrame.remBox.isChecked()
                settings.setValue("rem", rem)
                if rem:
                    settings.setValue("email", email)
                    settings.setValue("pw", password)
                else:
                    settings.remove("email")
                    settings.remove("pw")
                self.user.email = email
                self.user.displayName = users[email][2]
                self.user.role = users[email][3]
                self.user.password = password
                self.user.userId = users[email][1]
                self.user.project_ids = users[email][4]
                user_ref.document(users[email][1]).set({'isOnline': True, 'uuid': env.UUID}, merge=True)
                self.accept()
        else:
            self.loginFrame.errLabel.setText(ll['Login_Error'])
            qsriputils.print_rip('Email or password does not exist')
            # qsriputils.print_rip(result.get("error_description"))
            # qsriputils.print_rip(result.get("correlation_id"))  # You may need this when reporting a bug
            self.loginFrame.buttonLogin.setChecked(False)
