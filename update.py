import firebase_admin
import os
import subprocess
import sys
from firebase_admin import storage, credentials
from PyQt5.QtWidgets import QWidget, QApplication, QMainWindow, QVBoxLayout, QLabel
import zipfile

import env
from RIPUtils import cipher_utils


class RipUpdater(QMainWindow):
    def __init__(self, version):
        QMainWindow.__init__(self)
        filename = f"rip_{version}.zip"
        filepath = os.path.join(env.PATH["root_path"], filename)
        if os.path.exists(filepath):
            os.remove(filepath)
        blob = bucket.blob(f"version/{filename}")
        blob.download_to_filename(filepath)
        with zipfile.ZipFile(filepath, 'r') as zip_ref:
            zip_ref.extractall(env.PATH["root_path"])
        os.remove(filepath)
        self.setWindowTitle("SecuRip Updater")
        lb = QLabel("Update done!")
        layout = QVBoxLayout()
        layout.addWidget(lb)
        content = QWidget()
        content.setLayout(layout)
        self.setCentralWidget(content)

    def relaunch(self):
        subprocess.Popen(f"{env.PATH['root_path']}\\..\\securrip.bat", creationflags=subprocess.DETACHED_PROCESS | subprocess.CREATE_NEW_PROCESS_GROUP)
        sys.exit()


if __name__ == "__main__":
    db_cert = cipher_utils.decrypt_db_cert(os.path.join(env.PATH["cert_path"], env.ENCRYPTED_DB_CERT_FILE))
    cred = credentials.Certificate(db_cert)
    firebase_admin.initialize_app(cred, {"storageBucket": env.DB_BUCKET_URI})
    bucket = storage.bucket()

    app = QApplication(sys.argv)
    v = sys.argv[1]
    updater = RipUpdater(v)
    updater.show()
    app.exec_()
