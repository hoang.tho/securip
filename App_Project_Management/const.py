MS_ENDPOINT = "https://graph.microsoft.com/v1.0/"
dbpath = r"\db\qsrip.db"
project_path = r""

add_layer = "Added Layer: <b>{}</b>"
client_text = "Société: <b>{}</b>"
project_text = "Projet: <b>{}</b>"
key_loggined = "loggined"

key_last_opened_project = "last_opened_project"
preimport_script = "ControlPreImportInBD_V0.0.5c.py"
sys_script = "ControlIngeOnBO_V0.0.4b.py"
sor_date_format = "%a %b %d %H:%M:%S %Y"
version_client = '1.2.14'
version_default = 'Version: <b>{}</b>'
uuid = ''
app_mode = "dev" # dev or proddev

errCode = {}
sorError = [
    "Reserve errors",
    "BOX Location errors",
    "Fiber Constraint errors",
    "Inconsistent optical measurements"
]
srip_error_key = "#SRip_Error"
co_double_error_key = "#CO_Double"
customError = {
    "#SRip_Error": "SecurRip a reconstruit l'attribut car il n'était pas renseigné. Il vous faut le vérifier dans le fichier d'origine.",
    "#CO_Double": "SecurRip a détecté un able en double qui ne devrait pas être présent. Il vous faut le vérifier dans le fichier d'origine"
}
etat = ["A traiter", "En cours", "A reprendre", "Ignoré", "Cloturé"]
sor_time_error = "Il semblerait que la mesure soit dupliquée"
sql_create_projects_table = """ CREATE TABLE IF NOT EXISTS project (
                                    id integer PRIMARY KEY,
                                    name text NOT NULL,
                                    client text NOT NULL,
                                    type text NOT NULL,
                                    ver integer DEFAULT 1 NOT NULL,
                                    sro text,
                                    boite_layer text,
                                    cable_layer text,
                                    error integer DEFAULT 0 NOT NULL,
                                    warning integer DEFAULT 0 NOT NULL,
                                    tested integer DEFAULT 0 NOT NULL,
                                    create_date timestamp,
                                    last_modified timestamp,
                                    scripted integer DEFAULT 0 NOT NULL,
                                    user_id text NOT NULL
                                ); """

sql_create_cable_table = """ CREATE TABLE IF NOT EXISTS cable (
                                    id integer PRIMARY KEY,
                                    project_id integer NOT NULL,
                                    qgis_id integer NOT NULL,
                                    name text NOT NULL,
                                    src text,
                                    dest text,
                                    length text,
                                    capacity integer DEFAULT 0 NOT NULL,
                                    section text
                                ); """

sql_create_issues_table = """ CREATE TABLE IF NOT EXISTS issues (
                                    id integer PRIMARY KEY,
                                    project_id integer NOT NULL,
                                    level integer DEFAULT 1 NOT NULL,
                                    type text,
                                    code text,
                                    create_date timestamp,
                                    pbo text
                                ); """

sql_create_pbo_table = """ CREATE TABLE IF NOT EXISTS pbo (
                                    id integer PRIMARY KEY,
                                    name text NOT NULL,
                                    qgis_id integer NOT NULL,
                                    project_id integer NOT NULL,
                                    reference text,
                                    type text DEFAULT "PBO" NOT NULL,
                                    support text
                                ); """

sql_create_user_table = """ CREATE TABLE IF NOT EXISTS user (
                                    id integer PRIMARY KEY,
                                    userId text NOT NULL,
                                    displayName text NOT NULL,
                                    email text,
                                    last_loggined timestamp,
                                    role text DEFAULT 'user'
                                ); """

sql_create_config_table = """ CREATE TABLE IF NOT EXISTS config (
                                    key text NOT NULL,
                                    value text NOT NULL
                                ); """
