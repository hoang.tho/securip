import json
import os
import sys

from PyQt5.QtWidgets import QProgressBar, QHeaderView, QApplication, QWidget, QGridLayout, QPushButton, QTableWidget, \
    QTableWidgetItem, QHBoxLayout, QMainWindow, QVBoxLayout, QDialog, QLabel
from PyQt5 import QtCore, QtWidgets
import cipher_utils

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

import const


class Users(QTableWidget):
    def __init__(self, data, *args):
        super(QTableWidget, self).__init__(*args)
        self.data = data
        self.layout2 = None
        self.layout3 = None
        self.setRowCount(len(data))
        self.setColumnCount(2)
        self.setHorizontalHeaderLabels(["Users_id", 'userName'])
        self.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        a = 0
        for row in range(len(data)):
            userIds = QTableWidgetItem(data['row' + str(a)][0])
            userName = QTableWidgetItem(data['row' + str(a)][1])
            self.setItem(row, 0, userIds)
            self.setItem(row, 1, userName)
            a += 1

        self.clicked.connect(self.showProject)

    def showProject(self, cell):
        user_selected = cell.data()
        list_proj = []
        role_user_select = None
        for user in self.data:
            if self.data[user][0] == user_selected:
                list_proj = self.data[user][2]
                role_user_select = self.data[user][3]
        self.layout2.change(user_selected, list_proj)
        self.layout3.select(role_user_select)


class Project_in_User(QTableWidget):
    def __init__(self, data, db, userWidget, *args):
        super(QTableWidget, self).__init__(*args)
        layout = QVBoxLayout()
        button_del = QPushButton('Del')
        self.procs_bar = QProgressBar()
        self.procs_bar.setGeometry(0, 0, 100, 10)
        self.procs_bar.setMaximum(100)
        layout.addWidget(button_del, alignment=QtCore.Qt.AlignBottom)
        layout.addWidget(self.procs_bar)
        self.setLayout(layout)
        self.data = data
        self.db = db
        self.userWidget = userWidget
        self.projs_del = dict()
        self.setRowCount(len(data))
        self.setColumnCount(2)
        self.setHorizontalHeaderLabels(["All Project", 'Delete Project'])
        self.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        c = 0
        for row in data:
            proj_id = QTableWidgetItem(data[row][0])
            proj_id.setCheckState(QtCore.Qt.Checked)
            self.setItem(c, 0, proj_id)
            check_box = QTableWidgetItem()
            check_box.setCheckState(QtCore.Qt.Unchecked)
            self.setItem(c, 1, check_box)
            c += 1
        self.clicked.connect(self.stateProject)
        button_del.clicked.connect(self.delete_proj)

    def change(self, user_id, list_proj):
        self.user_id = user_id
        self.list_proj = list_proj
        self.setHorizontalHeaderLabels(["Project in User", 'Delete Project'])
        self.setRowCount(len(self.data))
        self.setColumnCount(2)
        i = 0
        for proj in self.data:
            if self.data[proj][0] in list_proj:
                item = QTableWidgetItem(self.data[proj][0])
                item.setCheckState(QtCore.Qt.Checked)
                self.setItem(i, 0, item)
            else:
                item = QTableWidgetItem(self.data[proj][0])
                item.setCheckState(QtCore.Qt.Unchecked)
                self.setItem(i, 0, item)
            i += 1

    def stateProject(self, cell):
        row = cell.row()
        col = cell.column()
        state = self.item(row, col).checkState()
        if col == 0:
            if state == 2:
                self.list_proj.append(cell.data())
            elif state == 0:
                self.list_proj.remove(cell.data())
            db.collection('users').document(self.user_id).set({'project_ids': self.list_proj}, merge=True)
        if col == 1:
            if state == 2:
                self.projs_del[self.item(row, 0).text()] = row
            elif state == 0:
                del self.projs_del[self.item(row, 0).text()]

    def delete_proj(self):
        print(self.projs_del)
        self.procs_bar.setValue(0)
        size = len(self.projs_del)
        self.delete_db(size, u'cable', 1, 24)
        self.delete_db(size, u'issues', 25, 24)
        self.delete_db(size, u'pbo', 50, 24)
        self.delete_db(size, u'sor_quality', 75, 24)
        rows = list()
        batch = self.db.batch()
        for proj_id in self.projs_del:
            doc = self.db.collection(u'proj').document(proj_id)
            user_id = doc.get().to_dict()['user_id']
            user_ref = db.collection('users').document(user_id)
            project_ids = user_ref.get().to_dict()['project_ids']
            project_ids.remove(proj_id)
            user_ref.set({'project_ids': project_ids}, merge=True)
            batch.delete(doc)
            rows.append(self.projs_del[proj_id])
        batch.commit()
        rows.sort(reverse=True)
        for row in rows:
            self.remove_row(row)
        self.projs_del = dict()
        self.procs_bar.setValue(100)

    def delete_db(self, size, table, barpercent_start, barpercent_length):
        docs = self.db.collection(table).get()
        count = 0
        for proj_id in self.projs_del.keys():
            print(table, 'proj_id:', proj_id)
            batch = self.db.batch()
            batch_count = 0
            for doc in docs:
                doc_dict = doc.to_dict()
                if 'proj_id' in doc_dict and doc_dict['proj_id'] == proj_id:
                    batch.delete(doc.reference)
                    if batch_count == 300:
                        print('batch commit', table)
                        batch.commit()
                        batch = self.db.batch()
                        batch_count = 0
                    batch_count += 1
            print('batch commit', table)
            batch.commit()
            count += 1
            self.procs_bar.setValue(barpercent_start + barpercent_length * (count // size))

    def remove_row(self, row):
        self.removeRow(row)
        for i in range(row, len(self.data) - 1):
            self.data['row' + str(i)] = self.data['row' + str(i + 1)]
        self.data.pop('row' + str(len(self.data) - 1), None)
        print(self.data)


class Role(QTableWidget):
    def __init__(self, *args):
        super(QTableWidget, self).__init__(*args)
        self.roles = {'admin': 'admin', 'be': 'be', 'cdtx': 'cdtx', 'vqse': 'vqse', 'tech': 'tech', 'stt': 'stt'}
        self.setRowCount(len(self.roles))
        self.setColumnCount(1)
        self.setHorizontalHeaderLabels(["Role"])
        self.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        r = 0
        for role in self.roles:
            item = QTableWidgetItem(role)
            item.setCheckState(QtCore.Qt.Unchecked)
            self.setItem(r, 0, item)
            r += 1

        layout = QVBoxLayout()
        lb = QLabel('LABEL')
        button = QPushButton('CLICK')
        layout.addWidget(lb)
        layout.addWidget(button)
        self.setLayout(layout)
        button.clicked.connect(self.dialog)

    def dialog(self):
        dlg = QDialog()
        layout = QVBoxLayout()
        wg1 = QLabel('label 1')
        wg2 = QLabel('label 2')
        layout.addWidget(wg1)
        layout.addWidget(wg2)
        dlg.setLayout(layout)
        dlg.exec()


    def select(self, role):
        i = 0
        for r in self.roles:
            if r == role:
                item = QTableWidgetItem(r)
                item.setCheckState(QtCore.Qt.Checked)
                self.setItem(i, 0, item)
                i += 1
            else:
                item = QTableWidgetItem(r)
                item.setCheckState(QtCore.Qt.Unchecked)
                self.setItem(i, 0, item)
                i += 1


class MainWindow(QMainWindow):
    def __init__(self, data_proj, data_user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        main_layout = QHBoxLayout()
        layout_1 = Users(data_user)
        layout_2 = Project_in_User(data_proj, db, layout_1)
        layout_3 = Role()
        layout_1.layout2 = layout_2
        layout_1.layout3 = layout_3
        main_layout.addWidget(layout_1)
        main_layout.addWidget(layout_2)
        main_layout.addWidget(layout_3)
        layout_2.setMaximumWidth(800)
        layout_3.setMaximumWidth(350)
        main_widget = QWidget()
        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)


app = QApplication(sys.argv)

if const.app_mode == 'prod':
    firebase_json = json.loads(cipher_utils.decrypt('encrypted-firebase-admin.json'))

    cred = credentials.Certificate(firebase_json)
    firebase_admin.initialize_app(cred, {
        'storageBucket': 'secu-rip-ad662.appspot.com'
    })
else:
    cred = credentials.Certificate('dev-firebase-admin.json')
    firebase_admin.initialize_app(cred, {
        'storageBucket': 'securip-dev.appspot.com'
    })
db = firestore.client()
projs = db.collection(u'proj').stream()
data_proj = dict()
data_user = dict()
i = 0
for proj in projs:
    p = proj.to_dict()
    data_proj['row' + str(i)] = [p['proj_id'], p['user_id']]
    i += 1

users = db.collection('users').stream()
x = 0
for user in users:
    u = user.to_dict()
    data_user['row' + str(x)] = [u['userId'], u['displayName'], u['project_ids'], u['role']]
    x += 1

widget = MainWindow(data_proj, data_user)
widget.setWindowTitle("Project Management")
widget.setGeometry(50, 100, 2200, 700)
widget.show()
app.exec()
