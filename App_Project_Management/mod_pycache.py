import os

def rename():
    pycache_path = '../__pycache__/'
    filenames = os.listdir(pycache_path)
    urly_names = []
    pretty_names = []
    for filename in filenames:
        filename_part = filename.split('.')
        if len(filename_part) == 2:
            pretty_names.append(filename)
        else:
            urly_names.append(filename_part)
    for filename_part in urly_names:
        mod_filename = '.'.join(filename_part[:-2]) + '.' + filename_part[-1]
        if mod_filename in pretty_names:
            os.remove(pycache_path + mod_filename)
        os.rename(pycache_path + '.'.join(filename_part), pycache_path + mod_filename)

if __name__ == '__main__':
    rename()