import json
import hashlib
import os
import subprocess


APP_MODE = "prod"
# APP_MODE = "dev"
# ENCRYPTED_DB_CERT_FILE = "encrypted-firebase-admin.cert"
# DB_BUCKET_URI = "secu-rip-ad662.appspot.com"
# ENCRYPTED_DB_CERT_FILE = "encrypted-dev-firebase-admin.cert"
# DB_BUCKET_URI = "securip-dev.appspot.com"
ENCRYPTED_DB_CERT_FILE = "eiffage-firebase.cert"
DB_BUCKET_URI = "eiffage-1b45f.appspot.com"

DB_KEY = b"SecurRipSecurRip"
LICENSE_PUBKEY = "-----BEGIN PUBLIC KEY-----\n" \
                 "MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgH7WA0ueaMyvZn/w4G0qlprDucHI\n" \
                 "/bnTebuiCYKstCt1qCISAxYDyCHcVQ6/PWEH5r0GcoPltqngMeupjUNUKLTBhngz\n" \
                 "SFJuy+y+1lUwd6yOjJCVke7JmpNOsF1jyMPW528/qfD+NqkM4BJgNfOnvIw+Nxws\n" \
                 "ZBjFVbk2xXK0QMdLAgMBAAE=\n" \
                 "-----END PUBLIC KEY-----"

VM_MODELS = ["HVM domU", "Virtual Machine", "VirtualBox", "KVM", "VMware Virtual Platform"]
VM_MNFTS = ["Xen", "Microsoft Corporation", "innotek GmbH", "Red Hat", "VMware, Inc."]

MAIL_SENDER_ADDR = "securrip.crash@gmail.com"
MAIL_SENDER_PWD = "S@curRip86"
MAIL_RECEIVER_ADDR = "toanleduc1@gmail.com"
SMTP_ADDR = "smtp.gmail.com"
SMTP_PORT = 587

UUID = ""
PATH = dict()
CONFIG = dict()
SERVER_ADDRESS = "http://35.192.145.103"

SYS_SCRIPT = "P01_V0.0.4b.pyc"
PREIMPORT_SCRIPT = "B01_V0.0.5c.pyc"


def __get_paths() -> None:
    global PATH
    root_path = os.path.abspath("./")
    PATH["root_path"] = root_path
    create_paths = {
        "db_path": os.path.join(root_path, "db"),
        "logs_path": os.path.join(root_path, "logs"),
        "projects_path": os.path.join(root_path, "projects"),
        "temp_path": os.path.join(root_path, "temp"),
        "tools_path": os.path.join(root_path, "tools")
    }  # path will be create if not exists
    exist_paths = {
        "images_path": os.path.join(root_path, "images"),
        "fonts_path": os.path.join(root_path, "fonts"),
        "ui_path": os.path.join(root_path, "ui"),
        "cert_path": os.path.join(root_path, "cert"),
        "videos_path": os.path.join(root_path, "videos"),
        "box_path": os.path.join(root_path, "box")
    }  # path must exist
    for path in create_paths:
        if not os.path.exists(create_paths[path]):
            os.mkdir(create_paths[path])
        PATH[path] = create_paths[path]
    for path in exist_paths:
        if not os.path.exists(exist_paths[path]):
            raise Exception(f"{exist_paths[path]} not found!")
        PATH[path] = exist_paths[path]


def __get_uuid() -> None:
    global UUID
    uuid = subprocess.check_output("WMIC CSPRODUCT GET UUID").split(b"\n")[1].split()[0]
    uuid_md5 = hashlib.md5(uuid).hexdigest()
    UUID = "-".join([uuid_md5[i:i+5].upper() for i in range(0, 32, 8)])


def __get_config() -> None:
    global CONFIG
    config_file = open(os.path.join(PATH["root_path"], "config.json"))
    CONFIG = json.load(config_file)


__get_paths()
__get_config()
if APP_MODE == 'prod':
    __get_uuid()
