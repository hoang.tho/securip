# This Python file uses the following encoding: utf-8
import numpy as np
from scipy.ndimage import zoom
from matplotlib import cm
from matplotlib import colors
from scipy.signal import find_peaks


def round_sig(value, significant_figures):
    '''Rounds a value to a number of significant figures.
    However the value is less than 1 then simply round it to 1 D.P.'''
    if value < 1:
        return round(value, 1)
    return round(value, -int(np.floor(np.sign(value) * np.log10(abs(value)))) + significant_figures)


def _low_pass_filter_trace(a_raw_trace, window_len):
    '''A simple LowPass Hanning filter'''
    samples = np.r_[a_raw_trace[0][window_len-1:0:-1],
                    a_raw_trace[0],
                    a_raw_trace[0][-2:-window_len-1:-1]]
    window = np.hanning(window_len)
    a_smoothed_levels = np.convolve(window/window.sum(), samples, mode='valid')
    trim = min(len(a_smoothed_levels), len(a_raw_trace[1]))
    a_trace = np.array([a_smoothed_levels[:trim], a_raw_trace[1][:trim]])
    return a_trace


def prepare_data(d_data, window_len):
    '''Transforms the trace data to unify sample width and signal quality'''
    a_raw_trace = d_data["trace"]
    # Smoothing
    a_smooth_trace = _low_pass_filter_trace(a_raw_trace, window_len)
    # Scale to ensure resolution per distance unit is equal.
    a_trace = zoom(a_smooth_trace, zoom=(1.0, d_data["meta"]["FxdParams"]["resolution"]), order=1)
    # Offsetting to make all launch levels the same
    n_offset = -a_trace[0][325]
    a_trace[0] = a_trace[0] + n_offset
    return {"meta": d_data["meta"], "trace": a_trace}


def differentiate_data(d_data):
    '''Calculates the 1st order differential of the data'''
    a_raw_trace = d_data["trace"]
    a_diff_trace = np.diff(a_raw_trace[0])
    a_clean_trace = []
    for sample_index in range(len(a_raw_trace[0])):
        if sample_index < len(a_diff_trace)-1:
            a_clean_trace.append(a_diff_trace[sample_index])
        else:
            a_clean_trace.append(0)
    return [a_clean_trace, a_raw_trace]


def find_edges(a_differential_trace):
    '''Finds windows that contain features'''
    a_abs_trace = [abs(sample) for sample in a_differential_trace[0]]
    a_peaks = find_peaks(a_abs_trace, 0.00125, width=5, distance=150)
    return [a_peaks[0],
            [a_differential_trace[1][0][peak] for peak in a_peaks[0]],
            [a_differential_trace[1][1][peak] for peak in a_peaks[0]]]


def wavelength_to_rgb(s_wavelength):
    '''Convert the wavelength to a spectral 'false' colour'''
    wavelength = int(s_wavelength[:-3])
    norm = colors.Normalize(vmin=1250, vmax=1650, clip=True)
    mapper = cm.ScalarMappable(norm=norm, cmap=cm.jet_r)
    red, green, blue, _ = mapper.to_rgba(wavelength)
    return "#{:02X}{:02X}{:02X}".format(int(red*255), int(green*255), int(blue*255))

