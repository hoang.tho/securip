from PyQt5 import QtWidgets, QtCore, uic, QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QTextDocument, QPixmap
from PyQt5.QtWidgets import QTableWidgetItem, QHBoxLayout, QToolButton, QPushButton, QLabel, QTreeWidgetItem, \
    QVBoxLayout, QWidget
from qsripproject import QSRIPProject
import qsriputils
import os
import const
from PyQt5 import QtCore, QtGui, QtWidgets, QtPrintSupport
from PyQt5.Qt import QCursor
from pathlib import Path
import zipfile
from qgis.core import QgsApplication, QgsVectorLayer, QgsProject, QgsFeatureRequest, QgsExpression
from qgis.gui import QgsMapCanvas

f = open("lang.txt", 'r')
from multilang import english, french

if f.read() == 'en':
    ll = english
else:
    ll = french
f.close()


class FieldTab(QtWidgets.QFrame):
    proj: QSRIPProject = None
    window = None
    pendingDraw = True
    issues = []
    selectedIssue = None

    def on_change_assignee(self, index):
        if index > 0:
            row = self.tableWidget.currentRow()
            qsriputils.print_rip(f"on_change_assignee: {index} selected row: {self.tableWidget.currentRow()}")
            self.tableWidget.item(row, 6).setText(self.assigneeBox.currentText())
            pbo = self.selectedIssue.pbo
            sub = [issue for issue in self.issues if
                   issue.pbo == pbo and issue.issue_type == self.selectedIssue.issue_type]
            for issue in sub:
                issue.assignee_id = self.window.techniciens[index - 1].userId
                issue.assignee_name = self.window.techniciens[index - 1].displayName
                qsriputils.update_issue(issue)
                for i in range(self.tableWidget.rowCount()):
                    it = self.tableWidget.item(i, 0)
                    issue_id = it.data(Qt.UserRole)
                    if issue_id == issue.id:
                        self.tableWidget.item(i, 6).setText(self.assigneeBox.currentText())
                        break

    def on_change_issue_status(self, index):
        qsriputils.print_rip(f"on_change_issue_status: {index} selected row: {self.tableWidget.currentRow()}")
        row = self.tableWidget.currentRow()
        qsriputils.print_rip(f"on_change_assignee: {index} selected row: {self.tableWidget.currentRow()}")
        # self.tableWidget.item(row, 6).setText(self.assigneeBox.currentText())
        self.tableWidget.item(row, 7).setText(self.issueBox.currentText())
        pbo = self.selectedIssue.pbo
        sub = [issue for issue in self.issues if issue.pbo == pbo and issue.issue_type == self.selectedIssue.issue_type]
        for issue in sub:
            issue.status = index
            qsriputils.update_issue(issue)
            # for i in range(self.tableWidget.rowCount()):
            #     it = self.tableWidget.item(i, 0)
            #     issue_id = it.data(Qt.UserRole)
            #     if issue_id == issue.id:
            #         if index == 0:
            #             self.tableWidget.setRowHidden(i, False)
            #         else:
            #             self.tableWidget.setRowHidden(i, True)
            #         break

    def __init__(self, window=None):
        super(FieldTab, self).__init__()
        self.window = window
        uic.loadUi('ui/field.ui', self)

        self.item0 = self.tableWidget.horizontalHeaderItem(0)
        self.item0.setText(ll['statut'])
        self.item1 = self.tableWidget.horizontalHeaderItem(1)
        self.item1.setText(ll['Date_Creation'])
        self.item2 = self.tableWidget.horizontalHeaderItem(2)
        self.item2.setText(ll['projectReport'])
        self.item3 = self.tableWidget.horizontalHeaderItem(3)
        self.item3.setText("NRO")
        self.item4 = self.tableWidget.horizontalHeaderItem(4)
        self.item4.setText(ll['Point_tech'])
        self.item5 = self.tableWidget.horizontalHeaderItem(5)
        self.item5.setText("Type")
        self.item6 = self.tableWidget.horizontalHeaderItem(6)
        self.item6.setText(ll['assign_to'])
        self.item7 = self.tableWidget.horizontalHeaderItem(7)
        self.item7.setText(ll['etat'])
        self.labelCreateDatetitle.setText(ll['Date_Creation'])
        self.labelCloseDatetitle.setText(ll['Dernier_Event'])
        self.label_3.setText(ll['Resume_date'])
        self.label_6.setText(ll['List_Details'])
        self.label_2.setText(ll['Source_data_files'])
        self.issueBox.clear()
        self.issueBox.addItems(ll['etat_list'])

        self.statusRootItem = QTreeWidgetItem([ll['status_tree']])
        self.statusRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.statusTreeWidget.addTopLevelItem(self.statusRootItem)
        self.statusTreeWidget.itemClicked.connect(self.on_status_clicked)

        self.__addTreeItem(self.statusRootItem, ll['Error'], checked=True)
        self.__addTreeItem(self.statusRootItem, ll['warning'], checked=True)

        self.buttonClose.setText(ll['Hide_Detail'])
        self.buttonClose.clicked.connect(self.close_detail)
        self.statusRootItem.setExpanded(True)
        # self.tableWidget.itemSelectionChanged.connect(self.select_row)
        self.current_page = 1
        self.tableWidget.itemClicked.connect(self.on_item_clicked)
        self.tableWidget.setSortingEnabled(True)
        self.tableWidget.resizeRowsToContents()
        self.tableWidget.horizontalHeader().sortIndicatorChanged.connect(
            self.tableWidget.resizeRowsToContents)

        self.detailFrame.setHidden(True)
        w = QtWidgets.QWidget()
        closebox = QHBoxLayout()
        closebox.setContentsMargins(0, 0, 0, 0)

        self.close_button = QToolButton()
        self.close_button.setFont(QtGui.QFont("Segoe MDL2 Assets", 10))
        self.close_button.setText("\uE106")
        self.close_button.setStyleSheet("background-color:transparent; color:white")
        self.close_button.clicked.connect(self.close_detail)
        closebox.addWidget(self.close_button)
        closebox.insertStretch(0, 100)
        w.setLayout(closebox)
        # self.toolFrameLayout.addWidget(w)
        # self.toolFrameLayout.insertStretch(0, 500)
        # self.toolFrameLayout.setStretch(2, 500)
        # self.close_button.setHidden(True)
        if self.window.user.role != "admin":
            self.assigneeBox.setHidden(True)
        else:
            self.assigneeBox.setHidden(False)
            self.assigneeBox.clear()
            self.assigneeBox.addItem(ll['assign_to'])
            self.assigneeBox.addItems([f"{t.displayName}" for t in self.window.techniciens])
        self.assigneeBox.currentIndexChanged.connect(self.on_change_assignee)
        if self.window.user.role in ['vqse', 'tech', 'stt']:
            self.issueBox.setEnabled(False)
        else:
            self.issueBox.currentIndexChanged.connect(self.on_change_issue_status)

        self.element_page = 100
        self.len_page = 1
        self.issues_2 = []
        self.button_pre = QPushButton(ll['previ'])
        self.button_next = QPushButton(ll['next'])
        self.button_pagination = QPushButton('{}/{}'.format(self.current_page, self.len_page))
        self.layout_pagination = QHBoxLayout()
        self.layout_pagination.addWidget(self.button_pre)
        self.layout_pagination.addWidget(self.button_pagination)
        self.layout_pagination.addWidget(self.button_next)
        widget_pagination = QWidget()
        widget_pagination.setMaximumWidth(200)
        widget_pagination.setLayout(self.layout_pagination)
        layout_main = QVBoxLayout()
        layout_main.addWidget(widget_pagination, alignment=QtCore.Qt.AlignBottom)
        self.tableWidget.setLayout(layout_main)
        self.button_pre.clicked.connect(self.pre_page)
        self.button_next.clicked.connect(self.next_page)

    @QtCore.pyqtSlot(QtWidgets.QTableWidgetItem)
    def on_item_clicked(self, item):
        row = self.tableWidget.currentRow()
        it = self.tableWidget.item(row, 0)
        qsriputils.print_rip(f"data: {it.data(Qt.UserRole)} current row: {self.tableWidget.currentRow()}")
        issue_id = it.data(Qt.UserRole)
        for issue in self.issues:
            if issue.id == issue_id:
                self.show_issue(issue)
                break

    @QtCore.pyqtSlot(QtWidgets.QTreeWidgetItem, int)
    def on_status_clicked(self, it, col):
        root = it.parent()
        level = [1, 2]
        self.issues_2 = []
        if root is not None:
            if root.child(0).checkState(0) == QtCore.Qt.Unchecked:
                level.remove(2)
            if root.child(1).checkState(0) == QtCore.Qt.Unchecked:
                level.remove(1)
            if len(level) < 1:
                self.issues_2 = []
            else:
                for issue in self.issues:
                    if issue.level in level:
                        self.issues_2.append(issue)
            self.create_table()

    def updateText(self, lang):
        global ll
        ll = lang

        # self.statusRootItem.
        self.buttonClose.setText(ll['Hide_Detail'])
        self.item0.setText(ll['statut'])
        self.item1.setText(ll['Date_Creation'])
        self.item2.setText(ll['projectReport'])
        self.item3.setText("NRO")
        self.item4.setText(ll['Point_tech'])
        self.item5.setText("Type")
        self.item6.setText(ll['assign_to'])
        self.item7.setText(ll['etat'])
        self.labelCreateDatetitle.setText(ll['Date_Creation'])
        self.labelCloseDatetitle.setText(ll['Dernier_Event'])
        self.label_3.setText(ll['Resume_date'])
        self.label_6.setText(ll['List_Details'])
        self.label_2.setText(ll['Source_data_files'])

        self.issueBox.disconnect()
        self.issueBox.clear()
        self.issueBox.addItems(ll['etat_list'])
        self.issueBox.currentIndexChanged.connect(self.on_change_issue_status)

        if hasattr(self, 'button_pre'):
            self.button_pre.setText(ll['previ'])
        if hasattr(self, 'button_next'):
            self.button_next.setText(ll['next'])

        if self.window.user.role != "admin":
            self.assigneeBox.setHidden(True)
        else:
            self.assigneeBox.setHidden(False)
            self.assigneeBox.disconnect()
            indexassign = self.assigneeBox.currentIndex()
            self.assigneeBox.clear()
            self.assigneeBox.addItem(ll['assign_to'])
            self.assigneeBox.addItems([f"{t.displayName}" for t in self.window.techniciens])
            self.assigneeBox.setCurrentIndex(indexassign)
            self.assigneeBox.currentIndexChanged.connect(self.on_change_assignee)

        # self.button_pre.setText(ll['previ'])
        # self.button_next.setText(ll['next'])

        self.create_table()

        self.statusRootItem.setText(0, ll['status_tree'])

        lst_child = []
        for i in range(self.statusRootItem.childCount()):
            lst_child.append(self.statusRootItem.child(i))
        for i in lst_child:
            self.statusRootItem.removeChild(i)

        self.__addTreeItem(self.statusRootItem, ll['Error'], checked=True)
        self.__addTreeItem(self.statusRootItem, ll['warning'], checked=True)

    def __addTreeItem(self, parent, text, checked=False):
        item = QTreeWidgetItem(parent, [text])
        item.setForeground(0, QtGui.QBrush(Qt.white))
        item.setCheckState(0, Qt.Checked if checked else Qt.Unchecked)

    def close_detail(self):
        if not self.detailFrame.isHidden():
            self.detailFrame.setHidden(True)

    def get_last_project_in_sro(self, sro):
        apd = sorted([f"{p.type} V{p.ver}" for p in self.window.projs if p.sro == sro])
        if len(apd) > 0:
            return apd[-1]
        return None

    def showEvent(self, a0: QtGui.QShowEvent) -> None:
        self.issues = []
        sros = list(set([p.sro for p in self.window.projs]))
        for sro in sros:
            apd = self.get_last_project_in_sro(sro)
            qsriputils.print_rip(f"sro: {sro} last: {apd}")
            a = apd.split(" ")
            t = a[0]
            v = int(a[1][1:])
            self.issues.extend(qsriputils.get_issues_in_sro(sro, t, v))
        self.issues_2 = self.issues
        if len(self.issues) < self.element_page:
            self.len_page = 1
        else:
            if len(self.issues) % self.element_page == 0:
                self.len_page = len(self.issues) // self.element_page
            else:
                self.len_page = len(self.issues) // self.element_page + 1
        self.tableWidget.setStyleSheet("""
                    font-family:'SF UI Text';
                    font-size:11px;
                 """)
        self.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.create_table()

    def create_table(self):
        element_first = (self.current_page - 1) * self.element_page
        element_end = self.current_page * self.element_page
        self.tableWidget.setRowCount(0)
        self.button_pagination.setText('{}/{}'.format(self.current_page, self.len_page))
        self.check_pagination()
        if self.current_page == self.len_page:
            self.tableWidget.setRowCount(len(self.issues_2) - element_first)
        else:
            self.tableWidget.setRowCount(self.element_page)
        i = 0
        for issue in self.issues_2[element_first:element_end]:
            if issue.level == 2:
                color = QtGui.QColor(252, 38, 50)
                text = ll['Error']
            elif issue.level == 1:
                color = QtGui.QColor(235, 126, 62)
                text = ll['warning']
            elif issue.level == 0:
                color = QtGui.QColor(50, 166, 27)
                text = ll['clo']
            item = QTableWidgetItem(text)
            item.setFont(QFont("SF UI Text", weight=QFont.Bold, pointSize=9))
            item.setForeground(color)
            item.setData(Qt.UserRole, issue.id)
            self.tableWidget.setItem(i, 0, item)
            self.tableWidget.setItem(i, 1, QTableWidgetItem(issue.create_date.strftime("%d-%m-%Y")))
            self.tableWidget.setItem(i, 2, QTableWidgetItem(issue.project))
            self.tableWidget.setItem(i, 3, QTableWidgetItem(issue.nro))
            self.tableWidget.setItem(i, 4, QTableWidgetItem(issue.pbo))
            self.tableWidget.setItem(i, 5, QTableWidgetItem(
                const.errCode[issue.code] if issue.code in const.errCode else issue.code))
            if len(issue.last_update) > 0:
                self.tableWidget.setItem(i, 6, QTableWidgetItem(issue.last_update[-1]['user']))
            else:
                self.tableWidget.setItem(i, 6, QTableWidgetItem(issue.assignee_name))
            self.tableWidget.setItem(i, 7, QTableWidgetItem(ll['etat_list'][issue.status]))
            i += 1

    def next_page(self):
        self.current_page += 1
        self.create_table()

    def pre_page(self):
        self.current_page -= 1
        self.create_table()

    def check_pagination(self):
        if self.current_page >= self.len_page:
            self.button_next.setEnabled(False)
        else:
            self.button_next.setEnabled(True)
        if self.current_page <= 1:
            self.button_pre.setEnabled(False)
        else:
            self.button_pre.setEnabled(True)

    def download_data_if_need(self, p):
        pbo_layer = os.path.join(p.qgis_path, p.boite_layer)
        cable_layer = os.path.join(p.qgis_path, p.cable_layer)
        qsriputils.print_rip(f"pbo file: {pbo_layer}, cable: {cable_layer}")
        if not os.path.exists(pbo_layer):
            file = f"{p.sro}_{p.type}_V{p.ver}_qgis.zip"
            ppath = Path(p.qgis_path).parent
            qsriputils.print_rip(f"download missing qgis data: {file}")
            qsriputils.save_file(file, ppath)
            zfile = os.path.join(ppath, file)
            with zipfile.ZipFile(zfile, 'r') as zip_ref:
                zip_ref.extractall(p.qgis_path)
            os.remove(zfile)
            qsriputils.print_rip(f"remove zip qgis data: {zfile}")

    def show_issue(self, issue):
        self.issueBox.disconnect()
        self.assigneeBox.disconnect()
        self.detailFrame.show()
        self.selectedIssue = issue
        name = issue.pbo
        sro = issue.nro
        apd = issue.apd
        ver = issue.ver
        ip = None
        for p in self.window.projs:
            if p.sro == sro and p.type == apd and p.ver == ver:
                ip = p
                break
        if ip is not None:
            self.download_data_if_need(ip)
            ip.files = qsriputils.get_files(ip)

        self.nodeNameButton.setText(name)
        # if name.endswith(" \uE70F"):
        #     name = name.replace(" \uE70F", "")
        level = issue.level
        err_text = const.errCode[issue.code] if issue.code in const.errCode else issue.code
        assignee = issue.assignee_name
        status = issue.status

        self.issueBox.setCurrentIndex(status)
        qsriputils.print_rip(f"issue level: {level}")
        if level == 1:
            if status == 0:
                self.nodeNameButton.setStyleSheet(
                    'QPushButton {background: rgb(235,125,60); color: white; border:none; border-radius: 3px; padding: 3px} '
                )
            else:
                self.nodeNameButton.setStyleSheet(
                    'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                )
        elif level == 2:
            if status == 0:
                self.nodeNameButton.setStyleSheet(
                    'QPushButton {background: rgb(252,13,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                )
            else:
                self.nodeNameButton.setStyleSheet(
                    'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                )
        qsriputils.print_rip(f"issue assignee: {assignee}")
        if assignee != "":
            techniciens = self.window.techniciens
            foundTech = False
            for tech in techniciens:
                if assignee == tech.displayName:
                    self.assigneeBox.setCurrentText(assignee)
                    foundTech = True
                    break
            if not foundTech:
                self.assigneeBox.setCurrentIndex(0)
        else:
            self.assigneeBox.setCurrentIndex(0)

        self.labelCreateDate.setText(f"{issue.create_date.strftime('%d-%m-%Y')}")
        self.labelVersion.setText(f"{issue.apd} V{issue.ver}")
        self.labelVersion.adjustSize()
        self.labelImportUser.setText(issue.username)
        self.errorLabel.setText(err_text)
        #
        # qsriputils.clearLayout(self.detailLayout)
        # qsriputils.clearLayout(self.mapLayout)
        # # clear other widget selected status
        # for i in range(self.graphLayout.count()):
        #     w = self.graphLayout.itemAt(i).widget()
        #     if isinstance(w, QPushButton) and w != self.sender():
        #         w.setChecked(False)
        # # fill pbo detail info
        if issue.issue_type == "script":
            qsriputils.clearLayout(self.detailLayout)

            pbo = qsriputils.get_pbo(issue)
            if pbo:
                lines = [pbo.reference, pbo.function, pbo.support]
                for line in lines:
                    label = QLabel(line)
                    label.setAlignment(Qt.AlignCenter)
                    label.setFont(QFont('SF UI Text', 9))
                    self.detailLayout.addWidget(label)
                self.__showPBOMap(pbo, ip)
        else:
            c = qsriputils.get_cable(issue, self.proj._id)
            qsriputils.clearLayout(self.detailLayout)

            if c:
                label = QLabel(f"{c.src} - Section {c.section} - {c.dest}")
                label.setAlignment(Qt.AlignCenter)
                label.setWordWrap(True)
                self.detailLayout.addWidget(label)
                lines = [c.type, f"{c.length}ml"]
                for line in lines:
                    label = QLabel(line)
                    label.setAlignment(Qt.AlignCenter)
                    label.setFont(QFont('SF UI Text', 9))
                    self.detailLayout.addWidget(label)
        qsriputils.clearLayout(self.downloadLayout)
        for file in ip.files:
            fileButton = QPushButton(file)
            fileButton.setStyleSheet(
                "background-color:transparent; color: rgb(18,67,105); text-align:left; text-decoration: underline; border:none; padding: 3px")
            fileButton.clicked.connect(self.download_file)
            fileButton.setCursor(QCursor(QtCore.Qt.PointingHandCursor))

            self.downloadLayout.addWidget(fileButton)
        self.downloadLayout.insertStretch(-1, 100)
        self.issueBox.currentIndexChanged.connect(self.on_change_issue_status)
        self.assigneeBox.currentIndexChanged.connect(self.on_change_assignee)

    def __showPBOMap(self, pbo, proj):
        cableLayer = QgsVectorLayer(os.path.join(proj.qgis_path, proj.cable_layer))
        boiteLayer = QgsVectorLayer(os.path.join(proj.qgis_path, proj.boite_layer))
        QgsProject.instance().addMapLayer(cableLayer)
        QgsProject.instance().addMapLayer(boiteLayer)

        canvas = QgsMapCanvas()
        canvas.setCanvasColor(Qt.white)
        canvas.enableAntiAliasing(True)
        canvas.setLayers([cableLayer, boiteLayer])
        qsriputils.clearLayout(self.mapLayout)
        self.mapLayout.addWidget(canvas)
        boiteLayer.selectByIds([pbo.pbo_id])
        box = boiteLayer.boundingBoxOfSelected()
        box.set(box.xMinimum() - 50, box.yMinimum() - 50, box.xMaximum() + 50, box.yMaximum() + 50)
        canvas.setExtent(box)
        canvas.refresh()

    @QtCore.pyqtSlot()
    def export_pdf(self):
        # qsriputils.print_widget(self.scrollAreaWidgetContents, "test.pdf")
        printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        # printer.setPageSize(QtPrintSupport.QPrinter.PageSize)
        dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        dialog.paintRequested.connect(self.print_preview)
        dialog.exec_()

    @QtCore.pyqtSlot(QtPrintSupport.QPrinter)
    def print_preview(self, printer):
        html = '<body>'
        if not self.detailFrame.isHidden():
            pix = QPixmap(self.detailScrollArea.size())
            painter = QtGui.QPainter(pix)
            self.detailScrollArea.render(painter)
            pix.save("temp/detail.png", 'png')
            painter.end()
            html += '<div style="page-break-after: always"><img src="temp/detail.png" height="800"></div>'

        html += '<table width="100%" border="1" style="font-size: small"><tr>'
        for i in range(self.tableWidget.columnCount()):
            html += '<td align="center">' + str(self.tableWidget.horizontalHeaderItem(i).text()) + '</td>'
        html += '</tr>'
        for i in range(self.tableWidget.rowCount()):
            if i % 20 == 0 and i != 0:
                html += '</table><table width="100%" border="1" style="page-break-before: always; font-size: small">'
            html += '<tr>'
            for j in range(self.tableWidget.columnCount()):
                text = str(self.tableWidget.item(i, j).text())
                if j == 0 and text == ll['Error']:
                    html += '<td width="13%" valign="middle"><b style="color:red">' + text + '</b></td>'
                elif j == 0:
                    html += '<td width="13%" valign="middle"><b style="color:yellow">' + text + '</b></td>'
                elif j == 4 or j == 5:
                    html += '<td width="17%" valign="middle" style="margin-left: 5px">' + text + '</td>'
                else:
                    html += '<td width="13%" valign="middle">' + text + '</td>'
            html += '</tr>'
        html += '</table></body>'

        web = QtWidgets.QTextBrowser()
        web.setHtml(html)
        web.print(printer)

    def download_file(self):
        file = self.sender().text()
        qsriputils.print_rip(f"file: {file}")
        options = QtWidgets.QFileDialog.Options()
        # options |= QtWidgets.QFileDialog.DontUseNativeDialog
        options |= QtWidgets.QFileDialog.ShowDirsOnly
        path = QtWidgets.QFileDialog.getExistingDirectory(
            None,
            ll["Select_Directory"],
            "/home",
            options=options)
        if path:
            try:
                qsriputils.save_file(file, path)
            except Exception as e:
                qsriputils.showMessage(e)
