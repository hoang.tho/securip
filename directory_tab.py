from PyQt5 import QtWidgets, QtCore, uic
from qsripproject import QSRIPProject


class DirectoryTab(QtWidgets.QFrame):
    proj: QSRIPProject = None
    window = None

    def __init__(self, window=None):
        super(DirectoryTab, self).__init__()
        self.window = window
        uic.loadUi('ui/directory.ui', self)
