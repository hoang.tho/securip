import hashlib

from PyQt5.Qt import QMessageBox, QDialog, QLabel, QProgressBar
import sqlite3
from PyQt5 import QtCore, QtGui, QtWidgets, QtPrintSupport
from sqlite3 import Error

from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QVBoxLayout, QScrollArea, QTextBrowser

from user import QSRIPUser
from qsripproject import QSRIPProject, QSRIPIssue, PBO, Cable, PointTechnique
from datetime import datetime
import os
import random
import string
import const
import zipfile
from pyproj import Transformer
from pyproj import _datadir, datadir

from firebase_admin import storage, firestore
from firebase_admin.storage import bucket


def zipdir(path, zipfilename):
    # ziph is zipfile handle
    lenDirPath = len(path)
    zipf = zipfile.ZipFile(zipfilename, 'w', zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(path):
        for file in files:
            filePath = os.path.join(root, file)
            zipf.write(filePath, filePath[lenDirPath:])
    zipf.close()


def ConvertL93Wgs84(MyPoint):
    tranformer = Transformer.from_crs("epsg:2154", "epsg:4326")
    Wgs84Output = tranformer.transform(float(MyPoint.x()), float(MyPoint.y()))
    return Wgs84Output


db = None
bucket = None


def upload_file(filepath, sro):
    filename = os.path.basename(filepath)
    blob = bucket.blob(filename)
    blob.upload_from_filename(filepath)
    ref = db.collection("files").document(sro)
    doc = ref.get()
    if doc.exists:
        ref.update({"files": firestore.ArrayUnion([filename])})
    else:
        ref.set({"files": [filename]})


def nro_from_sro(sro):
    tt = sro.split("-")
    if len(tt) == 4:
        return "{}-{}".format(tt[1], tt[2])
    return None


def print_widget(widget, filename):
    printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
    printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
    printer.setOutputFileName(filename)
    printer.setOrientation(QtPrintSupport.QPrinter.Landscape)
    painter = QtGui.QPainter(printer)

    # start scale
    xscale = printer.pageRect().width() * 1.0 / widget.width()
    yscale = printer.pageRect().height() * 1.0 / widget.height()
    scale = min(xscale, yscale)
    painter.translate(printer.paperRect().center())
    painter.scale(scale, scale)
    painter.translate(-widget.width() / 2, -widget.height() / 2)
    # end scale

    widget.render(painter)
    painter.end()


def resolve(name, basepath=None):
    if not basepath:
        basepath = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(basepath, name)


def get_cable_length_from_sor(meta):
    key_events = meta["KeyEvents"]
    print_rip(f"key: {key_events}")
    num = key_events["num events"]
    ii = 2
    start = float(key_events["event 1"]["distance"])
    if start == 0.0:
        ii = 3
        start = float(key_events["event 2"]["distance"])
    value = []
    for i in range(ii, num + 1):
        k = "event {}".format(i)
        e = key_events[k]
        d = float(e["distance"])
        value.append(round(1000 * (d - start)))
        start = d
    return value


def get_cable_total_length(meta):
    key_events = meta["KeyEvents"]
    num = key_events["num events"]
    ii = 2
    start = float(key_events["event 1"]["distance"])
    value = []
    for i in range(ii, num + 1):
        k = "event {}".format(i)
        e = key_events[k]
        d = float(e["distance"])
        value.append(round(1000 * (d - start)))
        start = d
    return sum(value)


def showMessage(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText(text)
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()


def showProgressBar():
    prog_win = QDialog()
    prog_win.resize(400, 100)
    prog_win.setFixedSize(prog_win.size())
    prog_win.setWindowTitle("Processing...")
    lbl = QLabel(prog_win)
    lbl.setText("Please Wait.  .  .")
    lbl.move(15, 18)
    progressBar = QProgressBar(prog_win)
    progressBar.resize(410, 25)
    progressBar.move(15, 40)
    progressBar.setRange(0, 0)
    return (progressBar, prog_win)


def remove_unit(s):
    v = s.split(" ")
    if len(v) > 0:
        return v[0]
    return None


def show_image(path, pbo, photos):
    print_rip("show image:", path)
    html = ''
    for photo in photos:
        image = path + photo
        html += '<img src="{}" width=400><br>'.format(image)
    web = QTextBrowser()
    web.setHtml(html)
    mainLayout = QVBoxLayout()
    mainLayout.addWidget(web)
    mainWidget = QDialog()
    mainWidget.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowCloseButtonHint)
    mainWidget.setWindowTitle(pbo)
    if len(photos) > 1:
        mainWidget.setFixedSize(450, 600)
    else:
        mainWidget.setFixedSize(450, 350)
    mainWidget.setLayout(mainLayout)
    mainWidget.exec()


def clearLayout(layout):
    for i in reversed(range(layout.count())):
        widgetToRemove = layout.itemAt(i).widget()
        # remove it from the layout list
        layout.removeWidget(widgetToRemove)
        # remove it from the gui
        if widgetToRemove:
            widgetToRemove.setParent(None)


def db_connect(file):
    conn = None
    try:
        conn = sqlite3.connect(file, detect_types=sqlite3.PARSE_DECLTYPES |
                                                  sqlite3.PARSE_COLNAMES)
    except Error as e:
        print_rip(e)
    return conn


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print_rip(e)


def create_project(project):
    """
    Create a new project into the projects table
    :param conn:
    :param project:
    :return: project id
    """
    pid = f"{project.client}_{project.project}_{project.sro}_{project.type}_V{project.ver}"
    pid += '_' + ''.join(random.choice(string.ascii_lowercase) for _ in range(10))
    ref = db.collection("proj").document(pid)
    print_rip(f"create project with id: {pid}")
    data = {
        "proj_id": pid,
        "name": project.project,
        "client": project.client,
        "type": project.type,
        "ver": project.ver,
        "sro": project.sro,
        "nro": project.nro,
        "boite_layer": project.boite_layer,
        "cable_layer": project.cable_layer,
        "pt_layer": project.pt_layer,
        "create_date": project.create_date,
        "last_modified": project.last_modified,
        "user_id": project.user_id,
        "error": 0,
        "warn": 0,
        "ok": 0,
        "vqse": 0,
        "vqse2": 0,
        "tested": 0,
        "sor_error": 0,
        "sor_warn": 0,
        "sor_tested": 0,
        "sor_ok": 0,
        "scripted": 0
    }
    print_rip(f"project data:{data}")
    ref.set(data)
    user_ref = db.collection('users').document(project.user_id)
    user_projects = user_ref.get().to_dict()
    user_projects['project_ids'].append(pid)
    user_ref.set({'project_ids': user_projects['project_ids']}, merge=True)

    return pid


def save_sor_quality(proj, pbo, errors, mqs=""):
    docid = f"{proj.client}_{proj.project}_{proj.sro}_{proj.type}_V{proj.ver}_{errors[0]}"
    print_rip(f"save error: {docid}")
    ref = db.collection("sor_quality").document(docid)
    ref.set({
        "proj_id": proj._id,
        "error": errors,
        "pbo": pbo,
        "project": f"{proj.client}_{proj.project}_{proj.sro}_{proj.type}_V{proj.ver}",
        "count": errors[1],
        "mqs": mqs
    }, merge=True)


def get_sor_quality(proj, pbo):
    ref = db.collection("sor_quality").where("project", "==",
                                             f"{proj.client}_{proj.project}_{proj.sro}_{proj.type}_V{proj.ver}").where(
        "pbo", "==", pbo)
    docs = ref.get()
    data = []
    if len(docs) > 0:
        for doc in docs:
            data.append(doc.to_dict())
    return data


def update_counts(fields, value, doc=None):
    # keys: client_script_error, client_script_tested, client_script_warn, client_sor_error, client_sor_tested, client_sor_warn
    #

    document = doc if doc is not None else datetime.now().strftime("%Y-%m")
    ref = db.collection('counter').document(document)
    d = ref.get()
    u = {}
    data = None
    if d.exists:
        data = d.to_dict()
    for f, v in zip(fields, value):
        count = 0
        if data and f in data:
            count = data[f]
        count = count + v
        u[f] = count
    ref.set(u, merge=True)


def update_count(field, value=1, doc=None):
    # keys: client_script_error, client_script_tested, client_script_warn, client_sor_error, client_sor_tested, client_sor_warn
    #

    document = doc if doc is not None else datetime.now().strftime("%Y-%m")
    ref = db.collection('counter').document(document)
    d = ref.get()
    count = 0
    if d.exists:
        data = d.to_dict()
        if field in data:
            count = data[field]
    count = count + value

    ref.set({field: count}, merge=True)


def get_pbo(issue):
    docs = db.collection(u'pbo').where("name", "==", issue.pbo).where("ver", "==", f"{issue.apd} V{issue.ver}").get()
    if len(docs) > 0:
        doc = docs[0]
        d = doc.to_dict()
        p = PBO()
        p.id = doc.id
        p.sro = d["sro"]
        p.ver = d["ver"]
        p.name = d["name"]
        p.cable = d["cable"]
        p.pbo_id = d["pbo_id"]
        p.reference = d["reference"]
        p.function = d["function"]
        p.support = d["support"]
        p.vsqe = d["vsqe"]
        p.x: float = d["x"]
        p.y: float = d["x"]
        return p
    return None


def get_cable(issue, proj_id):
    docs = db.collection(u'cable').where(
        "dest", "==", issue.pbo).where(
        "ver", "==", f"{issue.apd} V{issue.ver}").where(
        "proj_id", "==", proj_id).get()
    if len(docs) > 0:
        doc = docs[0]
        d = doc.to_dict()
        c = Cable()
        c.id = doc.id
        c.sro = d["sro"]
        c.ver = d["ver"]
        c.name = d["name"]
        c.src = d["src"]
        c.dest = d["dest"]
        c.type = d["type"]
        c.vsqe = d["vsqe"]
        c.length = d["length"]
        c.section = d["section"]
        c.capacity = d["capacity"]
        return c
    return None


def get_sor_customer_value():
    ref = db.collection("sor_customer_value").document("default")
    doc = ref.get()
    return doc.to_dict()


def save_sor_customer_value(customer_value):
    ref = db.collection("sor_customer_value").document("default")
    ref.set(customer_value, merge=True)


def update_issue(issue):
    ref = db.collection("issues").document(issue.id)
    ref.set({
        "level": issue.level,
        "assignee_id": issue.assignee_id,
        "assignee_name": issue.assignee_name,
        "last_update": issue.last_update,
        "status": issue.status
    }, merge=True)


def remove_data():
    docs = db.collection("cable").where("sro", "==", "CIRCET FIBRE 31 SRO-31-191-544").stream()
    for doc in docs:
        doc.reference.delete()
    docs = db.collection("pbo").where("sro", "==", "CIRCET FIBRE 31 SRO-31-191-544").stream()
    for doc in docs:
        doc.reference.delete()


def get_proj_sor_quality(proj):
    p = f"{proj.client}_{proj.project}_{proj.sro}_{proj.type}_V{proj.ver}"
    docs = db.collection("sor_quality").where("project", "==", p).stream()
    sor = []
    for doc in docs:
        sor.append(doc.to_dict())
    return sor


def get_all_issues(user_id, admin=False, date=datetime.now()):
    print_rip("user_id", user_id)
    user = get_user(user_id)
    firstday = date.replace(day=1, hour=0, minute=0, second=0)
    import dateutil.relativedelta
    nextmonth = firstday + dateutil.relativedelta.relativedelta(months=1)
    print_rip(f"firstday: {firstday} nextmonth: {nextmonth}")
    if admin:
        docs = db.collection(u'issues').where("create_date", ">=", firstday).where("create_date", "<=",
                                                                                   nextmonth).limit(100).stream()
    else:
        docs = db.collection(u'issues').where("create_date", ">=", firstday).where("create_date", "<=",
                                                                                   nextmonth).where("assignee_id", "==",
                                                                                                    user_id).limit(
            100).stream()
    issues = []
    ids = []
    for doc in docs:
        data = doc.to_dict()
        p = unpack_issue(data)
        p.id = doc.id
        ids.append(p.id)
        p.username = user.email
        issues.append(p)

    return issues


def unpack_issue(data):
    p = QSRIPIssue()
    p.level = data["level"]
    p.project = data["project"]
    p.nro = data["nro"]
    p.create_date = data["create_date"]
    p.last_update = data["last_update"]
    p.type = data["type"]
    p.user_id = data["user_id"]
    if "content" in data:
        p.content = data["content"]
    p.assignee_id = data["assignee_id"]
    p.assignee_name = data["assignee_name"]
    p.username = data["username"]
    p.issue_type = data["issue_type"]
    p.status = data["status"]
    p.text = data["text"]
    p.user_id = data["user_id"]
    p.code = data["code"]
    p.pbo = data["pbo"]
    p.apd = data["apd"]
    p.ver = data["ver"]
    return p


def get_all_issues_in_project(proj):
    docs = db.collection(u'issues').where("nro", "==", proj.sro).where("apd", "==", proj.type).where("ver", "==",
                                                                                                     proj.ver).stream()
    issues = []
    # error = 0
    # warning = 0
    # sor_err = 0
    # sor_warn = 0
    # sor_tested = proj.sor_tested
    # tested = proj.tested
    for doc in docs:
        data = doc.to_dict()
        p = unpack_issue(data)
        p.id = doc.id
        # if p.level == 2:
        #     if p.issue_type == "script":
        #         error += 1
        #     else:
        #         sor_err += 1
        # elif p.level == 1:
        #     if p.issue_type == "script":
        #         warning += 1
        #     else:
        #         sor_warn += 1
        issues.append(p)
    # if proj.count_error == 1:
    #     update_count(f"{proj.client}_error", error)
    #     update_count(f"{proj.client}_warn", warning)
    #     update_count(f"{proj.client}_tested", tested)
    #     update_count(f"{proj.client}_sor_error", sor_err)
    #     update_count(f"{proj.client}_sor_warn", sor_warn)
    #     update_count(f"{proj.client}_sor_tested", sor_tested)
    #     update_count(f"{proj.client}_{proj.project}_error", error)
    #     update_count(f"{proj.client}_{proj.project}_warn", warning)
    #     update_count(f"{proj.client}_{proj.project}_tested", tested)
    #     update_count(f"{proj.client}_{proj.project}_sor_error", sor_err)
    #     update_count(f"{proj.client}_{proj.project}_sor_warn", sor_warn)
    #     update_count(f"{proj.client}_{proj.project}_sor_tested", sor_tested)
    #     update_count(f"{proj.sro}_error", error)
    #     update_count(f"{proj.sro}_warn", warning)
    #     update_count(f"{proj.sro}_tested", tested)
    #     update_count(f"{proj.sro}_sor_error", sor_err)
    #     update_count(f"{proj.sro}_sor_warn", sor_warn)
    #     update_count(f"{proj.sro}_sor_tested", sor_tested)
    #     ref = db.collection(u'project').document(proj._id)
    #     ref.set({'count_error': 0}, merge=True)
    return issues


def update_project(project):
    ref = db.collection(u'proj').document(project._id)
    ref.set({
        "ok": project.ok,
        "scripted": project.scripted
    }, merge=True)


def update_project_data(project, data):
    ref = db.collection(u'proj').document(project._id)
    ref.set(data, merge=True)


def create_user(user):
    user_ref = db.collection(u'users').document(user.userId)
    user_ref.set({
        'email': user.email,
        'displayName': user.displayName,
        'role': user.role,
        'last_login': user.last_loggined,
        'password': user.password,
        'userId': user.userId,
        'project_ids': []
    })


def unpack_project(dict, path=""):
    print_rip(dict)
    p = QSRIPProject()
    p.project = dict["name"]
    p.client = dict["client"]
    p.type = dict["type"]
    p.ver = dict["ver"]
    p.sro = dict["sro"]
    p.boite_layer = dict["boite_layer"]
    p.cable_layer = dict["cable_layer"]
    p.pt_layer = dict["pt_layer"]
    p.create_date = dict["create_date"]
    p.last_modified = dict["last_modified"]
    p.user_id = dict["user_id"]
    p.scripted = dict["scripted"]
    p.error = dict["error"]
    p.warn = dict["warn"]
    p.tested = dict["tested"]
    if 'csv' in dict:
        setattr(p, 'csv', dict['csv'])
    if "vqse1" in dict:
        p.vqse1 = dict["vqse1"]
    if "vqse2" in dict:
        p.vqse2 = dict["vqse2"]
    if "vqse3" in dict:
        p.vqse3 = dict["vqse3"]
    if "images" in dict:
        p.images = dict["images"]
    if "count_error" in dict:
        p.count_error = dict["count_error"]
    if "info" in dict:
        p.info = dict["info"]
    new_keys = ['ok', 'sor_error', 'sor_warn', 'sor_ok', 'sor_tested', 'vqse_error', 'vqse_warn', 'vqse_ok',
                'vqse_tested']
    for k in new_keys:
        if k in dict:
            setattr(p, k, dict[k])
    if 'code' in dict:
        p.code = dict['code'].copy()
    p.cable = []
    p.pbo = []
    p.issues = []
    p.files = get_files(p)
    p.set_project_path(path)
    return p


def save_file(filename, path):
    blob = bucket.blob(filename)
    blob.download_to_filename(os.path.join(path, filename))


def get_files(p):
    doc = db.collection("files").document(f"{p.client}_{p.project}_{p.sro}").get()
    if doc.exists:
        d = doc.to_dict()
        return sorted(d["files"])
    return []


def select_all_projects_by_user(user_id, path, admin=False):
    """
    get all project by user
    :return:
    """
    print_rip("user_id", user_id)
    projects = []
    if admin:
        docs = db.collection(u'proj').stream()
        for doc in docs:
            p = unpack_project(doc.to_dict(), path)
            p._id = doc.id
            projects.append(p)
    else:
        user_ref = db.collection('users').document(user_id).get()
        proj_in_user = user_ref.to_dict()['project_ids']
        for proj in proj_in_user:
            doc = db.collection('proj').document(proj).get()
            p = unpack_project(doc.to_dict(), path)
            p._id = doc.id
            projects.append(p)

    return projects


def create_issues(issues, proj_id="", total=0, ok=0, client=""):
    # print_rip(issues)
    batch = db.batch()
    e = 0
    w = 0

    err = {'script': 0, 'sor': 0, 'vqse': 0}
    warn = {'script': 0, 'sor': 0, 'vqse': 0}
    proj_issues_code = {}

    tested = total if total > 0 else len(issues)
    issue_type = issues[0].issue_type
    sro = issues[0].nro
    project = issues[0].project
    code = {}
    for issue in issues:
        code[f"[{issue.client}]{issue.code}"] = (
                code[f"[{issue.client}]{issue.code}"] + 1) if f"[{issue.client}]{issue.code}" in code else 1
        code[f"[{issue.client}][{issue.project}]{issue.code}"] = (code[
                                                                      f"[{issue.client}][{issue.project}]{issue.code}"] + 1) if f"[{issue.client}][{issue.project}]{issue.code}" in code else 1
        code[f"[{issue.nro}]{issue.code}"] = (
                code[f"[{issue.nro}]{issue.code}"] + 1) if f"[{issue.nro}]{issue.code}" in code else 1
        ref = db.collection(u'issues').document()
        issue.id = ref.id
        batch.create(ref, {
            "proj_id": proj_id,
            "level": issue.level,
            "code": issue.code,
            "project": issue.project,
            "client": issue.client,
            "nro": issue.nro,
            "create_date": issue.create_date,
            "last_update": [],
            "user_id": issue.user_id,
            "username": issue.username,
            "assignee_id": issue.assignee_id,
            "assignee_name": issue.assignee_name,
            "type": issue.type,
            "issue_type": issue.issue_type,
            "pbo": issue.pbo,
            "text": issue.text,
            "apd": issue.apd,
            "ver": issue.ver,
            "status": issue.status,
            "content": issue.content
        })
        if issue.level == 2:
            e += 1
        elif issue.level == 1:
            w += 1

        if issue.level == 2:
            err[issue.issue_type] += 1
        elif issue.level == 1:
            warn[issue.issue_type] += 1
        if issue.code not in proj_issues_code:
            proj_issues_code[issue.code] = 1
        else:
            proj_issues_code[issue.code] += 1

    batch.commit()

    proj = db.collection('proj').document(proj_id)
    proj_data = proj.get().to_dict()
    proj_update = {}
    code_count = {}
    for i in err:
        if i == 'script':
            proj_update['error'] = proj_data['error'] + err[i]
            proj_update['warn'] = proj_data['warn'] + warn[i]
        else:
            if i + '_error' in proj_data:
                proj_update[i + '_error'] = proj_data[i + '_error'] + err[i]
                proj_update[i + '_warn'] = proj_data[i + '_warn'] + warn[i]
            else:
                proj_update[i + '_error'] = err[i]
                proj_update[i + '_warn'] = warn[i]
    for i in proj_issues_code:
        if 'code' in proj_data and i in proj_data['code']:
            code_count[i] = proj_data['code'][i] + proj_issues_code[i]
        else:
            code_count[i] = proj_issues_code[i]
    proj_update['code'] = code_count
    print_rip(proj_update)
    proj.set(proj_update, merge=True)

    if issue_type == "script":
        update_counts([f"{client}_error", f"{client}_warn", f"{client}_tested", f"{client}_ok",
                       f"{client}_{project}_error", f"{client}_{project}_warn", f"{client}_{project}_tested",
                       f"{client}_{project}_ok",
                       f"{sro}_error", f"{sro}_warn", f"{sro}_tested", f"{sro}_ok"],
                      [e, w, tested, ok, e, w, tested, ok, e, w, tested, ok])

    else:
        update_counts([f"{client}_sor_error", f"{client}_sor_warn", f"{client}_sor_tested", f"{client}_sor_ok",
                       f"{client}_{project}_sor_error", f"{client}_{project}_sor_warn",
                       f"{client}_{project}_sor_tested",
                       f"{client}_{project}_sor_ok",
                       f"{sro}_sor_error", f"{sro}_sor_warn", f"{sro}_sor_tested", f"{sro}_sor_ok"],
                      [e, w, tested, ok, e, w, tested, ok, e, w, tested, ok])
    thismonth = datetime.now().strftime("%Y-%m")
    update_counts(list(code.keys()), list(code.values()), doc=f"{thismonth}_code")


def get_issue_report(mon):
    doc = db.collection("counter").document(mon).get()

    if doc.exists:
        return doc.to_dict()
    else:
        return {}


def get_issues_with_type(client, project, sro, type):
    # if mode == "all":
    #     docs = db.collection("issues").where("issue_type", "==", type).order_by("create_date",
    #                                                                             direction=firestore.Query.DESCENDING).limit(
    #         100).stream()
    # elif mode == "client":
    #     docs = db.collection("issues").where("client", "==", name).where("issue_type", "==", type).order_by(
    #         "create_date",
    #         direction=firestore.Query.DESCENDING).limit(
    #         100).stream()
    # elif mode == "project":
    #     docs = db.collection("issues").where("project", "==", name).where("issue_type", "==", type).order_by(
    #         "create_date",
    #         direction=firestore.Query.DESCENDING).limit(
    #         100).stream()
    # elif mode == "nro":
    #     return []

    date = datetime.now()

    firstday = date.replace(day=1, hour=0, minute=0, second=0)
    import dateutil.relativedelta
    nextmonth = firstday + dateutil.relativedelta.relativedelta(months=1)
    online_proj = db.collection(u'proj').where("create_date", ">=", firstday).where("create_date", "<=",
                                                                                    nextmonth).stream()

    sro_had_set = {}

    for p in online_proj:
        proj = p.to_dict()
        if proj['client'] in client and proj['name'] in project and proj['sro'] in sro:
            if ((proj['sro'] not in sro_had_set) or
                    (sro_had_set[proj['sro']][0] == 'APD' and proj['type'] == 'DOE') or
                    (sro_had_set[proj['sro']][0] == proj['type'] and sro_had_set[proj['sro']][1] < proj['ver'])):
                sro_had_set[proj['sro']] = p.id

    issues = []

    for p in sro_had_set:
        docs = None
        if type == 'all':
            docs = db.collection('issues').where('proj_id', '==', sro_had_set[p]).stream()
        else:
            docs = db.collection('issues').where('proj_id', '==', sro_had_set[p]).where('issue_type', '==',
                                                                                        type).stream()
        for doc in docs:
            i = unpack_issue(doc.to_dict())
            i.id = doc.id
            issues.append(i)
    return issues


def get_last_vqse(name, sro, apd, ver):
    docs = db.collection("issues").where("nro", "==", sro).where("pbo", "==", name).where("apd", "==", apd).where("ver",
                                                                                                                  "==",
                                                                                                                  ver).where(
        "issue_type", "==", "vqse").stream()
    issues = []
    for doc in docs:
        i = unpack_issue(doc.to_dict())
        i.id = doc.id
        issues.append(i)

    from operator import attrgetter
    issues = sorted(issues, key=attrgetter('create_date'), reverse=True)
    print_rip(f"sorted: {issues}")
    if len(issues) > 0:
        return issues[0]
    else:
        return None


def get_issues(code, sro):
    docs = db.collection("issues").where("nro", "==", sro).where("code", "==", code).stream()
    issues = []
    for doc in docs:
        i = unpack_issue(doc.to_dict())
        i.id = doc.id
        issues.append(i)
    return issues


def get_issues_in_sro(sro, type, ver):
    docs = db.collection("issues").where("nro", "==", sro).where("apd", "==", type).where("ver", "==", ver).stream()
    issues = []
    for doc in docs:
        i = unpack_issue(doc.to_dict())
        i.id = doc.id
        issues.append(i)
    return issues


def get_technicien_list():
    docs = db.collection(u"users").where("role", "==", "user").stream()
    users = []
    for doc in docs:
        u = doc.to_dict()
        users.append(
            QSRIPUser(displayName=u["displayName"], email=u["email"], userId=doc.id, last_loggined=u["last_login"],
                      role=u["role"], password=u['password']))
    return users


def load_project_data(project):
    ver = f"{project.type} V{project.ver}"
    sro = f"{project.client} {project.project} {project.sro}"
    project.pbo.clear()
    project.cable.clear()
    project.pts.clear()
    project.sor_quality = get_proj_sor_quality(project)
    print_rip(f"load data: {project.sro} ver: {ver}")
    docs = db.collection("pbo").where("proj_id", "==", project._id).stream()
    vqse = {"vqse1": 0, "vqse2": 0, "vqse3": 0}
    for doc in docs:
        d = doc.to_dict()
        p = PBO()
        p.id = doc.id
        p.sro = d["sro"]
        p.ver = d["ver"]
        p.name = d["name"]
        p.cable = d["cable"]
        p.pbo_id = d["pbo_id"]
        if "vsqe" in d:
            p.vsqe = d["vsqe"]
            if p.vsqe > 0:
                vqse[f"vqse{p.vsqe}"] += 1
        p.type = d["type"]
        if "images" in d:
            p.images = d["images"]
        else:
            p.images = []
        if 'comments' in d:
            p.comments = d['comments']
        p.model = d["model"]
        p.reference = d["reference"]
        p.function = d["function"]
        p.support = d["support"]
        p.x: float = d["x"]
        p.y: float = d["x"]
        if "error" in d:
            p.error = d["error"]
        project.pbo.append(p)
    # pts = db.collection("pts").where("sro", "==", sro).where("ver", "==", ver).stream()
    # for doc in pts:
    #     d = doc.to_dict()
    #     p = PointTechnique()
    #     p.sro = d["sro"]
    #     p.ver = d["ver"]
    #     p.name = d["name"]
    #     p.code = d["code"]
    #     p.type = d["type"]
    #     p.x: float = d["x"]
    #     p.y: float = d["x"]
    #     project.pts.append(p)
    cabs = db.collection("cable").where("proj_id", "==", project._id).stream()
    for cc in cabs:
        d = cc.to_dict()
        c = Cable()
        c.id = cc.id
        c.sro = d["sro"]
        c.ver = d["ver"]
        c.name = d["name"]
        if "vsqe" in d:
            c.vsqe = d["vsqe"]
            if c.vsqe > 0:
                vqse[f"vqse{c.vsqe}"] += 1

        c.src = d["src"]
        c.dest = d["dest"]
        c.type = d["type"]
        c.length = d["length"]
        c.section = d["section"]
        c.capacity = d["capacity"]
        project.cable.append(c)

    update_project_data(project, vqse)
    print_rip(f"cable:{project.cable}")
    print_rip(f"pbo:{len(project.pbo)}")
    # if len(project.cable) == 0 or len(project.pbo) == 0:
    #     remove_project(project)


def remove_project(project):
    db.collection('proj').document(project._id).delete()


def update_project_vqse(project):
    ref = db.collection(u'proj').document(project._id)
    ref.set({
        "vqse": project.vqse_count
    }, merge=True)


def save_pbo(project, pbos):
    try:
        print_rip(pbos)
        batch = db.batch()
        for p in pbos:
            ref = db.collection(u'pbo').document()
            p.id = ref.id
            batch.create(ref, {
                "proj_id": project._id,
                "sro": p.sro,
                "ver": p.ver,
                "name": p.name,
                "cable": p.cable,
                "vsqe": p.vsqe,
                "type": p.type,
                "model": p.model,
                "images": p.images,
                "pbo_id": p.pbo_id,
                "reference": p.reference,
                "function": p.function,
                "support": p.support,
                "x": p.x,
                "y": p.y
            })
        batch.commit()
        return 0
    except Exception as e:
        delete_proj(project)
        return 1


def update_pbos_image(pbos):
    # print_rip(pbos)
    batch = db.batch()
    for p in pbos:
        ref = db.collection(u'pbo').document(p.id)
        print_rip(f"{p.name} - {p.images} - {p.id}")
        batch.update(ref, {
            "images": p.images
        })
    batch.commit()


def save_pts(project):
    pts = project.pts
    print_rip(pts)
    batch = db.batch()
    for p in pts:
        ref = db.collection(u'pts').document()
        batch.create(ref, {
            "sro": p.sro,
            "ver": p.ver,
            "name": p.name,
            "code": p.code,
            "type": p.type,
            "x": p.x,
            "y": p.y
        })
    batch.commit()


def update_cable(cable):
    ref = db.collection("cable").document(cable.id)
    ref.update({"vsqe": cable.vsqe})


def update_pbo(pbo):
    ref = db.collection("pbo").document(pbo.id)
    ref.update({"vsqe": pbo.vsqe})


def save_cables(cables, proj, proj_id):
    try:
        print_rip(cables)
        batch = db.batch()
        for c in cables:
            ref = db.collection(u'cable').document()
            batch.create(ref, {
                "proj_id": proj_id,
                "sro": c.sro,
                "ver": c.ver,
                "name": c.name,
                "src": c.src,
                "dest": c.dest,
                "vsqe": c.vsqe,
                "section": c.section,
                "capacity": c.capacity,
                "type": c.type,
                "length": c.length
            })
        batch.commit()
        return 0
    except Exception as e:
        delete_proj(proj)
        return 1


def update_last_login(userid, last_loggined):
    user_ref = db.collection(u'users').document(userid)
    user_ref.update({"last_login": last_loggined})


def get_user(userid):
    user_ref = db.collection(u'users').document(userid)
    user_get = user_ref.get()
    if user_get.exists:
        u = user_get.to_dict()
        user = QSRIPUser()
        user.project_ids = u['project_ids']
        user.displayName = u['displayName']
        user.email = u['email']
        user.userId = u['userId']
        user.last_loggined = u['last_login']
        user.role = u['role']
        user.password = u['password']
        return user
    return None


def get_user_login(userid):
    settings = QSettings("settings.ini", QSettings.IniFormat)
    email = settings.value("email")
    pw = settings.value("pw")
    pw_md5 = hashlib.md5(pw.encode("utf-8")).hexdigest()
    user_ref = db.collection(u'users').document(userid)
    user_get = user_ref.get()
    if user_get.exists:
        u = user_get.to_dict()
        if u['email'] == email and u['password'] == pw_md5:
            if u['isOnline'] == False:
                user = QSRIPUser()
                user.project_ids = u['project_ids']
                user.displayName = u['displayName']
                user.email = u['email']
                user.userId = u['userId']
                user.last_loggined = u['last_login']
                user.role = u['role']
                user.password = u['password']
                return user
            else:
                return None
        else:
            return None
    return None


def get_config(conn, key):
    """
    get config value for key
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT value FROM config WHERE key = ? LIMIT 1", (key,))
    rows = cur.fetchall()
    if len(rows) > 0:
        value = rows[0][0]
        return value
    return None


def set_config(conn, key, value):
    """
    set config value for key
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT value FROM config WHERE key = ? LIMIT 1", (key,))
    rows = cur.fetchall()
    if len(rows) > 0:
        cur.execute("UPDATE config "
                    "SET value = ? "
                    "WHERE key = ?", (value, key))
    else:
        cur.execute("INSERT INTO config(key,value) VALUES(?,?)", (key, value))
    conn.commit()


def remove_config(conn, key):
    """
    remove config key=value
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("DELETE FROM config WHERE key = ?", (key,))
    conn.commit()


def delete_proj(project):
    delete_collection_in_proj(project._id, u'cable')
    delete_collection_in_proj(project._id, u'issues')
    delete_collection_in_proj(project._id, u'pbo')
    delete_collection_in_proj(project._id, u'sor_quality')
    db_ref = db.collection('users').document(project.user_id)
    project_ids = db_ref.get().to_dict()['project_ids']
    project_ids.remove(project._id)
    db_ref.set({'project_ids': project_ids}, merge=True)
    db.collection(u'proj').document(project._id).delete()


def delete_collection_in_proj(proj_id, table):
    docs = db.collection(table).stream()
    batch = db.batch()
    batch_count = 0
    for doc in docs:
        doc_dict = doc.to_dict()
        if 'proj_id' in doc_dict and doc_dict['proj_id'] == proj_id:
            batch.delete(doc.reference)
            if batch_count == 300:
                batch.commit()
                batch = db.batch()
                batch_count = 0
            batch_count += 1
    batch.commit()


def print_rip(*args):
    if const.show_debug:
        print(*args)


def compare_string(name):
    compare_name = name.upper().replace('_', '-').replace(' ', '-')
    return compare_name


def dlg_notification(text):
    dlg = QDialog()
    dlg.setWindowTitle('notification')
    dlg.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowCloseButtonHint)
    layout = QtWidgets.QHBoxLayout()
    dlg.setLayout(layout)
    lb = QLabel(text)
    layout.addWidget(lb)
    # layout.addWidget(lb, alignment=QtCore.Qt.AlignCenter)
    dlg.exec()
