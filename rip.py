import datetime
import os
import sys
import subprocess

from PyQt5 import QtWidgets, QtGui
from PyQt5 import QtCore
from PyQt5.QtGui import QFont, QIcon, QFontDatabase, QPixmap
from PyQt5.QtCore import Qt
from PyQt5.QtNetwork import QNetworkConfigurationManager
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QPushButton, QFileDialog, \
    QApplication, QFrame, QSplashScreen, QAction, QMessageBox, QLineEdit

import env
import qsriputils
import const
from pathlib import Path
import zipfile

import session
from workflow_tab import WorkflowTab

f = open("lang.txt", 'r')
from multilang import english, french
if f.read() == 'en':
    ll = english
else:
    ll = french
f.close()

from RIPUtils import system_utils, db_utils, qt_utils, auth_utils, cipher_utils
from RIPModels import User
from reporting_tab import ReportingTab

from import_tab import ImportTab
from sys_tab import SysnoptiqueTab
from field_tab import FieldTab
from tool_tab import ToolTab
from logindialog import LoginDialog, LoginMacOS
from user import QSRIPUser

from qgis.core import QgsApplication, QgsVectorLayer, QgsProject, QgsFeatureRequest

import ssl
ssl._create_default_https_context = ssl._create_unverified_context

os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"


class TitleBar(QtWidgets.QDialog):
    window = None

    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.window = parent
        css = """
        QWidget{
            color: rgb(242,242,245);
            border: none;
        }
        QDialog{
            font-size:10px;
            color: black;
            background-color: rgb(242,242,245);

        }
        QLabel {
            font-family:'SF UI Text';
            font-weight: bold;
            font-size:12;
            color: rgb(46,46,46);
            qproperty-alignment: 'AlignVCenter | AlignLeft';
        }
        QToolButton{
            font-family:'Segoe MDL2 Assets';
            color: rgb(46,46,46);
            font-size:11;
            background-color: none;
        }

        QToolButton:hover{
            color: rgb(14,113,235);
        }
        QToolButton:checked{
            color: rgb(14,113,235);
        }
        QComboBox {
            border: 1px solid gray;
            border-radius: 3px;
            padding: 3px 18px 3px 3px;
            min-width: 6em;
            color: rgb(46,46,46);
            background-color: rgb(222,222,227);
        }
        QComboBox QListView
        {
            color: rgb(46,46,46);
            selection-background-color: #ffaa00;
        }
        



        """
        # font = QFont("Segoe MDL2 Assets")
        self.setAutoFillBackground(True)
        # self.setBackgroundRole(QtGui.QPalette.Highlight)
        self.setStyleSheet(css)
        self.minimize = QtWidgets.QToolButton(self)
        # self.minimize.setFont(font)
        self.minimize.setText("\uE949")
        self.maximize = QtWidgets.QToolButton(self)
        # self.maximize.setFont(font)
        self.maximize.setText("\uE739")
        close = QtWidgets.QToolButton(self)
        # close.setFont(font)
        close.setText("\uE106")
        self.minimize.setMinimumHeight(10)
        close.setMinimumHeight(10)
        self.maximize.setMinimumHeight(10)
        label = QtWidgets.QLabel("<html><img src='images/securiplogo.png'/></html>")
        brand_label = QtWidgets.QLabel("<html><img src='images/brandico.png'/></html>")
        hbox = QtWidgets.QHBoxLayout(self)
        # hbox.insertStretch(1, 200)
        lbox = QtWidgets.QHBoxLayout()
        lbox.addWidget(label)
        lbox.addWidget(brand_label)
        self.sline = QtWidgets.QLineEdit()
        self.sline.setPlaceholderText(ll['search_elements'])
        self.sline.setFont(QtGui.QFont("SF UI Text", 9))
        self.sline.setStyleSheet(
            "border: 1px solid rgb(46,46,46); padding: 3px;border-radius:3px; background-color: rgb(222,222,227); color: rgb(46,46,46); min-width: 100px")
        self.completer = QtWidgets.QCompleter()
        self.completer.setFilterMode(Qt.MatchContains)
        self.sline.setCompleter(self.completer)
        self.completer.activated.connect(self.window.select_pbo)
        lbox.addWidget(self.sline)
        lbox.insertStretch(3, 100)
        lbox.setStretch(2, 300)
        rbox = QtWidgets.QHBoxLayout()
        tbox = QtWidgets.QHBoxLayout()

        self.selectedimages = ["sysico.png", "fieldico.png", "reportico.png", "toolico.png", "importico.png",
                               "workflowico.png",
                               "exportico.png", "poleico.png"]
        self.deselectedimages = ["sysico_deselected.png", "fieldico_deselected.png", "reportico_deselected.png",
                                 "toolico_deselected.png", "importico_deselected.png", "workflowico_deselected.png",
                                 "exportico_deselected.png",
                                 "poleico_deselected.png"]
        self.toolbuttons = []
        sysbutton = self.toolbarButton(ll['syn'], "images/sysico.png")
        polebutton = self.toolbarButton(ll['aer'], "images/poleico_deselected.png")
        tableaubutton = self.toolbarButton(ll['tab'], "images/fieldico_deselected.png")
        reportbutton = self.toolbarButton(ll['rep'], "images/reportico_deselected.png")
        toolbutton = self.toolbarButton(ll['tool'], "images/toolico_deselected.png")
        importbutton = self.toolbarButton(ll['imp'], "images/importico_deselected.png")
        exportbutton = self.toolbarButton(ll['exp'], "images/exportico_deselected.png")

        chat = dict()
        chats = qsriputils.db.collection('chats').get()
        if len(chats) > 0:
            for i in chats:
                k = i.to_dict()
                id_doc = i.id
                chat[k['date_create']] = [k['user_read'], id_doc, k['proj_id']]
            time_sort = sorted(chat.keys(), reverse=True)
            self.time_new_chat = time_sort[0]
            user_read = chat[self.time_new_chat][0]
            proj_new_chat = chat[self.time_new_chat][2]
            if proj_new_chat in self.window.user.project_ids:
                if self.window.user.userId in user_read:
                    workflowbutton = self.toolbarButton(ll['wflow'], "images/workflowico_deselected.png")
                else:
                    workflowbutton = self.toolbarButton(ll['wflow'], "images/workflowico_new.png")
                    self.deselectedimages[5] = 'workflowico_new.png'
            else:
                workflowbutton = self.toolbarButton(ll['wflow'], "images/workflowico_deselected.png")
        else:
            workflowbutton = self.toolbarButton(ll['wflow'], "images/workflowico_deselected.png")

        sysbutton.setChecked(True)
        tbox.addWidget(sysbutton)
        tbox.addWidget(polebutton)
        tbox.addWidget(tableaubutton)
        tbox.addWidget(reportbutton)
        tbox.addWidget(toolbutton)
        tbox.addWidget(importbutton)
        tbox.addWidget(exportbutton)
        tbox.addWidget(workflowbutton)
        self.toolbuttons = [sysbutton, tableaubutton, reportbutton, toolbutton, importbutton, workflowbutton,
                            exportbutton, polebutton]
        lw = QtWidgets.QWidget(self)
        tw = QtWidgets.QWidget(self)
        rw = QtWidgets.QWidget(self)
        lw.setLayout(lbox)
        tw.setLayout(tbox)
        rw.setLayout(rbox)
        hbox.addWidget(lw)
        hbox.addWidget(tw)
        hbox.addWidget(rw)
        self.combobox = QtWidgets.QComboBox()
        self.combobox.addItem(ll['selecPro'])
        self.combobox.addItems([f"{p.project} {p.sro} {p.type} V{p.ver}" for p in self.window.projs])
        self.combobox.currentIndexChanged.connect(self.window.on_change_project)

        self.changelang = QtWidgets.QComboBox()
        self.changelang.setStyleSheet("min-width: 2.05em; max-width: 2.05em; margin-left: 10px")
        self.changelang.setGeometry(200, 150, 100, 40)
        self.changelang.addItem(ll['En'])
        self.changelang.addItem(ll['Fr'])
        self.f = open('lang.txt', 'r')
        if self.f.read() == 'en':
            self.changelang.setCurrentText(ll['En'])
        else:
            self.changelang.setCurrentText(ll['Fr'])
        self.f.close()
        self.changelang.currentIndexChanged.connect(self.window.chang_lang)

        setting = QtWidgets.QToolButton()
        setting.setIcon(QtGui.QIcon("images/settingico.png"))
        setting.clicked.connect(self.window.show_setting)
        about = QtWidgets.QToolButton()
        about.setIcon(QtGui.QIcon('images/abouticon.png'))
        about.clicked.connect(self.window.show_about)
        self.logout = QtWidgets.QToolButton()
        self.logout.setIcon(QtGui.QIcon('images/logout.png'))
        # self.logout.setStyleSheet("font-weight:bold;")
        self.logout.clicked.connect(self.window.logout)
        self.usernameLabel = QtWidgets.QToolButton()
        self.usernameLabel.setIcon(QtGui.QIcon("images/User_Infor.png"))
        self.usernameLabel.setStyleSheet("margin-left: 10px")
        self.usernameLabel.clicked.connect(self.window.ShowInfoUser)
        # self.usernameLabel = QtWidgets.QLabel(self.window.user.displayName if self.window.user else "Username")
        # self.usernameLabel.setStyleSheet("padding-left:7; padding-right:30")
        rbox.addWidget(self.combobox)
        rbox.addWidget(self.changelang)
        rbox.addWidget(self.usernameLabel)
        rbox.addWidget(about)
        rbox.addWidget(setting)
        rbox.addWidget(self.logout)
        rbox.addWidget(self.minimize)
        rbox.addWidget(self.maximize)
        rbox.addWidget(close)
        rbox.setSpacing(3)
        hbox.setSpacing(0)
        hbox.setStretch(0, 500)
        hbox.setStretch(2, 500)
        hbox.setStretch(1, 100)
        rbox.insertStretch(0, 100)
        rbox.setStretch(1, 300)
        rbox.setStretch(2, 200)
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)

        self.maxNormal = True
        close.clicked.connect(self.close)
        self.minimize.clicked.connect(self.showSmall)
        self.maximize.clicked.connect(self.showMaxRestore)

    def toolbarButton(self, text, image):
        button = QtWidgets.QToolButton()
        button.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        button.setText(text)
        button.setCheckable(True)
        button.setIconSize(QtCore.QSize(25, 20))
        button.setIcon(QtGui.QIcon(image))
        button.setFont(QtGui.QFont("SF UI Text", 9))
        fontinfo = QtGui.QFontInfo(button.font())
        qsriputils.print_rip(fontinfo.family())
        button.clicked.connect(self.window.select_tab)
        return button

    def showSmall(self):
        window.showMinimized()

    def showMaxRestore(self):
        if (self.maxNormal):
            window.showNormal()
            self.maxNormal = False
            self.maximize.setText("\uE739")
            qsriputils.print_rip('1')
        else:
            window.showMaximized()
            self.maxNormal = True
            qsriputils.print_rip('2')
            self.maximize.setText("\uE923")

    def close(self):
        if session.USER is not None:
            db_utils.update_user({"isOnline": False})
        window.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            window.moving = True
            window.offset = event.pos()

    def mouseMoveEvent(self, event):
        if window.moving: window.move(event.globalPos() - window.offset)


class WindowFrame(QFrame):
    project_changed = QtCore.pyqtSignal()
    pbo_activated = QtCore.pyqtSignal(str)
    request_reload = QtCore.pyqtSignal()
    projs = []
    techniciens = []
    user: QSRIPUser = None
    tabs = []
    boiteLayer = None
    cableLayer = None
    activeId = 0
    customer_value = {}
    activeQgsProject: QgsProject = None
    layout: QtWidgets.QStackedLayout = None
    loaded = False

    def __init__(self):
        super(QFrame, self).__init__()

        self.ic_manager = QNetworkConfigurationManager()
        # QObject.connect(self.ic_manager, QtCore.PYQT_SIGNAL("configurationAdded()"), self.network_config_added)
        # QObject.connect(self.ic_manager, QtCore.PYQT_SIGNAL("configurationChanged()"), self.network_config_changed)
        # QObject.connect(self.ic_manager, QtCore.PYQT_SIGNAL("configurationRemoved()"), self.network_config_removed)
        self.ic_manager.configurationAdded.connect(self.network_config_added)
        self.ic_manager.configurationChanged.connect(self.network_config_changed)
        self.ic_manager.configurationRemoved.connect(self.network_config_removed)

        self.offline_dialog = qt_utils.msg_dialog(
            ll['check_connect'],
            "SecurRip"
        )
        self.offline_dialog.setHidden(True)

        self.is_logout = False
        self.dlg_about = None
        self.user_read = None
        self.id_proj_wf = None

        chat = dict()
        chats = qsriputils.db.collection('chats').get()
        self.user_read = []
        self.id_doc_new = ''
        if len(chats) > 0:
            for i in chats:
                k = i.to_dict()
                id_doc = i.id
                chat[k['date_create']] = [k['user_read'], id_doc]
            time_sort = sorted(chat.keys(), reverse=True)
            time_new_chat = time_sort[0]
            self.user_read = chat[time_new_chat][0]
            self.id_doc_new = chat[time_new_chat][1]

        # temporary assign for backward compatible
        self.dlg_version = None
        # temporary assign for backward compatible

        if env.APP_MODE == "prod" and const.version_client != session.CONFIG["default"]["version"]:
            self.dlg_version = qt_utils.btn_dialog(
                f"<u style='color:#2E64FE'>SecurRip</u> {session.CONFIG['default']['version']} {ll['update_is_available']}.<br>"
                f"{ll['Current_version']}: {const.version_client}<br>"
                f"{ll['Change_log']}: {session.CONFIG['default']['change_log']}",
                {f"{ll['update']}": self.update_version},
                f"{ll['update']}"
            )
            if self.dlg_version.exec() == 0:
                sys.exit()

        conn = qsriputils.db_connect(os.path.join(env.PATH["db_path"], "qsrip.sqlite"))
        if conn:
            qsriputils.create_table(conn, const.sql_create_user_table)
            qsriputils.create_table(conn, const.sql_create_config_table)
            last_login = qsriputils.get_config(conn, const.key_loggined)
            qsriputils.print_rip(last_login)
            if last_login and len(last_login) > 0:
                qsriputils.print_rip("last login:", last_login)
                self.user = qsriputils.get_user_login(last_login)
                if self.user:
                    qsriputils.db.collection('users').document(self.user.userId).set({'isOnline': True}, merge=True)
            if not self.user:
                first_time_login = self.login()
                # 1 is first time, 0 is not
                if first_time_login == 1:
                    user_license = auth_utils.license_check(self.user.userId)
                    session.USER = User(self.user.userId, user_license)
                    self.init_ui()
            else:
                user_license = auth_utils.license_check(self.user.userId)
                session.USER = User(self.user.userId, user_license)
                self.load_all_project()
                self.techniciens.extend(qsriputils.get_technicien_list())
                last_opened = qsriputils.get_config(conn, f"{const.key_last_opened_project}_{self.user.userId}")
                if last_opened:
                    self.activeId = last_opened
                    qsriputils.print_rip("last active id: ", self.activeId)
                conn.close()
                self.init_ui()
                self.last_selected_tab = 0

        system_utils.load_csv(self.projs)
        new_chats = qsriputils.db.collection('chats')
        query_watch = new_chats.on_snapshot(self.on_snapshot)
        QAction("Quit", self).triggered.connect(self.closeEvent)

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        if session.USER is not None:
            db_utils.update_user({"isOnline": False})

    def __handle_online_state(self):
        is_online = system_utils.is_online()
        self.setDisabled(not is_online)
        self.offline_dialog.setHidden(is_online)

    def network_config_added(self, config):
        print(f"Network configuration added: {config.name()}")
        self.__handle_online_state()

    def network_config_changed(self, config):
        print(f"Network configuration changed: {config.name()}")
        self.__handle_online_state()

    def network_config_removed(self, config):
        print(f"Network configuration removed: {config.name()}")
        self.__handle_online_state()

    def on_snapshot(self, doc_snapshot, changes, read_time):
        for change in changes:
            if change.type.name == 'ADDED':
                if change.document.to_dict()['date_create'] >= self.m_titleBar.time_new_chat:
                    if change.document.to_dict()['proj_id'] in self.user.project_ids or self.user.role == 'admin':
                        self.user_read = change.document.to_dict()['user_read']
                        self.id_doc_new = change.document.id
                        if self.user.userId not in self.user_read:
                            self.deselectedimages[5] = 'workflowico_new.png'
                            self.m_titleBar.toolbuttons[5].setIcon(QtGui.QIcon("images/workflowico_new.png"))
                            self.id_proj_wf = change.document.to_dict()['proj_id']

    def init_ui(self):
        self.loaded = True
        self.m_mouse_down = False
        self.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setMouseTracking(True)
        self.setAttribute(Qt.WA_TranslucentBackground)
        v = QtWidgets.QVBoxLayout(self)
        w = QtWidgets.QWidget(self)
        w.setObjectName("backgroundFrame")
        v.addWidget(w)
        v.setContentsMargins(0, 0, 0, 0)
        # qsriputils.remove_data()
        w.setStyleSheet("#backgroundFrame{background-color:rgb(36,36,36);border-radius:10px;}")
        self.m_content = QtWidgets.QWidget(w)
        self.m_titleBar = TitleBar(self)
        self.selectedimages = self.m_titleBar.selectedimages
        self.deselectedimages = self.m_titleBar.deselectedimages

        self.moving = False
        vbox = QtWidgets.QVBoxLayout(w)
        vbox.addWidget(self.m_titleBar)
        found = False
        for (i, p) in enumerate(self.projs):
            if p._id == self.activeId:
                self.m_titleBar.combobox.setCurrentIndex(i + 1)
                found = True
                break
        if not found:
            self.m_titleBar.combobox.setCurrentIndex(1)

        self.layout = QtWidgets.QStackedLayout()
        self.tab1 = SysnoptiqueTab(self)
        self.tab3 = ReportingTab(self)
        self.tab2 = FieldTab(self)
        self.tab4 = ToolTab(self)
        self.tab5 = ImportTab(self)
        self.tab6 = WorkflowTab(self)

        self.project_changed.connect(self.tab1.on_project_changed)
        self.project_changed.connect(self.tab5.on_project_changed)
        # self.project_changed.connect(tab3.on_project_changed)
        # self.project_changed.connect(self.tab6.on_project_changed)
        self.pbo_activated.connect(self.tab1.on_pbo_activated)
        self.request_reload.connect(self.tab1.request_reload)
        self.layout.addWidget(self.tab1)
        self.layout.addWidget(self.tab2)
        self.layout.addWidget(self.tab3)
        self.layout.addWidget(self.tab4)
        self.layout.addWidget(self.tab5)
        self.layout.addWidget(self.tab6)

        self.layout.addWidget(self.no_internet_tab())
        vbox.addLayout(self.layout)
        self.layout.setCurrentIndex(0)

    def update_version(self):
        if self.dlg_version is not None:
            self.dlg_version.close()
        if self.dlg_about is not None:
            self.dlg_about.close()
        if session.USER is not None:
            db_utils.update_user({"isOnline": False})
        if os.path.exists(os.path.join(env.PATH["root_path"], "update.py")):
            subprocess.Popen(f"python update.py {session.CONFIG['default']['version']}", creationflags=subprocess.DETACHED_PROCESS | subprocess.CREATE_NEW_PROCESS_GROUP)
            sys.exit()
        elif os.path.exists(os.path.join(env.PATH["root_path"], "update.pyc")):
            subprocess.Popen(f"python update.pyc {session.CONFIG['default']['version']}", creationflags=subprocess.DETACHED_PROCESS | subprocess.CREATE_NEW_PROCESS_GROUP)
            sys.exit()

    def import_license(self):
        file_dialog = QFileDialog()
        file_dialog.setFileMode(QFileDialog.ExistingFile)
        if file_dialog.exec():
            file_path = file_dialog.selectedFiles()
            d = eval(open(file_path[0]).read())
            if d['type'] == 'Personal':
                msg = env.UUID + '_' + d['type'] + '_' + d['expired_time']
            else:
                msg = d['key'] + '_' + d['type'] + '_' + d['expired_time']
            if cipher_utils.verify_license(msg, d['signature']) and datetime.datetime.strptime(
                    d['expired_time'], '%m/%d/%Y') > datetime.datetime.now():
                qsriputils.db.collection('users').document(self.user.userId).set({'license': d['key']}, merge=True)
                qsriputils.db.collection('license').document(d['key']).set(d, merge=True)
                qsriputils.print_rip('SecurRip is licensed!')
                session.USER.license = (d["key"], "License valid")
                self.button_license.setText(ll['licenOk'])
                self.button_license.setStyleSheet('QPushButton {color: green;}')
                self.button_license.disconnect()
            else:
                qsriputils.print_rip('Invalid license')
                self.button_license.setText(ll['licenNok'])
                self.button_license.setStyleSheet('QPushButton {color: red;}')
    def updateText(self):
        self.m_titleBar.sline.setPlaceholderText(ll['search_elements'])
        self.m_titleBar.toolbuttons[0].setText(ll['syn'])
        self.m_titleBar.toolbuttons[7].setText(ll['aer'])
        self.m_titleBar.toolbuttons[1].setText(ll['tab'])
        self.m_titleBar.toolbuttons[2].setText(ll['rep'])
        self.m_titleBar.toolbuttons[3].setText(ll['tool'])
        self.m_titleBar.toolbuttons[4].setText(ll['imp'])
        self.m_titleBar.toolbuttons[6].setText(ll['exp'])
        self.m_titleBar.toolbuttons[5].setText(ll['wflow'])

        self.m_titleBar.changelang.disconnect()
        self.m_titleBar.changelang.clear()
        self.m_titleBar.changelang.addItem(ll['En'])
        self.m_titleBar.changelang.addItem(ll['Fr'])
        self.f = open('lang.txt', 'r')
        if self.f.read() == 'en':
            self.m_titleBar.changelang.setCurrentText(ll['En'])
        else:
            self.m_titleBar.changelang.setCurrentText(ll['Fr'])
        self.f.close()
        self.m_titleBar.changelang.currentIndexChanged.connect(self.chang_lang)

        self.m_titleBar.logout.setText(ll['log_out'])

        self.offline_dialog = qt_utils.msg_dialog(
            ll['check_connect'],
            "SecurRip"
        )

    def chang_lang(self, text):
        global ll
        if text == 0:
            ll = english
        elif text == 1:
            ll = french
        self.f = open('lang.txt', 'w')
        if text == 0:
            self.f.write('en')
        elif text == 1:
            self.f.write('fr')
        self.f.close()


        self.updateText()
        self.tab1.updateText(ll)
        self.tab2.updateText(ll)
        self.tab3.updateText(ll)
        self.tab4.updateText(ll)
        self.tab5.updateText(ll)
        self.tab6.updateText(ll)

    def show_about(self):
        qsriputils.print_rip('show about')
        dlg_about = QDialog()
        self.dlg_about = dlg_about
        dlg_about.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowCloseButtonHint)

        lb1 = QLabel()
        lb1.setFixedSize(100, 50)
        pixmap = QtGui.QPixmap('images/securiplogo.png')
        lb1.setPixmap(pixmap.scaled(lb1.size(), QtCore.Qt.KeepAspectRatio))
        lb2 = QLabel('SecurRip 2021')
        self.lb3 = QLabel(ll['build'])
        lb4 = QLabel('Windows 10 10.0')
        lb5 = QLabel(f'Runtime version: {const.version_client}')
        self.lb6 = QPushButton(ll['check_update'])
        self.lb6.clicked.connect(self.update_version)
        lb7 = QLabel('Copyright © 2019-2021 CFi Tech')
        lb_key = QLabel('Key:')
        key_value = QLineEdit('{}'.format(env.UUID))
        key_value.setReadOnly(True)
        if session.USER.license[0] is None:
            self.button_license = QPushButton(ll['imp_licen'])
            self.button_license.clicked.connect(self.import_license)
        else:
            self.button_license = QPushButton(ll['licenOk'])
            self.button_license.setStyleSheet('QPushButton {color: green;}')

        layoutDlg = QVBoxLayout()
        layoutDlg.addWidget(lb1, alignment=QtCore.Qt.AlignCenter)
        layoutDlg.addWidget(lb2)
        layoutDlg.addWidget(self.lb3)
        layoutDlg.addWidget(lb4)
        layoutDlg.addWidget(lb5)
        layoutDlg.addWidget(lb_key)
        layoutDlg.addWidget(key_value)
        layoutDlg.addWidget(self.button_license)
        layoutDlg.addWidget(self.lb6)
        layoutDlg.addWidget(lb7, alignment=QtCore.Qt.AlignCenter)
        layoutDlg.setAlignment(QtCore.Qt.AlignVCenter)
        dlg_about.setLayout(layoutDlg)
        dlg_about.setWindowTitle('About')
        dlg_about.exec()

    def ShowInfoUser(self):
        show = QMessageBox()
        show.setIcon(QMessageBox.Information)
        show.setWindowTitle(ll['user_infor'])
        show.setText(f'{ll["user"]} : {self.user.displayName} \nEmail: {self.user.email}\n{ll["role"]} : {self.user.role}')
        show.exec()

    def show_setting(self):
        self.dlg = QtWidgets.QDialog()

        def save_cfg():
            self.customer_value["wave_lendth"] = waveLenEdit.text().split(",")
            self.customer_value["pulse_width"] = [int(minPulseEdit.text()), int(maxPulseEdit.text())]
            self.customer_value["backcast_index"] = [float(irEdit.text()), float(irToEdit.text())]
            self.customer_value["bc"] = [float(bc1Edit.text()), float(bc1ToEdit.text())]
            self.customer_value["bc2"] = [float(bc2Edit.text()), float(bc2ToEdit.text())]
            self.customer_value["splice_threshold"] = [float(spliceEdit.text()), float(spliceToEdit.text())]
            self.customer_value["refl_threshold"] = [float(reflEdit.text()), float(reflToEdit.text())]
            self.customer_value["end_threshold"] = [float(eofEdit.text()), float(eofToEdit.text())]
            self.customer_value["connector"] = [float(clEdit.text()), float(crEdit.text())]
            self.customer_value["section_event_loss"] = float(slEdit.text())
            self.customer_value["splice_event_loss"] = float(splEdit.text())
            self.customer_value["total_loss"] = float(osEdit.text())
            self.customer_value["max_tolerance"] = float(evEdit.text())
            self.customer_value["pbo_reserve_in_chamber"] = float(crcEdit.text())
            self.customer_value["pbo_reserve_on_pole"] = float(crpEdit.text())
            self.customer_value["sro_reserve"] = float(crsEdit.text())

            qsriputils.print_rip(f"cv:{self.customer_value}")
            qsriputils.save_sor_customer_value(self.customer_value)
            self.dlg.accept()

        self.customer_value = qsriputils.get_sor_customer_value()
        self.dlg.resize(400, 300)
        self.dlg.setWindowTitle(ll['setting'])
        layout = QtWidgets.QVBoxLayout()
        layout.setSpacing(10)
        self.label = QtWidgets.QLabel(ll['setting_cus_value'])
        self.label.setFont(QFont("SF UI Text", 20))
        self.label.setStyleSheet("padding:10px;text-align:center")
        layout.addWidget(self.label)
        # Wave Length
        hbox1 = QtWidgets.QHBoxLayout()
        self.label1 = QtWidgets.QLabel(ll['Wave_length'])
        waveLenEdit = QtWidgets.QLineEdit(",".join(self.customer_value["wavelength"]))
        hbox1.addWidget(self.label1)
        hbox1.addWidget(waveLenEdit)
        hbox1.setStretch(0, 2)
        hbox1.setStretch(1, 3)
        layout.addLayout(hbox1)
        # Pulse width
        hbox2 = QtWidgets.QHBoxLayout()
        self.label2 = QtWidgets.QLabel(ll['Pulse_width'])
        pw = self.customer_value["pulse_width"]
        minPulseEdit = QtWidgets.QLineEdit(f"{pw[0]}")
        minPulseLabel = QtWidgets.QLabel("Min:")
        maxPulseLabel = QtWidgets.QLabel("Max:")
        maxPulseEdit = QtWidgets.QLineEdit(f"{pw[1]}")
        hbox2.addWidget(self.label2)
        hbox2.addWidget(minPulseLabel)
        hbox2.addWidget(minPulseEdit)
        hbox2.addWidget(maxPulseLabel)
        hbox2.addWidget(maxPulseEdit)
        hbox2.setStretch(0, 4)
        hbox2.setStretch(1, 1)
        hbox2.setStretch(2, 2)
        hbox2.setStretch(3, 1)
        hbox2.setStretch(4, 2)
        layout.addLayout(hbox2)
        # IR (Backcast index)
        ir_cv = self.customer_value["backcast_index"]
        hbox3 = QtWidgets.QHBoxLayout()
        self.label3 = QtWidgets.QLabel(ll['IR'])
        irEdit = QtWidgets.QLineEdit(f"{ir_cv[0]}")
        self.irToleranceLabel = QtWidgets.QLabel(ll['Tolerance'])
        irToEdit = QtWidgets.QLineEdit(f"{ir_cv[1]}")
        hbox3.addWidget(self.label3)
        hbox3.addWidget(irEdit)
        hbox3.addWidget(self.irToleranceLabel)
        hbox3.addWidget(irToEdit)
        hbox3.setStretch(0, 4)
        hbox3.setStretch(1, 3)
        hbox3.setStretch(2, 1)
        hbox3.setStretch(3, 2)
        layout.addLayout(hbox3)
        # Backscattering coefficient for 1310nm
        bc_vc = self.customer_value["bc"]
        hbox4 = QtWidgets.QHBoxLayout()
        self.label4 = QtWidgets.QLabel(f"{ll['Backscat']} 1310nm:")
        bc1Edit = QtWidgets.QLineEdit(f"{bc_vc[0]}")
        self.bc1ToleranceLabel = QtWidgets.QLabel(ll['Tolerance'])
        bc1ToEdit = QtWidgets.QLineEdit(f"{bc_vc[1]}")
        hbox4.addWidget(self.label4)
        hbox4.addWidget(bc1Edit)
        hbox4.addWidget(self.bc1ToleranceLabel)
        hbox4.addWidget(bc1ToEdit)
        hbox4.setStretch(0, 4)
        hbox4.setStretch(1, 3)
        hbox4.setStretch(2, 1)
        hbox4.setStretch(3, 2)
        layout.addLayout(hbox4)
        # Backscattering coefficient for 1550nm
        bc_vc2 = self.customer_value["bc2"]
        hbox5 = QtWidgets.QHBoxLayout()
        self.label5 = QtWidgets.QLabel(f"{ll['Backscat']} 1550nm:")
        bc2Edit = QtWidgets.QLineEdit(f"{bc_vc2[0]}")
        self.bc2ToleranceLabel = QtWidgets.QLabel(ll['Tolerance'])
        bc2ToEdit = QtWidgets.QLineEdit(f"{bc_vc[1]}")
        hbox5.addWidget(self.label5)
        hbox5.addWidget(bc2Edit)
        hbox5.addWidget(self.bc2ToleranceLabel)
        hbox5.addWidget(bc2ToEdit)
        hbox5.setStretch(0, 4)
        hbox5.setStretch(1, 3)
        hbox5.setStretch(2, 1)
        hbox5.setStretch(3, 2)
        layout.addLayout(hbox5)
        #  Splice detection threshold
        splice_cv = self.customer_value["splice_threshold"]
        hbox6 = QtWidgets.QHBoxLayout()
        self.label6 = QtWidgets.QLabel(ll['Splice_detec'])
        spliceEdit = QtWidgets.QLineEdit(f"{splice_cv[0]}")
        self.spliceToleranceLabel = QtWidgets.QLabel(ll['Tolerance'])
        spliceToEdit = QtWidgets.QLineEdit(f"{splice_cv[1]}")
        hbox6.addWidget(self.label6)
        hbox6.addWidget(spliceEdit)
        hbox6.addWidget(self.spliceToleranceLabel)
        hbox6.addWidget(spliceToEdit)
        hbox6.setStretch(0, 4)
        hbox6.setStretch(1, 3)
        hbox6.setStretch(2, 1)
        hbox6.setStretch(3, 2)
        layout.addLayout(hbox6)
        #  Reflectance detection threshold
        refl_cv = self.customer_value["refl_threshold"]
        hbox7 = QtWidgets.QHBoxLayout()
        self.label7 = QtWidgets.QLabel(ll['Reflectance'])
        reflEdit = QtWidgets.QLineEdit(f"{refl_cv[0]}")
        self.reflToleranceLabel = QtWidgets.QLabel(ll['Tolerance'])
        reflToEdit = QtWidgets.QLineEdit(f"{refl_cv[1]}")
        hbox7.addWidget(self.label7)
        hbox7.addWidget(reflEdit)
        hbox7.addWidget(self.reflToleranceLabel)
        hbox7.addWidget(reflToEdit)
        hbox7.setStretch(0, 4)
        hbox7.setStretch(1, 3)
        hbox7.setStretch(2, 1)
        hbox7.setStretch(3, 2)
        layout.addLayout(hbox7)
        #  End of fiber detection threshold
        eot_cv = self.customer_value["end_threshold"]
        hbox8 = QtWidgets.QHBoxLayout()
        self.label8 = QtWidgets.QLabel(ll['End_of_fiber'])
        eofEdit = QtWidgets.QLineEdit(f"{eot_cv[0]}")
        self.eofToleranceLabel = QtWidgets.QLabel(ll['Tolerance'])
        eofToEdit = QtWidgets.QLineEdit(f"{eot_cv[1]}")
        hbox8.addWidget(self.label8)
        hbox8.addWidget(eofEdit)
        hbox8.addWidget(self.eofToleranceLabel)
        hbox8.addWidget(eofToEdit)
        hbox8.setStretch(0, 4)
        hbox8.setStretch(1, 3)
        hbox8.setStretch(2, 1)
        hbox8.setStretch(3, 2)
        layout.addLayout(hbox8)
        #  Connector Loss
        connector = self.customer_value["connector"]
        hbox9 = QtWidgets.QHBoxLayout()
        self.label9 = QtWidgets.QLabel(ll['Connector_Loss'])
        clEdit = QtWidgets.QLineEdit(f"{connector[0]}")
        hbox9.addWidget(self.label9)
        hbox9.addWidget(clEdit)
        hbox9.setStretch(0, 2)
        hbox9.setStretch(1, 3)
        layout.addLayout(hbox9)
        # Connector Reflectance
        hbox10 = QtWidgets.QHBoxLayout()
        self.label10 = QtWidgets.QLabel(ll['Connector_Ref'])
        crEdit = QtWidgets.QLineEdit(f"{connector[1]}")
        hbox10.addWidget(self.label10)
        hbox10.addWidget(crEdit)
        hbox10.setStretch(0, 2)
        hbox10.setStretch(1, 3)
        layout.addLayout(hbox10)
        # Section Loss
        section_cv = self.customer_value["section_event_loss"]

        hbox11 = QtWidgets.QHBoxLayout()
        self.label11 = QtWidgets.QLabel(ll['Section_Loss'])
        slEdit = QtWidgets.QLineEdit(f"{section_cv}")
        hbox11.addWidget(self.label11)
        hbox11.addWidget(slEdit)
        hbox11.setStretch(0, 2)
        hbox11.setStretch(1, 3)
        layout.addLayout(hbox11)
        # Splice Loss No Reflectance
        splice_cv = self.customer_value["splice_event_loss"]

        hbox12 = QtWidgets.QHBoxLayout()
        self.label12 = QtWidgets.QLabel(ll['Splice_Loss'])
        splEdit = QtWidgets.QLineEdit(f"{splice_cv}")
        hbox12.addWidget(self.label12)
        hbox12.addWidget(splEdit)
        hbox12.setStretch(0, 2)
        hbox12.setStretch(1, 3)
        layout.addLayout(hbox12)
        # Optical assessment
        total_loss = self.customer_value["total_loss"]

        hbox13 = QtWidgets.QHBoxLayout()
        self.label13 = QtWidgets.QLabel(ll['Optical_ass'])
        osEdit = QtWidgets.QLineEdit(f"{total_loss}")
        hbox13.addWidget(self.label13)
        hbox13.addWidget(osEdit)
        hbox13.setStretch(0, 2)
        hbox13.setStretch(1, 3)
        layout.addLayout(hbox13)
        # Ecart position : Event QGIS / Event Sor
        max_tor = self.customer_value["max_tolerance"]
        hbox14 = QtWidgets.QHBoxLayout()
        self.label14 = QtWidgets.QLabel(ll['Ecart_pos'])
        evEdit = QtWidgets.QLineEdit(f"{max_tor}")
        hbox14.addWidget(self.label14)
        hbox14.addWidget(evEdit)
        hbox14.setStretch(0, 2)
        hbox14.setStretch(1, 3)
        layout.addLayout(hbox14)
        # Cable Reserve Chamber - Upstream/Downstream
        crc = self.customer_value["pbo_reserve_in_chamber"]
        hbox15 = QtWidgets.QHBoxLayout()
        self.label15 = QtWidgets.QLabel(ll['Cable_Reser_Cham'])
        crcEdit = QtWidgets.QLineEdit(f"{crc}")
        hbox15.addWidget(self.label15)
        hbox15.addWidget(crcEdit)
        hbox15.setStretch(0, 2)
        hbox15.setStretch(1, 3)
        layout.addLayout(hbox15)
        # Ecart position : Event QGIS / Event Sor
        crp = self.customer_value["pbo_reserve_on_pole"]
        hbox16 = QtWidgets.QHBoxLayout()
        self.label16 = QtWidgets.QLabel(ll['Cable_Reser_Pole'])
        crpEdit = QtWidgets.QLineEdit(f"{crp}")
        hbox16.addWidget(self.label16)
        hbox16.addWidget(crpEdit)
        hbox16.setStretch(0, 2)
        hbox16.setStretch(1, 3)
        layout.addLayout(hbox16)
        # Cable Reserve SRO - Upstream/Downstream
        crs = self.customer_value["sro_reserve"]
        hbox17 = QtWidgets.QHBoxLayout()
        self.label17 = QtWidgets.QLabel(ll['Cable_Reser_SRO'])
        crsEdit = QtWidgets.QLineEdit(f"{crs}")
        hbox17.addWidget(self.label17)
        hbox17.addWidget(crsEdit)
        hbox17.setStretch(0, 2)
        hbox17.setStretch(1, 3)
        layout.addLayout(hbox17)
        self.button = QtWidgets.QPushButton(ll['Save'])
        layout.addWidget(self.button)
        self.button.clicked.connect(save_cfg)
        layout.insertStretch(-1, 100)
        self.dlg.setLayout(layout)
        self.dlg.exec()

    def select_pbo(self, text):
        qsriputils.print_rip(f"select pbo: {text}")
        self.pbo_activated.emit(text)

    def no_internet_tab(self):
        w = QtWidgets.QWidget()
        l = QtWidgets.QHBoxLayout()
        w.setLayout(l)
        self.text_label = QtWidgets.QLabel()
        self.text_label.setText(ll['disconnect'])
        self.image_label = QtWidgets.QLabel()
        self.image_label.setMinimumSize(10, 10)
        l.addWidget(self.text_label)
        self.text_label.setFont(QFont('SF UI Text', 36))
        l.addWidget(self.image_label)
        pixmap = QtGui.QPixmap()
        pixmap.load("images/deconnection.jpg")
        self.image_label.setPixmap(pixmap)
        self.image_label.setAlignment(Qt.AlignRight)
        self.image_label.setStyleSheet("background-color:white")
        self.text_label.setStyleSheet("background-color:white; padding-left:20dp; padding-top:50dp")
        # get resize events for the label
        self.image_label.installEventFilter(self)
        l.setSpacing(0)
        l.setContentsMargins(0, 0, 0, 0)
        return w

    # event filter
    def eventFilter(self, source, event):
        if (source is self.image_label and event.type() == QtCore.QEvent.Resize):
            # re-scale the pixmap when the label resizes
            self.image_label.setPixmap(self.image_label.pixmap().scaled(
                self.image_label.size(), QtCore.Qt.KeepAspectRatio,
                QtCore.Qt.SmoothTransformation))
        return super(WindowFrame, self).eventFilter(source, event)

    def requestUpdateTab(self):
        self.request_reload.emit()

    def activeProject(self):
        if len(self.projs) == 0:
            return None
        for p in self.projs:
            qsriputils.print_rip("p._id: ", p._id)
            if p._id == self.activeId:
                qsriputils.print_rip(f"active project: {p.sro}")
                return p
        return self.projs[0]

    def loadActiveProjectLayer(self):
        self.load_layers(self.activeProject())
        # import threading
        # ex_thread = threading.Thread(target=self.load_layers, args=(self.activeProject(),))
        # ex_thread.start()

    def load_layers(self, p):
        # layer_name = os.path.splitext(p.cable_layer)[0]
        # layers = QgsProject.instance().mapLayersByName(layer_name)
        QgsProject.instance().clear()
        qsriputils.print_rip("sro: {}, loading layers".format(p.sro))
        cable_path = os.path.join(p.qgis_path, p.cable_layer)
        # load qgis if need
        file = f"{p.sro}_{p.type}_V{p.ver}_qgis.zip"
        if not os.path.exists(cable_path):
            ppath = Path(p.qgis_path).parent
            qsriputils.save_file(file, ppath)
            with zipfile.ZipFile(os.path.join(ppath, file), 'r') as zip_ref:
                zip_ref.extractall(p.qgis_path)
            os.remove(os.path.join(ppath, file))
        # load sor if need

        cableLayer = QgsVectorLayer(os.path.join(p.qgis_path, p.cable_layer), os.path.splitext(p.cable_layer)[0])
        QgsProject.instance().addMapLayer(cableLayer)
        qsriputils.print_rip("load layer:", p.cable_layer)
        boiteLayer = QgsVectorLayer(os.path.join(p.qgis_path, p.boite_layer), os.path.splitext(p.boite_layer)[0])
        QgsProject.instance().addMapLayer(boiteLayer)
        qsriputils.print_rip("load layer:", p.boite_layer)
        if not p.info:
            for root, dirs, files in os.walk(p.qgis_path):
                for file in files:
                    if "DBL COMPLET" in file and file.endswith(".shp"):
                        dfile = os.path.join(root, file)
                        qsriputils.print_rip(f"dbl: {dfile}")
                        dLayer = QgsVectorLayer(dfile, "DBLLayer")
                    elif "SITE" in file and file.endswith(".shp"):
                        sfile = os.path.join(root, file)
                        qsriputils.print_rip(f"site: {sfile}")
                        sLayer = QgsVectorLayer(sfile, "SiteLayer")
            # boite layer:
            nbpriseIdx = dLayer.fields().indexFromName("nb_prise")
            typeIdx = dLayer.fields().indexFromName("type_bat")
            request = QgsFeatureRequest().setSubsetOfAttributes(
                [nbpriseIdx, typeIdx])
            features = dLayer.getFeatures(request)
            dInfo = {}
            dtotal = 0
            for f in features:
                type = f["type_bat"]
                prise = f["nb_prise"]
                v = 0
                if type in dInfo:
                    v = dInfo[type]
                v += prise
                dtotal += prise
                dInfo[type] = v
                dInfo["total"] = dtotal
            p.info["dbl"] = dInfo

            # boite layer:
            typeStrucIdx = boiteLayer.fields().indexFromName("TYPE_STRUC")
            refIdx = boiteLayer.fields().indexFromName("REFERENCE")
            fabIdx = boiteLayer.fields().indexFromName("FABRICANT")
            request = QgsFeatureRequest().setSubsetOfAttributes(
                [typeStrucIdx, refIdx, fabIdx])
            features = boiteLayer.getFeatures(request)
            boiteInfo = {}
            for f in features:
                type = f["TYPE_STRUC"]
                ref = f["REFERENCE"]
                fab = f["FABRICANT"]
                key = f"{type} {fab} {ref}"
                v = 0
                if key in boiteInfo:
                    v = boiteInfo[key]
                v += 1
                boiteInfo[key] = v
            p.info["boite"] = boiteInfo
            # cable layer:
            typeStrucIdx = cableLayer.fields().indexFromName("TYPE_STRUC")
            lenIdx = cableLayer.fields().indexFromName("LGR_CARTO")
            refIdx = cableLayer.fields().indexFromName("REFERENCE")
            fabIdx = cableLayer.fields().indexFromName("FABRICANT")
            request = QgsFeatureRequest().setSubsetOfAttributes(
                [typeStrucIdx, lenIdx, refIdx, fabIdx])
            features = cableLayer.getFeatures(request)
            totalLen = 0
            cableLen = {}
            matLen = {}
            for f in features:
                type = f["TYPE_STRUC"]
                ref = f["REFERENCE"]
                fab = f["FABRICANT"]
                matKey = f"{fab} {ref}"
                len = f["LGR_CARTO"]
                l = 0
                if type in cableLen:
                    l = cableLen[type]
                m = 0
                if matKey in matLen:
                    m = matLen[matKey]
                m += len
                l += len
                cableLen[type] = l
                matLen[matKey] = m
                totalLen += len
            cableLen["total"] = totalLen
            p.info["cable"] = cableLen
            p.info["material"] = matLen
            if sLayer:
                # nomIdx = layer.fields().indexFromName("NOM")
                # srcIdx = layer.fields().indexFromName("ORIGINE")
                # destIdx = layer.fields().indexFromName("EXTREMITE")
                # capacityIdx = layer.fields().indexFromName("CAPACITE")
                # typeIdx = layer.fields().indexFromName("TYPE_STRUC")
                # lenIdx = layer.fields().indexFromName("LGR_CARTO")
                # secIdx = layer.fields().indexFromName("SECTION")
                # request = QgsFeatureRequest().setSubsetOfAttributes(
                #     [nomIdx, secIdx, srcIdx, destIdx, capacityIdx, typeIdx, lenIdx])
                features = sLayer.getFeatures()
                for f in features:
                    p.info["site1"] = ["Propriétaire", f["PROPRIETAI"]]
                    p.info["site2"] = ["Gestionnaire", f["GESTIONNAI"]]
                    p.info["site3"] = ["Code INSEE", f["INSEE"]]
                    break

    def updateUser(self, u):
        self.user = u
        self.m_titleBar.usernameLabel.setText(self.user.displayName)

    def add_project(self, p):
        self.projs.append(p)
        self.activeId = p._id
        self.loadActiveProjectLayer()
        self.m_titleBar.combobox.addItem(f"{p.project} {p.sro} {p.type} V{p.ver}")
        self.m_titleBar.combobox.setCurrentIndex(self.m_titleBar.combobox.count() - 1)

    def logout(self):
        qsriputils.print_rip(self.user)
        conn = qsriputils.db_connect(os.path.join(env.PATH["db_path"], "qsrip.sqlite"))
        if conn:
            qsriputils.remove_config(conn, const.key_loggined)
            conn.close()
        qsriputils.db.collection('users').document(self.user.userId).set({'isOnline': False}, merge=True)
        self.user = None
        session.USER = None
        self.projs = []
        self.setHidden(True)
        self.login()

    def toolbarButton(self, text, image):
        button = QtWidgets.QToolButton()
        button.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        button.setText(text)
        button.setCheckable(True)
        button.setIcon(QtGui.QIcon(image))
        button.setFont(QtGui.QFont("SF UI Text", 9))
        fontinfo = QtGui.QFontInfo(button.font())
        qsriputils.print_rip(fontinfo.family())
        button.clicked.connect(self.select_tab)
        return button

    def on_change_project(self, value):
        qsriputils.print_rip("change project to: ", value)
        if value > 0:
            self.activeId = self.projs[value - 1]._id
            p = self.activeProject()
            qsriputils.print_rip(f"sro:{p.sro} cable: {len(p.cable)}")
            qsriputils.print_rip(f"cable: {len(p.pbo)}")
            if len(p.cable) == 0:
                qsriputils.load_project_data(p)
            if len(p.issues) == 0:
                p.issues = qsriputils.get_all_issues_in_project(p)
            if len(p.pbo) > 0:
                pbo = [pp.name for pp in p.pbo]
                model = QtCore.QStringListModel()
                model.setStringList(pbo)
                self.m_titleBar.completer.setModel(model)
            else:
                self.m_titleBar.completer.setModel(None)
            self.m_titleBar.sline.setText("")
            self.loadActiveProjectLayer()
            conn = qsriputils.db_connect(os.path.join(env.PATH["db_path"], "qsrip.sqlite"))
            if conn:
                qsriputils.set_config(conn, f"{const.key_last_opened_project}_{self.user.userId}",
                                      self.activeId)
                conn.close()
            self.project_changed.emit()
        # for t in self.tabs:
        #     if hasattr(t, "update_active_project"):
        #         t.update_active_project(self.projs[value])

    def contentWidget(self):
        return self.m_content

    def dlg_role(self):
        self.dlg_checkRole = QDialog()
        self.dlg_checkRole.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowCloseButtonHint)
        self.lb = QLabel(ll['notAdmin'])
        layout = QVBoxLayout()
        layout.addWidget(self.lb)
        layout.setAlignment(QtCore.Qt.AlignVCenter)
        self.dlg_checkRole.setLayout(layout)
        self.dlg_checkRole.setWindowTitle(ll['warning'])
        self.dlg_checkRole.exec()

    def select_tab(self):
        if session.USER.license[0] is None:
            self.dlg = QDialog()
            self.dlg.resize(200, 100)
            layout = QtWidgets.QHBoxLayout()
            self.dlg.setLayout(layout)
            self.lbc = QLabel(ll['notLicen'])
            self.lbc.setAlignment(Qt.AlignCenter)
            layout.addWidget(self.lbc)
            self.dlg.exec()
        else:
            text = self.sender().text()
            qsriputils.print_rip("tab:", self.sender().text())
            self.m_titleBar.sline.setEnabled(False)
            currentIndex = self.layout.currentIndex()
            qsriputils.print_rip(f"curent: {currentIndex}")
            role_report = ['admin', 'be', 'cdtx', 'vqse']

            if text == ll['wflow']:
                if self.user.userId not in self.user_read:
                    self.user_read.append(self.user.userId)
                    docs = qsriputils.db.collection('chats').document(self.id_doc_new)
                    if self.id_doc_new != '':
                        docs.set({'user_read': self.user_read}, merge=True)
                        self.deselectedimages[5] = "workflowico_deselected.png"

            for (i, t) in enumerate(self.m_titleBar.toolbuttons):
                if t.text() == text:
                    if t.text() == ll['syn'] or t.text() == ll['imp'] or t.text() == ll['exp']:
                        self.m_titleBar.combobox.setHidden(False)
                    else:
                        self.m_titleBar.combobox.setHidden(True)
                    if text == ll['imp'] and self.user.role not in ['admin', 'be']:
                        qsriputils.print_rip('not admin')
                        self.dlg_role()
                    elif text == ll['rep'] and self.user.role not in role_report:
                        self.dlg_role()
                    else:
                        t.setIcon(QtGui.QIcon(f"images/{self.selectedimages[i]}"))
                        t.setChecked(True)
                        if i < 6 and i != currentIndex:
                            self.layout.setCurrentIndex(i)
                        if i == 0:
                            self.m_titleBar.sline.setEnabled(True)
                else:
                    if text != ll['exp']:
                        t.setIcon(QtGui.QIcon(f"images/{self.deselectedimages[i]}"))
                        t.setChecked(False)
            if text == ll['exp']:
                user_allow_export = ['admin', 'be', 'vqse']
                if self.user.role in user_allow_export:
                    tabLayout = self.layout.currentWidget()
                    if str(type(tabLayout)) == "<class 'sys_tab.SysnoptiqueTab'>":
                        press_status = dict()

                        class ExportButton(QtWidgets.QPushButton):
                            def __init__(self, name, press_status, dlg):
                                super(QtWidgets.QPushButton, self).__init__(name)
                                self.name = name.split()[1]
                                self.dlg = dlg
                                self.press_status = press_status
                                self.clicked.connect(self.pressed)

                            def pressed(self):
                                press_status['button'] = self.name
                                self.dlg.accept()

                        self.dlg = QtWidgets.QDialog()
                        self.dlg.resize(100, 100)
                        self.dlg.setWindowTitle(ll['exp'])

                        self.btnExcel = ExportButton(ll['Export_Excel'], press_status, self.dlg)
                        self.btnPDF = ExportButton(ll['Export_PDF'], press_status, self.dlg)
                        self.btnCSV = ExportButton(ll['Export_CSV'], press_status, self.dlg)
                        box = QtWidgets.QVBoxLayout()
                        box.addWidget(self.btnExcel)
                        box.addWidget(self.btnPDF)
                        if hasattr(self.activeProject(), 'csv'):
                            box.addWidget(self.btnCSV)
                        box.setStretch(0, 100)
                        box.setStretch(1, 1)
                        self.dlg.setLayout(box)
                        self.dlg.exec()

                        if 'button' in press_status and press_status['button'] == 'PDF':
                            if hasattr(tabLayout, "export_pdf"):
                                tabLayout.export_pdf()
                        elif 'button' in press_status and press_status['button'] == 'Excel':
                            tabLayout.export_all_excel()
                        elif 'button' in press_status and press_status['button'] == 'CSV':
                            tabLayout.export_csv()
                    elif hasattr(tabLayout, "export_pdf"):
                        tabLayout.export_pdf()
                else:
                    self.dlg = QtWidgets.QDialog()
                    self.dlg.resize(100, 100)
                    self.dlg.setWindowTitle(ll['exp'])
                    self.notEXP = QLabel(ll['notExp'])
                    layout = QVBoxLayout()
                    layout.addWidget(self.notEXP)
                    self.dlg.setLayout(layout)
                    self.dlg.exec()
            if text == ll['wflow']:
                tabLayout = self.layout.currentWidget()
                tabLayout.showListWF()

    def load_all_project(self):
        if self.user.role == 'admin':
            self.projs = qsriputils.select_all_projects_by_user(self.user.userId, env.PATH["projects_path"],
                                                                admin=self.user.role == 'admin')
        else:
            self.projs = qsriputils.select_all_projects_by_user(self.user.userId, env.PATH["projects_path"])

    def login(self):
        global ll
        login = LoginDialog(ll)
        accept = login.exec()
        if accept:
            login.user.last_loggined = datetime.datetime.now()
            conn = qsriputils.db_connect(os.path.join(env.PATH["db_path"], "qsrip.sqlite"))
            if conn:
                u = qsriputils.get_user(login.user.userId)
                if u:
                    qsriputils.update_last_login(login.user.userId, login.user.last_loggined)
                else:
                    qsriputils.create_user(login.user)
                qsriputils.set_config(conn, const.key_loggined, login.user.userId)
                conn.close()
            if self.loaded:
                self.updateUser(login.user)
            self.user = login.user
            user_license = auth_utils.license_check(self.user.userId)
            session.USER = User(self.user.userId, user_license)
            self.is_logout = True
            self.load_all_project()
            if hasattr(self, 'm_titleBar'):
                for (i, t) in enumerate(self.m_titleBar.toolbuttons):
                    if i == 0:
                        t.setIcon(QtGui.QIcon(f"images/{self.selectedimages[i]}"))
                        t.setChecked(True)
                        self.m_titleBar.sline.setEnabled(True)
                    else:
                        t.setIcon(QtGui.QIcon(f"images/{self.deselectedimages[i]}"))
                        t.setChecked(False)
                self.layout.setCurrentIndex(0)
                self.last_selected_tab = 0
                self.setHidden(False)
                return 0
            return 1
        else:
            sys.exit()


if __name__ == "__main__":
    sys.excepthook = system_utils.crash_handle
    if env.APP_MODE == "prod":
        system_utils.detect_vm()

    app = QApplication(sys.argv)

    splash_screen = QSplashScreen(QPixmap(os.path.join(env.PATH["images_path"], "splash.png")))
    splash_screen.show()

    db_utils.connect_firebase()
    db_utils.get_config()

    # temporary assign for backward compatible
    qsriputils.db = session.DB
    qsriputils.bucket = session.BUCKET
    const.errCode = session.CONFIG["errCode"]
    const.project_path = env.PATH["projects_path"]
    const.dbpath = env.PATH["db_path"]
    # temporary assign for backward compatible

    app.setWindowIcon(QIcon(os.path.join(env.PATH["images_path"], "securrip.ico")))
    app.setAttribute(Qt.AA_DisableHighDpiScaling)
    for font in os.listdir(env.PATH["fonts_path"]):
        QFontDatabase.addApplicationFont(os.path.join(env.PATH["fonts_path"], font))

    qgs = QgsApplication([], True)
    qgs.initQgis()

    window = WindowFrame()
    window.showMaximized()

    splash_screen.finish(window)

    app.exec()
