# This Python file uses the following encoding: utf-8
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QVBoxLayout, QWidget
from matplotlib.figure import Figure
import OTDRlib
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import qsriputils


class OTDRTrace(QWidget):
    def __init__(self, raw_trace, *args, **kwargs):
        super(OTDRTrace, self).__init__(*args, **kwargs)
        layout = QVBoxLayout()
        self.raw_trace = raw_trace
        self.canvas = None
        self.setLayout(layout)
        self._draw()

    def _draw(self):
        '''(re)draw the plot with the latest data'''
        keyEvents = self.raw_trace["meta"]["KeyEvents"]
        i = 1
        start_fiber = 0
        end_fiber = 0
        while f"event {i}" in keyEvents:
            event = keyEvents[f"event {i}"]
            qsriputils.print_rip(f"event {i}: {event}")

            refl = float(event["refl loss"])
            splice = float(event["splice loss"])
            if refl < 0 or splice != 0:
                if start_fiber == 0:
                    start_fiber = float(event["distance"])
            if splice == 0 and refl < 0:
                end_fiber = float(event["distance"])
                break
            i += 1
        qsriputils.print_rip(f"start fiber: {start_fiber} end: {end_fiber}")
        fig = Figure()
        plt = fig.add_subplot(1, 1, 1)
        wavelength = self.raw_trace["meta"]["GenParams"]["wavelength"]
        xData = []
        yData = []
        start_fiber = start_fiber * 1000 - 50
        end_fiber = end_fiber * 1000 + 50
        for x, y in zip(self.raw_trace["trace"][1], self.raw_trace["trace"][0]):
            if x * 1000 > start_fiber and x*1000 < end_fiber:
                xData.append(x)
                yData.append(y)
            if x * 1000 >= end_fiber:
                break

        qsriputils.print_rip(f"len:{len(xData)} ")
        plt.plot(xData,
                 yData,
                 label=wavelength,
                 color=OTDRlib.wavelength_to_rgb(wavelength),
                 linewidth=0.5)
        if self.canvas:
            self.canvas.close()

        fig.legend()
        self.canvas = FigureCanvas(fig)
        self.layout().addWidget(self.canvas)
