import os
import time

import pandas as pd
import xlsxwriter
from collections import deque
import numpy as np
import datetime
from PyQt5 import QtWidgets, QtCore, uic, QtGui, QtPrintSupport, QtWebEngineWidgets
from PyQt5.QtCore import Qt, QUrl, QVariant
from PyQt5.Qt import QPushButton, QLabel, QProgressDialog, QCursor, QSize, QImage, QTimer
from PyQt5.QtGui import QFont, QIcon, QPixmap, QPainter, QTextDocument, QColor
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWebKitWidgets import QWebView
# from PyQt5.QWebEngineWidgets import QWebEngineView
from qgis._core import QgsField

import const
from random import randint
from pyOTDR import sorparse
import OTDRlib
from PyQt5.QtWidgets import QTreeWidget, QTextEdit, QTreeWidgetItem, QDialog, QTableWidget, QVBoxLayout, \
    QTableWidgetItem, QHBoxLayout, QTextBrowser

import session
from qsripproject import QSRIPProject, QSRIPIssue, Chat
from qgis.core import QgsApplication, QgsFeature, QgsRasterLayer, QgsVectorLayer, QgsProject, QgsFeatureRequest, \
    QgsExpression, QgsCoordinateReferenceSystem, QgsSymbol, QgsSimpleFillSymbolLayer, QgsRendererCategory, \
    QgsCategorizedSymbolRenderer
from qgis.gui import QgsMapCanvas, QgsMapToolIdentify, QgsMapToolIdentifyFeature

from SROWire import SROWire, LeftWire
from OTDRTrace import OTDRTrace
import qsriputils
import workflow_tab
from firebase_admin import storage, firestore
from firebase_admin.storage import bucket
import resources

from RIPUtils import system_utils

import env

f = open("lang.txt", 'r')
from multilang import english, french
if f.read() == 'en':
    ll = english
else:
    ll = french
f.close()


class IdentifyFeature(QgsMapToolIdentify):
    featureIdentified = QtCore.pyqtSignal(object)

    def __init__(self, canvas, pickMode='all'):
        self.canvas = canvas
        QgsMapToolIdentify.__init__(self, canvas)
        self.setCursor(QCursor())
        if pickMode == 'all':
            self.selectionMode = self.TopDownAll
        elif pickMode == 'selection':
            self.selectionMode = self.LayerSelection
        elif pickMode == 'active':
            self.selectionMode = self.ActiveLayer

    def canvasReleaseEvent(self, mouseEvent):
        '''
        try:
            results = self.identify(mouseEvent.x(), mouseEvent.y(), self.LayerSelection, self.VectorLayer)
        except:
        '''
        results = self.identify(mouseEvent.x(), mouseEvent.y(), self.selectionMode, self.VectorLayer)

        if len(results) > 0:
            qsriputils.print_rip(f"result: {results}")
            self.featureIdentified.emit(results)


class SysnoptiqueTab(QtWidgets.QFrame):
    taskFinished = QtCore.pyqtSignal(int)
    selected_pbo = None

    def reload_projects(self, client):
        projects = []
        # self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(1))
        for p in self.window.projs:
            if p.client == client and p.project not in projects:
                projects.append(p.project)
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(1), p.project)

    def reload_sro(self, client, project):
        sro = []
        # self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(2))
        for p in self.window.projs:
            if p.client == client and p.project == project and p.sro not in sro:
                sro.append(p.sro)
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(2), p.sro)

    def reload_apd(self, client, project, sro):
        apddoe = []
        # self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(3))
        for p in self.window.projs:
            apd = f"{p.type} V{p.ver}"
            if p.client == client and p.project == project and p.sro == sro and apd not in apddoe:
                apddoe.append(apd)
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(3), apd)

    @QtCore.pyqtSlot(QtWidgets.QTreeWidgetItem, int)
    def on_tree_client_changed(self, it, col):
        qsriputils.print_rip("on_tree_client_changed")
        text = it.text(col)
        root = it.parent()
        if root == self.clientTreeWidget.topLevelItem(0):
            qsriputils.print_rip(f"click on client brand: {text}")
            if text != self.selectedClient:
                self.selectedClient = text
                self.__selectTreeItem(self.clientTreeWidget.topLevelItem(0), text)
                self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(1))
                self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(2))
                self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(3))
                self.reload_projects(self.selectedClient)
        elif root == self.clientTreeWidget.topLevelItem(1):
            qsriputils.print_rip(f"click on project brand: {text}")
            if text != self.selectedProject:
                self.selectedProject = text
                self.__selectTreeItem(self.clientTreeWidget.topLevelItem(1), text)
                self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(2))
                self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(3))
                self.reload_sro(self.selectedClient, self.selectedProject)
        elif root == self.clientTreeWidget.topLevelItem(2):
            qsriputils.print_rip(f"click on sro brand: {text}")
            if text != self.selectedSRO:
                self.selectedSRO = text
                self.__selectTreeItem(self.clientTreeWidget.topLevelItem(2), text)
                self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(3))
                self.reload_apd(self.selectedClient, self.selectedProject, self.selectedSRO)
        elif root == self.clientTreeWidget.topLevelItem(3):
            qsriputils.print_rip(f"click on apd brand:{text}")
            apd = f"{self.proj.type} V{self.proj.ver}"
            if self.selectedClient != self.proj.client or self.selectedProject != self.proj.project or self.selectedSRO != self.proj.sro or text != apd:
                self.__selectTreeItem(self.clientTreeWidget.topLevelItem(3), text)
                p = f"{self.selectedProject} {self.selectedSRO} {text}"
                self.require_update_menu = False
                self.window.m_titleBar.combobox.setCurrentText(p)

    def on_tree_status_changed(self):
        qsriputils.print_rip("on_tree_status_changed")

    def download_file(self):
        file = self.sender().text()
        qsriputils.print_rip(f"file: {file}")
        options = QtWidgets.QFileDialog.Options()
        # options |= QtWidgets.QFileDialog.DontUseNativeDialog
        options |= QtWidgets.QFileDialog.ShowDirsOnly
        path = QtWidgets.QFileDialog.getExistingDirectory(
            None,
            ll['Select_Directory'],
            "/home",
            options=options)
        if path:
            try:
                qsriputils.save_file(file, path)
            except Exception as e:
                qsriputils.showMessage(e)

    def findsor(self, name):
        for root, dirs, files in os.walk(self.proj.sor_path):
            if name in files:
                return os.path.join(root, name)

    def on_change_sor(self, index):
        qsriputils.print_rip(f"on sor: {index}")
        sor_err = self.sor_quality[index]
        sor = sor_err["error"][0]
        spath = self.findsor(sor)
        if spath:
            _, d_meta, l_raw_trace = sorparse(spath)
            a_trace = self.__preprocess_data(d_meta, l_raw_trace)
            self.raw_trace = OTDRlib.prepare_data({"meta": d_meta, "trace": a_trace}, self.window_len)
            trace = OTDRTrace(self.raw_trace)
            qsriputils.clearLayout(self.mapLayout)
            self.mapLayout.addWidget(trace)
        max_err = sor_err["error"][1]
        sor_err["error"][1] = f"Number of issues: {max_err}"
        text = "<br/>".join(sor_err["error"])
        br_index = text.rindex("<br/>")
        output_line = text[:br_index]
        mqsText = '<b>Synthèse MQS:</b><br/>'
        if len(sor_err["mqs"]) == 0:
            mqsText += "Sor and QGIS are identical"
        else:
            mqsText += sor_err["mqs"]
        # if "444" in sor_err["error"][0]:
        #     mqsText += "<br/>Contrainte sur section 6 à 449m"
        # elif "555" in sor_err["error"][0]:
        #     mqsText += "<br/>Evénement positif sur section 4 à 3622m"
        # elif "666" in sor_err["error"][0]:
        #     mqsText += "<br/>Fibre coupé sur section 6 à 1008m"
        # else:
        #     mqsText += text[br_index:]
        #     #
        #     # text += f"<br/><b>Synthèse MQS:</b>"
        #     mqsText += f"<br/>qgis:{self.qgis_length}"
        #     ss = sor_err["error"][-1][4:]
        #     sorl = ss.split(",")
        #     sl = [int(i) for i in sorl]
        #     if len(sl) == len(self.qgis_length):
        #         total_diff = 0
        #         diff_index = []
        #         jj = 0
        #         for s, q in zip(sl, self.qgis_length):
        #             if abs(s - q) > 1:
        #                 total_diff += 1
        #                 diff_index.append(jj)
        #             jj += 1
        #         if total_diff == 0:
        #             mqsText += "<br/>SOR and QGIS is same"
        #         elif total_diff == 1:
        #             mqsText += f"<br/>Reserve error on section {diff_index[0]}"
        #         elif total_diff == 2:
        #             if abs(sl[diff_index[0]] + sl[diff_index[1]] - self.qgis_length[diff_index[0]] - self.qgis_length[
        #                 diff_index[1]]) <= 2:
        #                 mqsText += f"<br/>PBO location is error in section {diff_index[0]}"
        #             else:
        #                 mqsText += f"<br/>{const.sorError[2]}"
        #         else:
        #             mqsText += f"<br/>{const.sorError[2]}"
        #     else:
        #         mqsText += f"<br/>{const.sorError[3]}"
        self.sorContent.setText(output_line)
        self.sorContent.setTextFormat(Qt.AutoText)
        self.mqsLabel.setText(mqsText)
        self.mqsLabel.setTextFormat(Qt.AutoText)

    def on_project_changed(self):
        qsriputils.print_rip("sys tab: on_project_changed")
        self.update_type_tree(self.window.activeProject())
        self.proj = self.window.activeProject()
        self.p_user = qsriputils.get_user(self.proj.user_id)

        if not self.isHidden():
            self.__drawProject()
        else:
            self.pendingDraw = True

    def on_change_vsqe_status(self, index):
        qsriputils.print_rip(f"on_change_vsqe_status: {index}")
        if self.trigger_on_change_vsqe:
            name = self.nodeNameButton.text()
            if name.endswith(" \uE70F"):
                name = name.replace(" \uE70F", "")
            issues_ = []
            pbo = self.getPBOWithName(name)
            vqse = {"vqse1": 0, "vqse2": 0, "vqse3": 0}

            if pbo:
                issues_.extend(self.get_issue(name))
                pbo.vsqe = index
            else:
                c = self.__getCableNode(name)
                if c:
                    c.vsqe = index
                    for i in range(self.graphLayout.count()):
                        w = self.graphLayout.itemAt(i).widget()
                        if isinstance(w, SROWire) and name in w.cable:
                            w.setVQSE(index)
                            break
                    issues_.extend(self.get_issue(c.dest, issue_type="sor"))
            # if len(issues_) > 0:
            if pbo:
                qsriputils.update_pbo(pbo)
            else:
                qsriputils.update_cable(c)
            for cable in self.proj.cable:
                if cable.vsqe > 0:
                    vqse[f"vqse{cable.vsqe}"] += 1
            for pp in self.proj.pbo:
                if pp.vsqe > 0:
                    vqse[f"vqse{pp.vsqe}"] += 1
            qsriputils.update_project_data(self.proj, vqse)
            vqse = qsriputils.get_last_vqse(name, self.proj.sro, self.proj.type, self.proj.ver)

            if index == 2 and (vqse is None or vqse.status == 3):
                dlg = QDialog()
                dlg.resize(300, 300)
                dlg.setWindowTitle("VQSE")

                textEdit = QTextEdit()
                box = QVBoxLayout()
                box.addWidget(textEdit)
                btnOk = QPushButton("Save")
                btnOk.clicked.connect(dlg.accept)
                box.addWidget(btnOk)
                box.setStretch(0, 100)
                box.setStretch(1, 1)
                dlg.setLayout(box)
                accept = dlg.exec()
                qsriputils.print_rip(f"accept: {accept}")
                if accept == 1:
                    issue = QSRIPIssue(pbo=name, code="VQSE Error",
                                       text=textEdit.toPlainText())
                    issue.level = 2
                    issue.issue_type = "vqse"
                    issue.status = index
                    issue.fromProject(p=self.proj, u=self.window.user)
                    self.proj.issues.append(issue)
                    qsriputils.create_issues(issues=[issue], proj_id=self.proj._id, total=1, ok=0,
                                             client=self.proj.client)
            else:
                vqse = qsriputils.get_last_vqse(name, self.proj.sro, self.proj.type, self.proj.ver)
                qsriputils.print_rip(f"vqse:{vqse}")
                if vqse:
                    vqse.status = index
                    qsriputils.update_issue(vqse)
            # elif index == 3:
            #     print(f"index:{index}")
            #     if len(issues_) > 0:
            #         self.issueBox.setCurrentIndex(4)

    def on_change_issue_status(self, index):
        if self.trigger_on_change_status and self.window.user.role == 'vqse' and index != 0:
            dlg = QDialog()
            dlg.resize(100, 100)
            dlg.setWindowTitle('Issue')
            layout = QVBoxLayout()
            layout.addWidget(QLabel('VQSE only allow to choose A traiter'))
            dlg.setLayout(layout)
            dlg.exec()
        elif self.trigger_on_change_status:
            qsriputils.print_rip(f"on_change_issue_status: {index}")
            level = 0

            log = ""
            if index != 0:
                dlg = QDialog()
                dlg.resize(300, 300)
                dlg.setWindowTitle("Issue")
                textEdit = QTextEdit()
                box = QVBoxLayout()
                box.addWidget(textEdit)
                btnOk = QPushButton("Save")
                btnOk.clicked.connect(dlg.accept)
                box.addWidget(btnOk)
                box.setStretch(0, 100)
                box.setStretch(1, 1)
                dlg.setLayout(box)
                accept = dlg.exec()
                log = textEdit.toPlainText()

            now = datetime.datetime.now()
            if self.selected_pbo is not None:
                pbo = self.nodeNameButton.text()
                if pbo.endswith(" \uE70F"):
                    pbo = pbo.replace(" \uE70F", "")
                issue = self.get_issue(pbo)
                level = 0
                if len(issue) > 0:
                    if isinstance(self.selected_pbo, QPushButton):
                        self.selected_pbo.setText(f"{pbo} \uE70F" if index > 0 else pbo)
                    self.nodeNameButton.setText(f"{pbo} \uE70F" if index > 0 else pbo)
                    self.labelCloseDate.setText(f"{now.strftime('%d-%m-%Y')}")
                    for i in issue:
                        i.status = index
                        if level < i.level:
                            level = i.level
                        i.last_update.append(
                            {"date": now, "etat": index, "user": self.window.user.displayName, "log": log})
                        qsriputils.update_issue(i)
            else:
                c = self.nodeNameButton.text()
                if c.endswith(" \uE70F"):
                    c = c.replace(" \uE70F", "")
                cable = self.__getCableNode(c)
                if cable and (index > 2 or index == 0):
                    pbo = cable.dest
                    issue = self.get_issue(pbo, issue_type="sor")
                    level = 0
                    if len(issue) > 0:
                        self.nodeNameButton.setText(f"{c} \uE70F" if index > 0 else c)
                        self.labelCloseDate.setText(f"{now.strftime('%d-%m-%Y')}")

                        for i in issue:
                            i.status = index
                            if level < i.level:
                                level = i.level
                            i.last_update.append({"date": now, "etat": index, "user": self.window.user.displayName})
                            qsriputils.update_issue(i)
                    for i in range(self.graphLayout.count()):
                        w = self.graphLayout.itemAt(i).widget()
                        if isinstance(w, SROWire) and cable.name in w.cable:
                            w.setEditted(index > 2)
                            break

            if index > 2:
                self.nodeNameButton.setStyleSheet(
                    'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                )
                if self.selected_pbo and isinstance(self.selected_pbo, QPushButton):
                    self.selected_pbo.setStyleSheet(
                        'QPushButton {background: rgb(50,166,27);margin-top:5px; margin-bottom:5px ;padding:4px 10px 4px 10px; color: white; border:none; border-radius: 3px;} '
                        'QPushButton:checked {border: 3px solid rgb(46,46,46) } '
                    )

            else:
                if level == 1:
                    self.nodeNameButton.setStyleSheet(
                        'QPushButton {background: rgb(235,125,60); color: white; border:none; border-radius: 3px; padding: 3px} '
                    )
                    if self.selected_pbo and isinstance(self.selected_pbo, QPushButton):
                        self.selected_pbo.setStyleSheet(
                            'QPushButton {background: rgb(235,125,60);margin-top:5px; margin-bottom:5px ;padding:4px 10px 4px 10px; color: white; border:none; border-radius: 3px;} '
                            'QPushButton:checked {border: 3px solid rgb(46,46,46) } '
                        )
                elif level == 2:
                    self.nodeNameButton.setStyleSheet(
                        'QPushButton {background: rgb(252,13,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                    )
                    if self.selected_pbo and isinstance(self.selected_pbo, QPushButton):
                        self.selected_pbo.setStyleSheet(
                            'QPushButton {background: rgb(252,13,27);margin-top:5px; margin-bottom:5px ;padding:4px 10px 4px 10px; color: white; border:none; border-radius: 3px;} '
                            'QPushButton:checked {border: 3px solid rgb(46,46,46) } '
                        )

    def on_change_assignee(self, index):
        if self.trigger_on_change_assignee:
            qsriputils.print_rip(f"on_change_assignee: {index}")
            if index > 0:
                tech = self.window.techniciens[index - 1]
                pbo = self.nodeNameButton.text()
                if pbo.endswith(" \uE70F"):
                    pbo = pbo.replace(" \uE70F", "")

                issue = self.get_issue(pbo)
                if len(issue) == 0:
                    qsriputils.print_rip("get all cable issue")
                    c = self.__getCableNode(pbo)
                    if c:
                        issue = self.get_issue(c.dest, issue_type="sor")
                        qsriputils.print_rip(f"total {len(issue)} sor")
                if len(issue) > 0:

                    for i in issue:
                        qsriputils.print_rip(f"update issue: {i.id}")
                        i.assignee_id = tech.userId
                        i.assignee_name = tech.displayName
                        qsriputils.update_issue(i)

    projs = []
    proj: QSRIPProject = None
    cableLayer: QgsVectorLayer = None
    boiteLayer: QgsVectorLayer = None
    window = None
    pendingDraw = True
    p_user = None

    def __init__(self, window=None):
        super(SysnoptiqueTab, self).__init__()
        uic.loadUi('ui/sys.ui', self)

        self.labelCreateDatetitle.setText(ll['Date_Creation'])
        self.labelCloseDatetitle.setText(ll['Dernier_Event'])
        self.label_3.setText(ll['Resume_date'])
        self.label_6.setText(ll['List_Details'])
        self.label_9.setText(ll['Comment'])
        self.label_2.setText(ll['Source_data_files'])
        self.buttonReload.setText(ll['Reload'])
        self.label.setText(ll['Filters'])
        self.btnSwitchView.setText(ll['view_map'])
        self.vqseBox.clear()
        self.vqseBox.addItems(ll['VQSE_error'])
        self.issueBox.clear()
        self.issueBox.addItems(ll['etat_list'])
        self.CSVLabel.setText(ll['Epissu'])
        self.EtatBox.addItems(ll['EtatBox'])
        self.FilterLabel.setText(ll['FilterLabel'])
        self.ResultLabel.setText(ll['ResultLabel'])
        self.ValidButton.setText(ll['Valid'])

        self.View3DButton.setHidden(True)
        self.ViewBIMButton.setHidden(True)
        self.View3DButton.clicked.connect(self.view_3d)
        self.ViewBIMButton.clicked.connect(self.view_bim)

        self.window = window
        self.projs = window.projs
        self.treeOfFeatures = {}
        self.require_update_menu = True
        self.sor_quality = []
        self.timer = None
        self.qgis_length = []
        self.icons = ["blueshield.png", "redshield.png", "greenshield.png", "bluecapture.png", "redcapture.png",
                      "greencapture.png"]
        self.selectedClient = ""
        self.selectedProject = ""
        self.selectedSRO = ""
        self.renderedMap = False
        self.leafCount = {}
        self.sorpbo = []
        self.projects = []
        self.clients = []
        self.sros = []
        self.apddoe = []
        self.window_len = 0
        self.trigger_on_change_status = False
        self.trigger_on_change_assignee = False
        self.trigger_on_change_vsqe = False
        self.mapSysFrame.setHidden(True)
        self.View3DFrame.setHidden(True)
        self.raw_trace = None
        self.mapTool = None
        self.btnSwitchView.clicked.connect(self.switch_view)
        self.buttonReload.clicked.connect(self.reload_sys)
        self.historyButton.clicked.connect(self.show_history)
        path = os.getcwd()
        self.historyButton.setIcon(QIcon(os.path.join(path, "images", "hisico.jpeg")))
        self.historyButton.setIconSize(QSize(35, 35))
        self.nodeNameButton.setFont(QFont('Segoe MDL2 Assets', 9))
        self.nodeNameButton.clicked.connect(self.__show_image)
        self.showAttach = False
        self.attach_pbo = None
        self.icons_button = []
        self.selectedIcon = ['workflowico.png', 'icon_attach']
        self.deselectedIcon = ['workflowico_deselected.png', 'icon_attach_deselect.png']
        layout_icon = QHBoxLayout()
        self.icon_box.setLayout(layout_icon)
        icon_comment = self.iconButton('comment', 'images/workflowico.png')
        icon_attach = self.iconButton('attach', 'images/icon_attach_deselect.png')
        layout_icon.addWidget(icon_comment)
        layout_icon.addWidget(icon_attach)
        self.icons_button = [icon_comment, icon_attach]
        self.socialRootItem = QTreeWidgetItem([ll['soci']])
        self.canvas = None
        self.socialRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.clientTreeWidget.addTopLevelItem(self.socialRootItem)
        self.socialRootItem.setExpanded(True)
        self.projectRootItem = QTreeWidgetItem([ll['project']])
        self.projectRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.clientTreeWidget.addTopLevelItem(self.projectRootItem)
        self.projectRootItem.setExpanded(True)
        # statusRootItem = QTreeWidgetItem(["» Statut"])
        # statusRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        # self.statusTreeWidget.addTopLevelItem(statusRootItem)
        sroRootItem = QTreeWidgetItem(["» SRO"])
        sroRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.clientTreeWidget.addTopLevelItem(sroRootItem)
        sroRootItem.setExpanded(True)
        self.typeRootItem = QTreeWidgetItem([ll['ver_max']])
        self.typeRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.clientTreeWidget.addTopLevelItem(self.typeRootItem)
        self.typeRootItem.setExpanded(True)
        self.clientTreeWidget.itemClicked.connect(self.on_tree_client_changed)

        self.assigneeBox.currentIndexChanged.connect(self.on_change_assignee)
        self.vqseBox.currentIndexChanged.connect(self.on_change_vsqe_status)
        self.sorBox.currentIndexChanged.connect(self.on_change_sor)
        self.issueBox.currentIndexChanged.connect(self.on_change_issue_status)

        self.sendButton.setIcon(QIcon("./images/sendicon.png"))
        self.sendButton.setIconSize(QSize(30, 20))
        self.sendButton.clicked.connect(self.sendComment)

        self.sendAttachButton.setIcon(QIcon("./images/attachicon.png"))
        self.sendAttachButton.setIconSize(QSize(30, 20))
        self.sendAttachButton.clicked.connect(self.sendAttach)

        self.textBrowser.setOpenLinks(False)
        self.textBrowser.anchorClicked.connect(self.downloadAttach)

        self.detailFrame.setHidden(True)
        self.proj = self.window.activeProject()
        if self.proj:
            self.selectedClient = self.proj.client
            self.selectedProject = self.proj.project
            self.selectedSRO = self.proj.sro
            self.p_user = qsriputils.get_user(self.proj.user_id)
            self.update_left_menu()
            self.icon_new = None

        self.check_user_role()
        self.CodeCasBox.currentIndexChanged.connect(self.epissFilterChange)
        self.FaceCasBox.currentIndexChanged.connect(self.epissFilterChange)
        self.PosCasBox.currentIndexChanged.connect(self.epissFilterChange)
        self.ValidButton.clicked.connect(self.updateResult)

    def check_user_role(self):
        user_allow_reload = ['admin', 'be', 'cdtx', 'vqse']
        user_allow_read_vqse = ['be', 'cdtx', 'tech', 'stt']
        user_allow_change_vqse = ['admin', 'vqse']
        user_allow_change_issue = ['admin', 'be', 'cdtx', 'vqse']
        if self.window.user.role in user_allow_reload:
            self.buttonReload.setHidden(False)
        else:
            self.buttonReload.setHidden(True)
        if self.window.user.role in user_allow_read_vqse:
            self.vqseBox.setDisabled(True)
        elif self.window.user.role in user_allow_change_vqse:
            self.vqseBox.setDisabled(False)
        else:
            self.vqseBox.setHidden(True)
        if self.window.user.role in user_allow_change_issue:
            self.issueBox.setDisabled(False)
        else:
            self.issueBox.setDisabled(True)
    def updateText(self, lang):
        global ll
        ll = lang
        self.labelCreateDatetitle.setText(ll['Date_Creation'])
        self.labelCloseDatetitle.setText(ll['Dernier_Event'])
        self.label_3.setText(ll['Resume_date'])
        self.label_6.setText(ll['List_Details'])
        self.label_9.setText(ll['Comment'])
        self.label_2.setText(ll['Source_data_files'])
        self.buttonReload.setText(ll['Reload'])
        self.label.setText(ll['Filters'])
        self.btnSwitchView.setText(ll['view_map'])\


        self.vqseBox.disconnect()
        index = self.vqseBox.currentIndex()
        self.vqseBox.clear()
        self.vqseBox.addItems(ll['VQSE_error'])
        self.vqseBox.setCurrentIndex(index)
        self.vqseBox.currentIndexChanged.connect(self.on_change_vsqe_status)

        self.issueBox.disconnect()
        self.issueBox.clear()
        self.issueBox.addItems(ll['etat_list'])
        self.issueBox.currentIndexChanged.connect(self.on_change_issue_status)

        self.CSVLabel.setText(ll['Epissu'])

        indexEtat = self.EtatBox.currentIndex()
        self.EtatBox.clear()
        self.EtatBox.addItems(ll['EtatBox'])
        self.EtatBox.setCurrentIndex(indexEtat)

        if self.window.user.role != "admin":
            self.assigneeBox.setHidden(True)
        else:
            self.assigneeBox.setHidden(False)
            self.assigneeBox.disconnect()
            indexassign = self.assigneeBox.currentIndex()
            self.assigneeBox.clear()
            self.assigneeBox.addItem(ll['assign_to'])
            self.assigneeBox.addItems([f"{t.displayName}" for t in self.window.techniciens])
            self.assigneeBox.setCurrentIndex(indexassign)
            self.assigneeBox.currentIndexChanged.connect(self.on_change_assignee)

        self.FilterLabel.setText(ll['FilterLabel'])
        self.ResultLabel.setText(ll['ResultLabel'])
        self.ValidButton.setText(ll['Valid'])
        self.socialRootItem.setText(0, ll['soci'])
        self.projectRootItem.setText(0, ll['project'])
        self.typeRootItem.setText(0, ll['ver_max'])
        # self.btnSwitchView.setText(ll['View_Syn'])

    def show_history(self):
        name = self.nodeNameButton.text()
        if name.endswith(" \uE70F"):
            name = name.replace(" \uE70F", "")
        issues_ = []
        if self.selected_pbo:
            issues_.extend(self.get_issue(name))
        else:
            c = self.__getCableNode(name)
            if c:
                issues_.extend(self.get_issue(c.dest, issue_type="sor"))
        if len(issues_) > 0:
            # print(issues_[0].last_update)
            history = issues_[0].last_update
            dlg = QDialog()
            dlg.resize(500, 300)
            dlg.setWindowTitle(ll['Modify_History'])

            tableWidget = QTableWidget()
            box = QVBoxLayout()
            box.addWidget(tableWidget)
            tableWidget.setColumnCount(5)
            tableWidget.verticalHeader().setVisible(False)
            tableWidget.setHorizontalHeaderLabels(ll['history_table'])
            tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
            # def on_item_clicked(item):
            #     row = tableWidget.currentRow()
            #     name = tableWidget.item(row,0).text()
            #     nom = tableWidget.item(row,1).text()
            #     print(f"layer: {name} feature: {nom}")
            #     dlg.close()
            #     self.featureSelected(nom, "BOITE" in name)
            # tableWidget.itemClicked.connect(on_item_clicked)
            i = 0
            tableWidget.setRowCount(len(history))
            for h in history[::-1]:
                if isinstance(h, dict):
                    d = h["date"]
                    tableWidget.setItem(i, 0, QTableWidgetItem(ll['etat_list'][h["etat"]]))
                    tableWidget.setItem(i, 3, QTableWidgetItem(h["user"]))
                    if "log" in h:
                        tableWidget.setItem(i, 4, QTableWidgetItem(h["log"]))
                    else:
                        tableWidget.setItem(i, 4, QTableWidgetItem(""))
                else:
                    d = h
                tableWidget.setItem(i, 1, QTableWidgetItem(d.strftime('%d-%m-%Y')))
                tableWidget.setItem(i, 2, QTableWidgetItem(d.strftime('%H:%M:%S')))
                i += 1

            dlg.setLayout(box)
            dlg.exec()

    def select_cable_feature(self, selected):
        qsriputils.print_rip(f"selected: {selected}")

    def select_feature(self, selected):
        qsriputils.print_rip(f"canvas selected: {selected}")

    def featureSelected(self, nom, pbo=True):
        if not pbo:
            self.select_cable_node_(nom)
        else:
            self.select_pbo_node_(nom)

    def onFeatureIdentified(self, results):
        layers = []
        for result in results:
            if "CABLE" in result.mLayer.name() or "BOITE" in result.mLayer.name():
                layers.append(result)
        if len(layers) > 1:
            dlg = QDialog()
            dlg.resize(400, 300)
            dlg.setWindowTitle(ll["Select_Feature"])

            tableWidget = QTableWidget()
            box = QVBoxLayout()
            box.addWidget(tableWidget)
            tableWidget.setColumnCount(2)
            tableWidget.verticalHeader().setVisible(False)
            tableWidget.setHorizontalHeaderLabels([ll["Layer"], ll["Name"]])
            tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

            def on_item_clicked(item):
                row = tableWidget.currentRow()
                name = tableWidget.item(row, 0).text()
                nom = tableWidget.item(row, 1).text()
                qsriputils.print_rip(f"layer: {name} feature: {nom}")
                dlg.close()
                self.featureSelected(nom, "BOITE" in name)

            tableWidget.itemClicked.connect(on_item_clicked)
            i = 0
            tableWidget.setRowCount(len(layers))
            for layer in layers:
                tableWidget.setItem(i, 0, QTableWidgetItem(layer.mLayer.name()))
                tableWidget.setItem(i, 1, QTableWidgetItem(
                    layer.mFeature["Nom"]))
                i += 1

            dlg.setLayout(box)
            dlg.exec()


        elif len(layers) == 1:
            r = layers[0]
            if "CABLE" in r.mLayer.name():
                self.select_cable_node_(r.mFeature["Nom"])
            else:
                self.select_pbo_node_(r.mFeature["Nom"])
        # print(f"layer name: {layer.name()}")
        # nom = feature['Nom']
        # print(f"onFeatureIdentified: {nom}")
        # # self.select_pbo_node_(nom)

    def switch_view(self):
        qsriputils.print_rip("switch view")
        if self.mapSysFrame.isHidden():
            self.mapSysFrame.setHidden(False)
            self.mapSysGraph.setHidden(True)
            self.View3DFrame.setHidden(True)
            self.btnSwitchView.setText(ll['View_Syn'])
            if not self.renderedMap and self.proj != None:
                qsriputils.clearLayout(self.mapSysLayout)
                self.cableLayer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.cable_layer)[0])[0]
                self.boiteLayer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.boite_layer)[0])[0]

                # add field
                # if 'ISSUE' not in self.boiteLayer.fields().names():
                #     self.boiteLayer.dataProvider().addAttributes([QgsField("ISSUE", QVariant.Double)])
                #     self.boiteLayer.updateFields()
                #     self.boiteLayer.startEditing()
                #     attr_index = len(self.boiteLayer.fields().names()) - 1
                #     for feature in self.boiteLayer.getFeatures():
                #         name_pbo = feature.attribute('NOM')
                #         id = feature.id()
                #         for issue in self.proj.issues:
                #             if issue.pbo == name_pbo:
                #                 attr_value = {attr_index: issue.level}
                #                 self.boiteLayer.dataProvider().changeAttributeValues({id: attr_value})
                #     self.boiteLayer.commitChanges()

                fni = self.boiteLayer.fieldConstraints(1)
                unique_values = self.boiteLayer.dataProvider().uniqueValues(fni)
                categories = []
                for unique_value in unique_values:
                    layer_style = {}
                    layer_style['outline'] = '#000000'
                    symbol = QgsSymbol.defaultSymbol(self.boiteLayer.geometryType())
                    symbol_layer = QgsSimpleFillSymbolLayer.create(layer_style)
                    if symbol_layer:
                        symbol.changeSymbolLayer(0, symbol_layer)
                    category = QgsRendererCategory(unique_value, symbol, str(unique_value))
                    for issue in self.proj.issues:
                        if issue.pbo == unique_value:
                            level = issue.level
                            if level == 1:
                                category.symbol().setColor(QColor('orange'))
                            elif level == 2:
                                category.symbol().setColor(QColor('red'))
                            else:
                                category.symbol().setColor(QColor('#00FF00'))
                    category.symbol().setOpacity(100)
                    category.symbol().setSize(3)
                    categories.append(category)
                renderer = QgsCategorizedSymbolRenderer('NOM', categories)
                if renderer is not None:
                    self.boiteLayer.setRenderer(renderer)

                self.canvas = QgsMapCanvas()
                self.canvas.setMouseTracking(True)
                self.canvas.setCanvasColor(Qt.white)
                self.canvas.enableAntiAliasing(True)
                self.canvas.enableMapTileRendering(True)
                layers = []
                # crs = QgsCoordinateReferenceSystem("EPSG:3857")
                layers.append(self.cableLayer)
                layers.append(self.boiteLayer)
                # self.cableLayer.setCrs(crs)
                # self.boiteLayer.setCrs(crs)

                for root, dirs, files in os.walk(self.proj.qgis_path):
                    for file in files:
                        if "POINT_TECHNIQUE" in file and file.endswith(".shp"):
                            ptfile = os.path.join(root, file)
                            qsriputils.print_rip(f"pt: {ptfile}")
                            ptLayer = QgsVectorLayer(ptfile, "PtLayer")
                            # ptLayer.setCrs(crs)
                            QgsProject.instance().addMapLayer(ptLayer)
                            layers.append(ptLayer)
                        elif "SITE" in file and file.endswith(".shp"):
                            sfile = os.path.join(root, file)
                            qsriputils.print_rip(f"site: {sfile}")
                            sLayer = QgsVectorLayer(sfile, "SiteLayer")
                            # ptLayer.setCrs(crs)
                            QgsProject.instance().addMapLayer(sLayer)
                            layers.append(sLayer)

                urlWithParams = 'crs=EPSG:3857&type=xyz&url=http://tile.openstreetmap.org/%7Bz%7D/%7Bx%7D/%7By%7D.png&zmax=19&zmin=0'
                rlayer = QgsRasterLayer(urlWithParams, 'OpenStreetMap', 'wms')

                if rlayer.isValid():

                    QgsProject.instance().addMapLayer(rlayer)
                    layers.append(rlayer)
                    # print("r extent:", rlayer.extent())

                else:
                    qsriputils.print_rip('invalid layer')

                qsriputils.print_rip("boite extent:", self.boiteLayer.extent())
                QgsProject.instance().addMapLayer(self.boiteLayer)
                self.canvas.setLayers(layers)
                # self.boiteLayer.selectionChanged.connect(self.select_pbo_feature)
                # self.cableLayer.selectionChanged.connect(self.select_cable_feature)
                # self.canvas.selectionChanged.connect(self.select_feature)
                self.mapSysLayout.addWidget(self.canvas)
                box = self.boiteLayer.extent()
                box.set(box.xMinimum() - 50, box.yMinimum() - 50, box.xMaximum() + 50, box.yMaximum() + 50)
                self.canvas.setEnabled(True)
                self.canvas.setExtent(self.boiteLayer.extent())
                crs = QgsCoordinateReferenceSystem("EPSG:2154")
                self.canvas.setDestinationCrs(crs)
                self.canvas.update()
                rlayer = QgsProject.instance().mapLayersByName('OpenStreetMap')[0]
                rlayer.triggerRepaint()
                self.mapTool = IdentifyFeature(self.canvas)
                self.mapTool.featureIdentified.connect(self.onFeatureIdentified)
                self.canvas.setMapTool(self.mapTool)
            self.renderedMap = True

        else:
            self.mapSysFrame.setHidden(True)
            self.mapSysGraph.setHidden(False)
            self.View3DFrame.setHidden(True)
            self.btnSwitchView.setText(ll['view_map'])

    def onMousePressEvent(self, event):
        qsriputils.print_rip(f"onMousePressEvent:{event.x(), event.y()}")

    def request_reload(self):
        self.pendingDraw = True

    def on_pbo_activated(self, pbo):
        qsriputils.print_rip(f"on_pbo_activated: {pbo}")
        for i in range(self.graphLayout.count()):
            w = self.graphLayout.itemAt(i).widget()
            if isinstance(w, QPushButton):
                if pbo in w.text():
                    w.animateClick()
                    break
        if not self.mapSysFrame.isHidden():
            qsriputils.print_rip(f"expr: Nom='{pbo}'")
            self.boiteLayer.selectByExpression(f"Nom='{pbo}'", QgsVectorLayer.SetSelection)
            self.canvas.zoomToSelected(self.boiteLayer)
            self.canvas.refresh()

    def hideEvent(self, a0: QtGui.QHideEvent) -> None:
        qsriputils.print_rip("sysnoptique now hidden")
        # self.pendingDraw = True

    def showEvent(self, a0: QtGui.QShowEvent) -> None:
        qsriputils.print_rip("sysnoptique now show")
        if self.window.is_logout:
            self.window.is_logout = False
            self.selectedClient = ''
            self.selectedProject = ''
            self.selectedSRO = ''
            self.proj = None
            self.check_user_role()
            self.reload_sys()
        elif self.window.activeProject() and self.proj != self.window.activeProject():
            self.proj = self.window.activeProject()
            self.__drawProject()
        elif self.pendingDraw and self.proj:
            self.__drawProject()
        self.pendingDraw = False

    def reload_sys(self):
        self.showProgress(ll['reload_pro'])
        self.taskFinished.connect(self.taskDone)
        self.clients = []
        self.projects = []
        self.sros = []
        self.apddoe = []
        proj_deleted = True
        self.window.projs = qsriputils.select_all_projects_by_user(self.window.user.userId, const.project_path,
                                                                   admin=self.window.user.role == 'admin')
        for proj in self.window.projs:
            if self.proj is not None and proj._id == self.proj._id:
                proj_deleted = False
            if hasattr(proj, 'client') and proj.client not in self.clients:
                self.clients.append(proj.client)
        qsriputils.print_rip('Projects: ', len(self.window.projs))
        self.window.m_titleBar.combobox.clear()
        self.window.m_titleBar.combobox.addItem(ll['selecPro'])
        self.window.m_titleBar.combobox.addItems([f"{p.project} {p.sro} {p.type} V{p.ver}" for p in self.window.projs])
        for i in range(4):
            self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(i))
        for client in self.clients:
            self.__addTreeItem(self.clientTreeWidget.topLevelItem(0), client)
        if proj_deleted:
            if len(self.window.projs) > 0:
                import copy
                client_deleted = True
                for proj in self.window.projs:
                    if self.proj is None or self.proj.client == proj.client:
                        self.proj = copy.copy(proj)
                        client_deleted = False
                        break
                if client_deleted:
                    self.proj = copy.copy(self.window.projs[0])
                self.selectedClient = self.proj.client
                self.selectedProject = self.proj.project
                self.selectedSRO = self.proj.sro
                self.update_left_menu()
                self.__selectTreeItem(self.clientTreeWidget.topLevelItem(0), self.proj.client)
                self.window.m_titleBar.combobox.setCurrentText(
                    f"{self.proj.project} {self.proj.sro} {self.proj.type} V{self.proj.ver}")
            else:
                self.proj = None
                self.detailFrame.setHidden(True)
                qsriputils.clearLayout(self.graphLayout)
                if self.window.user.role != "admin":
                    self.assigneeBox.setHidden(True)
                else:
                    self.assigneeBox.setHidden(False)
                    self.assigneeBox.clear()
                    self.assigneeBox.addItem(ll['assign_to'])
                    self.assigneeBox.addItems([f"{t.displayName}" for t in self.window.techniciens])
        else:
            self.selectedClient = self.proj.client
            self.selectedProject = self.proj.project
            self.selectedSRO = self.proj.sro
            self.update_left_menu()
            self.__selectTreeItem(self.clientTreeWidget.topLevelItem(0), self.proj.client)
            self.window.m_titleBar.combobox.setCurrentText(
                f"{self.proj.project} {self.proj.sro} {self.proj.type} V{self.proj.ver}")

        import threading
        ex_thread = threading.Thread(target=self.reload_project, args=(self.proj,))
        ex_thread.start()

    def reload_project(self, p):
        if p is not None:
            p.cable.clear()
            p.pbo.clear()
            p.issues.clear()
            qsriputils.load_project_data(p)
            p.issues = qsriputils.get_all_issues_in_project(self.proj)
            self.taskFinished.emit(1)
        else:
            self.taskFinished.emit(0)

    def update_left_menu(self):
        for p in self.window.projs:
            if p.project not in self.projects and p.client == self.proj.client:
                self.projects.append(p.project)
                # set project
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(1), p.project, p.project == self.proj.project)

            if p.client not in self.clients:
                self.clients.append(p.client)
                # set client to the menu
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(0), p.client, p.client == self.proj.client)
            if p.sro not in self.sros and p.project == self.proj.project:
                self.sros.append(p.sro)
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(2), p.sro, p.sro == self.proj.sro)

            if p.project == self.proj.project and p.client == self.proj.client and p.sro == self.proj.sro:
                apd = "{} V{}".format(p.type, p.ver)
                self.apddoe.append(apd)
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(3), apd,
                                   p.type == self.proj.type and p.ver == self.proj.ver)

    def run_ex_script(self, pid=0):
        if os.path.exists(os.path.join(env.PATH["box_path"], env.SYS_SCRIPT)):
            script_filepath = os.path.join(env.PATH["box_path"], env.SYS_SCRIPT)
        else:
            script_filepath = ""
        for p in self.window.projs:
            if p._id == pid:
                try:

                    import runpy
                    run = runpy.run_path(script_filepath, init_globals={"qgis_path": p.qgis_path}, run_name="__main__")
                    qsriputils.print_rip(f"Runpy result:\n {run}")
                    if "exception" in run or "Exception" in run:
                        qsriputils.print_rip(f"Runpy result:\n {run}")
                    else:
                        result = run["output"]
                        qsriputils.print_rip("script run result:", len(result))
                        error = 0
                        warning = 0
                        tested = run["total"]
                        ok = run["total"] - len(result)
                        w = ["#BO_FS", "#BO_BF", "#BO_ZP", "#PI_MD", "#BO_EX", "#BO_CO", "#PT_PG", "#BO_PT", "#BO_AM"]
                        e = ["#PT_TY", "#PT_MD", "#BO_FO", "#BO_MF", "#CO_DE", "#CO_IN", "#CO_CA", "#CO_OT", "#BO_AO"]
                        po = run["po"]
                        issues = []
                        now = datetime.datetime.now()
                        for key in result.keys():
                            value = result[key]

                            for v in value:
                                qsriputils.print_rip(f"{key}:{v}")
                                issue = QSRIPIssue(pbo=po[key], code=v,
                                                   text=const.errCode[v] if v in const.errCode else "")

                                issue.fromProject(p=p, u=self.window.user)
                                if v in e:
                                    error += 1
                                    issue.level = 2
                                    issues.append(issue)
                                elif v in w:
                                    warning += 1
                                    issue.level = 1
                                    issues.append(issue)

                        p.tested += tested
                        p.error += error
                        p.warn += warning
                        p.ok = ok
                        p.scripted = 1
                        p.issues.extend(issues)
                        qsriputils.print_rip('Run Ex Script Issues Count:', len(issues))
                        if len(issues) > 0:
                            qsriputils.update_project_data(p, {"ok": p.ok, "scripted": 1})
                            qsriputils.create_issues(issues=issues, proj_id=self.proj._id, total=tested, ok=ok,
                                                     client=p.client)
                    self.taskFinished.emit(1)
                except Exception as e:
                    p.scripted = 1
                    import traceback
                    print(f"Runpy exception {e}")
                    print(traceback.format_exc())
                    self.taskFinished.emit(3)
                break

    def get_issue(self, pbo, issue_type="script"):
        # print(f"issues: len: {len(self.proj.issues)}")
        issues = []
        for issue in self.proj.issues:
            if issue.pbo == pbo and issue.issue_type == issue_type:
                issues.append(issue)
        return issues

    def tree_item_clear_child(self, parent):
        children = []
        for i in range(parent.childCount()):
            children.append(parent.child(i))
        for child in children:
            parent.removeChild(child)

    def update_type_tree(self, pp):
        clients = []
        for i in range(self.clientTreeWidget.topLevelItem(0).childCount()):
            clients.append(self.clientTreeWidget.topLevelItem(0).child(i).text(0))
        if pp.client not in clients:
            self.__addTreeItem(self.clientTreeWidget.topLevelItem(0), pp.client, True)

        projects = []
        if self.proj is None or pp.client != self.proj.client:
            self.__selectTreeItem(self.clientTreeWidget.topLevelItem(0), pp.client)
            self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(1))
            for p in self.window.projs:
                if p.client == pp.client and p.project not in projects:
                    projects.append(p.project)
                    self.__addTreeItem(self.clientTreeWidget.topLevelItem(1), p.project, p.project == pp.project)
        elif pp.client == self.proj.client:
            for i in range(self.clientTreeWidget.topLevelItem(1).childCount()):
                projects.append(self.clientTreeWidget.topLevelItem(1).child(i).text(0))
            if pp.project not in projects:
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(1), pp.project, True)
        sros = []
        if self.proj is None or pp.project != self.proj.project:
            self.__selectTreeItem(self.clientTreeWidget.topLevelItem(1), pp.project)
            self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(2))
            for p in self.window.projs:
                if p.project == pp.project and p.sro not in sros:
                    sros.append(p.sro)
                    self.__addTreeItem(self.clientTreeWidget.topLevelItem(2), p.sro, p.sro == pp.sro)
        elif pp.project == self.proj.project:
            for i in range(self.clientTreeWidget.topLevelItem(2).childCount()):
                sros.append(self.clientTreeWidget.topLevelItem(2).child(i).text(0))
            if pp.sro not in sros:
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(2), pp.sro, True)
        # add APD/DOE Tree
        if self.proj is None or pp.sro != self.proj.sro:
            self.__selectTreeItem(self.clientTreeWidget.topLevelItem(2), pp.sro)
            self.tree_item_clear_child(self.clientTreeWidget.topLevelItem(3))
        apddoe = []
        for i in range(self.clientTreeWidget.topLevelItem(3).childCount()):
            apddoe.append(self.clientTreeWidget.topLevelItem(3).child(i).text(0))
        for p in self.window.projs:
            apd = f"{p.type} V{p.ver}"
            if p.sro == pp.sro and apd not in apddoe:
                apddoe.append(apd)
                self.__addTreeItem(self.clientTreeWidget.topLevelItem(3), apd, apd == f"{pp.type} V{pp.ver}")
        else:
            self.__selectTreeItem(self.clientTreeWidget.topLevelItem(3), f"{pp.type} V{pp.ver}")

    def __addTreeItem(self, parent, text, checked=False):
        item = QTreeWidgetItem(parent, [text])
        item.setForeground(0, QtGui.QBrush(Qt.white))
        item.setCheckState(0, Qt.Checked if checked else Qt.Unchecked)

    def __selectTreeItem(self, parent, selected):
        child_count = parent.childCount()
        for i in range(child_count):
            item = parent.child(i)
            # print(item.text(0))
            if item.text(0) == selected:
                item.setCheckState(0, Qt.Checked)
            else:
                item.setCheckState(0, Qt.Unchecked)

    @QtCore.pyqtSlot()
    def export_pdf(self):
        # qsriputils.print_widget(self.scrollAreaWidgetContents, "test.pdf")
        printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        # printer.setPageSize(QtPrintSupport.QPrinter.A4)
        dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        dialog.paintRequested.connect(self.print_preview)
        dialog.exec_()

    @QtCore.pyqtSlot()
    def export_csv(self):
        conn = qsriputils.db_connect(os.path.join(const.dbpath, "qsrip.sqlite"))
        if conn:
            name = '{}_{}_V{}_CSV'.format(self.proj.sro, self.proj.type, self.proj.ver).replace('-', '_')
            query = "SELECT * FROM {}".format(name)
            db_df = pd.read_sql_query(query, conn)
            filename = QtGui.QFileDialog.getSaveFileName(self, 'Save File', ""'csv_export', '(*.csv)')
            if filename[0]:
                db_df.to_csv(filename[0], index=False)

    @QtCore.pyqtSlot(QtPrintSupport.QPrinter)
    def print_preview(self, printer):
        if not self.detailFrame.isHidden() and self.nodeNameButton.text() == self.proj.sro:
            qsriputils.clearLayout(self.mapSysLayout)
            self.cableLayer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.cable_layer)[0])[0]
            self.boiteLayer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.boite_layer)[0])[0]

            self.canvas = QgsMapCanvas()
            self.canvas.setMouseTracking(True)
            self.canvas.setCanvasColor(Qt.white)
            self.canvas.enableAntiAliasing(True)
            self.canvas.enableMapTileRendering(True)
            layers = []
            # crs = QgsCoordinateReferenceSystem("EPSG:3857")
            layers.append(self.cableLayer)
            layers.append(self.boiteLayer)
            # self.cableLayer.setCrs(crs)
            # self.boiteLayer.setCrs(crs)

            for root, dirs, files in os.walk(self.proj.qgis_path):
                for file in files:
                    if "POINT_TECHNIQUE" in file and file.endswith(".shp"):
                        ptfile = os.path.join(root, file)
                        qsriputils.print_rip(f"pt: {ptfile}")
                        ptLayer = QgsVectorLayer(ptfile, "PtLayer")
                        # ptLayer.setCrs(crs)
                        QgsProject.instance().addMapLayer(ptLayer)
                        layers.append(ptLayer)
                    elif "SITE" in file and file.endswith(".shp"):
                        sfile = os.path.join(root, file)
                        qsriputils.print_rip(f"site: {sfile}")
                        sLayer = QgsVectorLayer(sfile, "SiteLayer")
                        # ptLayer.setCrs(crs)
                        QgsProject.instance().addMapLayer(sLayer)
                        layers.append(sLayer)

            urlWithParams = 'crs=EPSG:3857&type=xyz&url=http://tile.openstreetmap.org/%7Bz%7D/%7Bx%7D/%7By%7D.png&zmax=19&zmin=0'
            rlayer = QgsRasterLayer(urlWithParams, 'OpenStreetMap', 'wms')

            if rlayer.isValid():

                QgsProject.instance().addMapLayer(rlayer)
                layers.append(rlayer)
                # print("r extent:", rlayer.extent())

            else:
                qsriputils.print_rip('invalid layer')

            qsriputils.print_rip("boite extent:", self.boiteLayer.extent())

            self.canvas.setLayers(layers)
            box = self.boiteLayer.extent()
            box.set(box.xMinimum() - 50, box.yMinimum() - 50, box.xMaximum() + 50, box.yMaximum() + 50)
            self.canvas.setEnabled(True)
            self.canvas.setExtent(self.boiteLayer.extent())
            crs = QgsCoordinateReferenceSystem("EPSG:2154")
            self.canvas.setDestinationCrs(crs)
            self.canvas.update()
            rlayer = QgsProject.instance().mapLayersByName('OpenStreetMap')[0]
            rlayer.triggerRepaint()
            self.mapTool = IdentifyFeature(self.canvas)
            self.mapTool.featureIdentified.connect(self.onFeatureIdentified)
            self.canvas.setMapTool(self.mapTool)
            self.canvas.waitWhileRendering()
            self.canvas.saveAsImage("temp/map.png")

            pix = QPixmap(self.scrollAreaWidgetContents.size())
            painter = QtGui.QPainter(pix)
            self.scrollAreaWidgetContents.render(painter)
            pix.save("temp/sysnotique.png", 'png')
            painter.end()

            from PIL import Image
            img = Image.open('temp/sysnotique.png')
            width, height = img.size
            if width > height:
                img = img.rotate(angle=-90, expand=True)
                img.save('temp/sysnotique.png')
            img = Image.open('temp/map.png')
            width, height = img.size
            if width > height:
                img = img.rotate(angle=-90, expand=True)
                img.save('temp/map.png')

            html = '''<style type=\'text/css\'>table.tbl {border-width: 1px; border-style: solid; border-color: black; border-collapse: collapse; page-break-before: always;}</style>
            <body><table width='100%' border='1'><tr>
            <td colspan='2' align='center' valign='middle'><img src='images/securiplogo.png' width='100'></td>
            <td colspan='2' align='center' valign='middle'>Rapport SecurRIP</td>
            <td colspan='2' valign='middle'>Code : RRIP.01<br>Page : 1 / 3<br>Révision :''' + datetime.datetime.now().strftime(
                "%m/%d/%Y") + '''
            <br></td></tr><tr><td colspan='6'></tr>
                <tr><td colspan='6' align='center' valign='middle'><b>SRO : ''' + str(self.proj.sro) + '''</b></td></tr>
                <tr>
                    <td>''' + datetime.datetime.now().strftime("%m/%d/%Y") + '''</td>
                    <td>''' + str(self.proj.info['site3'][1]) + '''</td>
                    <td>''' + str(self.proj.info['site1'][1]) + '''</td>
                    <td>''' + str(self.proj.info['site2'][1]) + '''</td>
                    <td>''' + str(self.proj.info['dbl']['total']) + '''</td>
                    <td>''' + str(self.proj.type) + 'V' + str(self.proj.ver) + '''</td>
                </tr>
                <tr>
                    <td>DATE</td>
                    <td>Code INSEE</td>
                    <td>Propriétaire</td>
                    <td>Gestionnaire</td>
                    <td>Nbre Prise</td>
                    <td>Version</td>
                </tr>
                <tr>
                    <td>Nb Page</td>
                    <td colspan='2'>3</td>
                    <td>Utilisateur</td>
                    <td colspan='2'>''' + str(self.window.user.displayName) + '''</td>
                </tr>
                <tr><td colspan='6'>Situation</td></tr>
                <tr>
                    <td colspan='6' align='center'><img src="temp/map.png" width=500></td>
                </tr>
                <tr>
                    <td>''' + str(round(self.proj.info['dbl']['total'], 2)) + '''</td>
                    <td>''' + str(self.proj.info['dbl']['PAVILLON']) + '''</td>
                    <td>''' + str(self.proj.info['dbl']['IMMEUBLE']) + '''</td>
                    <td>''' + str(round(self.proj.info['cable']['total'], 2)) + '''m</td>
                    <td>''' + str(round(self.proj.info['cable']['AERIEN'], 2)) + '''m</td>
                    <td>''' + str(round(self.proj.info['cable']['CONDUITE'], 2)) + '''m</td>
                </tr>
                <tr>
                    <td>Nbre Prise</td>
                    <td>Nbr Pavillon</td>
                    <td>Nbr Immeuble</td>
                    <td>Taille du réseau</td>
                    <td>Aerien</td>
                    <td>Conduite</td>
                </tr>
            </table>
            <table width='100%' border='1' class='tbl'>
                <tr>
                    <td width=200 align='center' valign='middle'><img src="images/securiplogo.png" width="100"></td>
                    <td width=200 align='center' valign='middle'>Rapport SecurRIP</td>
                    <td width=200 valign='middle'>
                        Code : RRIP.01<br>
                        Page : 2 / 3<br>
                        Révision : ''' + datetime.datetime.now().strftime("%m/%d/%Y") + '''<br>
                    </td>
                </tr>
                <tr></tr>
                <tr><td colspan='3' align='center' valign='middle' style='background-color: yellow'><b>Résumé du matériel</b></td></tr>
                <tr><td colspan='3' align='center' valign='middle' style='background-color: gray'>Câbles</td></tr>
            '''

            mat = self.proj.info["material"]
            allKeys = list(mat.keys())
            for key in allKeys:
                html += "<tr><td colspan='2'>" + key + "</td><td>" + str(round(mat[key], 2)) + "</td></tr>"

            html += "<tr><td colspan='3' align='center' valign='middle' style='background-color: gray'>Boites Optiques</td></tr>"

            boite = self.proj.info["boite"]
            allKeys = list(boite.keys())
            for key in allKeys:
                html += "<tr><td colspan='2'>" + key + "</td><td>" + str(boite[key]) + "</td></tr>"

            html += '''
            </table>
            <table width='100%' border='1' class='tbl'>
                <tr>
                    <td width=200 align='center' valign='middle'><img src="images/securiplogo.png" width="100"></td>
                    <td width=200 align='center' valign='middle'>Rapport SecurRIP</td>
                    <td width=200 valign='middle'>
                        Code : RRIP.01<br>
                        Page : 3 / 3<br>
                        Révision : ''' + datetime.datetime.now().strftime("%m/%d/%Y") + '''<br>
                    </td>
                </tr>
                <tr><td colspan='3' align='center' valign='middle'>Sysnotique</td></tr>
                <tr><td colspan='3'></td></tr>
                <tr><td colspan='3' align='center' valign='middle'><img src="temp/sysnotique.png" width=500></td></tr>
            </table>
            </body>
            '''

            web = QTextBrowser()
            web.setHtml(html)
            web.print(printer)
        else:
            qsriputils.print_rip('invalid layer')
            painter = QtGui.QPainter(printer)
            if not self.detailFrame.isHidden():
                xscale = printer.pageRect().width() * 1.0 / self.detailScrollArea.width()
                yscale = printer.pageRect().height() * 1.0 / self.detailScrollArea.height()
                scale = min(xscale, yscale)
                painter.translate(printer.paperRect().center())
                painter.scale(scale, scale)
                painter.translate(-self.detailScrollArea.width() / 2, -self.detailScrollArea.height() / 2)
                # end scale

                self.detailScrollArea.render(painter)
                painter.resetTransform()
                printer.newPage()

            # start scale
            xscale = printer.pageRect().width() * 1.0 / self.scrollAreaWidgetContents.width()
            yscale = printer.pageRect().height() * 1.0 / self.scrollAreaWidgetContents.height()
            scale = min(xscale, yscale)
            painter.translate(printer.paperRect().center())
            painter.scale(scale, scale)
            painter.translate(-self.scrollAreaWidgetContents.width() / 2, -self.scrollAreaWidgetContents.height() / 2)
            # end scale

            self.scrollAreaWidgetContents.render(painter)
            painter.end()

    @QtCore.pyqtSlot(QtWidgets.QTreeWidgetItem, int)
    def onProjectClicked(self, it, col):
        qsriputils.print_rip(it, col, it.text(col))
        text = it.text(col)
        self.__selectTreeItem(self.typeTreeWidget, text)
        tt = text.split()
        type = tt[0]
        ver = int(tt[1][1:])
        for p in self.window.projs:
            if p.type == type and p.ver == ver:
                self.proj = p
                self.window.activeId = p._id
                self.window.loadActiveProjectLayer()
                self.__drawProject()
                break

    def __preprocess_data(self, d_meta, l_raw_trace):
        '''Convert the raw data into a numpy array'''
        sample_spacing = float(d_meta["FxdParams"]["sample spacing"][:-5])
        self.window_len = int(0.5 / sample_spacing)
        q_trace = deque(l_raw_trace)
        l_distance = list()
        l_level = list()
        raw_row = q_trace.popleft()
        while q_trace:
            raw_distance, raw_level = raw_row.replace("\n", "").split("\t")
            f_distance = float(raw_distance)
            f_level = float(raw_level)
            l_distance.append(f_distance)
            l_level.append(f_level)
            raw_row = q_trace.popleft()
        a_trace = np.array([l_level, l_distance])
        return a_trace

    def __drawProject(self):
        self.detailFrame.setHidden(True)
        qsriputils.clearLayout(self.graphLayout)
        if self.window.user.role != "admin":
            self.assigneeBox.setHidden(True)
        else:
            self.assigneeBox.setHidden(False)
            self.assigneeBox.clear()
            self.assigneeBox.addItem(ll['assign_to'])
            self.assigneeBox.addItems([f"{t.displayName}" for t in self.window.techniciens])
        # self.cableLayer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.cable_layer)[0])[0]
        # self.boiteLayer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.boite_layer)[0])[0]
        if os.path.exists(self.proj.sor_path):
            self.sorpbo = os.listdir(self.proj.sor_path)
        else:
            self.sorpbo.clear()
        self.treeOfFeatures = {}
        self.leafCount = {}

        qsriputils.print_rip("sro: ", self.proj.sro)
        button = QPushButton(self.proj.sro)
        button.setFont(QFont('Segoe MDL2 Assets', 9))
        button.setCheckable(True)
        button.setStyleSheet(
            'QPushButton {background-color: rgb(68,114,196);margin-top:5px; margin-bottom:5px ;padding:4px 10px 4px 10px; color: white; border:3px solid rgb(68,114,196); border-radius: 3px;} '
            'QPushButton:checked {border: 3px solid rgb(46,46,46) } '
        )
        button.clicked.connect(self.selectSRONode)
        # button.setIcon(self.comboIcon("redshield.png", "captureico.png"))
        # jpg = os.path.join(self.proj.jpg_path, f"{self.proj.sro}.jpg")
        # print("jpg:", jpg)
        if len(self.proj.images) > 0:
            # print("ok:", jpg)
            path = os.getcwd()
            button.setIcon(QIcon(os.path.join(path, "images", "captureico.png")))

        self.graphLayout.addWidget(button, 0, 0)
        start = time.process_time()
        qsriputils.print_rip("before __getChildNodes")
        self.__getChildNodes(self.proj.sro)
        qsriputils.print_rip("get child time: ", time.process_time() - start)
        qsriputils.print_rip("tree:", str(self.treeOfFeatures))
        start = time.process_time()
        self.__updateGrid(self.proj.sro, 0, 0)

        qsriputils.print_rip("update grid time: ", time.process_time() - start)
        qsriputils.print_rip(f"grid: {self.graphLayout.rowCount()}x{self.graphLayout.columnCount()}")
        qsriputils.clearLayout(self.downloadLayout)
        for file in self.proj.files:
            fileButton = QPushButton(file)
            fileButton.setStyleSheet(
                "background-color:transparent; color: rgb(18,67,105); text-align:left; text-decoration: underline; border:none; padding: 3px")
            fileButton.clicked.connect(self.download_file)
            fileButton.setCursor(QCursor(QtCore.Qt.PointingHandCursor))

            self.downloadLayout.addWidget(fileButton)
        self.downloadLayout.insertStretch(-1, 100)
        self.renderedMap = False
        if self.mapSysGraph.isHidden():
            self.switch_view()

        if self.proj.client != "CFI":
            if not self.proj.scripted:
                self.showProgress(ll['contrl_progress'])
                self.taskFinished.connect(self.taskDone)
                import threading
                ex_thread = threading.Thread(target=self.run_ex_script, args=(self.proj._id,))
                ex_thread.start()

    def showProgress(self, text):
        self.progressBar = QProgressDialog(text, None, 0, 0, self.window)
        self.progressBar.setWindowTitle(ll['please_wait'])
        self.progressBar.setWindowModality(Qt.WindowModal)
        window_flags = self.progressBar.windowFlags()  # Disable close button
        self.progressBar.setWindowFlags(window_flags & ~Qt.WindowCloseButtonHint)
        self.progressBar.setWindowFlags(window_flags & ~Qt.WindowContextHelpButtonHint)
        self.progressBar.setModal(True)
        self.progressBar.setRange(0, 100)
        self.progressBar.setMinimumDuration(0)
        self.progressBar.setAutoClose(False)
        self.progressBar.show()
        self.timer = QTimer(self, timeout=self.onTimeout)
        self.timer.start(randint(1, 3) * 100)

    def onTimeout(self):
        if self.progressBar.value() >= 100:
            self.progressBar.setValue(0)
        self.progressBar.setValue(self.progressBar.value() + 1)
        QtWidgets.qApp.processEvents(QtCore.QEventLoop.ExcludeUserInputEvents)

    def taskDone(self, status):
        self.progressBar.setValue(100)
        self.progressBar.close()
        self.taskFinished.disconnect()
        if self.timer:
            self.timer.stop()
            # self.timer.deleteLater()
            # del self.timer
        if status > 0:
            self.__drawProject()
        elif status == 0 and len(self.window.projs) > 0:
            import copy
            self.proj = copy.copy(self.window.projs[0])
            self.__drawProject()
        elif status == 0:
            self.detailFrame.setHidden(True)
            qsriputils.clearLayout(self.graphLayout)
            if self.window.user.role != "admin":
                self.assigneeBox.setHidden(True)
            else:
                self.assigneeBox.setHidden(False)
                self.assigneeBox.clear()
                self.assigneeBox.addItem(ll['assign_to'])
                self.assigneeBox.addItems([f"{t.displayName}" for t in self.window.techniciens])
            dlg = QDialog()
            dlg.resize(200, 100)
            layout = QtWidgets.QHBoxLayout()
            dlg.setLayout(layout)
            lb = QLabel(ll['non_pro'])
            lb.setAlignment(Qt.AlignCenter)
            layout.addWidget(lb)
            dlg.exec()

    def __isLeaf(self, name):
        values = self.treeOfFeatures[name]
        if len(values) == 0:
            return True
        return False

    def __getNumLeaf(self, fname):
        # print("count leaf:", fname)
        values = self.treeOfFeatures[fname]
        num = 0
        if len(values) > 0:
            for v in values:
                name = v["dest"]
                if self.__isLeaf(name):
                    num += 1
                else:
                    num += self.__getNumLeaf(name)
            self.leafCount[fname] = num
            # print("leaf of {}: {}".format(fname, num))

            return num
        else:
            return 0

    @QtCore.pyqtSlot()
    def __show_image(self):
        dlg = QProgressDialog(ll['Loading ...'], ll['done'], 0, 100)
        dlg.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowCloseButtonHint)
        dlg.setWindowModality(Qt.WindowModal)
        dlg.setCancelButton(None)
        dlg.forceShow()
        process = 1
        dlg.setValue(process)

        if not os.path.exists(self.proj.jpg_path):
            os.mkdir(self.proj.jpg_path)
        pbo = self.sender().text()
        if pbo.endswith(" \uE70F"):
            pbo = pbo.replace(" \uE70F", "")
        pbos = self.getPBOWithName(pbo)
        if pbo == self.proj.sro and pbos is None:
            pbos = self.proj

        if pbos is not None:
            if len(pbos.images) > 0:
                foder = f'{self.proj.sro}_{self.proj.type}_V{self.proj.ver}_jpg/'
                for image in pbos.images:
                    localName = os.path.join(self.proj.jpg_path, image)
                    if not os.path.exists(localName):
                        blobname = foder + image
                        bucket = storage.bucket()
                        blob = bucket.blob(blobname)
                        blob.download_to_filename(localName)
                        if process < 100:
                            process += round(100 / len(pbos.images))
                            dlg.setValue(process)

                dlg.setValue(100)
                img_path = 'projects/{}/{}/{}/{} V{}/{}/'.format(self.proj.client, self.proj.project, self.proj.sro,
                                                                 self.proj.type, self.proj.ver, 'jpg')
                qsriputils.show_image(img_path, pbo, pbos.images)

        dlg.close()

    def iconButton(self, text, image):
        button = QtWidgets.QToolButton()
        button.setText(text)
        button.setFixedSize(20, 15)
        button.setCheckable(True)
        button.setIconSize(QtCore.QSize(50, 50))
        button.setIcon(QtGui.QIcon(image))
        button.clicked.connect(self.select_icon)
        return button

    def select_icon(self):
        text = self.sender().text()
        for (i, t) in enumerate(self.icons_button):
            if text == 'attach':
                self.showAttach = True
            elif text == 'comment':
                self.showAttach = False
            self.__showPBOComment(self.attach_pbo)
            if t.text() == text:
                t.setChecked(True)
                self.icons_button[i].setIcon(QtGui.QIcon('images/{}'.format(self.selectedIcon[i])))
            else:
                t.setIcon(QtGui.QIcon('images/{}'.format(self.deselectedIcon[i])))
                t.setChecked(False)

    def __showPBOComment(self, name):
        self.pbo_comment = name
        query = qsriputils.db.collection('chats').where('proj_id', '==', self.proj._id) \
            .where('pbo', '==', name)
        chats = query.get()
        comments = dict()
        for chat in chats:
            comment = chat.to_dict()
            if str(comment['date_create']) not in comments:
                if 'text' in comment and not self.showAttach:
                    comments[str(comment['date_create'])] = [(comment['user_name'], comment['text'])]
                elif 'file' in comment:
                    comments[str(comment['date_create'])] = [
                        (comment['user_name'],
                         comment['file'] + ' <a href="' + comment[
                             'file'] + '"><img src="./images/attachicon.png" width=20 height=20>'
                         )]
            else:
                if 'text' in comment and not self.showAttach:
                    comments[str(comment['date_create'])].append((comment['user_name'], comment['text']))
                elif 'file' in comment:
                    comments[str(comment['date_create'])].append(
                        (
                            comment['user_name'],
                            comment['file'] + ' <a href=">' + comment[
                                'file'] + '"><img src="./images/attachicon.png" width=20 height=20>'
                        )
                    )

        html = ""
        if comments:
            k = list(comments.keys())
            k = [int(x) for x in k]
            k.sort(reverse=True)
            k = [str(x) for x in k]

            for key in k:
                for c in comments[key]:
                    html += "<p style='background-color:#F2F2F2'><b>" + c[
                        0] + ":</b> " + datetime.datetime.fromtimestamp(int(key) // 1e9).strftime(
                        '%d-%m-%Y %H:%M:%S') + "<br>" + c[1] + "</p>"

        self.textBrowser.setHtml(html)

    def sendComment(self):
        comment = self.comment.text()
        if comment != '':
            c = Chat()
            c.date_create = time.time_ns()
            c.user_name = self.window.user.displayName
            c.user_id = self.window.user.userId
            c.proj_id = self.proj._id
            c.pbo = self.pbo_comment
            c.text = self.comment.text()
            c.user_read = [self.window.user.userId]

            qsriputils.db.collection('chats').document().set({
                'user_name': c.user_name,
                'user_id': c.user_id,
                'proj_id': c.proj_id,
                'pbo': c.pbo,
                'text': c.text,
                'date_create': c.date_create,
                'user_read': c.user_read
            })
            self.comment.setText('')
        else:
            qsriputils.print_rip('Comment without content')
        self.__showPBOComment(self.pbo_comment)

    def __showPBOMap(self, pbo):
        # QgsProject.instance().addMapLayer(self.cableLayer)
        # QgsProject.instance().addMapLayer(self.boiteLayer)
        self.cableLayer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.cable_layer)[0])[0]
        self.boiteLayer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.boite_layer)[0])[0]

        canvas = QgsMapCanvas()
        canvas.setCanvasColor(Qt.white)
        canvas.enableAntiAliasing(True)
        canvas.setLayers([self.cableLayer, self.boiteLayer])

        self.mapLayout.addWidget(canvas)
        self.boiteLayer.selectByIds([pbo.pbo_id])
        box = self.boiteLayer.boundingBoxOfSelected()
        box.set(box.xMinimum() - 50, box.yMinimum() - 50, box.xMaximum() + 50, box.yMaximum() + 50)
        canvas.setExtent(box)
        canvas.refresh()

    def _get_sor_quality(self, pbo):
        ss = []
        for s in self.proj.sor_quality:
            if s["pbo"] == pbo:
                ss.append(s)
        return ss

    def __updateGrid(self, fname, x, y):
        # print("__updateGrid:", fname)
        values = self.treeOfFeatures[fname]
        xx = x
        yy = y + 2
        i = 0
        for v in values:
            i += 1
            d = v["dest"]
            button = QPushButton(d)
            button.setFont(QFont('Segoe MDL2 Assets', 9))
            button.setCheckable(True)
            if self.proj.client == "CFI":
                button.setStyleSheet(
                    'QPushButton {background: rgb(50,166,27);margin-top:5px; margin-bottom:5px ;padding:4px 10px 4px 10px; color: white; border:3px solid rgb(50,166,27); border-radius: 3px;} '
                    'QPushButton:checked {border: 3px solid rgb(46,46,46) } '
                )
            else:
                issues = self.get_issue(d)
                status = 0
                level = 0
                if len(issues) > 0:
                    for issue in issues:
                        if status < issue.status:
                            status = issue.status
                        if level < issue.level:
                            level = issue.level
                qsriputils.print_rip(f"{d} status: {status} level: {level}")
                if level == 1 and status < 3:
                    button.setStyleSheet(
                        'QPushButton {background: rgb(235,125,60);margin-top:5px; margin-bottom:5px ;padding:4px 10px 4px 10px; color: white; border:3px solid rgb(235,125,60); border-radius: 3px;} '
                        'QPushButton:checked {border: 3px solid rgb(46,46,46) } '
                    )
                elif level == 2 and status < 3:
                    button.setStyleSheet(
                        'QPushButton {background: rgb(252,13,27);margin-top:5px; margin-bottom:5px ;padding:4px 10px 4px 10px; color: white; border:3px solid rgb(252,13,27); border-radius: 3px;} '
                        'QPushButton:checked {border: 3px solid rgb(46,46,46) } '
                    )
                else:
                    button.setStyleSheet(
                        'QPushButton {background: rgb(50,166,27);margin-top:5px; margin-bottom:5px ;padding:4px 10px 4px 10px; color: white; border:3px solid rgb(50,166,27); border-radius: 3px;} '
                        'QPushButton:checked {border: 3px solid rgb(46,46,46) } '
                    )
                if status > 0:
                    button.setText(f"{d} \uE70F")
            jpg = os.path.join(self.proj.jpg_path, f"{d}.jpg")
            pbo = self.getPBOWithName(d)
            if pbo:
                # print("jpg:", jpg)
                path = os.getcwd()
                if len(pbo.images) > 0:
                    qsriputils.print_rip("ok:", jpg)
                    if pbo.vsqe == 0:
                        button.setIcon(QIcon(os.path.join(path, "images", "captureico.png")))
                        button.setIconSize(QSize(16, 16))
                    else:
                        button.setIcon(QIcon(os.path.join(path, "images", self.icons[pbo.vsqe + 2])))
                        button.setIconSize(QSize(32, 16))
                else:
                    if pbo.vsqe > 0:
                        button.setIcon(QIcon(os.path.join(path, "images", self.icons[pbo.vsqe - 1])))
                        button.setIconSize(QSize(16, 16))

            button.clicked.connect(self.selectPBONode)
            self.graphLayout.addWidget(button, xx, yy)
            cable = v["cable"]
            if isinstance(cable, str):
                cable = [cable]
            c = self.__getCableNodes(cable)
            issues = self.get_issue(d, issue_type="sor")

            err = self._get_sor_quality(d)
            # print(err)
            status = -1
            editted = False
            if len(issues) > 0:
                for iss in issues:
                    if status < iss.level:
                        status = iss.level
                    if iss.status > 2:
                        editted = True
            if len(err) > 0 and len(issues) == 0:
                status = 0
            fo = "/".join(f"{cc.capacity}" for cc in c)
            qsriputils.print_rip(" ".join(cc.name for cc in c))

            cable_info = dict()
            for proj_cable in self.proj.cable:
                if proj_cable.name == cable[0]:
                    cable_info['type'] = proj_cable.type
            wire = SROWire(f"{fo}FO",
                           cable=cable,
                           status=status,
                           left_border=len(values) > 1,
                           last_node=i == len(values),
                           first_node=i == 1,
                           vqse=c[0].vsqe,
                           cable_info=cable_info
                           )
            if editted:
                wire.setEditted(editted)
            # status = -1
            # if len(issues) > 0:
            #     status = 0
            #     for issue in issues:
            #         if status < issue.status:
            #             status = issue.status
            # if status >= 0:
            #     wire.setStatus(status)
            wire.clicked.connect(self.selectCableNode)
            self.graphLayout.addWidget(wire, xx, yy - 1)
            self.__updateGrid(d, xx, yy)
            # print("add button {} at {}, {}".format(d,xx,yy))
            le = self.leafCount[d] if d in self.leafCount else self.__getNumLeaf(d)
            if le > 1 and i < len(values):
                # for ll in range(1, le):
                lw = LeftWire()
                self.graphLayout.addWidget(lw, xx + 1, yy - 1, le - 1, 1)
            xx += 1 if le == 0 else le

    def __getCableNode(self, nom):
        for path in self.proj.cable:
            if nom == path.name:
                return path
        return None

    def __getCableNodes(self, noms):
        c = []
        for path in self.proj.cable:
            if path.name in noms:
                c.append(path)
        return c

    def __getChildNodes(self, parent):
        child = []
        for path in self.proj.cable:
            if path.src == path.dest:
                qsriputils.print_rip(f"Error cable: {path}")
                continue
            if parent == path.src:
                # print("path: ", path[self.cableDEST]
                found = False
                for c in child:
                    if c["dest"] == path.dest:
                        found = True
                        if isinstance(c["cable"], str):
                            if c["cable"] != path.name:
                                c["cable"] = [c["cable"], path.name]
                        else:
                            if path.name not in c["cable"]:
                                c["cable"].append(path.name)
                        break
                if not found:
                    child.append({"cable": path.name, "dest": path.dest})
        self.treeOfFeatures[parent] = child

        for org in self.treeOfFeatures[parent]:
            self.__getChildNodes(org["dest"])

    def getPBOWithName(self, name):
        for p in self.proj.pbo:
            # print(f"pbo {p.name} -> {name}")
            if p.name == name:
                return p
        return None

    '''select pbo slot'''

    @QtCore.pyqtSlot()
    def selectPBONode(self):
        if session.USER.license[0] is None:
            dlg = QDialog()
            dlg.resize(200, 100)
            layout = QtWidgets.QHBoxLayout()
            dlg.setLayout(layout)
            lb = QLabel(ll['notLicen'])
            lb.setAlignment(Qt.AlignCenter)
            layout.addWidget(lb)
            dlg.exec()
        else:
            p = self.sender()
            self.unselect_all_cable()
            # self.unselect_all_pbo_node(p.text())
            self.select_pbo_node_(p)

    def select_pbo_node_(self, p):
        qsriputils.print_rip(f"go here: {p.text() if isinstance(p, QPushButton) else p}")
        if isinstance(p, QPushButton) and not p.isChecked():
            self.detailFrame.hide()
        else:
            self.detailFrame.show()
            if not hasattr(self.proj, "csv"):
                self.CSVFrame.setHidden(True)
            else:
                self.CSVFrame.setHidden(False)
                conn = qsriputils.db_connect(os.path.join(const.dbpath, 'qsrip.sqlite'))
                if conn:
                    self.CodeBoiteLabel.setText(p.text())
                    sql_query = f"SELECT CODE_CAS, FACE_CAS, POS_CAS, CB_O, CB_D, TUBE_O, TUBE_D, B_TUBE_O, B_TUBE_D, NUM_FIB_O, NUM_FIB_D, ETAT FROM {self.proj.csv} WHERE CODE_BOITE LIKE \"%{p.text()}%\";"
                    self.rows = conn.cursor().execute(sql_query).fetchall()
                    if self.rows:
                        code_cas_rows = [ir[0] for ir in self.rows]
                        face_cas_rows = [ir[1] for ir in self.rows]
                        pos_cas_rows = [ir[2] for ir in self.rows]
                        self.CodeCasBox.clear()
                        self.FaceCasBox.clear()
                        self.PosCasBox.clear()
                        self.CodeCasBox.addItems(code_cas_rows)
                        self.FaceCasBox.addItems(face_cas_rows)
                        self.PosCasBox.addItems(pos_cas_rows)
                        self.CodeCasBox.setCurrentIndex(0)
                        self.FaceCasBox.setCurrentIndex(0)
                        self.PosCasBox.setCurrentIndex(0)
                        self.setResultText(0)
                    conn.close()
            self.sorBox.setHidden(True)
            self.mqsFrame.setHidden(True)
            self.sorContent.setHidden(True)
            self.trigger_on_change_assignee = False
            self.trigger_on_change_status = False
            self.trigger_on_change_vsqe = False
            name = p.text() if isinstance(p, QPushButton) else p
            vqseIssue = qsriputils.get_last_vqse(name, self.proj.sro, self.proj.type, self.proj.ver)

            self.nodeNameButton.setText(name)
            if name.endswith(" \uE70F"):
                name = name.replace(" \uE70F", "")
            issues = self.get_issue(name)
            self.selected_pbo = None
            level = 0
            err_text = "N/A"
            assignee = ""
            status = 0
            last_modified = None
            if vqseIssue:
                err_text = f"VQSE: {vqseIssue.text}"
            if self.proj.client == "CFI":
                self.nodeNameButton.setStyleSheet(
                    'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                )
            else:
                for issue in issues:
                    last_modified = issue.last_update
                    assignee = issue.assignee_name
                    qsriputils.print_rip(f"issue:{issue.pbo} - {assignee}")
                    if issue.status > status:
                        status = issue.status
                    if err_text == "N/A":
                        err_text = ""
                    else:
                        err_text += "\n"
                    err_text += const.errCode[issue.code] if issue.code in const.errCode else issue.code
                    if issue.level > level:
                        level = issue.level

                self.issueBox.setCurrentIndex(status)
                qsriputils.print_rip(f"issue status: {status}")
                qsriputils.print_rip(f"issue level: {level}")
                if level == 1:
                    if status <= 2:
                        self.nodeNameButton.setStyleSheet(
                            'QPushButton {background: rgb(235,125,60); color: white; border:none; border-radius: 3px; padding: 3px} '
                        )
                    else:
                        self.nodeNameButton.setStyleSheet(
                            'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                        )
                    self.issueBox.setHidden(False)
                    self.selected_pbo = p
                elif level == 2:
                    if status <= 2:
                        self.nodeNameButton.setStyleSheet(
                            'QPushButton {background: rgb(252,13,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                        )
                    else:
                        self.nodeNameButton.setStyleSheet(
                            'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                        )
                    self.issueBox.setHidden(False)
                    self.selected_pbo = p
                else:
                    self.nodeNameButton.setStyleSheet(
                        'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                    )
                    self.issueBox.setHidden(True)
                if assignee != "":
                    techniciens = self.window.techniciens
                    foundTech = False
                    for tech in techniciens:
                        if assignee == tech.displayName:
                            self.assigneeBox.setCurrentText(assignee)
                            foundTech = True
                            break
                    if not foundTech:
                        self.assigneeBox.setCurrentIndex(0)
                else:
                    self.assigneeBox.setCurrentIndex(0)

            self.labelCreateDate.setText(f"{self.proj.create_date.strftime('%d-%m-%Y')}")
            if last_modified and len(last_modified) > 0:
                ldate = last_modified[-1]
                qsriputils.print_rip(f"date: {type(ldate)}")
                if isinstance(ldate, dict):
                    ldate = ldate["date"]
                self.labelCloseDate.setText(f"{ldate.strftime('%d-%m-%Y')}")
            self.labelVersion.setText(f"{self.proj.type} V{self.proj.ver}")
            self.labelVersion.adjustSize()
            self.labelImportUser.setText(self.p_user.displayName)
            self.errorLabel.setText(err_text)
            jpg = os.path.join(self.proj.jpg_path, f"{name}.jpg")
            qsriputils.print_rip("jpg:", jpg)
            pbo = self.getPBOWithName(name)
            if hasattr(pbo, 'images') and len(pbo.images) > 0:
                qsriputils.print_rip("ok:", jpg)
                path = os.getcwd()
                self.nodeNameButton.setIcon(QIcon(os.path.join(path, "images", "captureico.png")))
            else:
                qsriputils.print_rip("jpg: no photo")
                self.nodeNameButton.setIcon(QIcon())

            qsriputils.clearLayout(self.detailLayout)
            qsriputils.clearLayout(self.mapLayout)
            # clear other widget selected status
            for i in range(self.graphLayout.count()):
                w = self.graphLayout.itemAt(i).widget()
                if isinstance(w, QPushButton) and w != self.sender():
                    w.setChecked(False)
            # fill pbo detail info

            if pbo:
                self.vqseBox.setCurrentIndex(pbo.vsqe)
                lines = [f"{pbo.reference} {pbo.type}", pbo.function, f"{pbo.support} {pbo.model}"]
                for line in lines:
                    label = QLabel(line)
                    label.setAlignment(Qt.AlignCenter)
                    label.setFont(QFont('SF UI Text', 9))
                    self.detailLayout.addWidget(label)

                self.__showPBOMap(pbo)
                self.__showPBOComment(pbo.name)
                self.attach_pbo = pbo.name
            self.trigger_on_change_assignee = True
            self.trigger_on_change_status = True
            self.trigger_on_change_vsqe = True

    @QtCore.pyqtSlot()
    def selectSRONode(self):
        if not self.sender().isChecked():
            self.detailFrame.setHidden(True)
        else:
            self.detailFrame.setHidden(False)
            self.CSVFrame.setHidden(True)
            self.mqsFrame.setHidden(True)
            name = self.sender().text()
            self.nodeNameButton.setText(name)
            self.nodeNameButton.setStyleSheet('background-color: rgb(68,114,196); color: white')
            jpg = os.path.join(self.proj.jpg_path, f"{self.proj.sro}.jpg")
            # print("jpg:", jpg)
            if len(self.proj.images) > 0:
                qsriputils.print_rip("ok:", jpg)
                path = os.getcwd()
                self.nodeNameButton.setIcon(QIcon(os.path.join(path, "images", "captureico.png")))
            else:
                self.nodeNameButton.setIcon(QIcon())
            qsriputils.clearLayout(self.detailLayout)
            if self.proj.info:
                keys = ["site1", "site2", "site3"]
                for key in keys:
                    if key in self.proj.info:
                        v = self.proj.info[key]
                        label = QLabel(f"{v[0]}: <b>{v[1]}</b>")
                        self.detailLayout.addWidget(label)
                # dbl
                dbl = self.proj.info["dbl"]
                dtotal = dbl["total"]
                label = QLabel(f"Nombre de Prise <b>{dtotal}</b> dont:")
                self.detailLayout.addWidget(label)
                allKeys = list(dbl.keys())
                for key in allKeys:
                    if key == "total": continue
                    label = QLabel(f"{key}: <b>{dbl[key]}</b>:")
                    self.detailLayout.addWidget(label)
                # cable
                cable = self.proj.info["cable"]
                totalCable = cable["total"]
                label = QLabel(f"Constitution du réseau: <b>{totalCable:.2f}m</b>:")
                self.detailLayout.addWidget(label)
                allKeys = list(cable.keys())
                for key in allKeys:
                    if key == "total": continue
                    label = QLabel(f"{key}: <b>{cable[key]:.2f}m</b>:")
                    self.detailLayout.addWidget(label)
                # material
                label = QLabel("<b>Résumé du matériel:</b>")
                self.detailLayout.addWidget(label)
                label = QLabel("<u>Cables:</u>")
                self.detailLayout.addWidget(label)
                mat = self.proj.info["material"]
                allKeys = list(mat.keys())
                for key in allKeys:
                    label = QLabel(f"{key}: <b>{mat[key]:.2f}m</b>:")
                    self.detailLayout.addWidget(label)
                label = QLabel("<u>Boite Optiques:</u>")
                self.detailLayout.addWidget(label)
                boite = self.proj.info["boite"]
                allKeys = list(boite.keys())
                for key in allKeys:
                    label = QLabel(f"{key}: <b>{boite[key]}</b>:")
                    self.detailLayout.addWidget(label)
            qsriputils.clearLayout(self.mapLayout)
            self.sorBox.setHidden(True)
            self.mqsFrame.setHidden(True)
            self.sorContent.setHidden(True)
            self.issueBox.setHidden(True)
            self.assigneeBox.setHidden(True)
            self.labelCreateDate.setText(f"{self.proj.create_date.strftime('%d-%m-%Y')}")
            self.labelVersion.setText(f"{self.proj.type} V{self.proj.ver}")
            self.labelVersion.adjustSize()
            self.labelImportUser.setText(self.p_user.displayName)
            self.errorLabel.setText("N/A")
            self.__showPBOComment(name)

    '''select the cable slot'''

    def unselect_all_cable(self):
        for i in range(self.graphLayout.count()):
            w = self.graphLayout.itemAt(i).widget()
            if isinstance(w, SROWire) or isinstance(w, LeftWire):
                w.setSelected(False)

    def select_cable(self, cable):
        idx = self.graphLayout.indexOf(cable)
        location = self.graphLayout.getItemPosition(idx)
        cable.setSelected(True)
        row = location[0]
        column = location[1]
        rowspan = location[2]
        colspan = location[3]
        if column > 1:
            item = self.graphLayout.itemAtPosition(row, column - 2)
            if item:
                w = item.widget()
                if isinstance(w, SROWire):
                    if w.text == cable.text:
                        self.select_cable(w)
                # elif isinstance(w, LeftWire):

    def unselect_all_pbo_node(self, pn):
        for i in range(self.graphLayout.count()):
            w = self.graphLayout.itemAt(i).widget()
            if isinstance(w, QPushButton):
                if w.text != pn and w.isChecked():
                    w.setChecked(False)

    def select_cable_node_(self, cable):
        qsriputils.print_rip(f"cable: {cable}")
        self.detailFrame.show()
        self.CSVFrame.setHidden(True)
        self.trigger_on_change_assignee = False
        self.trigger_on_change_status = False
        self.trigger_on_change_vsqe = False
        self.errorLabel.setText("")
        self.nodeNameButton.setText(" ".join(cable))
        self.nodeNameButton.setIcon(QIcon())
        qsriputils.clearLayout(self.detailLayout)
        qsriputils.clearLayout(self.mapLayout)
        c = self.__getCableNodes(cable)
        issues = self.get_issue(c[0].dest, issue_type="sor")
        qsriputils.print_rip(issues)
        error_text = ""
        assignee = ""
        last_modified = None
        self.vqseBox.setCurrentIndex(c[0].vsqe)
        vqseIssue = qsriputils.get_last_vqse(cable[0], self.proj.sro, self.proj.type, self.proj.ver)
        if len(issues) > 0:
            self.issueBox.setHidden(False)
            status = 0
            for iss in issues:
                last_modified = iss.last_update
                qsriputils.print_rip(iss)
                if len(iss.text) == 0:
                    continue
                if iss.status > status:
                    status = iss.status
                    assignee = iss.assignee_name
                if iss.level == 1:
                    color = "orange"
                else:
                    color = "red"
                cc = iss.text.split("\n")
                error_text += f"<br/><b style='color:{color}'>{cc[0]}</b>"
                error_text += f"<br/>{cc[1]}"
            self.issueBox.setCurrentIndex(status)
            if status == 0:
                self.nodeNameButton.setStyleSheet(
                    'QPushButton {background: rgb(252,13,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                )
            else:
                self.nodeNameButton.setStyleSheet(
                    'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
                )
        else:
            self.nodeNameButton.setStyleSheet(
                'QPushButton {background: rgb(50,166,27); color: white; border:none; border-radius: 3px; padding: 3px} '
            )
            self.issueBox.setHidden(True)
        qsriputils.print_rip(f"assignee: {assignee}")
        if assignee != "":
            techniciens = self.window.techniciens
            foundTech = False
            for tech in techniciens:
                if assignee == tech.displayName:
                    self.assigneeBox.setCurrentText(assignee)
                    foundTech = True
                    break
            if not foundTech:
                self.assigneeBox.setCurrentIndex(0)
        else:
            self.assigneeBox.setCurrentIndex(0)
        qsriputils.print_rip("1")
        self.qgis_length = 0 if self.proj.client == "ORANGE" else self.__get_cable_length_from_qgis(c[0])
        qsriputils.print_rip("2")
        self.labelCreateDate.setText(f"{self.proj.create_date.strftime('%d-%m-%Y')}")
        if last_modified and len(last_modified) > 0:
            ldate = last_modified[-1]
            if isinstance(ldate, dict):
                ldate = ldate["date"]
            self.labelCloseDate.setText(f"{ldate.strftime('%d-%m-%Y')}")
        self.labelVersion.setText(f"{self.proj.type} V{self.proj.ver}")
        self.labelVersion.adjustSize()
        self.labelImportUser.setText(self.p_user.displayName)
        self.sorErrorLabel.setText("N/A")
        # print("qgis:", l)
        qsriputils.print_rip("3")
        if len(c) > 0:
            label = QLabel(f"{c[0].src} - Section {c[0].section} - {c[0].dest}")
            label.setAlignment(Qt.AlignCenter)
            label.setWordWrap(True)
            self.detailLayout.addWidget(label)
            lines = [c[0].type, f"{c[0].length}ml"]
            for line in lines:
                label = QLabel(line)
                label.setAlignment(Qt.AlignCenter)
                label.setFont(QFont('SF UI Text', 9))
                self.detailLayout.addWidget(label)
        self.sor_quality = qsriputils.get_sor_quality(self.proj, c[0].dest)
        if len(self.sor_quality) == 0:
            self.sorErrorLabel.setText("N/A")
            self.sorBox.setHidden(True)
            self.sorContent.setHidden(True)
            self.mqsFrame.setHidden(True)
        else:
            self.mqsFrame.setHidden(False)
            self.sorBox.setHidden(False)
            self.sorContent.setHidden(False)
            self.sorBox.clear()
            items = []
            for sor in self.sor_quality:
                items.append(sor["error"][0])
            self.sorBox.addItems(items)

            if len(error_text) > 0:
                text = "<b>Resume Errors .sor du Cable:</b>"
                text += error_text
                qsriputils.print_rip(text)
                self.sorErrorLabel.setText(text)
                self.sorErrorLabel.setTextFormat(Qt.AutoText)
        self.trigger_on_change_assignee = True
        self.trigger_on_change_status = True
        self.trigger_on_change_vsqe = True
        self.__showPBOComment(cable[0])

    @QtCore.pyqtSlot()
    def selectCableNode(self):
        if session.USER.license[0] is None:
            dlg = QDialog()
            dlg.resize(200, 100)
            layout = QtWidgets.QHBoxLayout()
            dlg.setLayout(layout)
            lb = QLabel(ll['notLicen'])
            lb.setAlignment(Qt.AlignCenter)
            layout.addWidget(lb)
            dlg.exec()
        else:
            self.selected_pbo = None
            self.unselect_all_cable()
            self.unselect_all_pbo_node("")
            # self.select_cable(self.sender())
            self.sender().setSelected(True)
            self.select_cable_node_(self.sender().cable)

    # if self.__checkSOR(c.dest):
    #     nom = c.dest.lower().replace("-", " ")
    #     for f in os.listdir(self.proj.sor_path):
    #         if nom.endswith(f):
    #             sorpath = os.path.join(self.proj.sor_path, f)
    #
    #     files = os.listdir(sorpath)
    #     for f in files:
    #         if f.endswith(".sor"):
    #             _, d_meta, l_raw_trace = sorparse(os.path.join(sorpath, f))
    #             print(d_meta)
    #             a_trace = self.__preprocess_data(d_meta, l_raw_trace)
    #             self.raw_trace = OTDRlib.prepare_data({"meta": d_meta, "trace": a_trace}, self.window_len)
    #             trace = OTDRTrace(self.raw_trace, d_meta)
    #             self.mapLayout.addWidget(trace)
    #             cable_length = qsriputils.get_cable_length_from_sor(d_meta)
    #             print("sor:", cable_length)
    #             qgis_l = sum(l)
    #             sor_l = sum(cable_length)
    #             diff = abs(sor_l - qgis_l) * 100 / qgis_l
    #             self.errorLabel.setText(
    #                 f"qgis: {l}= {qgis_l}\nsor: {cable_length}= {sor_l}\nResult:{diff:.2f}% pour {round(abs(sor_l - qgis_l))}m")
    #
    #             # raw_feature = OTDRlib.find_edges(OTDRlib.differentiate_data(self.raw_trace))
    #             # print(raw_feature)
    #             # d_events = self._filter_events(raw_feature)
    #             # print(d_events)
    #             break
    # else:
    #     self.errorLabel.setText("N/A")

    '''check if there is a sor file for this pbo'''

    def __checkSOR(self, pbo):
        nom = pbo.lower().replace("-", " ")
        qsriputils.print_rip(f"checksor: {nom}")
        for f in self.sorpbo:
            qsriputils.print_rip(f)
            if nom.endswith(f):
                # print("sor pbo:", nom)
                return True
        return False

    @staticmethod
    def _filter_events(trace_features):
        '''Filter the detected features of each trace to make a single set with no duplicates or ghosts'''
        d_events = {}
        for index, feature in enumerate(trace_features[2]):
            feature_position = OTDRlib.round_sig(feature, 1)
            if feature_position not in d_events and feature_position - 0.1 not in d_events and feature_position + 0.1 not in d_events:
                d_events[feature_position] = {
                    "indexes": []
                }
            if feature_position in d_events:
                d_events[feature_position]["indexes"].append(trace_features[0][index])
            elif feature_position - 0.1 in d_events:
                d_events[feature_position - 0.1]["indexes"].append(trace_features[0][index])
            elif feature_position + 0.1 in d_events:
                d_events[feature_position + 0.1]["indexes"].append(trace_features[0][index])
        return d_events

    def __get_extra_cable(self, pbo):
        return 10
        # if pbo.function == "PBO":
        #     if pbo.support == "CHAMBRE":
        #         return 5
        # elif "PEC" in pbo.function:
        #     if pbo.support == "CHAMBRE":
        #         return 10
        # return 0

    def __get_cable_length_from_qgis(self, cable):
        destpbo = cable.dest
        srcpbo = cable.src
        destPBO = self.getPBOWithName(destpbo)
        srcPBO = self.getPBOWithName(srcpbo)
        length = round(cable.length)
        length += self.__get_extra_cable(destPBO)
        length += self.__get_extra_cable(srcPBO)
        l = [length]
        while srcpbo != self.proj.sro:
            for c in self.proj.cable:
                if c.dest == srcpbo:
                    srcpbo = c.src
                    destpbo = c.dest
                    qsriputils.print_rip(f"{srcpbo}-{destpbo}")
                    destPBO = self.getPBOWithName(destpbo)
                    srcPBO = self.getPBOWithName(srcpbo)
                    length = round(c.length)
                    # print(f"cable: {c.length} src: {srcpbo} {srcPBO} dest: {destpbo} {destPBO}")
                    length += self.__get_extra_cable(destPBO)
                    if srcPBO:
                        length += self.__get_extra_cable(srcPBO)

                    if srcpbo == self.proj.sro:
                        length += 10
                    l.append(length)
                    break
        return list(reversed(l))

    def comboIcon(self, icon1, icon2):
        comboPixmap = QPixmap(50, 25)
        path = os.getcwd()
        firstImage = QImage(os.path.join(path, "images", icon1))
        secondImage = QPixmap(os.path.join(path, "images", icon2))
        painter = QPainter(comboPixmap)
        painter.drawImage(0, 0, firstImage.scaled(25, 25, Qt.KeepAspectRatio))
        # painter.drawPixmap(firstImage.width(), 0, secondImage)
        return QIcon(comboPixmap)

    # class QQWebView(QWebView):
    #     def __init__(self, printer, url):
    #         super(QWebView, self).__init__()
    #         self.printer = printer
    #         self.setUrl(QUrl.fromLocalFile(url))
    #         self.execpreview('done')
    #     def execpreview(self, ok):
    #         print(ok)
    #         pix = QPixmap(self.size())
    #         painter = QtGui.QPainter(pix)
    #         self.render(painter)
    #         pix.save("template/test1.png", 'png')
    #         painter.end()

    @QtCore.pyqtSlot()
    def export_all_excel(self):
        if self.proj != None:
            pbos = self.proj.pbo
            issues_ = []
            for pbo in pbos:
                issues_.extend(self.get_issue(pbo.name))
                issues_.extend(self.get_issue(pbo.name, "vqse"))
                issues_.extend(self.get_issue(pbo.name, "sor"))

            filename = QtGui.QFileDialog.getSaveFileName(self, 'Save File', ""'.xlsx', '(*.xlsx)')
            qsriputils.print_rip(filename)
            if filename[0] != '':
                workbook = xlsxwriter.Workbook(filename[0])
                worksheet = workbook.add_worksheet()
                worksheet.write(0, 0, 'No')
                worksheet.write(0, 1, 'Etat')
                worksheet.write(0, 2, 'Date')
                worksheet.write(0, 3, 'Time')
                worksheet.write(0, 4, 'Qui')
                worksheet.write(0, 5, 'Comment')

                vqse_error = ll['VQSE_error']

                row = 1
                for issue in issues_:
                    current_pbo = issue.pbo
                    if issue.issue_type != 'vqse':
                        history = issue.last_update
                        for h in history:
                            worksheet.write(row, 0, current_pbo)
                            if isinstance(h, dict):
                                d = h["date"]
                                worksheet.write(row, 1, ll['etat_list'][h["etat"]])
                                worksheet.write(row, 4, h["user"])
                                if "log" in h:
                                    worksheet.write(row, 5, h["log"])
                            worksheet.write(row, 2, d.strftime('%d-%m-%Y'))
                            worksheet.write(row, 3, d.strftime('%H:%M:%S'))
                            row += 1
                    else:
                        worksheet.write(row, 0, current_pbo)
                        worksheet.write(row, 1, vqse_error[issue.status])
                        worksheet.write(row, 2, issue.create_date.strftime('%d-%m-%Y'))
                        worksheet.write(row, 3, issue.create_date.strftime('%H:%M:%S'))
                        worksheet.write(row, 4, issue.username)
                        if hasattr(issue, 'type'):
                            worksheet.write(row, 5, issue.text)
                        else:
                            worksheet.write(row, 5, '')
                        row += 1
                workbook.close()

    def sendAttach(self):
        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.AnyFile)
        if dlg.exec():
            filepath = dlg.selectedFiles()
            if len(filepath) > 0:
                filename = os.path.basename(filepath[0])
                import random
                import string
                filename = ''.join(random.choice(string.ascii_lowercase) for _ in range(10)) + filename
                bucket = storage.bucket()
                blob = bucket.blob(filename)
                blob.upload_from_filename(filepath[0])

                c = Chat()
                c.date_create = time.time_ns()
                c.user_name = self.window.user.displayName
                c.user_id = self.window.user.userId
                c.proj_id = self.proj._id
                c.pbo = self.pbo_comment
                c.file = filename
                c.user_read = [self.window.user.userId]

                qsriputils.db.collection('chats').document().set({
                    'user_name': c.user_name,
                    'user_id': c.user_id,
                    'proj_id': c.proj_id,
                    'pbo': c.pbo,
                    'file': c.file,
                    'date_create': c.date_create,
                    'user_read': c.user_read
                })

                self.__showPBOComment(self.pbo_comment)

    def downloadAttach(self, item):
        bucket = storage.bucket()
        server_filename = item.toString()
        filepath = QtGui.QFileDialog.getSaveFileName(self, 'Save File')
        blob = bucket.blob(server_filename)
        if filepath[0] != '':
            blob.download_to_filename(filepath[0])

    def setResultText(self, i):
        self.CBOLabel.setText(self.rows[i][3])
        self.CBDLabel.setText(self.rows[i][4])
        self.TubeOLabel.setText(self.rows[i][5])
        self.TubeDLabel.setText(self.rows[i][6])
        self.BTubeOLabel.setText(self.rows[i][7])
        self.BTubeDLabel.setText(self.rows[i][8])
        self.NumFibOLabel.setText(self.rows[i][9])
        self.NumFibDLabel.setText(self.rows[i][10])
        self.EtatBox.setCurrentText(self.rows[i][11])

    def epissFilterChange(self, index):
        self.CodeCasBox.setCurrentIndex(index)
        self.FaceCasBox.setCurrentIndex(index)
        self.PosCasBox.setCurrentIndex(index)
        self.setResultText(index)

    def updateResult(self):
        conn = qsriputils.db_connect(os.path.join(const.dbpath, 'qsrip.sqlite'))
        if conn:
            code_boite = self.CodeBoiteLabel.text()
            code_cas = self.CodeCasBox.currentText()
            face_cas = self.FaceCasBox.currentText()
            pos_cas = self.PosCasBox.currentText()
            etat = self.EtatBox.currentText()
            cbo = self.CBOLabel.text()
            cbd = self.CBDLabel.text()
            tube_o = self.TubeOLabel.text()
            tube_d = self.TubeDLabel.text()
            b_tube_o = self.BTubeOLabel.text()
            b_tube_d = self.BTubeDLabel.text()
            num_fib_o = self.NumFibOLabel.text()
            num_fib_d = self.NumFibDLabel.text()
            #
            sql_query = f"UPDATE {self.proj.csv} SET ETAT=\"{etat}\" WHERE CODE_BOITE LIKE \"%{code_boite}%\" " \
                        f"AND CODE_CAS=\"{code_cas}\" AND FACE_CAS=\"{face_cas}\" " \
                        f"AND POS_CAS=\"{pos_cas}\" AND CB_O=\"{cbo}\" " \
                        f"AND CB_D=\"{cbd}\" AND TUBE_O=\"{tube_o}\" " \
                        f"AND TUBE_D=\"{tube_d}\" AND B_TUBE_O=\"{b_tube_o}\" " \
                        f"AND B_TUBE_D=\"{b_tube_d}\" AND NUM_FIB_O=\"{num_fib_o}\" " \
                        f"AND NUM_FIB_D=\"{num_fib_d}\";"
            conn.cursor().execute(sql_query)
            conn.commit()
            conn.close()

    def view_3d(self):
        self.mapSysFrame.setHidden(True)
        self.mapSysGraph.setHidden(True)
        self.View3DFrame.setHidden(False)
        browser = QWebView(self.View3DFrame)
        # browser.resize(self.View3DFrame.width() // 2.12, self.View3DFrame.height() // 1.65)
        browser.resize(self.View3DFrame.size())
        browser.load(QUrl("http://localhost:8000"))
        browser.show()

    def view_bim(self):
        self.mapSysFrame.setHidden(True)
        self.mapSysGraph.setHidden(True)
        self.View3DFrame.setHidden(False)
        browser = QWebView(self.View3DFrame)
        # browser.resize(self.View3DFrame.width() // 2.12, self.View3DFrame.height() // 1.65)
        browser.resize(self.View3DFrame.size())
        # browser.load(QUrl("https://autode.sk/3fcwLXo"))
        browser.load(QUrl('https://adjam93.github.io/threejs-model-viewer/'))
        browser.reload()
        browser.show()