import firebase_admin
import os

from firebase_admin import credentials, firestore, storage

import env
import session

from . import cipher_utils


def connect_firebase() -> None:
    db_cert = cipher_utils.decrypt_db_cert(os.path.join(env.PATH["cert_path"], env.ENCRYPTED_DB_CERT_FILE))
    cred = credentials.Certificate(db_cert)
    firebase_admin.initialize_app(cred, {"storageBucket": env.DB_BUCKET_URI})
    session.DB = firestore.client()
    session.BUCKET = storage.bucket()


def get_config() -> None:
    config = session.DB.collection("config").stream()
    for doc in config:
        session.CONFIG[doc.id] = doc.to_dict()


def receive_user_license(user_id: str):
    db = firestore.client()
    user = db.collection("users").document(user_id).get().to_dict()
    if "license" in user:
        user_license = db.collection("license").document(user["license"]).get().to_dict()
        user_license["id"] = user["license"]
        return user_license
    return None


def update_user(info: dict) -> None:
    session.DB.collection("users").document(session.USER.id).set(info, merge=True)
