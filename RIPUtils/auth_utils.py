from datetime import datetime

import env
from . import db_utils


def license_check(user_id: str) -> tuple:
    user_license = db_utils.receive_user_license(user_id)
    if user_license is None or (user_license["type"] == "Personal" and user_license["id"] != env.UUID):
        return None, "Unlicensed"
    elif datetime.strptime(user_license["expired_time"], "%m/%d/%Y") < datetime.now():
        return None, "License expired"
    else:
        return user_license["id"], "License valid"
