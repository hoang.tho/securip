from base64 import b64decode
from Crypto.Hash import SHA1
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
import json

import env


def verify_license(license_path) -> tuple:
    license_file = open(license_path)
    license_info = json.load(license_file)
    message = f"{license_info['type']}_{license_info['expired_time']}"
    if license_info['type'] == 'Personal':
        message = f"{env.UUID}_{message}"
    else:
        message = f"{license_info['key']}_{message}"
    key = RSA.import_key(env.LICENSE_PUBKEY)
    hashed_message = SHA1.new(message.encode("ascii"))
    try:
        pkcs1_15.new(key).verify(hashed_message, b64decode(license_info["signature"]))  # Ignore: SHA1Hash is valid
        return "Valid", license_info
    except Exception as e:
        print('Verify failed:', e)
        return "Invalid", license_info
