import os
import smtplib

from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

import env


MAIL_SUBJECT = {
    "Crash": "Crash log report."
}
MAIL_CONTENT = {
    "Crash": "Hello,\n"
             "Application had crashed.\n"
             "Please check crash log file for more information.\n"
             "Thank You."
}


def send_mail(mail_type: str, date: datetime, log_path: str = "") -> None:
    message = MIMEMultipart()
    message["From"] = env.MAIL_SENDER_ADDR
    message["To"] = env.MAIL_RECEIVER_ADDR
    message["Subject"] = MAIL_SUBJECT[mail_type]
    message.attach(MIMEText(MAIL_CONTENT[mail_type], "plain"))

    if os.path.exists(log_path):
        attach_file = open(log_path, "rb")
        payload = MIMEBase("application", "octate-stream")
        payload.set_payload(attach_file.read())
        encoders.encode_base64(payload)
        payload.add_header(
            "Content-Disposition",
            f"attachment; filename=\"crash_log_{date.strftime('%m%d%Y_%H%M%S')}.txt\"",
        )
        message.attach(payload)

    session = smtplib.SMTP(env.SMTP_ADDR, env.SMTP_PORT)
    session.starttls()
    session.login(env.MAIL_SENDER_ADDR, env.MAIL_SENDER_PWD)
    text = message.as_string()
    session.sendmail(env.MAIL_SENDER_ADDR, env.MAIL_RECEIVER_ADDR, text)
    session.quit()
