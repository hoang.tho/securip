import json

from base64 import b64encode, b64decode
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA1

from env import DB_KEY, LICENSE_PUBKEY


def encrypt_db_cert(file: str = '') -> str:
    db_cert = open(file, 'rb').read()
    db_cert += b' ' * (len(DB_KEY) - len(db_cert) % len(DB_KEY))
    init_vector = b'SecurRipSecurRip'
    cipher = Cipher(algorithms.AES(DB_KEY), modes.CBC(init_vector))
    encryptor = cipher.encryptor()
    encrypted_db_cert = b64encode(encryptor.update(db_cert) + encryptor.finalize()).decode('ascii')
    return encrypted_db_cert


def decrypt_db_cert(file: str = '') -> dict:
    encrypted_db_cert = b64decode(open(file, 'r').read())
    init_vector = b'SecurRipSecurRip'
    cipher = Cipher(algorithms.AES(DB_KEY), modes.CBC(init_vector))
    decryptor = cipher.decryptor()
    db_cert = json.loads((decryptor.update(encrypted_db_cert) + decryptor.finalize()).decode('ascii'))
    return db_cert


def verify_license(msg: str, signature: str) -> bool:
    key = RSA.import_key(LICENSE_PUBKEY)
    hashed_msg = SHA1.new(msg.encode("ascii"))
    try:
        pkcs1_15.new(key).verify(hashed_msg, b64decode(signature))  # Ignore: SHA1Hash is valid
        return True
    except Exception as e:
        print('Verify failed:', e)
        return False
