import os
import sys
import subprocess
import traceback

from datetime import datetime
from urllib.request import urlopen

import session
from . import qt_utils, mail_utils, db_utils
from firebase_admin import storage

import const
import env
import qsriputils


def detect_vm() -> None:
    serial = subprocess.check_output("WMIC BIOS GET SERIALNUMBER").split(b"\n")[1].split()[0].decode("ascii")
    model = subprocess.check_output("WMIC COMPUTERSYSTEM GET MODEL").split(b"\n")[1].split()[0].decode("ascii")
    mnft = subprocess.check_output("WMIC COMPUTERSYSTEM GET MANUFACTURER").split(b"\n")[1].split()[0].decode("ascii")
    print(f"System info: {serial} {model} {mnft}")
    if serial == "0" or model in env.VM_MODELS or mnft in env.VM_MNFTS:
        qt_utils.invalid_app("Virtual machine detected!", "SecurRip")


def crash_handle(exec_type, value, tb) -> None:
    date = datetime.now()
    log_path = os.path.join(env.PATH["logs_path"], f"crash_{date.strftime('%m%d%Y_%H%M%S')}.log")
    log_file = open(log_path, "w")
    log_file.write(f"Crash:{date.strftime('%m/%d/%Y %H:%M:%S')}\n")
    if session.USER is not None:
        db_utils.update_user({"isOnline": False})
        log_file.write(f"User: {session.USER.id}\n")
        log_file.write(f"License: {session.USER.license}\n")
    tb_lines = traceback.format_exception(exec_type, value, tb)
    for line in tb_lines:
        log_file.write(line)
    log_file.close()
    if env.APP_MODE == "prod":
        mail_utils.send_mail("Crash", date, log_path)
    else:
        print(f"Crash:{date.strftime('%m/%d/%Y %H:%M:%S')}")
        if session.USER is not None:
            print(f"User: {session.USER.id}")
            print(f"License: {session.USER.license}")
        for line in tb_lines:
            print(line, end="")
    sys.exit()


def is_online():
    try:
        urlopen("https://google.com")
        return True
    except Exception as e:
        print(f"Network failed: {e}")
        return False


def load_csv(projs):
    qsriputils.print_rip('load csv')
    for proj in projs:
        if hasattr(proj, 'csv'):
            filename = proj.csv + '.csv'
            bucket = storage.bucket()
            blob = bucket.blob(filename)
            if blob.exists():
                csv_dir = proj.jpg_path.replace('jpg', 'csv')
                if not os.path.exists(csv_dir):
                    os.mkdir(csv_dir)
                csv_path = os.path.join(csv_dir, filename)
                blob.download_to_filename(csv_path)
                conn = qsriputils.db_connect(os.path.join(const.dbpath, 'qsrip.sqlite'))
                if conn:
                    with open(csv_path, 'r') as f:
                        cols = f.readline().replace(';', ',').strip()
                        table_name = proj.csv.replace('-', '_')
                        sql_drop_table = 'DROP TABLE IF EXISTS {};'.format(table_name)
                        conn.cursor().execute(sql_drop_table)
                        conn.commit()
                        sql_create_csv_table = 'CREATE TABLE IF NOT EXISTS {} ({});'.format(table_name, cols)
                        qsriputils.create_table(conn, sql_create_csv_table)
                        for line in f.readlines():
                            values = ','.join(['"' + i + '"' for i in line.strip().split(';')])
                            sql_query = 'INSERT INTO {} ({}) VALUES ({});'.format(table_name, cols, values)
                            conn.cursor().execute(sql_query)
                    conn.commit()

