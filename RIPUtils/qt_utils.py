import os
import sys

from PyQt5 import QtCore
from PyQt5.QtCore import Qt, QObject
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QDialog, QPushButton

import env


def invalid_app(msg: str, title: str = "") -> None:
    app = QApplication([])
    app.setWindowIcon(QIcon(os.path.join(env.PATH["images_path"], "securrip.ico")))
    window = QWidget()
    window.resize(250, 100)
    window.setWindowTitle(title)
    label = QLabel(msg)
    label.setAlignment(Qt.AlignCenter)
    layout = QVBoxLayout()
    layout.addWidget(label)
    window.setLayout(layout)
    window.show()
    app.exec()
    sys.exit()


def msg_dialog(msg: str, title: str = "") -> QDialog:
    dlg = QDialog()
    dlg.resize(200, 100)
    dlg.setWindowTitle(title)
    dlg.setWindowFlags(Qt.Window | Qt.WindowCloseButtonHint)
    label = QLabel(msg)
    label.setAlignment(Qt.AlignCenter)
    layout = QVBoxLayout()
    layout.addWidget(label)
    dlg.setLayout(layout)
    return dlg


def btn_dialog(msg: str = "", buttons=None, title: str = "") -> QDialog:
    if buttons is None:
        buttons = dict()
    dlg = QDialog()
    dlg.setWindowTitle(title)
    dlg.setWindowFlags(Qt.Window | Qt.WindowCloseButtonHint)
    label = QLabel(msg)
    label.setAlignment(Qt.AlignLeft)
    layout = QVBoxLayout()
    layout.setSpacing(8)
    layout.setContentsMargins(20, 10, 20, 10)
    layout.addWidget(label)
    for button in buttons:
        btn = QPushButton(button)
        btn.clicked.connect(buttons[button])
        layout.addWidget(btn)
    dlg.setLayout(layout)
    return dlg
