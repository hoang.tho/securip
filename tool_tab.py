import datetime
import subprocess

from PyQt5 import QtWidgets, QtCore, uic
from qsripproject import QSRIPProject
f = open("lang.txt", 'r')
from multilang import english, french
if f.read() == 'en':
    ll = english
else:
    ll = french
f.close()

import os
import json
import env
from RIPUtils import qt_utils


class ToolTab(QtWidgets.QFrame):
    proj: QSRIPProject = None
    window: None

    def __init__(self, w=None):
        super(ToolTab, self).__init__()
        uic.loadUi('ui/tool.ui', self)

        self.label.setText(ll['CFI_Tech_Tools'])
        self.label_7.setText(ll['file_for_import'])
        self.buttonSelectQGIS.setText(ll['select_qgis_display'])
        self.btnStart.setText(ll['imp'])
        # self.buttonTopo.setText(ll['Topo'])
        # self.buttonNom.setText(ll['Nom'])
        # self.buttonSoudure.setText(ll['Soudure'])
        self.buttonLoadStyle.setText(ll['Grace'])
        # self.buttonLoadStyle.setDisabled(True)
        # self.buttonRename.setText(ll['Rename'])
        # self.buttonXlsxtoShape.setText(ll['Xlsx_to_Shape'])
        # self.buttonQGIStoCAPFT.setText(ll['Qgis_to_Capft'])
        # self.buttonQGIStoCOMAC.setText(ll['Qgis_to_COMAC'])

        self.buttonSelectQGIS.clicked.connect(self.select_qgis)
        self.buttonLoadStyle.clicked.connect(self.load_style)

        self.qgis_path = None
        with open(os.path.join(env.PATH['tools_path'], 'config.json')) as config_file:
            self.config = json.load(config_file)

        self.window = w
    def updateText(self, lang):
        global ll
        ll = lang

        self.label.setText(ll['CFI_Tech_Tools'])
        self.label_7.setText(ll['file_for_import'])
        self.buttonSelectQGIS.setText(ll['select_qgis_display'])
        self.btnStart.setText(ll['imp'])
        # self.buttonTopo.setText(ll['Topo'])
        # self.buttonNom.setText(ll['Nom'])
        # self.buttonSoudure.setText(ll['Soudure'])
        self.buttonLoadStyle.setText(ll['Grace'])
        # self.buttonRename.setText(ll['Rename'])
        # self.buttonXlsxtoShape.setText(ll['Xlsx_to_Shape'])
        # self.buttonQGIStoCAPFT.setText(ll['Qgis_to_Capft'])
        # self.buttonQGIStoCOMAC.setText(ll['Qgis_to_COMAC'])

    def select_qgis(self):
        options = QtWidgets.QFileDialog.Options()
        path = QtWidgets.QFileDialog.getExistingDirectory(None, ll['select_Qgis_folder'], "",
                                                          options=options)
        if path:
            self.qgis_path = path
            self.buttonSelectQGIS.setText(path.split('/')[-1])

    def load_style(self):
            if self.config['converter']['required_input'] and self.qgis_path is None:
                require_dialog = qt_utils.msg_dialog("Please select qgis folder", "SecurRip")
                require_dialog.exec()
            elif self.config['converter']['required_input']:
                # run script with qgis folder
                now = datetime.datetime.now().strftime('%m%d%Y_%H%M%S')
                try:
                    subprocess.check_output(f"python {env.PATH['tools_path']}\\converter\\SRIP_ConvertEiffage.py \"{self.qgis_path}\" > {env.PATH['tools_path']}\\converter\\log_{now}.txt", stderr=subprocess.STDOUT, shell=True)
                    log_file = open(f"{env.PATH['tools_path']}\\converter\\log_{now}.txt")
                    result = log_file.read()
                except subprocess.CalledProcessError as e:
                    result = e.output.decode()
                finish_dialog = qt_utils.msg_dialog(result, "SecurRip")
                finish_dialog.exec()
                pass
            else:
                # run script without qgis folder
                pass
