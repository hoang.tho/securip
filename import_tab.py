import os
from datetime import datetime
from PyQt5 import QtWidgets, QtCore, uic, QtPrintSupport
from PyQt5.QtCore import QSettings, Qt, QTimer
from PyQt5.Qt import QProgressDialog
from PyQt5.QtWidgets import QFileDialog
from firebase_admin import storage

import env
import resources_rc
from pyOTDR import sorparse

from RIPUtils import system_utils
from qsripproject import QSRIPProject, QSRIPIssue, Cable, PBO, PointTechnique
from collections import deque
import numpy as np
import OTDRlib
from qgis.core import QgsVectorLayer, QgsFeatureRequest, QgsProject, NULL
import const
import qsriputils
import runpy
import shutil
from SORMeta import SORMeta, SORSection
from pathlib import Path
from random import randint
f = open("lang.txt", 'r')
from multilang import english, french
if f.read() == 'en':
    ll = english
else:
    ll = french
f.close()

import csv


class ImportTab(QtWidgets.QFrame):
    proj: QSRIPProject = None
    qgisProjectPath: str = ''
    window = None
    taskFinished = QtCore.pyqtSignal()

    def __init__(self, window=None):
        super(ImportTab, self).__init__()
        uic.loadUi("ui/import.ui", self)

        self.label_7.setText(ll['file_for_import'])
        self.buttonSelectQGIS.setText(ll['select_qgis_display'])
        self.buttonSelectSOR.setText(ll['select_sor_display'])
        self.buttonSelectJPG.setText(ll['select_jpg_display'])
        self.btnImport.setText(ll['imp'])
        self.label.setText(ll['Import_result'])
        self.buttonSelectCSV.setText(ll['Select_CSV'])
        self.buttonSelectQGIS.clicked.connect(self.selectQGIS)
        self.buttonSelectSOR.clicked.connect(self.selectSOR)
        self.buttonSelectJPG.clicked.connect(self.selectJPG)
        self.buttonSelectCSV.clicked.connect(self.selectCSV)
        self.btnImport.clicked.connect(self.importSlot)
        self.buttonPdf.clicked.connect(self.export_pdf)
        self.window = window
        self.proj = self.window.activeProject()
        self.customer_value = {}
        # qsriputils.update_count("project", "total")

    def updateText(self, lang):
        global ll
        ll = lang

        self.label_7.setText(ll['file_for_import'])
        self.buttonSelectQGIS.setText(ll['select_qgis_display'])
        self.buttonSelectSOR.setText(ll['select_sor_display'])
        self.buttonSelectJPG.setText(ll['select_jpg_display'])
        self.btnImport.setText(ll['imp'])
        self.label.setText(ll['Import_result'])
        self.buttonSelectCSV.setText(ll['Select_CSV'])

    def get_project_path(self):
        path = os.getcwd()
        return os.path.join(path, "projects")

    @QtCore.pyqtSlot()
    def export_pdf(self):
        printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        dialog.paintRequested.connect(self.textBrowser.document().print_)
        dialog.exec_()

    def update_active_project(self, p):
        self.proj = p

    def taskDone(self):
        self.progressBar.setValue(100)
        self.progressBar.close()
        if self.timer:
            self.timer.stop()

    def showProgress(self, text):
        self.progressBar = QProgressDialog(text, None, 0, 0, self.window)
        self.progressBar.setWindowTitle(ll['please_wait'])
        self.progressBar.setWindowModality(Qt.WindowModal)
        window_flags = self.progressBar.windowFlags()  # Disable close button
        self.progressBar.setWindowFlags(window_flags & ~Qt.WindowCloseButtonHint)
        self.progressBar.setWindowFlags(window_flags & ~Qt.WindowContextHelpButtonHint)
        self.progressBar.setModal(True)
        self.progressBar.setRange(0, 100)
        self.progressBar.setMinimumDuration(0)
        self.progressBar.setAutoClose(False)
        self.progressBar.show()
        self.timer = QTimer(self, timeout=self.onTimeout)
        self.timer.start(randint(1, 3) * 100)

    def onTimeout(self):
        if self.progressBar.value() >= 100:
            self.progressBar.setValue(0)
        self.progressBar.setValue(self.progressBar.value() + 1)
        QtWidgets.qApp.processEvents(QtCore.QEventLoop.ExcludeUserInputEvents)

    @QtCore.pyqtSlot()
    def importSlot(self):
        qsriputils.print_rip("importSlot")
        if not self.proj:
            qsriputils.showMessage(ll['select_Qgis'])
        else:
            self.textBrowser.clear()
            # qsriputils.showMessage('Run script: {}'.format("\n".join(const.scripts)))
            script = const.preimport_script
            script_path = qsriputils.resolve("scripts")
            script_filepath = os.path.join(script_path, script)
            text = "QGIS project: <b>{}</b>".format(self.proj.project)
            self.textBrowser.append(text)
            text = "Run script: <b>{}</b><br/>Script path: <i>{}</i>".format(script, script_filepath)
            self.textBrowser.append(text)
            self.showProgress(ll['contrl_progress'])
            self.taskFinished.connect(self.taskDone)
            import threading
            ex_thread = threading.Thread(target=self.run_ex_script)
            ex_thread.start()

    def run_ex_script(self):
        try:
            if os.path.exists(os.path.join(env.PATH["box_path"], env.PREIMPORT_SCRIPT)):
                script_filepath = os.path.join(env.PATH["box_path"], env.PREIMPORT_SCRIPT)
            else:
                script_filepath = ""
            run = runpy.run_path(script_filepath, init_globals={"qgis_path": self.proj.qgis_path},
                                 run_name="__main__")
            # exec(open(r"{}".format(script_filepath).encode("utf-8")).read())
            # MyPBar.close()
            #     qsriputils.print_rip("runpy:", str(run))
            if "exception" in run:
                self.textBrowser.append(str(run["exception"]))
            else:
                output = run["output"]
                for line in output:
                    self.log(str(line))

        except Exception as e:
            import traceback
            self.log(traceback.format_exc())
        self.taskFinished.emit()

    @QtCore.pyqtSlot()
    def selectQGIS(self):
        try:
            options = QtWidgets.QFileDialog.Options()
            path = QtWidgets.QFileDialog.getExistingDirectory(None, ll['select_Qgis_folder'], "",
                                                              options=options)
            if path:
                self.qgisProjectPath = path
                qsriputils.print_rip("qgis path", self.qgisProjectPath)
                text = f"{ll['import_qgis']} <i>{self.qgisProjectPath}</i>"
                self.textBrowser.append(text)
                layerFound = 0
                path = self.qgisProjectPath
                self.proj = QSRIPProject()
                for file in os.listdir(path):
                    if file.endswith(".shp"):
                        if 'SITE' in file:
                            siteLayer = QgsVectorLayer(os.path.join(path, file))
                            for f in siteLayer.getFeatures():
                                self.proj.project = str(f["GESTIONNAI"])
                                self.proj.sro = str(f["NOM"])
                                nro = qsriputils.nro_from_sro(self.proj.sro)
                                if nro is None:
                                    nro = f["RATTACH"]
                                self.proj.nro = nro
                                break
                            layerFound += 1

                        elif 'CABLE_OPTIQUE' in file:
                            self.proj.cable_layer = file
                            layerFound += 1
                        elif 'BOITE_OPTIQUE' in file:
                            self.proj.boite_layer = file
                            layerFound += 1
                        elif 'POINT_TECHNIQUE' in file:
                            self.proj.pt_layer = file
                            layerFound += 1
                        elif 'ZASRO' in file and file.endswith('.shp'):
                            zasroLayer = QgsVectorLayer(os.path.join(path, file))
                            for f in zasroLayer.getFeatures():
                                self.proj.client = str(f["SST"])
                                if self.proj.client == "ORANGE":
                                    self.proj.nro = f["REF_NRO"]
                                break
                            layerFound += 1
                        if layerFound == 5:
                            break
                if layerFound < 5:
                    qsriputils.showMessage(
                        f"{ll['five_layers']} SITE, ZASRO, CABLE_OPTIQUE, POINT_TECHNIQUE {ll['And']} BOITE_OPTIQUE")
                else:
                    text = const.project_text.format(ll['projectReport'],self.proj.project)
                    self.log(text)
                    text = f"SRO: <b>{self.proj.sro}</b>"
                    self.log(text)
                    text = const.client_text.format(ll['sociReport'],self.proj.client)
                    self.log(text)
                    for file in os.listdir(path):
                        if file.endswith(".shp"):
                            text = const.add_layer.format(ll['Added_Layer'],file)
                            self.log(text)
                    self.proj.create_date = datetime.now()
                    self.proj.user_id = self.window.user.userId
                    self.proj.type = "APD" if self.checkboxAPD.isChecked() else "DOE"
                    v = 0
                    for p in self.window.projs:
                        if p.project == self.proj.project and p.client == self.proj.client and p.sro == self.proj.sro and p.type == self.proj.type:
                            if p.ver > v:
                                v = p.ver

                    self.proj.ver = v + 1
                    self.copyProjectQgis(self.proj)
                    self.proj._id = qsriputils.create_project(self.proj)
                    self.window.add_project(self.proj)
                    # qsriputils.db.collection('proj').document(self.proj._id).set({'info': self.proj.info}, merge=True)
                    cable_status = self.save_cables_to_server()
                    pbo_status = self.save_pbo_to_server()

                    if pbo_status == 0 and cable_status == 0:
                        conn = qsriputils.db_connect(os.path.join(const.dbpath, "qsrip.sqlite"))
                        qsriputils.set_config(conn, f"{const.key_last_opened_project}_{self.window.user.userId}",
                                              self.proj._id)
                        conn.close()
                        self.log(
                            f"{ll['done_add_pro']} {self.proj.project} {self.proj.sro} {self.proj.type} V{self.proj.ver}")
                    else:
                        dlg = QtWidgets.QDialog()
                        dlg.resize(100, 100)
                        dlg.setWindowTitle(ll['imp'])
                        lb = QtWidgets.QLabel(ll['import_error'])
                        box = QtWidgets.QVBoxLayout()
                        box.addWidget(lb)
                        box.setStretch(0, 100)
                        box.setStretch(1, 1)
                        dlg.setLayout(box)
                        dlg.exec()
                        self.log(
                            f'{ll["import_pro_failed"]} {self.proj.project} {self.proj.sro} {self.proj.type} V{self.proj.ver}')
                        self.window.is_logout = True
        except Exception as e:
            import traceback
            qsriputils.print_rip(f"Import exception {e}")
            qsriputils.print_rip(traceback.format_exc())

    def log(self, text):
        self.textBrowser.append(text)
        QtWidgets.qApp.processEvents(QtCore.QEventLoop.ExcludeUserInputEvents)

    def on_project_changed(self):
        qsriputils.print_rip("import tab: on_project_changed")
        self.proj = self.window.activeProject()

    def save_cables_to_server(self):
        self.log(ll['saving_cable'])

        layer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.cable_layer)[0])[0]
        if layer:
            nomIdx = layer.fields().indexFromName("NOM")
            srcIdx = layer.fields().indexFromName("ORIGINE")
            destIdx = layer.fields().indexFromName("EXTREMITE")
            capacityIdx = layer.fields().indexFromName("CAPACITE")
            typeIdx = layer.fields().indexFromName("TYPE_STRUC")
            lenIdx = layer.fields().indexFromName("LGR_CARTO")
            secIdx = layer.fields().indexFromName("SECTION")
            request = QgsFeatureRequest().setSubsetOfAttributes(
                [nomIdx, secIdx, srcIdx, destIdx, capacityIdx, typeIdx, lenIdx])
            cables = []
            issues = []
            ver = f"{self.proj.type} V{self.proj.ver}"
            cableKeys = []
            for f in layer.getFeatures(request):
                c = Cable()
                c.name = f["NOM"]
                c.src = f["ORIGINE"]
                c.length = f["LGR_CARTO"]
                if c.length == 0.0:
                    continue
                if c.src == NULL:
                    continue
                c.dest = f["EXTREMITE"]
                key = f"{c.src} {c.dest}"
                if key in cableKeys:
                    issue = QSRIPIssue(pbo=c.dest, code=const.co_double_error_key,
                                       text=const.customError[const.co_double_error_key])
                    issue.level = 1
                    issue.fromProject(p=self.proj, u=self.window.user)
                    issues.append(issue)
                else:
                    cableKeys.append(key)
                qsriputils.print_rip(f"cable: {c.name} capacite: {f['CAPACITE']}")
                c.capacity = int(f["CAPACITE"]) if f["CAPACITE"] is not None else 0
                c.type = f["TYPE_STRUC"]
                # c.length = round(f.geometry().length())

                c.section = f["SECTION"]
                if c.section == NULL:
                    c.section = ""
                c.sro = f"{self.proj.client} {self.proj.project} {self.proj.sro}"
                c.ver = ver
                cables.append(c)
                if c.dest.startswith("SRip_"):
                    issue = QSRIPIssue(pbo=c.dest, code=const.srip_error_key,
                                       text=const.customError[const.srip_error_key])
                    issue.level = 1
                    issue.fromProject(p=self.proj, u=self.window.user)
                    issues.append(issue)
            ic = len(issues)
            if ic > 0:
                self.proj.tested += ic
                self.proj.warn += ic
                qsriputils.create_issues(issues=issues, proj_id=self.proj._id, client=self.proj.client)
                qsriputils.update_project_data(self.proj, {"tested": self.proj.tested, "warn": self.proj.warn})
                self.proj.issues = issues
            self.proj.cable = cables
            return qsriputils.save_cables(cables, self.proj, proj_id=self.proj._id)

    def save_pbo_to_server(self):
        self.log(ll['saving_point'])
        player = QgsVectorLayer(os.path.join(self.proj.qgis_path, self.proj.pt_layer))
        modelIdx = player.fields().indexFromName("MODELE")
        layer = QgsProject.instance().mapLayersByName(os.path.splitext(self.proj.boite_layer)[0])[0]
        if layer:
            nomIdx = layer.fields().indexFromName("NOM")
            cabIdx = layer.fields().indexFromName("AMONT")
            refIdx = layer.fields().indexFromName("REFERENCE")
            funcIdx = layer.fields().indexFromName("FONCTION")
            supIdx = layer.fields().indexFromName("SUPPORT")
            typeIdx = layer.fields().indexFromName("TYPE_STRUC")
            thdIdx = layer.fields().indexFromName("ThdCodeExt")
            request = QgsFeatureRequest().setSubsetOfAttributes(
                [nomIdx, cabIdx, refIdx, funcIdx, supIdx, typeIdx, thdIdx])
            pbo = []
            issues = []
            ver = f"{self.proj.type} V{self.proj.ver}"

            for f in layer.getFeatures(request):
                p = PBO()
                p.name = f["NOM"]
                p.pbo_id = f.id()
                p.function = f["FONCTION"] if f["FONCTION"] != NULL else ""
                p.support = f["SUPPORT"] if f["SUPPORT"] != NULL else ""
                p.reference = f["REFERENCE"] if f["REFERENCE"] != NULL else ""
                p.cable = f["AMONT"] if f["AMONT"] != NULL else ""
                p.images = []
                if p.cable == NULL:
                    continue
                p.type = f["TYPE_STRUC"] if f["TYPE_STRUC"] != NULL else ""
                if p.thdCodeExt and p.thdCodeExt != NULL:
                    p.thdCodeExt = f["ThdCodeExt"]
                p.sro = f"{self.proj.client} {self.proj.project} {self.proj.sro}"
                p.ver = ver
                lat, long = qsriputils.ConvertL93Wgs84(f.geometry().asPoint())
                p.x = lat
                p.y = long
                pbo.append(p)
                req = QgsFeatureRequest().setSubsetOfAttributes([modelIdx]).setFilterRect(f.geometry().boundingBox())
                for pf in player.getFeatures(req):
                    p.model = pf["MODELE"]
                    break
                if p.name.startswith("SRip_"):
                    issue = QSRIPIssue(pbo=p.name, code=const.srip_error_key,
                                       text=const.customError[const.srip_error_key])
                    issue.level = 1
                    issue.fromProject(p=self.proj, u=self.window.user)
                    issues.append(issue)
            ic = len(issues)
            if ic > 0:
                self.proj.tested += ic
                self.proj.warn += ic
                qsriputils.create_issues(issues=issues, proj_id=self.proj._id, client=self.proj.client)
                qsriputils.update_project_data(self.proj, {"tested": self.proj.tested, "warn": self.proj.warn})
                self.proj.issues.extend(issues)
            self.proj.pbo.extend(pbo)
            return qsriputils.save_pbo(self.proj, pbo)

    def save_pt_to_server(self):
        self.log(ll['saving_point'])
        layer = QgsVectorLayer(os.path.join(self.proj.qgis_path, self.proj.pt_layer))
        if layer:
            nomIdx = layer.fields().indexFromName("NOM")
            codeIdx = layer.fields().indexFromName("CODE")
            modelIdx = layer.fields().indexFromName("MODELE")
            request = QgsFeatureRequest().setSubsetOfAttributes([nomIdx, codeIdx, modelIdx])

            pts = []
            ver = f"{self.proj.type} V{self.proj.ver}"
            sro = f"{self.proj.client} {self.proj.project} {self.proj.sro}"
            for f in layer.getFeatures(request):
                p = PointTechnique()
                p.name = f["NOM"]
                p.code = f["CODE"]
                p.type = f["TYPE_STRUC"]
                p.sro = sro
                p.ver = ver
                lat, long = qsriputils.ConvertL93Wgs84(f.geometry().asPoint())
                p.x = lat
                p.y = long
                pts.append(p)
            self.proj.pts.extend(pts)
            qsriputils.save_pts(self.proj)

    def copyProjectQgis(self, p):
        self.log(ll['copy_data'])
        p.set_project_path(const.project_path)
        # project_path = os.path.join(path, "projects", p.client, p.project, p.sro, "{} V{}".format(p.type, p.ver))
        qsriputils.print_rip("qgis_path", p.project_path)

        if not os.path.exists(p.project_path):
            os.makedirs(p.project_path)
        if os.path.exists(p.qgis_path):
            shutil.rmtree(p.qgis_path)
        shutil.copytree(self.qgisProjectPath, p.qgis_path)
        zipfile = f"{p.sro}_{p.type}_V{p.ver}_qgis.zip"
        zippath = os.path.join(p.project_path, zipfile)
        self.log(f"{ll['creating_zip']} {zipfile}")
        qsriputils.zipdir(p.qgis_path, zippath)
        self.log(f"{ll['upload_zip']} {zipfile} {ll['to_sever']}")
        qsriputils.upload_file(zippath, f"{p.client}_{p.project}_{p.sro}")
        self.log(f"{ll['remove']} {zipfile}")
        os.remove(zippath)

    @QtCore.pyqtSlot()
    def selectJPG(self):
        options = QtWidgets.QFileDialog.Options()
        options != QtWidgets.QFileDialog.DontUseNativeDialog
        options != QtWidgets.QFileDialog.ShowDirsOnly
        jpg_path = QtWidgets.QFileDialog.getExistingDirectory(None, ll['select_photo'], "",
                                                              options=options)
        if jpg_path:
            qsriputils.print_rip("photo path:", jpg_path)
            text = f"{ll['import_photo_path']} <i>{jpg_path}</i><br/> {ll['to_path']} <i>{self.proj.jpg_path}</i>"
            self.textBrowser.append(text)
            pbo = [p.name for p in self.proj.pbo]
            if not os.path.exists(self.proj.jpg_path):
                os.mkdir(self.proj.jpg_path)
            count_jpg = 0
            update_pbo = []
            for f in os.listdir(jpg_path):
                # n = os.path.splitext(f)[0]
                if f.startswith(self.proj.sro):
                    if f not in self.proj.images:
                        self.proj.images.append(f)
                        count_jpg += 1
                        # copy images
                        src = os.path.join(jpg_path, f)
                        dest = os.path.join(self.proj.jpg_path, f)
                        if not os.path.exists(dest):
                            shutil.copyfile(src, dest)
                        qsriputils.update_project_data(self.proj, {"images": self.proj.images})
                else:
                    for p in self.proj.pbo:
                        if f.startswith(p.name):
                            self.log(f" {f} pbo: {p.name}")
                            qsriputils.print_rip(f"f: {f} p.images: {p.images}")
                            if f not in p.images:
                                self.log(f"{ll['add']} {f} pbo: {p.name}")
                                p.images.append(f)
                                update_pbo.append(p)
                                count_jpg += 1
                                # copy images
                                src = os.path.join(jpg_path, f)
                                dest = os.path.join(self.proj.jpg_path, f)
                                if not os.path.exists(dest):
                                    shutil.copyfile(src, dest)
                            break
            if len(update_pbo) > 0:
                qsriputils.update_pbos_image(update_pbo)
            if count_jpg > 0:
                zipfile = f"{self.proj.sro}_{self.proj.type}_V{self.proj.ver}_jpg.zip"
                zippath = os.path.join(self.proj.project_path, zipfile)
                self.log(f"{ll['creating_zip_of_jpg_data']} {zipfile} - {count_jpg} photos")
                qsriputils.zipdir(self.proj.jpg_path, zippath)
                self.log(f"{ll['upload_jpg']}: {zipfile} {ll['to_sever']}")
                qsriputils.upload_file(zippath, f"{self.proj.client}_{self.proj.project}_{self.proj.sro}")
                self.log(f"{ll['remove']} {zipfile}")
                os.remove(zippath)
        self.log(f"{ll['done']}")
        self.window.requestUpdateTab()

    @QtCore.pyqtSlot()
    def selectCSV(self):
        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        if dlg.exec():
            csv_path = dlg.selectedFiles()
            if csv_path[0].endswith('.csv'):
                self.log(ll['load_CSV'])
                bucket = storage.bucket()
                name = '{}_{}_V{}_CSV'.format(self.proj.sro, self.proj.type, self.proj.ver).replace('-', '_')
                blob = bucket.blob(name + '.csv')
                blob.upload_from_filename(csv_path[0])
                qsriputils.db.collection('proj').document(self.proj._id).set({'csv': name}, merge=True)
                setattr(self.proj, "csv", name)
                system_utils.load_csv([self.proj])
                self.log(ll['Import_CSV_done'])
            else:
                self.log(ll['Note_CSV'])


    @QtCore.pyqtSlot()
    def selectSOR(self):
        self.textBrowser.clear()
        options = QtWidgets.QFileDialog.Options()
        sor_path = QtWidgets.QFileDialog.getExistingDirectory(None, ll['Select_SOR_data_folder'], "",
                                                              options=options)
        # self.proj.last_modified = datetime.now()
        if sor_path:
            self.customer_value = qsriputils.get_sor_customer_value()
            qsriputils.print_rip('sor path is', sor_path)

            text = f"{ll['Import_SOR_data_at_path']} <i>{sor_path}</i>"
            self.log(text)
            for f in os.listdir(sor_path):
                self.log(f)
            if os.path.exists(self.proj.sor_path):
                shutil.rmtree(self.proj.sor_path)
            shutil.copytree(sor_path, self.proj.sor_path)
            zipfile = f"{self.proj.sro}_{self.proj.type}_V{self.proj.ver}_sor.zip"
            zippath = os.path.join(self.proj.project_path, zipfile)
            self.log(f"{ll['creating_zip_of_sor_data']} {zipfile}: {ll['folder']} {self.proj.sor_path}")
            qsriputils.zipdir(self.proj.sor_path, zippath)
            self.log(f"{ll['upload_zip']} {zipfile} {ll['to_sever']}")
            qsriputils.upload_file(zippath, f"{self.proj.client}_{self.proj.project}_{self.proj.sro}")
            self.log(f"{ll['remove']} {zippath}")
            os.remove(zippath)
            self.log(ll['compare_sor_vs_qgis_data'])
            self.find_sor_issues()
            self.log(f"{ll['done']}")
            self.window.requestUpdateTab()

    def find_sor_issues(self):
        # sorpbo = os.listdir(self.proj.sor_path)
        allsor = [os.path.join(dp, f) for dp, dn, filenames in os.walk(self.proj.sor_path) for f in filenames if
                  os.path.splitext(f)[1] == '.sor']

        for sor in allsor:
            _, d_meta, _ = sorparse(sor)
            sname = Path(rf"{sor}").name
            (tsname, _) = os.path.splitext(sname)
            qsriputils.print_rip(f"sname: {sname}")
            if len(tsname.split('_')) == 2:
                sro_name, box_name_r = tsname.split('_')
                sro_name = sro_name.strip()
                print(f"box_name_r:{box_name_r}")
                box_name = [i.strip() for i in box_name_r.split('(')][0]

                print(f"box_name:{box_name}")

                name_matched = False
                genParams = d_meta["GenParams"]
                locB = genParams["location B"].upper().strip()
                if sro_name == str(self.proj.sro) and self.getPBOWithName(box_name):
                    self.log(f"{ll['found_pbo']}: {box_name}")
                    name_matched = True
                    locB = box_name
                else:
                    genParams["location A"] = str(self.proj.sro)
                    if self.getPBOWithName(locB):
                        name_matched = True

                if name_matched:
                    qsriputils.print_rip("locA = sro")
                    pbo = self.getPBOWithName(locB)
                    qsriputils.print_rip(f"pbo: {pbo}")
                    if pbo:
                        self.log(f"{ll['check_sor']}: {sor} pbo: {pbo.name} {ll['cable']}: {pbo.cable}")
                        qgis_l = self.__get_cable_length_from_qgis(pbo)
                        self.check_sor_measurements(d_meta, sname, locB, qgis_l)
                else:
                    self.log(ll['Match_error_unknown_naming'])

        # self.proj.issues.extend(issues)
        # qsriputils.update_project(self.proj)
        # qsriputils.create_issues(issues)

    def getCableWithName(self, c):
        for path in self.proj.cable:
            if c == path.name:
                return path
        return None

    def getPBOWithName(self, name):
        for f in self.proj.pbo:
            if f.name.endswith(name):
                return f
        return None

    def __get_extra_cable(self, pbo):
        # if pbo.function == "PBO":
        #     if pbo.support == "CHAMBRE":
        #         return 5
        # elif "PEC" in pbo.function:
        #     if pbo.support == "CHAMBRE":
        #         return 10
        return 10

    def __get_cable_length_from_qgis(self, pbo):
        srcpbo = ""
        for c in self.proj.cable:
            if c.dest == pbo.name:
                srcpbo = c.src
                length = round(c.length)
                break
        if srcpbo == "":
            return [0]
        destpbo = pbo.name
        destPBO = self.getPBOWithName(destpbo)
        srcPBO = self.getPBOWithName(srcpbo)
        length += self.__get_extra_cable(destPBO)
        length += self.__get_extra_cable(srcPBO)
        l = [length]
        e = [destpbo]
        while srcpbo != self.proj.sro:
            for c in self.proj.cable:
                if c.dest == srcpbo:
                    srcpbo = c.src
                    destpbo = c.dest
                    # qsriputils.print_rip("{}-{}".format(srcpbo, destpbo))
                    destPBO = self.getPBOWithName(destpbo)
                    srcPBO = self.getPBOWithName(srcpbo)
                    length = round(c.length)
                    # qsriputils.print_rip(f"cable: {c.length} src: {srcpbo} {srcPBO} dest: {destpbo} {destPBO}")
                    length += self.__get_extra_cable(destPBO)
                    e.append(destpbo)
                    if srcPBO:
                        length += self.__get_extra_cable(srcPBO)

                    if srcpbo == self.proj.sro:
                        length += 10
                        e.append(self.proj.sro)
                    l.append(length)
                    break
        l = list(reversed(l))
        e = list(reversed(e))
        return l

    def __preprocess_data(self, d_meta, l_raw_trace):
        '''Convert the raw data into a numpy array'''
        sample_spacing = float(d_meta["FxdParams"]["sample spacing"][:-5])
        self.window_len = int(0.5 / sample_spacing)
        q_trace = deque(l_raw_trace)
        l_distance = list()
        l_level = list()
        raw_row = q_trace.popleft()
        while q_trace:
            raw_distance, raw_level = raw_row.replace("\n", "").split("\t")
            f_distance = float(raw_distance)
            f_level = float(raw_level)
            l_distance.append(f_distance)
            l_level.append(f_level)
            raw_row = q_trace.popleft()
        a_trace = np.array([l_level, l_distance])
        return a_trace

    def check_sor_measurements(self, meta, sor, pbo, qgis_sections):
        qsriputils.print_rip(f"qgis_sections: {qgis_sections}")
        genParams = meta["GenParams"]
        fxdParams = meta["FxdParams"]
        errors = [sor]
        errors.append("<b>OTDR Settings</b>")
        error_count = 0
        error = 0
        warn = 0
        ok = 0
        now = datetime.now()

        measure_issues = []
        mqs_issues = []
        # wavelength

        w = genParams["wavelength"]
        w_cv = self.customer_value["wavelength"]
        qsriputils.print_rip(f"w_cv: {w_cv}")
        if all(s not in w for s in w_cv):
            errors.append(f"Bad wavelength error: <b style='color:red'>{w}</b>/{w_cv}")
            error_count += 1
            issue = QSRIPIssue(code="OTDR Setting", text=f"Bad wavelength error: {w}/{w_cv}\n{sor}", pbo=pbo,
                               issue_type="sor")
            issue.fromProject(p=self.proj, u=self.window.user)
            issue.level = 2
            measure_issues.append(issue)
        else:
            errors.append(f"wavelength ok: <b style='color:green'>{w}</b>/{w_cv}")

        # pulse width
        pw = qsriputils.remove_unit(fxdParams["pulse width"])
        if pw:
            pulse_width = int(pw)
            pw_cv = self.customer_value["pulse_width"]
            qsriputils.print_rip(f"pw_cv: {pw_cv}")
            if pulse_width < pw_cv[0] or pulse_width > pw_cv[1]:
                errors.append(f"Bad pulse width: <b style='color:red'>{pulse_width} ns</b>/{pw_cv} ns")
                error_count += 1
                issue = QSRIPIssue(code="OTDR Setting", text=f"Bad pulse width: {pulse_width}/{pw_cv}\n{sor}", pbo=pbo,
                                   issue_type="sor")
                issue.fromProject(p=self.proj, u=self.window.user)
                issue.level = 2
            else:
                errors.append(f"pulse width ok: <b style='color:green'>{pulse_width} ns</b>/{pw_cv} ns")
        else:
            errors.append(f"pulse width: no data")
            error_count += 1

        # IR (Backcast index)
        ir = float(fxdParams["index"])
        ir_cv = self.customer_value["backcast_index"]
        if abs(ir - ir_cv[0]) <= ir_cv[1]:
            errors.append(f"IR(backcast index) ok: <b style='color:green'>{ir}</b>/{ir_cv[0]}±{ir_cv[1]}")
        else:
            errors.append(f"Bad IR(backcast index) error: <b style='color:red'>{ir}</b>/{ir_cv[0]}±{ir_cv[1]}")
            error_count += 1
            issue = QSRIPIssue(code="OTDR Setting", text=f"Bad IR(backcast index): {ir}/{ir_cv[0]}±{ir_cv[1]}\n{sor}",
                               pbo=pbo,
                               issue_type="sor")
            issue.fromProject(p=self.proj, u=self.window.user)
            issue.level = 2

            measure_issues.append(issue)
        # backscattering coefficient
        bc = float(qsriputils.remove_unit(fxdParams["BC"]))
        bc_vc = self.customer_value["bc"] if "1310" in w else self.customer_value["bc2"]
        if abs(bc - bc_vc[0]) <= bc_vc[1]:
            errors.append(f"backscattering coefficient ok: <b style='color:green'>{bc}</b>/{bc_vc[0]}±{bc_vc[1]}")
        else:
            errors.append(f"Bad backscattering coefficient error: <b style='color:red'>{bc}</b>/{bc_vc[0]}±{bc_vc[1]}")
            issue = QSRIPIssue(code="OTDR Setting",
                               text=f"Bad backscattering coefficient: {bc}/{bc_vc[0]}±{bc_vc[1]}\n{sor}", pbo=pbo,
                               issue_type="sor")
            issue.fromProject(p=self.proj, u=self.window.user)
            issue.level = 2

            measure_issues.append(issue)
            error_count += 1
        # Splice detection threshold
        splice = float(qsriputils.remove_unit(fxdParams["loss thr"]))
        splice_cv = self.customer_value["splice_threshold"]
        if abs(splice - splice_cv[0]) <= splice_cv[1]:
            errors.append(
                f"Splice detection threshold ok: <b style='color:green'>{splice}</b>/{splice_cv[0]}±{splice_cv[1]}db")
        else:
            errors.append(
                f"Bad Splice detection threshold error: <b style='color:red'>{splice}</b>/{splice_cv[0]}±{splice_cv[1]}db")
            error_count += 1
            issue = QSRIPIssue(code="OTDR Setting",
                               text=f"Bad Splice detection threshold error: <b style='color:red'>{splice}</b>/{splice_cv[0]}±{splice_cv[1]}db\n{sor}",
                               pbo=pbo,
                               issue_type="sor")
            issue.fromProject(p=self.proj, u=self.window.user)
            issue.level = 2

            measure_issues.append(issue)
        # Reflectance detection threshold
        refl = float(qsriputils.remove_unit(fxdParams["refl thr"]))
        refl_cv = self.customer_value["refl_threshold"]
        if abs(refl - refl_cv[0]) <= refl_cv[1]:
            errors.append(
                f"Reflectance detection threshold ok: <b style='color:green'>{refl}</b>/{refl_cv[0]}±{refl_cv[1]}db")
        else:
            errors.append(
                f"Bad Reflectance detection threshold error: <b style='color:red'>{refl}</b>/{refl_cv[0]}±{refl_cv[1]}db")
            error_count += 1
            errors.append(
                f"Bad backscattering coefficient error: <b style='color:red'>{refl}</b>/{refl_cv[0]}±{refl_cv[1]}db")
            issue = QSRIPIssue(code="OTDR Setting",
                               text=f"Bad Reflectance detection threshold: {refl}/{refl_cv[0]}±{refl_cv[1]}\n{sor}",
                               pbo=pbo,
                               issue_type="sor")
            issue.fromProject(p=self.proj, u=self.window.user)
            issue.level = 2

            measure_issues.append(issue)

        # End of fiber detection threshold
        eot = float(qsriputils.remove_unit(fxdParams["EOT thr"]))
        eot_cv = self.customer_value["end_threshold"]
        if abs(eot - eot_cv[0]) <= eot_cv[1]:
            errors.append(
                f"End of fiber detection threshold ok: <b style='color:green'>{eot}</b>/{eot_cv[0]}±{eot_cv[1]}db")
        else:
            errors.append(
                f"Bad End of fiber detection threshold error: <b style='color:red'>{eot}</b>/{eot_cv[0]}±{eot_cv[1]}db")
            error_count += 1
            issue = QSRIPIssue(code="OTDR Setting",
                               text=f"Bad End of fiber detection threshold: {eot}/{eot_cv[0]}±{eot_cv[1]}\n{sor}",
                               pbo=pbo,
                               issue_type="sor")
            issue.fromProject(p=self.proj, u=self.window.user)
            issue.level = 2
            measure_issues.append(issue)
        otdrSettingError = len(measure_issues) > 0
        if otdrSettingError:
            issue = QSRIPIssue(code="MQS Error",
                               text=f"OTDR Setting failed, no further check",
                               pbo=pbo,
                               issue_type="sor")
            issue.fromProject(p=self.proj, u=self.window.user)
            issue.level = 2
            mqs_issues.append(issue)
        else:
            errors.append("<b>Measurements</b>")

            keyEvents = meta["KeyEvents"]
            i = 1
            distance = 0
            total_loss = 0
            connector = self.customer_value["connector"]
            qsriputils.print_rip(f"connector: {connector}")
            sections = []
            checkNextEvent = False
            skipEvent = 0
            while f"event {i}" in keyEvents:
                event = keyEvents[f"event {i}"]
                distance_ = float(event["distance"])
                length = distance_ - distance
                distance = distance_
                qsriputils.print_rip(f"event {i} length {length}")

                loss = float(event["splice loss"])
                refl = float(event["refl loss"])
                slope = float(event["slope"])
                if distance_ == 0.0:
                    i += 1
                    continue
                if i > 1:
                    sections.append(f"{length * 1000:.0f}")
                    if len(sections) > len(qgis_sections):
                        break
                    qsriputils.print_rip(f"sections: {sections}")
                    qsriputils.print_rip(f"qgis sections: {qgis_sections}")
                    delta = abs(length * 1000 - qgis_sections[len(sections) - 1 - skipEvent])
                    qsriputils.print_rip(f"section:{len(sections)} delta: {delta}")
                    if delta > self.customer_value["max_tolerance"]:
                        # bad event
                        if delta < self.customer_value["pbo_reserve_in_chamber"]:
                            # 222s
                            issue = QSRIPIssue(code="MQS Error",
                                               text=f"{const.sorError[0]} on event {i}\n{sor}",
                                               pbo=pbo,
                                               issue_type="sor")
                            issue.fromProject(p=self.proj, u=self.window.user)
                            issue.level = 2
                            mqs_issues.append(issue)
                        else:
                            if not checkNextEvent:
                                checkNextEvent = True
                            else:
                                checkNextEvent = False
                                nsor = len(sections)
                                last_section = int(sections[nsor - 2])

                                if abs(last_section + length - qgis_sections[nsor - 2 - skipEvent]) <= \
                                        self.customer_value["max_tolerance"]:
                                    qsriputils.print_rip("sor 444,555")
                                    skipEvent += 1
                                    lastEvent = keyEvents[f"event {i - 1}"]
                                    lastloss = float(lastEvent["splice loss"])
                                    lastrefl = float(lastEvent["refl loss"])
                                    lastslope = float(lastEvent["slope"])
                                    if lastrefl == 0:
                                        if lastloss < 0:
                                            # 555
                                            issue = QSRIPIssue(code="MQS Error",
                                                               text=f"Positive event on event {i}\n{sor}",
                                                               pbo=pbo,
                                                               issue_type="sor")
                                            issue.fromProject(p=self.proj, u=self.window.user)
                                            issue.level = 1
                                            mqs_issues.append(issue)
                                        else:
                                            # 444
                                            issue = QSRIPIssue(code="MQS Error",
                                                               text=f"{const.sorError[2]} on event {i}\n{sor}",
                                                               pbo=pbo,
                                                               issue_type="sor")
                                            issue.fromProject(p=self.proj, u=self.window.user)
                                            issue.level = 2
                                            mqs_issues.append(issue)
                                    # 444, 555
                                elif abs(last_section + length - qgis_sections[nsor - 2 - skipEvent] - qgis_sections[
                                    nsor - 1 - skipEvent]) <= 2 * self.customer_value["max_tolerance"]:
                                    qsriputils.print_rip("sor 333")
                                    issue = QSRIPIssue(code="MQS Error",
                                                       text=f"{const.sorError[1]} on event {i}\n{sor}",
                                                       pbo=pbo,
                                                       issue_type="sor")
                                    issue.fromProject(p=self.proj, u=self.window.user)
                                    issue.level = 2
                                    mqs_issues.append(issue)
                    if slope == 0.0:
                        slope = 0.300
                    if loss != 0 and refl < 0:
                        qsriputils.print_rip("connector event")
                        if loss <= connector[0]:
                            errors.append(
                                f"Event {i} (Connector) Loss ok: <b style='color:green'>{loss}</b>/{connector[0]}db")
                        else:
                            errors.append(
                                f"Event {i} (Connector) Loss error: <b style='color:red'>{loss}</b>/{connector[0]}db")
                            error_count += 1
                            issue = QSRIPIssue(code="OTDR Measurement",
                                               text=f"Event {i} (Connector) Loss error: {loss}/{connector[0]}\n{sor}",
                                               pbo=pbo,
                                               issue_type="sor")
                            issue.fromProject(p=self.proj, u=self.window.user)
                            issue.level = 1
                            measure_issues.append(issue)
                        if refl <= connector[1]:
                            errors.append(
                                f"Event {i} (Connector) Reflectance ok: <b style='color:green'>{refl}</b>/{connector[1]}db")
                        else:
                            errors.append(
                                f"Event {i} (Connector) Reflectance error: <b style='color:red'>{refl}</b>/{connector[1]}db")
                            error_count += 1
                            issue = QSRIPIssue(code="OTDR Measurement",
                                               text=f"Event {i} (Connector) Reflectance error: {refl}/{connector[1]}\n{sor}",
                                               pbo=pbo,
                                               issue_type="sor")
                            issue.fromProject(p=self.proj, u=self.window.user)
                            issue.level = 1
                            measure_issues.append(issue)
                        # coli loss
                        total_loss = length * slope + loss
                        qsriputils.print_rip(f"coli total loss: {total_loss}")
                    elif loss != 0 or refl != 0:
                        # section loss
                        total_loss += length * slope
                        # event loss
                        total_loss += loss
                        qsriputils.print_rip(f"total loss: {total_loss} section: {length * slope}: event: {loss}")
                        splice_loss = self.customer_value["section_event_loss"]
                        if loss < splice_loss:
                            errors.append(
                                f"Splice Event {i} loss ok: <b style='color:green'>{loss}</b>/{splice_loss}db")
                        else:
                            errors.append(
                                f"Splice Event {i} loss error: <b style='color:red'>{loss}</b>/{splice_loss}db")
                            error_count += 1
                            issue = QSRIPIssue(code="OTDR Measurement",
                                               text=f"Splice Event {i} loss error: {loss}/{splice_loss}\n{sor}",
                                               pbo=pbo,
                                               issue_type="sor")
                            issue.fromProject(p=self.proj, u=self.window.user)
                            issue.level = 1
                            measure_issues.append(issue)
                        section_cv = self.customer_value["section_event_loss"]
                        if slope <= section_cv:
                            errors.append(f"Section {i} loss ok: <b style='color:green'>{slope}</b>/{section_cv} db/km")
                        else:
                            errors.append(
                                f"Bad Section {i} loss error: <b style='color:red'>{slope}</b>/{section_cv} db/km")
                            error_count += 1
                            issue = QSRIPIssue(code="OTDR Measurement",
                                               text=f"Bad Section {i} loss: {slope}/{section_cv}\n{sor}",
                                               pbo=pbo,
                                               issue_type="sor")
                            issue.fromProject(p=self.proj, u=self.window.user)
                            issue.level = 1
                            measure_issues.append(issue)

                i += 1
            total_cv = self.customer_value["total_loss"]
            if total_loss <= total_cv:
                errors.append(f"Optical assessment ok: <b style='color:green'>{total_loss:.2f}</b>/{total_cv} db")
            else:
                errors.append(f"Bad Optical assessment: <b style='color:red'>{total_loss:.2f}</b>/{total_cv} db")
                error_count += 1
                qsriputils.print_rip(f"end of fiber: event {i}, {refl}")
                issue = QSRIPIssue(code="OTDR Measurement",
                                   text=f"Bad Optical assessment: {total_loss}/{total_cv}\n{sor}",
                                   pbo=pbo,
                                   issue_type="sor")
                issue.fromProject(p=self.proj, u=self.window.user)
                issue.level = 2

                measure_issues.append(issue)
            errors.append("sor: " + ",".join(sections))
            errors.insert(1, f"{error_count}")
        issues = []
        if checkNextEvent:
            issue = QSRIPIssue(code="MQS Error",
                               text=f"Fiber Cut at {sections[-1]}m\n{sor}",
                               pbo=pbo,
                               issue_type="sor")
            issue.fromProject(p=self.proj, u=self.window.user)
            issue.level = 2
            mqs_issues.append(issue)

        # disable mqs
        mqs_issues = []
        # disable mqs
        if len(mqs_issues) == 0 and len(measure_issues) == 0:
            qsriputils.print_rip("no error, sor and qgis are same")
            ok = 1
        else:
            issues.extend(measure_issues)
            issues.extend(mqs_issues)
            error = 0
            warn = 0
            ok = 0
            for iss in issues:
                if iss.level == 2:
                    error += 1
                else:
                    warn += 1
        qsriputils.print_rip("update project")
        self.proj.sor_tested += 1
        self.proj.sor_error += error
        self.proj.sor_warn += warn
        self.proj.sor_ok += ok
        self.proj.issues.extend(issues)
        qsriputils.update_project(self.proj)
        qsriputils.print_rip(f"mqs issues: {mqs_issues}")
        if len(issues) > 0:
            qsriputils.create_issues(issues=issues, proj_id=self.proj._id, total=1, ok=ok, client=self.proj.client)
        self.log(f"{ll['save_sor_quality']} {sor}")
        mqs = "\n".join([m.text for m in mqs_issues])
        qsriputils.save_sor_quality(self.proj, pbo, errors, mqs)
        self.window.is_logout = True
