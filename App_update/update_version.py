import json
import os
import sys

import firebase_admin
from PyQt5 import QtCore
from firebase_admin import credentials, firestore, storage

from PyQt5.QtWidgets import QMainWindow, QApplication, QVBoxLayout, QLabel, QFileDialog, QPushButton, QWidget, \
    QLineEdit, QTextEdit, QDialog

import cipher_utils


class MainWindow(QMainWindow):
    def __init__(self, config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config = config
        self.filepath = None
        main_layout = QVBoxLayout()
        self.widget_version = QLineEdit()
        self.widget_version.setPlaceholderText('Version')
        self.widget_changeLog = QTextEdit()
        self.widget_changeLog.setPlaceholderText('Change Log')
        self.widget_file = QPushButton('select file')
        widget_update = QPushButton('update')
        main_layout.addWidget(self.widget_version)
        main_layout.addWidget(self.widget_changeLog)
        main_layout.addWidget(self.widget_file)
        main_layout.addWidget(widget_update)
        main_widget = QWidget()
        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)

        self.widget_file.clicked.connect(self.selectFile)
        widget_update.clicked.connect(self.updateVersion)

    def selectFile(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.exec()
        self.filepath = dlg.selectedFiles()
        self.widget_file.setText(self.filepath[0])

    def updateVersion(self):
        if self.filepath != None and self.widget_version.text() != '' and self.widget_changeLog.toPlainText() != '':
            filename = 'rip_{}.zip'.format(self.widget_version.text())
            bucket = storage.bucket()
            blob = bucket.blob('version/{}'.format(filename))
            blob.upload_from_filename(self.filepath[0])
            dlg = QDialog()
            dlg.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowCloseButtonHint)
            layout = QVBoxLayout()
            dlg.setFixedSize(300, 100)
            if blob.exists():
                data = dict()
                data['version'] = self.widget_version.text()
                change_log = self.widget_changeLog.toPlainText().replace('\n', '<br/>')
                data['change_log'] = change_log
                self.config.set(data)
                lb = QLabel('Update success')
            else:
                lb = QLabel('Update False')
            layout.addWidget(lb)
            dlg.setLayout(layout)
            dlg.exec()


app = QApplication(sys.argv)

firebase_json = json.loads(cipher_utils.decrypt('encrypted-firebase-admin.json'))
cred = credentials.Certificate(firebase_json)
firebase_admin.initialize_app(cred, {
    'storageBucket': 'secu-rip-ad662.appspot.com'
})
db = firestore.client()
config_ref = db.collection('config').document('default')

widget = MainWindow(config_ref)
widget.setWindowTitle("Project Management")
widget.setGeometry(50, 100, 700, 400)
widget.show()
app.exec()
