import os

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from env import *


def encrypt(input_path: str = ''):
    if os.path.isfile(DB_KEY_PATH) and os.path.isfile(input_path):
        key = open(DB_KEY_PATH, 'rb').read()

        # read content and add padding
        raw_content = open(input_path, 'rb').read()
        raw_content += b' ' * (len(key) - len(raw_content) % len(key))

        init_vector = b'sseeccuurrrriipp'
        cipher = Cipher(algorithms.AES(key), modes.CBC(init_vector))
        encryptor = cipher.encryptor()
        return encryptor.update(raw_content) + encryptor.finalize()

    return None


def decrypt(input_path: str = ''):
    if os.path.isfile(DB_KEY_PATH) and os.path.isfile(input_path):
        key = open(DB_KEY_PATH, 'rb').read()

        encrypted_content = open(input_path, 'rb').read()

        init_vector = b'sseeccuurrrriipp'
        cipher = Cipher(algorithms.AES(key), modes.CBC(init_vector))
        decryptor = cipher.decryptor()
        return decryptor.update(encrypted_content) + decryptor.finalize()

    return None
