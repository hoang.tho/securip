from qsripproject import QSRIPProject

DATA_CLASSES = {
    'QSRIPProject': QSRIPProject(),
}

APP_MODE = 'dev'

ADMIN_ROLE = 'admin'

PROJECTS_PATH = 'projects'
LOGS_PATH = 'logs'

VM_MODELS = ['HVM domU', 'Virtual Machine', 'VirtualBox', 'KVM', 'VMware Virtual Platform']
VM_MNFTS = ['Xen', 'Microsoft Corporation', 'innotek GmbH', 'Red Hat', 'VMware, Inc.']

MAIL_SENDER_ADDR = 'securrip.crash@gmail.com'
MAIL_SENDER_PWD = 'S@curRip86'
MAIL_RECEIVER_ADDR = 'securrip.crash@cfitech.fr'
SMTP_ADDR = 'smtp.gmail.com'
SMTP_PORT = 587

CRASH_LOG_LEVEL = 1
CRASH_MAIL_SUBJECT = 'Crash log report.'
CRASH_MAIL_CONTENT = '''
Hello,
Application had crashed.
Please check crash log file for more information.
Thank You
'''

DB_KEY_PATH = 'key.txt'
db = None
