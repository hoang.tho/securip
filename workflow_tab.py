import os
import time

import xlsxwriter
from collections import deque
import numpy as np
import datetime
from PyQt5 import QtWidgets, QtCore, uic, QtGui, QtPrintSupport
from PyQt5.QtCore import Qt, QUrl, QAbstractListModel, QModelIndex, QVariant
from PyQt5.Qt import QPushButton, QLabel, QProgressDialog, QCursor, QSize, QImage, QTimer
from PyQt5.QtGui import QFont, QIcon, QPixmap, QPainter, QTextDocument, QStandardItemModel, QStandardItem
from PyQt5.QtWebKitWidgets import QWebView
# from PyQt5.QWebEngineWidgets import QWebEngineView
import const
from random import randint
from pyOTDR import sorparse
import OTDRlib
from PyQt5.QtWidgets import QTreeWidget, QTextEdit, QTreeWidgetItem, QDialog, QTableWidget, QVBoxLayout, \
    QTableWidgetItem, QHBoxLayout, QTableView, QListView, QHeaderView, QTextBrowser
from qsripproject import QSRIPProject, QSRIPIssue, Chat
from qgis.core import QgsApplication, QgsFeature, QgsRasterLayer, QgsVectorLayer, QgsProject, QgsFeatureRequest, \
    QgsExpression, QgsCoordinateReferenceSystem
from qgis.gui import QgsMapCanvas, QgsMapToolIdentify, QgsMapToolIdentifyFeature

from SROWire import SROWire, LeftWire
from OTDRTrace import OTDRTrace
import qsriputils
from firebase_admin import storage, firestore
from firebase_admin.storage import bucket
import resources

f = open("lang.txt", 'r')
from multilang import english, french
if f.read() == 'en':
    ll = english
else:
    ll = french
f.close()


class WorkflowTab(QtWidgets.QFrame):
    selected_pbo = None
    projs = []
    proj: QSRIPProject = None
    window = None

    def __init__(self, window=None):
        super(WorkflowTab, self).__init__()
        uic.loadUi('ui/workflow.ui', self)
        self.window = window
        self.selectedClient = ""
        self.selectedProject = ""
        self.selectedSRO = ""
        self.renderedMap = False
        self.leafCount = {}
        self.sorpbo = []
        self.projects = []
        self.clients = []
        self.sros = []
        self.apddoe = []
        self.cell_first = None
        self.select_cell = None
        self.showAttach = False
        self.icons_button = []
        self.selectedIcon = ['workflowico.png', 'icon_attach']
        self.deselectedIcon = ['workflowico_deselected.png', 'icon_attach_deselect.png']
        layout_icon = QHBoxLayout()
        self.icon_box.setLayout(layout_icon)
        icon_comment = self.iconButton('comment', 'images/workflowico.png')
        icon_attach = self.iconButton('attach', 'images/icon_attach_deselect.png')
        layout_icon.addWidget(icon_comment)
        layout_icon.addWidget(icon_attach)
        self.icons_button = [icon_comment, icon_attach]

        self.socialRootItem = QTreeWidgetItem([ll['soci']])
        self.socialRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.wf_clientTreeWidget.addTopLevelItem(self.socialRootItem)
        self.socialRootItem.setExpanded(True)
        self.projectRootItem = QTreeWidgetItem([ll['project']])
        self.projectRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.wf_clientTreeWidget.addTopLevelItem(self.projectRootItem)
        self.projectRootItem.setExpanded(True)
        sroRootItem = QTreeWidgetItem(["» SRO"])
        sroRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.wf_clientTreeWidget.addTopLevelItem(sroRootItem)
        sroRootItem.setExpanded(True)
        self.typeRootItem = QTreeWidgetItem([ll['ver_max']])
        self.typeRootItem.setForeground(0, QtGui.QBrush(Qt.white))
        self.wf_clientTreeWidget.addTopLevelItem(self.typeRootItem)
        self.typeRootItem.setExpanded(True)
        self.pushButton.setIcon(QIcon("./images/sendicon.png"))
        self.pushButton.setIconSize(QSize(30, 20))
        self.pushButton.clicked.connect(self.sendComment)

        self.pushButton_2.setIcon(QIcon("./images/attachicon.png"))
        self.pushButton_2.setIconSize(QSize(30, 20))
        self.pushButton_2.clicked.connect(self.sendAttach)

        self.textBrowser_2.setOpenLinks(False)
        self.textBrowser_2.anchorClicked.connect(self.downloadAttach)
        self.wf_clientTreeWidget.itemClicked.connect(self.on_wf_tree_client_changed)

        self.proj = self.window.activeProject()
        if self.proj:
            self.showListWF()

    def updateText(self, lang):
        global ll
        ll = lang


        self.socialRootItem.setText(0, ll['soci'])
        self.projectRootItem.setText(0, ll['project'])
        self.typeRootItem.setText(0, ll['ver_max'])

        self.showListWF()


    @QtCore.pyqtSlot(QtWidgets.QTreeWidgetItem, int)
    def on_wf_tree_client_changed(self, it, col):
        if self.proj != None:
            text = it.text(col)
            root = it.parent()
            if root == self.wf_clientTreeWidget.topLevelItem(0):
                qsriputils.print_rip(f"click on client brand: {text}")
                if text != self.selectedClient:
                    self.selectedClient = text
                    self.__selectTreeItem(self.wf_clientTreeWidget.topLevelItem(0), text)
                    self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(1))
                    self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(2))
                    self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(3))
                    self.reload_projects(self.selectedClient)
            elif root == self.wf_clientTreeWidget.topLevelItem(1):
                qsriputils.print_rip(f"click on project brand: {text}")
                if text != self.selectedProject:
                    self.selectedProject = text
                    self.__selectTreeItem(self.wf_clientTreeWidget.topLevelItem(1), text)
                    self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(2))
                    self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(3))
                    self.reload_sro(self.selectedClient, self.selectedProject)
            elif root == self.wf_clientTreeWidget.topLevelItem(2):
                qsriputils.print_rip(f"click on sro brand: {text}")
                if text != self.selectedSRO:
                    self.selectedSRO = text
                    self.__selectTreeItem(self.wf_clientTreeWidget.topLevelItem(2), text)
                    self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(3))
                    self.reload_apd(self.selectedClient, self.selectedProject, self.selectedSRO)
            else:
                qsriputils.print_rip(f"click on apd brand:{text}")
                apd = f"{self.proj.type} V{self.proj.ver}"
                if self.selectedClient != self.proj.client or self.selectedProject != self.proj.project or self.selectedSRO != self.proj.sro or text != apd:
                    self.__selectTreeItem(self.wf_clientTreeWidget.topLevelItem(3), text)
                    p = '{}_{}_{}_{}'.format(self.selectedClient, self.selectedProject, self.selectedSRO,
                                             text.replace(' ', '_'))
                    for proj in self.window.projs:
                        if p in proj._id:
                            self.proj = proj
                            self.showListWF()

    def __wf_addTreeItem(self, parent, text, checked=False):
        item = QTreeWidgetItem(parent, [text])
        item.setForeground(0, QtGui.QBrush(Qt.white))
        item.setCheckState(0, Qt.Checked if checked else Qt.Unchecked)

    def __selectTreeItem(self, parent, selected):
        child_count = parent.childCount()
        for i in range(child_count):
            item = parent.child(i)
            if item.text(0) == selected:
                item.setCheckState(0, Qt.Checked)
            else:
                item.setCheckState(0, Qt.Unchecked)

    def tree_item_clear_child(self, parent):
        children = []
        for i in range(parent.childCount()):
            children.append(parent.child(i))
        for child in children:
            parent.removeChild(child)

    def iconButton(self, text, image):
        button = QtWidgets.QToolButton()
        button.setText(text)
        button.setFixedSize(20, 15)
        button.setCheckable(True)
        button.setIconSize(QtCore.QSize(50, 50))
        button.setIcon(QtGui.QIcon(image))
        button.clicked.connect(self.select_icon)
        return button

    def select_icon(self):
        text = self.sender().text()
        if self.proj != None:
            for (i, t) in enumerate(self.icons_button):
                if text == 'attach':
                    self.showAttach = True
                elif text == 'comment':
                    self.showAttach = False
                if self.tableWidget.item(0, 0) != None:
                    if self.select_cell:
                        self.on_select_wf(self.select_cell)
                    else:
                        self.on_select_wf(self.cell_first)
                if t.text() == text:
                    t.setChecked(True)
                    self.icons_button[i].setIcon(QtGui.QIcon('images/{}'.format(self.selectedIcon[i])))
                else:
                    t.setIcon(QtGui.QIcon('images/{}'.format(self.deselectedIcon[i])))
                    t.setChecked(False)

    def on_select_wf(self, cell):
        self.select_cell = cell
        wf_select = self.lst_pbo[cell.row()]
        self.wf_pbo = self.wfs[wf_select][1]
        wf_user = self.wfs[wf_select][0]
        font = QFont()
        font.setBold(True)
        font.setPointSize(14)
        self.wf_label.setMaximumHeight(25)
        self.wf_label.setAlignment(Qt.AlignCenter)
        self.wf_label.setText('{}: {}'.format(ll['wflow'], self.wf_pbo))
        self.wf_label.setFont(font)
        # self.wf_userName.setText(wf_user)

        query = qsriputils.db.collection('chats').where('proj_id', '==', self.proj._id) \
            .where('pbo', '==', self.wf_pbo)
        chats = query.get()
        comments = dict()
        for chat in chats:
            comment = chat.to_dict()
            if str(comment['date_create']) not in comments:
                if 'text' in comment and not self.showAttach:
                    comments[str(comment['date_create'])] = [(comment['user_name'], comment['text'])]
                elif 'file' in comment:
                    comments[str(comment['date_create'])] = [
                        (comment['user_name'],
                         comment['file'] + ' <a href="' + comment[
                             'file'] + '"><img src="./images/attachicon.png" width=20 height=20>'
                         )]
            else:
                if 'text' in comment and not self.showAttach:
                    comments[str(comment['date_create'])].append((comment['user_name'], comment['text']))
                elif 'file' in comment:
                    comments[str(comment['date_create'])].append(
                        (
                            comment['user_name'],
                            comment['file'] + ' <a href=">' + comment[
                                'file'] + '"><img src="./images/attachicon.png" width=20 height=20>'
                        )
                    )
        html = ""
        if comments:
            k = list(comments.keys())
            k = [int(x) for x in k]
            k.sort(reverse=True)
            k = [str(x) for x in k]
            for key in k:
                for c in comments[key]:
                    html += "<p style='background-color:#F2F2F2'><b>" + c[
                        0] + ":</b> " + datetime.datetime.fromtimestamp(int(key) // 1e9).strftime(
                        '%d-%m-%Y %H:%M:%S') + "<br>" + c[1] + "</p>"
        self.textBrowser_2.setHtml(html)

    def check_proj(self):
        if self.window.id_proj_wf:
            for proj in self.window.projs:
                if proj._id == self.window.id_proj_wf:
                    self.proj = proj
        else:
            self.proj = self.window.activeProject()
        if self.proj:
            self.selectedClient = self.proj.client
            self.selectedProject = self.proj.project
            self.selectedSRO = self.proj.sro
            self.selectedAPD = '{} V{}'.format(self.proj.type, self.proj.ver)
            self.update_left_menu()

    def update_left_menu(self):
        self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(0))
        self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(1))
        self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(2))
        self.tree_item_clear_child(self.wf_clientTreeWidget.topLevelItem(3))
        clients = []
        for proj in self.window.projs:
            clients.append(proj.client)
        for client in list(set(clients)):
            self.__wf_addTreeItem(self.wf_clientTreeWidget.topLevelItem(0), client, client == self.selectedClient)
        self.reload_projects(self.selectedClient)
        self.reload_sro(self.selectedClient, self.selectedProject)
        apddoe = []
        for p in self.window.projs:
            apd = f"{p.type} V{p.ver}"
            if p.client == self.selectedClient and p.project == self.selectedProject and p.sro == self.selectedSRO and apd not in apddoe:
                apddoe.append(apd)
                self.__wf_addTreeItem(self.wf_clientTreeWidget.topLevelItem(3), apd, apd == self.selectedAPD)

    def showListWF(self):
        self.check_proj()
        if self.proj:
            chats = qsriputils.db.collection('chats').where('proj_id', '==', self.proj._id).get()
            self.wfs = dict()
            if len(chats) > 0:
                for chat in chats:
                    k = chat.to_dict()
                    if 'text' in k.keys():
                        self.wfs[k['date_create']] = [k['user_name'], k['pbo'], k['text']]
                    elif 'file' in k.keys():
                        self.wfs[k['date_create']] = [k['user_name'], k['pbo'], k['file']]

            time_sort = sorted(self.wfs.keys(), reverse=True)
            dict_pbo = dict()
            if len(time_sort) > 0:
                for tg in time_sort:
                    if self.wfs[tg][1] not in dict_pbo:
                        dict_pbo[self.wfs[tg][1]] = [tg, self.wfs[tg][0], self.wfs[tg][2]]

            if hasattr(self, 'tableWidget'):
                self.wf_layouts.removeWidget(self.tableWidget)
            self.tableWidget = QTableWidget()
            self.wf_layouts.addWidget(self.tableWidget)
            self.tableWidget.setRowCount(len(dict_pbo))
            self.tableWidget.setColumnCount(1)
            self.tableWidget.setStyleSheet("""font-family:'SF UI Text';font-size:11px;""")
            self.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
            self.tableWidget.horizontalHeader().hide()
            self.tableWidget.verticalHeader().hide()

            self.lst_pbo = []
            if len(dict_pbo) > 0:
                self.wfr_scrollArea.setHidden(False)
                i = 0
                for pbo in dict_pbo:
                    self.tableWidget.setRowHeight(i, 50)
                    secs = dict_pbo[pbo][0] / 1e9
                    dt = datetime.datetime.fromtimestamp(secs)
                    date = dt.strftime('%d-%m-%Y - %H:%M')
                    item = QTableWidgetItem()
                    text = '{} - {}\n{}\t\t\t\t{}'.format(self.proj.client, self.proj.project, pbo, date)
                    item.setText(text)
                    item.setFont(QFont("SF UI Text", weight=QFont.Bold, pointSize=10))
                    self.tableWidget.setItem(i, 0, item)
                    self.lst_pbo.append(dict_pbo[pbo][0])
                    i += 1
                # if self.tableWidget.item(0, 0) != None:
                self.cell_first = self.tableWidget.model().index(0, 0, QModelIndex())
                self.on_select_wf(self.cell_first)
                self.tableWidget.clicked.connect(self.on_select_wf)
            else:
                self.wfr_scrollArea.setHidden(True)

    def sendComment(self):
        comment = self.lineEdit.text()
        if len(self.lst_pbo) > 0:
            if comment != '':
                c = Chat()
                c.date_create = time.time_ns()
                c.user_name = self.window.user.displayName
                c.user_id = self.window.user.userId
                c.proj_id = self.proj._id
                c.pbo = self.wf_pbo
                c.text = self.lineEdit.text()
                c.user_read = [self.window.user.userId]
                # c.file = file

                qsriputils.db.collection('chats').document().set({
                    'user_name': c.user_name,
                    'user_id': c.user_id,
                    'proj_id': c.proj_id,
                    'pbo': c.pbo,
                    'text': c.text,
                    'date_create': c.date_create,
                    'user_read': c.user_read
                })
                self.lineEdit.setText('')
                self.showListWF()
            else:
                qsriputils.print_rip('Comment without content')

    def sendAttach(self):
        if self.proj != None and len(self.lst_pbo) > 0:
            dlg = QtWidgets.QFileDialog()
            dlg.setFileMode(QtWidgets.QFileDialog.AnyFile)
            if dlg.exec():
                filepath = dlg.selectedFiles()
                if len(filepath) > 0:
                    filename = os.path.basename(filepath[0])
                    import random
                    import string
                    filename = ''.join(random.choice(string.ascii_lowercase) for _ in range(10)) + filename
                    bucket = storage.bucket()
                    blob = bucket.blob(filename)
                    blob.upload_from_filename(filepath[0])

                    c = Chat()
                    c.date_create = time.time_ns()
                    c.user_name = self.window.user.displayName
                    c.user_id = self.window.user.userId
                    c.proj_id = self.proj._id
                    c.pbo = self.wf_pbo
                    c.file = filename
                    c.user_read = [self.window.user.userId]

                    qsriputils.db.collection('chats').document().set({
                        'user_name': c.user_name,
                        'user_id': c.user_id,
                        'proj_id': c.proj_id,
                        'pbo': c.pbo,
                        'file': c.file,
                        'date_create': c.date_create,
                        'user_read': c.user_read
                    })
                    self.showListWF()

    def downloadAttach(self, item):
        bucket = storage.bucket()
        server_filename = item.toString()
        filepath = QtGui.QFileDialog.getSaveFileName(self, 'Save File')
        blob = bucket.blob(server_filename)
        if filepath[0] != '':
            blob.download_to_filename(filepath[0])

    def reload_projects(self, client):
        projects = []
        for p in self.window.projs:
            if p.client == client and p.project not in projects:
                projects.append(p.project)
                self.__wf_addTreeItem(self.wf_clientTreeWidget.topLevelItem(1), p.project,
                                      p.project == self.selectedProject)

    def reload_sro(self, client, project):
        sro = []
        for p in self.window.projs:
            if p.client == client and p.project == project and p.sro not in sro:
                sro.append(p.sro)
                self.__wf_addTreeItem(self.wf_clientTreeWidget.topLevelItem(2), p.sro, p.sro == self.selectedSRO)

    def reload_apd(self, client, project, sro):
        apddoe = []
        for p in self.window.projs:
            apd = f"{p.type} V{p.ver}"
            if p.client == client and p.project == project and p.sro == sro and apd not in apddoe:
                apddoe.append(apd)
                self.__wf_addTreeItem(self.wf_clientTreeWidget.topLevelItem(3), apd)

    def export_pdf(self):
        # qsriputils.print_widget(self.scrollAreaWidgetContents, "test.pdf")
        printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        # printer.setPageSize(QtPrintSupport.QPrinter.PageSize)
        dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        dialog.paintRequested.connect(self.print_preview)
        dialog.exec_()

    @QtCore.pyqtSlot(QtPrintSupport.QPrinter)
    def print_preview(self, printer):
        painter = QtGui.QPainter(printer)

        # start scale
        xscale = printer.pageRect().width() * 1.0 / self.wf_detail.width()
        yscale = printer.pageRect().height() * 1.0 / self.wf_detail.height()
        scale = min(xscale, yscale)
        painter.translate(printer.paperRect().center())
        painter.scale(scale, scale)
        painter.translate(-self.wf_detail.width() / 2, -self.wf_detail.height() / 2)
        # end scale

        self.wf_detail.render(painter)
        painter.end()
