﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace SecurRipLicense
{
    public partial class SecurRipLicenseForm : Form
    {
        public SecurRipLicenseForm()
        {
            InitializeComponent();

            LicenseTypeComboBox.SelectedItem = "Personal";
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            string key = KeyTextBox.Text;
            int day = ExpiredTimePicker.Value.Day;
            int month = ExpiredTimePicker.Value.Month;
            int year = ExpiredTimePicker.Value.Year;
            string expired_time = month.ToString() + '/' + day.ToString() + '/' + year.ToString();
            string license_type = LicenseTypeComboBox.Text;

            if (key.Replace(" ", "") == "")
            {
                MessageBox.Show("Key must not empty!");
            }
            else
            {
                string license = key + '_' + license_type + '_' + expired_time;

                string private_key = "<RSAKeyValue><Modulus>ftYDS55ozK9mf/DgbSqWmsO5wcj9udN5u6IJgqy0K3WoIhIDFgPIIdxVDr89YQfmvQZyg+W2qeAx66mNQ1QotMGGeDNIUm7L7L7WVTB3rI6MkJWR7smak06wXWPIw9bnbz+p8P42qQzgEmA186e8jD43HCxkGMVVuTbFcrRAx0s=</Modulus><Exponent>AQAB</Exponent><P>14aXt7eQeZ0xWvtAAyXUYwvemrjZwLcNllMQmWaXinEz7YRVmahEN+e9ywo/jFhXI37NQ8lPjoMVIksxaaQ/dw==</P><Q>lqeoME/TQMDTqJj3y22S+6VUVP2pOgtCbxBUNFZULJWaN5SOltMDzs2RnXc2+psvHxcAsAEDNkJr9YLOH2TzzQ==</Q><DP>M4oEDfLfobVtcAxVKmuJdoYIHYfGysb7gM8sUQKvinO3rzzOG4iZh6TrRfqVZ8JYVCymeWdSlwnRq/m/Pzv/0Q==</DP><DQ>GMDTUZzMCQelvz3i+Pm3/MDdalmDRJY1BCqaqz7D9c9e12MBSqbBKWl9U3ITBnoY8/LDmieQb4naUyx60Y20wQ==</DQ><InverseQ>m1iEC+l379SbPBOxKy063s7nR9FPZDGPeKOzonDb63tTjtFhkMXnN06OpniHEIO42nSWG7gNKMJA9NeboF9HBQ==</InverseQ><D>Az/aX8KULsJDnv190m4n45kmFiffEDaxxLPXCIkCKKbgAtdxSswYAX4Sk8kkemroZ2lV37kv39RFkyMJ4nKyhD2bYf3TkQoAUtKCVBxg68DZHuIT6AQpmDe1iBzX+tZ+O8JhevF+XnL4/3u3HQmM/OFAda+PiPoUt8tbTZEK4Hk=</D></RSAKeyValue>";
                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                RSA.FromXmlString(private_key);
                string public_key = RSA.ToXmlString(false);
                RSAPKCS1SignatureFormatter RSAFormatter = new RSAPKCS1SignatureFormatter(RSA);
                RSAFormatter.SetHashAlgorithm("SHA1");
                SHA1Managed SHhash = new SHA1Managed();
                byte[] SignedHashValue = RSAFormatter.CreateSignature(SHhash.ComputeHash(new ASCIIEncoding().GetBytes(license)));
                string signature = Convert.ToBase64String(SignedHashValue);

                Stream FileStream;
                SaveFileDialog SaveLicenseFileDialog = new SaveFileDialog();

                SaveLicenseFileDialog.Filter = "txt files (*.json)|*.json|All files (*.*)|*.*";
                SaveLicenseFileDialog.FilterIndex = 2;
                SaveLicenseFileDialog.RestoreDirectory = true;

                if (SaveLicenseFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if ((FileStream = SaveLicenseFileDialog.OpenFile()) != null)
                    {
                        StreamWriter FileWriter = new StreamWriter(FileStream);
                        FileWriter.WriteLine("{\"key\": \"" + key + "\", \"expired_time\": \"" + expired_time + "\", \"type\": \"" + license_type + "\", \"signature\": \"" + signature + "\"}");
                        FileWriter.Close();
                        FileStream.Close();
                    }
                }

                MessageBox.Show("Key: " + key + "\nExpired Time: " + expired_time + "\nLicense Type: " + license_type + "\n\nSignature:\n" + signature + "\n\nPublic Key:\n" + public_key, "License Info");
            }
        }
    }
}
