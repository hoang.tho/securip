﻿
namespace SecurRipLicense
{
    partial class SecurRipLicenseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.KeyLabel = new System.Windows.Forms.Label();
            this.KeyTextBox = new System.Windows.Forms.TextBox();
            this.ExpiredTimePicker = new System.Windows.Forms.DateTimePicker();
            this.ExpiredTimeLabel = new System.Windows.Forms.Label();
            this.LicenseTypeComboBox = new System.Windows.Forms.ComboBox();
            this.LicenseTypeLabel = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // KeyLabel
            // 
            this.KeyLabel.AutoSize = true;
            this.KeyLabel.Location = new System.Drawing.Point(13, 13);
            this.KeyLabel.Name = "KeyLabel";
            this.KeyLabel.Size = new System.Drawing.Size(25, 13);
            this.KeyLabel.TabIndex = 0;
            this.KeyLabel.Text = "Key";
            // 
            // KeyTextBox
            // 
            this.KeyTextBox.Location = new System.Drawing.Point(90, 13);
            this.KeyTextBox.Name = "KeyTextBox";
            this.KeyTextBox.Size = new System.Drawing.Size(181, 20);
            this.KeyTextBox.TabIndex = 1;
            // 
            // ExpiredTimePicker
            // 
            this.ExpiredTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ExpiredTimePicker.Location = new System.Drawing.Point(90, 40);
            this.ExpiredTimePicker.Name = "ExpiredTimePicker";
            this.ExpiredTimePicker.Size = new System.Drawing.Size(180, 20);
            this.ExpiredTimePicker.TabIndex = 2;
            // 
            // ExpiredTimeLabel
            // 
            this.ExpiredTimeLabel.AutoSize = true;
            this.ExpiredTimeLabel.Location = new System.Drawing.Point(13, 40);
            this.ExpiredTimeLabel.Name = "ExpiredTimeLabel";
            this.ExpiredTimeLabel.Size = new System.Drawing.Size(68, 13);
            this.ExpiredTimeLabel.TabIndex = 3;
            this.ExpiredTimeLabel.Text = "Expired Time";
            // 
            // LicenseTypeComboBox
            // 
            this.LicenseTypeComboBox.FormattingEnabled = true;
            this.LicenseTypeComboBox.Items.AddRange(new object[] {
            "Personal",
            "Corporation"});
            this.LicenseTypeComboBox.Location = new System.Drawing.Point(90, 67);
            this.LicenseTypeComboBox.Name = "LicenseTypeComboBox";
            this.LicenseTypeComboBox.Size = new System.Drawing.Size(179, 21);
            this.LicenseTypeComboBox.TabIndex = 4;
            // 
            // LicenseTypeLabel
            // 
            this.LicenseTypeLabel.AutoSize = true;
            this.LicenseTypeLabel.Location = new System.Drawing.Point(13, 67);
            this.LicenseTypeLabel.Name = "LicenseTypeLabel";
            this.LicenseTypeLabel.Size = new System.Drawing.Size(71, 13);
            this.LicenseTypeLabel.TabIndex = 5;
            this.LicenseTypeLabel.Text = "License Type";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(193, 95);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 6;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // SecurRipLicenseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 143);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.LicenseTypeLabel);
            this.Controls.Add(this.LicenseTypeComboBox);
            this.Controls.Add(this.ExpiredTimeLabel);
            this.Controls.Add(this.ExpiredTimePicker);
            this.Controls.Add(this.KeyTextBox);
            this.Controls.Add(this.KeyLabel);
            this.Name = "SecurRipLicenseForm";
            this.Text = "SecurRip License";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label KeyLabel;
        private System.Windows.Forms.TextBox KeyTextBox;
        private System.Windows.Forms.DateTimePicker ExpiredTimePicker;
        private System.Windows.Forms.Label ExpiredTimeLabel;
        private System.Windows.Forms.ComboBox LicenseTypeComboBox;
        private System.Windows.Forms.Label LicenseTypeLabel;
        private System.Windows.Forms.Button SaveButton;
    }
}

