
#-*- coding: utf-8 -*-

#DATE DE FIN DE VALIDITE: NA

###################################################################
#Script Name	:NommageSIDFibre31
#Version       	:1.0.0
#Description  	:Rempli automatiquement certaines informations en partant des informations de couches requises
#                       Voir ReadMe.txt Pour plus d'informations
#                       Version FlowChart Disponible, demmander à l'auteur du script.
#Date DMAJ  	:derniere mise a jour le 03/12/2019
#Author        	:EL BASRI   Tristan
#Email          :tristan.el-basri@cfitech.fr
###################################################################

###################################################################
#
#Ce script est la propriete exclusive de CFItech.
#
###################################################################



#Imports
import sys
import os
from math import *
import datetime
import getpass

from PyQt5 import QtCore
from PyQt5.QtGui import  *
from PyQt5 import *
from PyQt5.QtWidgets import QProgressDialog
from qgis.core import *
import qgis.utils
import copy
import shutil
import csv
#Initialisations -------------------------------------------------
# consoleWidget = iface.mainWindow().findChild( QDockWidget, 'PythonConsole' )
NowIs = datetime.datetime.now()
GetDistance = QgsDistanceArea()
def showError():
	pass

def DisplayWarning(WarningText):
	# widget = iface.messageBar().createMessage(WarningText)#, "Show Me")
	#button = QPushButton(widget)
	#button.setText("Show Me")
	#button.pressed.connect(showError)
	#widget.layout().addWidget(button)
	# iface.messageBar().pushWidget(widget, Qgis.Warning)
	pass

def GetCircetForatShapesLoaded(studyName):
	#saveLocation = r'C:\\Users\\tristan.el-basri\\Desktop\\Temporaire\\SecuRip\\SecurRIP_ConvertOrange\\savingFolderTest'
	#baseFilesFolder = r'C:\\Users\\tristan.el-basri\\Desktop\\Temporaire\\SecuRip\\SecurRIP_ConvertOrange\\BaseShapeFolder'
	saveLocation = r'C:\\Users\\wirewind\\Documents\\teletravail\\SecurRIP_Eiffage\\savingFolderTest'
	baseFilesFolder = r'C:\\Users\\wirewind\\Documents\\teletravail\\SecurRIP_Eiffage\\BaseShapeFolder'
	print("SAVE LOCATION: "+str(saveLocation))
	print("BaseFolder Location: "+str(baseFilesFolder))
	#ZANOR
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.prj',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.qml',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.shp',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.shx',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.shx')
	
	ZANROlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.shp', str(studyName)+"_ZANRO", "ogr")
	QgsProject.instance().addMapLayer(ZANROlayer)
	if not ZANROlayer.isValid():
		print("ZANRO_layer failed to load!")

	#ZASOR
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.prj',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.qml',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.shp',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.shx',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.shx')
	
	ZASROlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.shp', str(studyName)+"_ZASRO", "ogr")
	QgsProject.instance().addMapLayer(ZASROlayer)
	if not ZASROlayer.isValid():
		print("ZASRO_layer failed to load!")

	#ZACABLE
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.prj',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.qml',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.shp',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.shx',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.shx')
	
	ZACABLElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.shp', str(studyName)+"_ZACABLE", "ogr")
	QgsProject.instance().addMapLayer(ZACABLElayer)
	if not ZACABLElayer.isValid():
		print("ZACABLE_layer failed to load!")

	#ZAPBO
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.prj',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.qml',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.shp',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.shx',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.shx')
	
	ZAPBOlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.shp', str(studyName)+"_ZAPBO", "ogr")
	QgsProject.instance().addMapLayer(ZAPBOlayer)
	if not ZAPBOlayer.isValid():
		print("ZAPBO_layer failed to load!")

	#SUPPORT
	#shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.cpg',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.dbf',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.prj',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.qml',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.shp',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.shx',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.shx')
	
	SUPPORTlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.shp', str(studyName)+"_SUPPORT", "ogr")
	QgsProject.instance().addMapLayer(SUPPORTlayer)
	if not SUPPORTlayer.isValid():
		print("SUPPORT_layer failed to load!")

	#CABLE_OPTIQUE
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.prj',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.qml',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.shp',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.shx',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.shx')
	
	CABLE_OPTIQUElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.shp', str(studyName)+"_CABLE_OPTIQUE", "ogr")
	QgsProject.instance().addMapLayer(CABLE_OPTIQUElayer)
	if not CABLE_OPTIQUElayer.isValid():
		print("CABLE_OPTIQUE_layer failed to load!")

	#ADDUCTION
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.prj',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.qml',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.shp',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.shx',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.shx')
	
	ADDUCTIONlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.shp', str(studyName)+"_ADDUCTION", "ogr")
	QgsProject.instance().addMapLayer(ADDUCTIONlayer)
	if not ADDUCTIONlayer.isValid():
		print("ADDUCTION_layer failed to load!")

	#SITE
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_SITE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_SITE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.prj',str(saveLocation)+"\\"+str(studyName)+r'_SITE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.qml',str(saveLocation)+"\\"+str(studyName)+r'_SITE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.shp',str(saveLocation)+"\\"+str(studyName)+r'_SITE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.shx',str(saveLocation)+"\\"+str(studyName)+r'_SITE.shx')
	
	SITElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_SITE.shp', str(studyName)+"_SITE", "ogr")
	QgsProject.instance().addMapLayer(SITElayer)
	if not SITElayer.isValid():
		print("SITE_Layer failed to load!")

	#POINT_TECHNIQUE
	#shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.prj',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.qml',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.shp',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.shx',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.shx')
	
	POINT_TECHNIQUElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.shp', str(studyName)+"_POINT_TECHNIQUE", "ogr")
	QgsProject.instance().addMapLayer(POINT_TECHNIQUElayer)
	if not POINT_TECHNIQUElayer.isValid():
		print("POINT_TECHNIQUE_layer failed to load!")

	#BOITE_OPTIQUE
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.prj',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.qml',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.shp',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.shx',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.shx')
	
	BOITE_OPTIQUElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.shp', str(studyName)+"_BOITE_OPTIQUE", "ogr")
	QgsProject.instance().addMapLayer(BOITE_OPTIQUElayer)
	if not BOITE_OPTIQUElayer.isValid():
		print("BOITE_OPTIQUE_layer failed to load!")

	#DBL_COMPLEMENT
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.cpg',str(saveLocation)+"\\"+r'DBL COMPLET.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.dbf',str(saveLocation)+"\\"+r'DBL COMPLET.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.prj',str(saveLocation)+"\\"+r'DBL COMPLET.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.qpj',str(saveLocation)+"\\"+r'DBL COMPLET.qpj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.shp',str(saveLocation)+"\\"+r'DBL COMPLET.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.shx',str(saveLocation)+"\\"+r'DBL COMPLET.shx')
	
	DBL_COMPLEMENTlayer = QgsVectorLayer(str(saveLocation)+"\\"+r'DBL COMPLET.shp', "DBL_COMPLET", "ogr")
	QgsProject.instance().addMapLayer(DBL_COMPLEMENTlayer)
	if not DBL_COMPLEMENTlayer.isValid():
		print("DBL_COMPLET_layer failed to load!")


def AddZANroFeature(LayersInfoDic):
	# add a feature
	ZNrofeat = QgsFeature()
	for Zone_Feat in LayersInfoDic["Eif_Zones_Layer"].getFeatures():
		if(Zone_Feat.attributes()[LayersInfoDic["EifZONE_Typ_PfAttr"]] == 'NRO'):
			zNroGeometry = Zone_Feat.geometry()
			break

	ZNrofeat.setGeometry(zNroGeometry)

	ZANRO_sid = 0 #id voir ce qu'on veux mettre
	ZANRO_ref_nro = LayersInfoDic["ZNRO_REF"]
	ZANRO_ref_nra = ''
	ZANRO_degroup = 0
	ZANRO_bt_sfr = 0
	ZANRO_nb_prise = 0
	ZANRO_planning = 1
	ZANRO_sst = 'EIFFAGE'
	ZANRO_zone = 'EIF'
	ZANRO_zn_code = str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["EifSitCH_CodeAttr"]])
	ZANRO_nb_prise_r = 0
	ZANRO_sst_rbal = ''
	ZANRO_date_rbal = ''
	ZANRO_com_tvx = ''
	ZANRO_com_sst = ''
	ZANRO_sst_rang2 = ''

	ZNrofeat.setAttributes([ZANRO_sid, ZANRO_ref_nro, ZANRO_ref_nra, ZANRO_degroup, ZANRO_bt_sfr, ZANRO_nb_prise, ZANRO_planning, ZANRO_sst, ZANRO_zone, ZANRO_zn_code, ZANRO_nb_prise_r, ZANRO_sst_rbal, ZANRO_date_rbal, ZANRO_com_tvx, ZANRO_com_sst, ZANRO_sst_rang2])
	LayersInfoDic["ZANRO_Layer"].dataProvider().addFeature(ZNrofeat)
	LayersInfoDic["ZANRO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ZANRO_Layer"])

def AddZASroFeature(LayersInfoDic):
	# add a feature
	ZSrofeat = QgsFeature()
	for thisSTech in LayersInfoDic["Eif_PF_Layer"].getFeatures():
		if(thisSTech.attributes()[LayersInfoDic["EifPF_TypSitAttr"]] == 'Armoire'):
			for Zsro_Feat in LayersInfoDic["Eif_Zones_Layer"].getFeatures():
				if(Zsro_Feat.attributes()[LayersInfoDic["EifZONE_Typ_PfAttr"]] == 'PMZ'):
					if(Zsro_Feat.geometry().intersects(thisSTech.geometry())):
						zSroGeometry = Zsro_Feat.geometry()
						break
			break

	ZSrofeat.setGeometry(zSroGeometry)

	ZASRO_ref_sro = LayersInfoDic["ZSRO_REF"]
	ZASRO_ref_nro = LayersInfoDic["ZNRO_REF"]
	ZASRO_nb_prises = 0
	ZASRO_epci = 'NR'
	ZASRO_planning = 0
	ZASRO_sst = 'EIFFAGE'

	#ZSrofeat.setAttributes([str(tZsro_Feat.attributes()[LayersInfoDic["ZS_RefPmAttr"]])])
	ZSrofeat.setAttributes([ZASRO_ref_sro, ZASRO_ref_nro, ZASRO_nb_prises, ZASRO_epci, ZASRO_planning, ZASRO_sst])
	LayersInfoDic["ZASRO_Layer"].dataProvider().addFeature(ZSrofeat)
	LayersInfoDic["ZASRO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ZASRO_Layer"])

def AddZAPboFeature(LayersInfoDic):
	# add a feature
	for ThistZpboFeat in LayersInfoDic["Eif_Zones_Layer"].getFeatures():
		if(ThistZpboFeat.attributes()[LayersInfoDic["EifZONE_Typ_PfAttr"]] != 'PB'): continue

		NewZPbofeat = QgsFeature()
		NewZPbofeat.setGeometry(ThistZpboFeat.geometry())

		ZAPBO_nom = 'ZPB-'+str(ThistZpboFeat.attributes()[LayersInfoDic["EifZONE_CodeAttr"]]).split('/')[-1]
		ZAPBO_nb_prise = 0
		if(str(ThistZpboFeat.attributes()[LayersInfoDic["EifZONE_Nb_ElAttr"]]).isdigit()):
			ZAPBO_nb_prise = int(ThistZpboFeat.attributes()[LayersInfoDic["EifZONE_Nb_ElAttr"]])
		ZAPBO_commentair = ThistZpboFeat.attributes()[LayersInfoDic["EifZONE_CommentAttr"]]
		ZAPBO_isole = 0

		#NewZPbofeat.setAttributes([str(ThistZpboFeat.attributes()[LayersInfoDic["ZP_CodeAttr"]]),0, ThistZpboFeat.id(),0])
		NewZPbofeat.setAttributes([ZAPBO_nom, ZAPBO_nb_prise, ZAPBO_commentair, ZAPBO_isole])
		LayersInfoDic["ZAPBO_Layer"].dataProvider().addFeature(NewZPbofeat)
		LayersInfoDic["ZAPBO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ZAPBO_Layer"])

def AddSIFeature(LayersInfoDic):
	# add a feature
	for ThistCheminFeat in LayersInfoDic["Eif_Parcour_Layer"].getFeatures():
		NewSIfeat = QgsFeature()
		NewSIfeat.setGeometry(ThistCheminFeat.geometry())

		SUPP_nom = str(ThistCheminFeat.attributes()[LayersInfoDic["EifSI_CodeAttr"]])
		SUPP_code = str(ThistCheminFeat.attributes()[LayersInfoDic["EifSI_CodeAttr"]])
		SUPP_propri = 'EIFFAGE'
		SUPP_gestio = str(ThistCheminFeat.attributes()[LayersInfoDic["EifSI_OperAttr"]])
		SUPP_type_str = 'TELECOM'
		SUPP_lg_reel = int(NewSIfeat.geometry().length())
		SUPP_largeur = 0
		SUPP_hauteur = 0
		
		SUPP_amont = str(ThistCheminFeat.attributes()[LayersInfoDic["EifSI_IdMet1Attr"]]).split('/')[0]
		SUPP_aval = str(ThistCheminFeat.attributes()[LayersInfoDic["EifSI_IdMetAttr"]]).split('/')[0]
		SUPP_insee = LayersInfoDic["INSEE"]
		SUPP_comment = str(ThistCheminFeat.attributes()[LayersInfoDic["EifSI_CommentAttr"]])
		SUPP_structure = 'EXI' #/!\ PROVISOIR
		'''
		if('AERIEN' in str(ThistCheminFeat.attributes()[LayersInfoDic["tdfCM_TypImpAttr"]])):
			SUPP_structure = 'AER'
		#elif(str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypImpAttr"]]) == '9'):
		#SUPP_structure = 'TRA'
		else:
			SUPP_structure = 'EXI'
		'''
		SUPP_isole = 0
		SUPP_compo = str(ThistCheminFeat.attributes()[LayersInfoDic["EifSI_CompoAttr"]])
		SUPP_disponible = ''#str(ThistCheminFeat.attributes()[LayersInfoDic["CM_EtatAttr"]])
		SUPP_uttilisation = ''
		'''
		if(str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypLogAttr"]]) == 'TR'):
			SUPP_uttilisation = 'T'
		elif(str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypLogAttr"]]) == 'TD'):
			SUPP_uttilisation = 'TD'
		elif(str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypLogAttr"]]) == 'DI'):
			SUPP_uttilisation = 'D'
		'''
		SUPP_casse_bloc = ''
		SUPP_ref_fiche = ''
		SUPP_mode_pose = ''#str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypImpAttr"]])

		#NewSIfeat.setAttributes([str(ThistCheminFeat.attributes()[LayersInfoDic["CM_CodeAttr"]]), str(ThistCheminFeat.attributes()[LayersInfoDic["CM_CodeAttr"]]),'','','TELECOM',0,0,0,'ND_Amont','ND_Aval','Insee',ThistCheminFeat.id(),str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypImpAttr"]]), 0,'compo','',str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypLogAttr"]])])
		NewSIfeat.setAttributes([SUPP_nom, SUPP_code, SUPP_propri, SUPP_gestio, SUPP_type_str, SUPP_lg_reel, SUPP_largeur, SUPP_hauteur, SUPP_amont, SUPP_aval, SUPP_insee, SUPP_comment, SUPP_structure, SUPP_isole, SUPP_compo, SUPP_disponible, SUPP_uttilisation, SUPP_casse_bloc, SUPP_ref_fiche, SUPP_mode_pose])
		LayersInfoDic["SI_Layer"].dataProvider().addFeature(NewSIfeat)
		LayersInfoDic["SI_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["SI_Layer"])

def AddCOFeature(LayersInfoDic):
	# add a feature
	for ThistCableLineFeat in LayersInfoDic["Eif_Cable_Layer"].getFeatures():
		print('Doing Co: '+str(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_CodeAttr"]])))
		nbAddedSegments = 0
		lineGeom = ThistCableLineFeat.geometry().asMultiPolyline()[0]
		origpoint = QgsGeometry.fromPointXY(QgsPointXY(lineGeom[0].x(),lineGeom[0].y()))
		ectrpoint = QgsGeometry.fromPointXY(QgsPointXY(lineGeom[-1].x(),lineGeom[-1].y()))
		#Get valid CO only (valid If orig && extr  in Zsro )
		if(not (origpoint.intersects(LayersInfoDic["ZSRO_GEOM"]) and ectrpoint.intersects(LayersInfoDic["ZSRO_GEOM"]))): continue
		NewCOfeat = QgsFeature()

		#Cut CO
		newAmAvBos = ['','']
		if(int(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]).split('/')[0]) == int(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_IdMetAttr"]]).split('/')[0])):
			newAmAvBos[0] = str(LayersInfoDic["ZSRO_REF"])#str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["EifPF_CodeAttr"]]).replace('/','.')
		else:
			newAmAvBos[0] = ''
		for ThisBo in LayersInfoDic["BO_Layer"].getFeatures():
			if(int(str(ThisBo.attributes()[LayersInfoDic["BO_IdParAttr"]])) == int(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_IdMetAttr"]]).split('/')[0])):
				newAmAvBos[0] = (str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]]))
		coPartInd = -1
		newCofeatParts = [ThistCableLineFeat.geometry().asMultiPolyline()[0][0]]
		allCbFeat = ThistCableLineFeat.geometry().asMultiPolyline()[0]
		for PoinCoInd, thisPointInCo in enumerate(allCbFeat[1:]):
			boCycle = 0
			for ThisBo in LayersInfoDic["BO_Layer"].getFeatures():
				

				#if(ThisBo.geometry().distance(QgsGeometry.fromPointXY(ThistCableLineFeat.geometry().asMultiPolyline()[0][int(len(allCbFeat)/2)])) > ThistCableLineFeat.geometry().length() / 2): continue
				if(boCycle == 0):
					newCofeatParts.append(thisPointInCo)
				
				if(ThisBo.geometry().asPoint() == thisPointInCo or (PoinCoInd == len(allCbFeat[1:])-1 and ThisBo.geometry().distance(QgsGeometry.fromPointXY(thisPointInCo))<.1)):
					newAmAvBos[1] = str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]])
					print('Extr Found?')
					placeOccup = False
					if(PoinCoInd < len(allCbFeat[1:])-1):
						for ThistCableLineFeatBis in LayersInfoDic["Eif_Cable_Layer"].getFeatures():
							for thisPointInCoBis in ThistCableLineFeatBis.geometry().asMultiPolyline()[0][1:]:
								if(ThistCableLineFeatBis.attributes()[LayersInfoDic["EifCB_CodeAttr"]] != ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_CodeAttr"]] and thisPointInCo == thisPointInCoBis):
									print('Occuped')
									placeOccup = True
									break
					if(placeOccup):break
					print("CUT HERE (bo: "+str(str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]]))+')')

					#Create Co Connection for 2BO is Same PT
					print("?doCycleCable : "+str(len(newCofeatParts)))
					if(len(newCofeatParts)==1):#[newCofeatParts[-1]]):
						print('newcofeat0: '+str(newCofeatParts))
						newCofeatParts.append(QgsPointXY(newCofeatParts[0].x()+0.01,newCofeatParts[0].y()))
						newCofeatParts.append(QgsPointXY(newCofeatParts[0].x(),newCofeatParts[0].y()))
						print('newcofeat1: '+str(newCofeatParts))
						print("CycleCable Done")

					coPartInd += 1 
					#print("co geo: "+str(newCofeatParts))
					#Process
					NewCOfeat.setGeometry(QgsGeometry.fromPolyline(QgsPoint(P) for P in newCofeatParts))#(ThistCableLineFeat.geometry())
					CO_nom = str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_CodeAttr"]])+'-'+str(coPartInd)
					CO_code = str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_RefCbAttr"]])
					CO_type_fonc = ''#str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_TypLogAttr"]])
					'''
					if(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_TypeLogAttr"]]) == 'DI'):
						CO_type_fonc = 'DISTRIBUTION'
					'''
					CO_type_fonc = 'DISTRIBUTION'
					if(not (origpoint.intersects(LayersInfoDic["ZSRO_GEOM"]) and ectrpoint.intersects(LayersInfoDic["ZSRO_GEOM"]))): 
						print("Cable Hors Zsro")
						CO_type_fonc = 'TRANSPOR'
					CO_type_struct = 'CONDUITE' #/!\ PROVISOIR
					'''
					if('AERIEN' in str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CommentAttr"]])):# DOUTE
						CO_type_struct = 'AERIEN'
					elif('SOUTERRAIN' in str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CommentAttr"]])):
						CO_type_struct = 'CONDUITE'
					'''
					CO_emprise = ''#str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_PropTypAttr"]])
					'''
					if(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'CST'):
						CO_emprise = 'CONSTRUCTION'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'RAC'):
						CO_emprise = 'RACHAT'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'CES'):
						CO_emprise = 'CESSION'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'IRU'):
						CO_emprise = 'IRU'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'LOC'):
						CO_emprise = 'LOCATION'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'OCC'):
						CO_emprise = 'OCCUPATION'
					'''
					CO_propri = 'EIFFAGE'
					CO_gestio = str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_OperAttr"]])
					CO_etat = ''
					'''
					if(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'PRE'):
						CO_etat = 'ETUDE PRELIMINAIRE'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'DIA'):
						CO_etat = 'ETUDE DE DIAGNOSTIC'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'AVP'):
						CO_etat = 'AVANT-PROJET'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'PRO'):
						CO_etat = 'PROJET'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'ACT'):
						CO_etat = 'PASSATION DES MARCHES DE TRAVAUX'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'EXE'):
						CO_etat = 'ETUDE D EXECUTION'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'TVX'):
						CO_etat = 'TRAVAUX'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'REC'):
						CO_etat = 'RECOLLEMENT'
					elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'MCO'):
						CO_etat = 'MAINTIEN EN CONDITIONS OPERATIONNELLES'
					'''
					CO_fabricant = ''
					CO_reference = ''
					CO_lg_reel = NewCOfeat.geometry().length()#int(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_LgReelAttr"]])
					CO_lg_carto = ThistCableLineFeat.geometry().length()
					CO_diametre = float(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_DiamAttr"]]).replace(',','.'))
					CO_typefibre = ''
					CO_comment = str(ThistCableLineFeat.id())
					CO_rattach = LayersInfoDic["ZSRO_REF"]
					CO_capacite = int(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_NbFibrAttr"]])
					#print("\n\nOrig-Extr co: "+str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CodeAttr"]]))
					
					CO_origine = newAmAvBos[0]
					CO_extremite = newAmAvBos[1]
					newAmAvBos[0] = newAmAvBos[1]
					newAmAvBos[1] = ''
					#if(int(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]).split('/')[0]) == int(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_IdMetAttr"]]).split('/')[0])):
					#	CO_origine = 'PMZ'#str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["EifPF_CodeAttr"]])
					#else:
					#	CO_origine = ''
					#if(int(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]).split('/')[0]) == int(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_IdMet1Attr"]]).split('/')[0])):
					#	CO_extremite = 'PMZ'#str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["EifPF_CodeAttr"]])
					#else:
					#	CO_extremite = ''
					#for ThisBo in LayersInfoDic["BO_Layer"].getFeatures():
					#	#print('thisBo to check: '+str(ThisBo.attributes()[LayersInfoDic["BO_CodeAttr"]]).split('/')[0])
					#	if(not str(ThisBo.attributes()[LayersInfoDic["BO_CodeAttr"]]).split('/')[0].isdigit()): continue
					#	#print("Bo infos:\n"+str(int(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_IdMetAttr"]]).split('/')[0]))+"\n"+str(int(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_IdMet1Attr"]]).split('/')[0]))+'\n')
					#	if(int(str(ThisBo.attributes()[LayersInfoDic["BO_CodeAttr"]]).split('/')[0]) == int(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_IdMetAttr"]]).split('/')[0])):
					#		CO_origine = str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]])
					#	if(int(str(ThisBo.attributes()[LayersInfoDic["BO_CodeAttr"]]).split('/')[0]) == int(str(ThistCableLineFeat.attributes()[LayersInfoDic["EifCB_IdMet1Attr"]]).split('/')[0])):
					#		CO_extremite = str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]])
					#	if(CO_origine != '' and CO_extremite != ''): break
					
					CO_section = ''
					CO_isole = 0

					#NewCOfeat.setAttributes([str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_CodeAttr"]]),'',str(ThisCableInfo.attributes()[LayersInfoDic["CB_TypeLogAttr"]]),'tCalbeLine Type','CONSTRUCTION','??','??',str(ThisCableInfo.attributes()[LayersInfoDic["CB_PropTypAttr"]]),'??','??',0.0,0.0,float(ThisCableInfo.attributes()[LayersInfoDic["CB_DiamAttr"]]),'??','','SRO',float(ThisCableInfo.attributes()[LayersInfoDic["CB_CapaFoAttr"]]),'','','', 0])
					NewCOfeat.setAttributes([CO_nom, CO_code, CO_type_fonc, CO_type_struct, CO_emprise, CO_propri, CO_gestio, CO_etat, CO_fabricant, CO_reference, CO_lg_reel, CO_lg_carto, CO_diametre, CO_typefibre, CO_comment, CO_rattach, CO_capacite, CO_origine, CO_extremite, CO_section, CO_isole])
					LayersInfoDic["CO_Layer"].dataProvider().addFeature(NewCOfeat)
					LayersInfoDic["CO_Layer"].updateExtents()

					newCofeatParts=[newCofeatParts[-1]]
					print('NewPart: '+str(newCofeatParts))
					boCycle += 1
					#break

	QgsProject.instance().addMapLayer(LayersInfoDic["CO_Layer"])

def AddSITEFeature(LayersInfoDic):
	# add a feature
	NewSITEfeat = QgsFeature()
	NewSITEfeat.setGeometry(LayersInfoDic["SRO_GEO"])

	SITE_code = LayersInfoDicGlob["ZSRO_REF"]#str(SroFeat.attributes()[LayersInfoDic["ST_CodeAttr"]])
	SITE_nom = LayersInfoDicGlob["ZSRO_REF"]#str(SroFeat.attributes()[LayersInfoDic["ST_CodeAttr"]])
	SITE_propri = 'EIFFAGE'#str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_PropAttr"]])
	SITE_gestio = 'EIFFAGE'#str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_GestAttr"]])
	SITE_type_fonc = 'SOUS-REPARTITEUR OPTIQUE'
	SITE_etat = ''
	'''
	if(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'PRE'):
		SITE_etat = 'ETUDE PRELIMINAIRE'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'DIA'):
		SITE_etat = 'ETUDE DE DIAGNOSTIC'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'AVP'):
		SITE_etat = 'AVANT-PROJET'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'PRO'):
		SITE_etat = 'PROJET'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'ACT'):
		SITE_etat = 'PASSATION DES MARCHES DE TRAVAUX'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'EXE'):
		SITE_etat = 'ETUDE D EXECUTION'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'TVX'):
		SITE_etat = 'TRAVAUX'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'REC'):
		SITE_etat = 'RECOLLEMENT'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StatAttr"]]) == 'MCO'):
		SITE_etat = 'MAINTIEN EN CONDITIONS OPERATIONNELLES'
	'''
	SITE_type_struc = ''#str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StTypPhyAttr"]])
	SITE_emprise = ''#str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_PropTypAttr"]])
	SITE_typerac = ''
	SITE_hexacle = ''#str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_HexAttr"]])
	SITE_nbprise = int(LayersInfoDic["ZSRO_FEAT"].attributes()[LayersInfoDic["EifZONE_Nb_ElAttr"]])
	SITE_comment = ''
	SITE_rattach = LayersInfoDic["ZNRO_REF"]
	SITE_insee = LayersInfoDic["INSEE"]
	SITE_st_code = 'EIF'
	SITE_date_pose = ''
	SITE_EXTRMETHOD = 'SRIP_EIFFAGE'

	#NewSITEfeat.setAttributes([str(SroFeat.attributes()[LayersInfoDic["ST_CodeAttr"]]),'Nom','??','??','SOUS-REPARTIREUR OPTIQUE','etat?','TypStruc',SroFeat.attributes()[LayersInfoDic["ST_PropTypAttr"]],'??','HAXc',0,str(ThisttNoeud.id()),'Nro','Insee','STcode',''])
	NewSITEfeat.setAttributes([SITE_code, SITE_nom, SITE_propri, SITE_gestio, SITE_type_fonc, SITE_etat, SITE_type_struc, SITE_emprise, SITE_typerac, SITE_hexacle, SITE_nbprise, SITE_comment, SITE_rattach, SITE_insee, SITE_st_code, SITE_date_pose, SITE_EXTRMETHOD])
	LayersInfoDic["SNRO_Layer"].dataProvider().addFeature(NewSITEfeat)
	LayersInfoDic["SNRO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["SNRO_Layer"])

def AddPTFeature(LayersInfoDic):
	# add a feature
	for thisPTech in LayersInfoDic["Eif_SiteCH_Layer"].getFeatures():	
		NewPTfeat = QgsFeature()
		NewPTfeat.setGeometry(thisPTech.geometry())

		PT_nom = str(thisPTech.attributes()[LayersInfoDic["EifSitCH_CodeAttr"]])
		if(str(thisPTech.attributes()[LayersInfoDic["EifSitCH_CodeCh1Attr"]]) != 'NULL'):
			PT_code = str(thisPTech.attributes()[LayersInfoDic["EifSitCH_CodeCh1Attr"]])
		else:
			PT_code = 'NoCh1Code'
		PT_gestionnai = 'EIFFAGE'#str(thisPTech.attributes()[LayersInfoDic["EifSitCH_IdGestAttr"]])
		PT_type_fonc = 'TIRAGE' #/!\ PROVISOIR
		'''
		if(str(thisPTech.attributes()[LayersInfoDic["tPT_TypeLogAttr"]]) == 'T'):
			PT_type_fonc = 'TIRAGE'
		elif(str(thisPTech.attributes()[LayersInfoDic["tPT_TypeLogAttr"]]) == 'R'):
			PT_type_fonc = 'RACCORDEMENT'
		'''
		PT_type_struc = 'CHAMBRE' #/!\ PROVISOIR #str(thisPTech.attributes()[LayersInfoDic["tdfPT_TypPhyAttr"]])
		PT_etat = 'ETUDE D EXECUTION' #/!\ PROVISOIR
		'''
		if(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'PRE'):
			PT_etat = 'ETUDE PRELIMINAIRE'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'DIA'):
			PT_etat = 'ETUDE DE DIAGNOSTIC'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'AVP'):
			PT_etat = 'AVANT-PROJET'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'PRO'):
			PT_etat = 'PROJET'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'ACT'):
			PT_etat = 'PASSATION DES MARCHES DE TRAVAUX'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'EXE'):
			PT_etat = 'ETUDE D EXECUTION'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'TVX'):
			PT_etat = 'TRAVAUX'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'REC'):
			PT_etat = 'RECOLLEMENT'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'MCO'):
			PT_etat = 'MAINTIEN EN CONDITIONS OPERATIONNELLES'
		'''
		PT_emprise = 'LOCATION' #/!\ PROVISOIR
		'''
		if(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'CST'):
			PT_emprise = 'CONSTRUCTION'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'RAC'):
			PT_emprise = 'RACHAT'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'CES'):
			PT_emprise = 'CESSION'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'IRU'):
			PT_emprise = 'IRU'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'LOC'):
			PT_emprise = 'LOCATION'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'OCC'):
			PT_emprise = 'OCCUPATION'
		'''
		PT_longeur = '0' #/!\ PROVISOIR
		PT_largeur = '0' #/!\ PROVISOIR
		PT_prof_haut = '0' #/!\ PROVISOIR
		PT_secteur = str(LayersInfoDic["ZSRO_REF"])[-5:]
		PT_rattach = str(LayersInfoDic["ZSRO_REF"])
		PT_modele = str(thisPTech.attributes()[LayersInfoDic["EifSitCH_RefChAttr"]])
		PT_comment = str(thisPTech.attributes()[LayersInfoDic["EifSitCH_CommentAttr"]])
		PT_isole = 0
		PT_proprietai = 'EIFFAGE'#str(thisPTech.attributes()[LayersInfoDic["EifSitCH_IdPropAttr"]])
		PT_idparent = ''
		PT_remplaceapp = ''
		PT_ThdCodeExt = ''#str(thisPTech.attributes()[LayersInfoDic["tPT_CodeAttr"]])

		#NewPTfeat.setAttributes([str(thisPTech.attributes()[LayersInfoDic["tPT_CodeAttr"]]),'','ORANGE',str(thisPTech.attributes()[LayersInfoDic["tPT_TypeLogAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_TypePhyAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_StatutAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_PropTypAttr"]]),0.0,0.0,PtAppuiHauteur,'','#SRO',str(thisPTech.attributes()[LayersInfoDic["tPT_NatureAttr"]]),'',0,'ORANGE','','',str(ThisttNoeud.id())])
		NewPTfeat.setAttributes([PT_nom, PT_code, PT_gestionnai, PT_type_fonc, PT_type_struc, PT_etat, PT_emprise, PT_longeur, PT_largeur, PT_prof_haut, PT_secteur, PT_rattach, PT_modele, PT_comment, PT_isole, PT_proprietai, PT_idparent, PT_remplaceapp,PT_ThdCodeExt])
		LayersInfoDic["PT_Layer"].dataProvider().addFeature(NewPTfeat)
		LayersInfoDic["PT_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["PT_Layer"])

def AddBOFeature(LayersInfoDic, EifxlsxOutput):
	# add a feature
	doneBoNamed = []
	for thisBO in LayersInfoDic["Eif_PF_Layer"].getFeatures():
		for outKey in EifxlsxOutput.keys():
			#GetPTinSupport
			thisPtRefPt = ''
			for ThisPtSup in LayersInfoDic["Eif_SiteCH_Layer"].getFeatures():
				if(str(str(thisBO.attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]).split('/')[0]).isdigit() and str(ThisPtSup.attributes()[LayersInfoDic["EifSitCH_CodeCh1Attr"]]).isdigit()):
					if(int(str(thisBO.attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]).split('/')[0]) == int(ThisPtSup.attributes()[LayersInfoDic["EifSitCH_CodeCh1Attr"]])):
						thisPtRefPt = str(ThisPtSup.attributes()[LayersInfoDic["EifSitCH_RefPtAttr"]])
						break
			if(thisPtRefPt == ''):
				thisPtRefPt = 'NoRef'
			if(str(outKey).split('-')[-1] == str(thisBO.attributes()[LayersInfoDic["EifPF_CodeAttr"]]).split('/')[-1]):
				print("Making BO(PA): "+str(thisBO.attributes()[LayersInfoDic["EifPF_CodeAttr"]])+"\n"+str(thisBO.attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]))
				#print('PA Found: '+str(outKey))
				NewBOfeat = QgsFeature()
				NewBOfeat.setGeometry(thisBO.geometry())
				BO_nom = str(thisPtRefPt)#str(thisBO.attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]).split('/')[0]+'/'+str(thisPtRefPt)
				doneBoNamed.append(thisPtRefPt)
				if(';' in thisPtRefPt):
					if(str(thisPtRefPt).split(";")[0] not in doneBoNamed):
						BO_nom = str(thisPtRefPt).split(";")[0]
						doneBoNamed.append(str(thisPtRefPt).split(";")[0])
					else:
						BO_nom = str(thisPtRefPt).split(";")[1]
						doneBoNamed.append(str(thisPtRefPt).split(";")[1])
				BO_code = str(thisBO.attributes()[LayersInfoDic["EifPF_CodeAttr"]])
				BO_typefonc = 'BOITIER PROTECTION EPISSURE'
				BO_etat = ''
				if(str(thisBO.attributes()[LayersInfoDic["EifPF_CodeStatuAttr"]]) == 'D'):
					BO_etat = 'DEPLOYE'
				elif(str(thisBO.attributes()[LayersInfoDic["EifPF_CodeStatuAttr"]]) == 'E'):
					BO_etat = 'EN COURS'
				BO_reference = 'NoRef'
				BO_proprietai = 'EIFFAGE'
				BO_gestionnai = str(thisBO.attributes()[LayersInfoDic["EifPF_OperAttr"]])
				BO_typestruc = 'BPE FO'
				typStr = (thisBO.attributes()[LayersInfoDic["EifPF_CommentAttr"]]).replace('PB','')
				if(typStr.isdigit()):
					BO_typestruc = 'BPE '+str(typStr)+'FO'
				BO_emprise = 'CONSTRUCTION'
				'''
				if(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'CST'):
					BO_emprise = 'CONSTRUCTION'
				elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'RAC'):
					BO_emprise = 'RACHAT'
				elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'CES'):
					BO_emprise = 'CESSION'
				elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'IRU'):
					BO_emprise = 'IRU'
				elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'LOC'):
					BO_emprise = 'LOCATION'
				elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'OCC'):
					BO_emprise = 'OCCUPATION'
				'''
				BO_fabricant = ''
				BO_dimension = ''
				BO_typeequip = ''
				BO_interco = ''#str(thisBO.attributes()[LayersInfoDic["tdfBP_FoncAttr"]])
				#print(EifxlsxOutput[outKey])
				BO_amont = '17019/EAE/'+str(EifxlsxOutput[outKey]['ID_CABLE'])
				BO_volume = 0.0
				BO_comment = ''
				BO_hauteur = 0.0
				BO_nbepissure = 0#int(thisBO.attributes()[LayersInfoDic["tdfBP_NbEpiAttr"]])
				BO_nbabbo = int(EifxlsxOutput[outKey]['Nbre EL'])
				BO_nbfafs = 0
				BO_nbfu = 0
				BO_support = 'NR'
				if(str(EifxlsxOutput[outKey]['Nbre FTTE']).isalpha()):
					BO_support = str(EifxlsxOutput[outKey]['Nbre FTTE']).upper()
				BO_fonction = 'PA'#str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]])
				BO_isole = '0'
				BO_idparent = ''
				for thisPTech in LayersInfoDic["Eif_SiteCH_Layer"].getFeatures():
					if(thisPTech.geometry().asPoint() == thisBO.geometry().asPoint()):
						BO_idparent = thisPTech.attributes()[LayersInfoDic["EifSitCH_Codech1cAttr"]]
						break
				BO_ThdCodeExt = ''#str(thisBO.attributes()[LayersInfoDic["tBP_CodeAttr"]])
				#NewBOfeat.setAttributes([str(thisBO.attributes()[LayersInfoDic["tBP_CodeAttr"]]),'','PointBranchement?',str(thisBO.attributes()[LayersInfoDic["tBP_StatutAttr"]]),'','','',str(thisBO.attributes()[LayersInfoDic["tBP_TypePhyAttr"]]),str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]),'','','','','',0.0,str(ThisttNoeud.id()),0.0,0,0,0,0,'',str(thisBO.attributes()[LayersInfoDic["tBP_TypeLogAttr"]]),'0',''])
				NewBOfeat.setAttributes([BO_nom, BO_code, BO_typefonc, BO_etat, BO_reference, BO_proprietai, BO_gestionnai, BO_typestruc, BO_emprise, BO_fabricant, BO_dimension, BO_typeequip, BO_interco, BO_amont, BO_volume, BO_comment, BO_hauteur, BO_nbepissure, BO_nbabbo, BO_nbfafs, BO_nbfu, BO_support, BO_fonction, BO_isole, BO_idparent, BO_ThdCodeExt])
				LayersInfoDic["BO_Layer"].dataProvider().addFeature(NewBOfeat)
				LayersInfoDic["BO_Layer"].updateExtents()
			elif(type(EifxlsxOutput[outKey]) == type({})):
				for outPBKey in EifxlsxOutput[outKey].keys():
					if(str(outPBKey).split('-')[-1] == str(thisBO.attributes()[LayersInfoDic["EifPF_CodeAttr"]]).split('/')[-1]):
						print("Making BO: "+str(thisBO.attributes()[LayersInfoDic["EifPF_CodeAttr"]])+"\n"+str(thisBO.attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]))
						NewBOfeat = QgsFeature()
						NewBOfeat.setGeometry(thisBO.geometry())
						BO_nom = str(thisPtRefPt)#str(thisBO.attributes()[LayersInfoDic["EifPF_CodeMetAttr"]]).split('/')[0]+'/'+str(thisPtRefPt)
						if(';' in thisPtRefPt):
							if(str(thisPtRefPt).split(";")[1] not in doneBoNamed):
								BO_nom = str(thisPtRefPt).split(";")[1]
								doneBoNamed.append(str(thisPtRefPt).split(";")[1])
							else:
								BO_nom = str(thisPtRefPt).split(";")[0]
								doneBoNamed.append(str(thisPtRefPt).split(";")[0])	
						print("DEBUG - DoneBoNames:"+str(doneBoNamed))
						BO_code = str(thisBO.attributes()[LayersInfoDic["EifPF_CodeAttr"]])
						BO_typefonc = 'POINT DE BRANCHEMENT OPTIQUE'
						BO_etat = ''
						if(str(thisBO.attributes()[LayersInfoDic["EifPF_OperAttr"]]) == 'D'):
							BO_etat = 'DEPLOYE'
						elif(str(thisBO.attributes()[LayersInfoDic["EifPF_OperAttr"]]) == 'E'):
							BO_etat = 'EN COURS'
						BO_reference = ''
						BO_proprietai = 'EIFFAGE'
						BO_gestionnai = str(thisBO.attributes()[LayersInfoDic["EifPF_OperAttr"]])
						BO_typestruc = 'BPE FO'
						typStr = (thisBO.attributes()[LayersInfoDic["EifPF_CommentAttr"]]).replace('PB','')
						if(typStr.isdigit()):
							BO_typestruc = 'BPE '+str(typStr)+'FO'
						BO_emprise = 'CONSTRUCTION'
						'''
						if(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'CST'):
							BO_emprise = 'CONSTRUCTION'
						elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'RAC'):
							BO_emprise = 'RACHAT'
						elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'CES'):
							BO_emprise = 'CESSION'
						elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'IRU'):
							BO_emprise = 'IRU'
						elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'LOC'):
							BO_emprise = 'LOCATION'
						elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'OCC'):
							BO_emprise = 'OCCUPATION'
						'''
						BO_fabricant = ''
						BO_dimension = ''
						BO_typeequip = ''
						BO_interco = ''#str(thisBO.attributes()[LayersInfoDic["tdfBP_FoncAttr"]])
						BO_amont = EifxlsxOutput[outKey][outPBKey]['ID_CABLE']
						BO_volume = 0.0
						BO_comment = ''
						BO_hauteur = 0.0
						BO_nbepissure = 0#int(thisBO.attributes()[LayersInfoDic["tdfBP_NbEpiAttr"]])
						BO_nbabbo = int(EifxlsxOutput[outKey][outPBKey]['Nbre EL'])
						BO_nbfafs = 0
						BO_nbfu = 0
						BO_support = 'NR'
						if(str(EifxlsxOutput[outKey][outPBKey]['Nbre FTTE']).isalpha()):
							BO_support = str(EifxlsxOutput[outKey][outPBKey]['Nbre FTTE']).upper()
						BO_fonction = 'PB'#str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]])
						BO_isole = '0'
						BO_idparent = ''
						for thisPTech in LayersInfoDic["Eif_SiteCH_Layer"].getFeatures():
							if(thisPTech.geometry().asPoint() == thisBO.geometry().asPoint()):
								BO_idparent = thisPTech.attributes()[LayersInfoDic["EifSitCH_CodeCh1Attr"]]
								break
						BO_ThdCodeExt = ''#str(thisBO.attributes()[LayersInfoDic["tBP_CodeAttr"]])
						#NewBOfeat.setAttributes([str(thisBO.attributes()[LayersInfoDic["tBP_CodeAttr"]]),'','PointBranchement?',str(thisBO.attributes()[LayersInfoDic["tBP_StatutAttr"]]),'','','',str(thisBO.attributes()[LayersInfoDic["tBP_TypePhyAttr"]]),str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]),'','','','','',0.0,str(ThisttNoeud.id()),0.0,0,0,0,0,'',str(thisBO.attributes()[LayersInfoDic["tBP_TypeLogAttr"]]),'0',''])
						NewBOfeat.setAttributes([BO_nom, BO_code, BO_typefonc, BO_etat, BO_reference, BO_proprietai, BO_gestionnai, BO_typestruc, BO_emprise, BO_fabricant, BO_dimension, BO_typeequip, BO_interco, BO_amont, BO_volume, BO_comment, BO_hauteur, BO_nbepissure, BO_nbabbo, BO_nbfafs, BO_nbfu, BO_support, BO_fonction, BO_isole, BO_idparent, BO_ThdCodeExt])
						LayersInfoDic["BO_Layer"].dataProvider().addFeature(NewBOfeat)
						LayersInfoDic["BO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["BO_Layer"])

def AddDBLFeature(LayersInfoDic, rbalFile):
	# add a feature
	rbalInfos = []
	with open(str(rbalFile), newline='',encoding = "ISO-8859-1") as f:
		reader = csv.reader(f,delimiter=';')
		for myid ,row in enumerate(reader):
			#print('LIGE: '+str(myid)+" == "+str(row))
			rbalInfos.append(row)
	#print("\n\nrbalInfos: "+str(rbalInfos))
	
	tr = QgsCoordinateTransform(QgsCoordinateReferenceSystem("EPSG:27572"), QgsCoordinateReferenceSystem("EPSG:2154"), QgsProject.instance())
	dblId = 0
	for thisDBL in rbalInfos[1:]:

		xPoint = float(thisDBL[19])
		yPoint = float(thisDBL[20])
		newGeom = QgsGeometry.fromPointXY(QgsPointXY(xPoint,yPoint))
		newGeom.transform(tr)
		#print('RBAL Geom: \n'+str(newGeom)+"\n"+str(QgsGeometry.fromPointXY(QgsPointXY(xPoint,yPoint)))+"\n")
		if(not newGeom.intersects(LayersInfoDic["ZSRO_GEOM"])): continue

		NewDBLfeat = QgsFeature()
		NewDBLfeat.setGeometry(newGeom)#QgsGeometry.fromPointXY(QgsPointXY(xPoint,yPoint)))
		#NewDBLfeat.geometry().transform(tr)
		DBL_id = dblId
		dblId+=1
		DBL_refimb = str(thisDBL[0])
		DBL_ad_code = str(thisDBL[1])
		DBL_hexacle = ''#str(thisDBL.attributes()[LayersInfoDic["tdfAD_HexAttr"]])
		DBL_id_ban = ''#str(thisDBL.attributes()[LayersInfoDic["tdfAD_CodeAttr"]])
		DBL_num_adr = 0
		if(str(thisDBL[2]).isdigit()):
			DBL_num_adr = int(thisDBL[2])
		'''
		if(str(thisDBL.attributes()[LayersInfoDic["tdfAD_NumAttr"]]).isdigit()):
			DBL_num_adr = int(thisDBL.attributes()[LayersInfoDic["tdfAD_NumAttr"]])
		else:
			DBL_num_adr = -1
		'''
		DBL_ex_num_adr = str(thisDBL[3])
		DBL_nom_bat = ''
		DBL_voie_adr = str(thisDBL[4])
		DBL_c_postal = str(thisDBL[8])
		DBL_c_insee = str(thisDBL[6])
		DBL_lib_com = ''
		DBL_planning = '2'
		DBL_nb_prise = 0
		if(str(thisDBL[10]).isdigit()):
			DBL_nb_prise = int(thisDBL[10])
		if(str(thisDBL[12]).isdigit()):
			DBL_nb_prise += int(thisDBL[12])
		#DBL_nb_prise = int(thisDBL[10])+int(thisDBL[12])
		DBL_xL93 = str(thisDBL[15])
		DBL_yL93 = str(thisDBL[16])
		DBL_ref_nro = str(thisDBL[47])
		DBL_ref_sro = str(thisDBL[45])
		DBL_ref_pbo = ''
		DBL_mdp_pbo = ''
		DBL_ddp_pbo = ''
		DBL_dpexp_pbo = ''
		DBL_statut = str(thisDBL[9])
		DBL_type_zone = ''
		'''
		if(str(thisDBL.attributes()[LayersInfoDic["tAD_TypZoneAttr"]]) == '1'):
			DBL_type_zone = 'ZTD HAUTE DENSITE'
		elif(str(thisDBL.attributes()[LayersInfoDic["tAD_TypZoneAttr"]]) == '2'):
			DBL_type_zone = 'ZTD BASSE DENSITE'
		if(str(thisDBL.attributes()[LayersInfoDic["tAD_TypZoneAttr"]]) == '3'):
			DBL_type_zone = 'ZMD'
		'''
		DBL_techno = ''
		DBL_type_bat = ''#str(thisDBL.attributes()[LayersInfoDic["tdfAD_TypImAttr"]])
		DBL_type_cli = ''
		DBL_racco_bat = ''#str(thisDBL.attributes()[LayersInfoDic["tdfAD_RaccAttr"]])
		DBL_trav_bat = ''
		DBL_rac_long = '0'
		DBL_acc_gest = ''#thisDBL.attributes()[LayersInfoDic["tAD_iAccGstAttr"]]
		DBL_rais_gest = ''
		DBL_cont_gest =''
		DBL_t_con_gest = ''
		DBL_type = ''
		DBL_comment = ''#str(thisDBL.attributes()[LayersInfoDic["tdfAD_CommentAttr"]])
		DBL_voie_adr_t = ''
		DBL_id_loc = ''
		DBL_t2_con_ges = ''
		DBL_fonct_gest = ''
		DBL_adr_gest = ''
		DBL_cp_gest = ''
		DBL_mail_gest = ''
		DBL_ville_gest = ''
		DBL_date_ag = ''
		DBL_date_envoie = ''
		DBL_date_relan = ''
		DBL_date_conv = ''
		DBL_besoin_dta = ''
		DBL_dta = ''
		DBL_date_prev = ''
		DBL_date_env_1 = ''
		DBL_date_rel_1 = ''
		DBL_date_signa = ''
		DBL_date_sig_1 = ''
		DBL_equipe = ''
		DBL_date_previ = ''
		DBL_date_fin_t = ''
		DBL_commentaire = ''#thisDBL.attributes()[LayersInfoDic["tAD_CommentAttr"]]
		DBL_orig_adr = 'EIFFAGE'
		DBL_phase = ''
		DBL_com_fib_31 = ''
		DBL_nbprhab = ''#str(thisDBL.attributes()[LayersInfoDic["tdfAD_NbHabAttr"]])
		DBL_nbprpro = ''#str(thisDBL.attributes()[LayersInfoDic["tdfAD_NbPROAttr"]])
		DBL_nbprtte = 0
		DBL_nbprgfu = 0
		DBL_nbprfon = 0
		DBL_sst = 'EIFFAGE'
		DBL_proj_futur = ''
		DBL_date_proj = ''
		DBL_date_refus = ''
		DBL_sst_nego = ''
		DBL_imb_prefib = ''
		DBL_edl_debut = ''
		DBL_edl_fin_tv = ''
		DBL_etapes_con = ''
		DBL_com_sst = ''
		DBL_date_prise = ''
		DBL_date_pri_1 = ''
		DBL_date_pri_2 = ''
		DBL_date_mesc = ''
		DBL_date_rdv_d = ''
		DBL_nd_code = ''
		DBL_zone = 'EIFFAGE'
		DBL_annee1_cir = 0
		DBL_STATUS = 'Couche Test'


		#TRUE!!! NewDBLfeat.setAttributes([str(thisDBL.attributes()[LayersInfoDic["tAD_BatCodeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_CodeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_HexAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_BanIdAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_NumAttr"]]),"??","??", str(thisDBL.attributes()[LayersInfoDic["tAD_NomVoieAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_PostalAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_InseeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_CommuAttr"]]), (int(str(thisDBL.attributes()[LayersInfoDic["tAD_NblHabAttr"]]))+int(str(thisDBL.attributes()[LayersInfoDic["tAD_NblProAttr"]]))),thisDBL.geometry().asPoint().x(),thisDBL.geometry().asPoint().y(),"NRO","SRO",None,"PBO CODE", "PBO PT",None,None, str(thisDBL.attributes()[LayersInfoDic["tAD_iEtatAttr"]]), None, "Z?D", "FTTH", str(thisDBL.attributes()[LayersInfoDic["tAD_iTypeImAttr"]]),"??","??",0])
		#NewDBLfeat.setAttributes([str(thisDBL.attributes()[LayersInfoDic["tAD_BatCodeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_CodeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_NumAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_iTypeImAttr"]]),"FTTH", (int(str(thisDBL.attributes()[LayersInfoDic["tAD_NblHabAttr"]]))+int(str(thisDBL.attributes()[LayersInfoDic["tAD_NblProAttr"]]))),0])
		NewDBLfeat.setAttributes([DBL_id,DBL_refimb,DBL_ad_code,DBL_hexacle,DBL_id_ban,DBL_num_adr,DBL_ex_num_adr,DBL_nom_bat,DBL_voie_adr,DBL_c_postal,DBL_c_insee,DBL_lib_com,DBL_planning,DBL_nb_prise,DBL_xL93,DBL_yL93,DBL_ref_nro,DBL_ref_sro,DBL_ref_pbo,DBL_mdp_pbo,DBL_ddp_pbo,DBL_dpexp_pbo,DBL_statut,DBL_type_zone,DBL_techno,DBL_type_bat,DBL_type_cli,DBL_racco_bat,DBL_trav_bat,DBL_rac_long,DBL_acc_gest,DBL_rais_gest,DBL_cont_gest,DBL_t_con_gest,DBL_type,DBL_comment,DBL_voie_adr_t,DBL_id_loc,DBL_t2_con_ges,DBL_fonct_gest,DBL_adr_gest,DBL_cp_gest,DBL_mail_gest,DBL_ville_gest,DBL_date_ag,DBL_date_envoie,DBL_date_relan,DBL_date_conv,DBL_besoin_dta,DBL_dta,DBL_date_prev,DBL_date_env_1,DBL_date_rel_1,DBL_date_signa,DBL_date_sig_1,DBL_equipe,DBL_date_previ,DBL_date_fin_t,DBL_commentaire,DBL_orig_adr,DBL_phase,DBL_com_fib_31,DBL_nbprhab,DBL_nbprpro,DBL_nbprtte,DBL_nbprgfu,DBL_nbprfon,DBL_sst,DBL_proj_futur,DBL_date_proj,DBL_date_refus,DBL_sst_nego,DBL_imb_prefib,DBL_edl_debut,DBL_edl_fin_tv,DBL_etapes_con,DBL_com_sst,DBL_date_prise,DBL_date_pri_1,DBL_date_pri_2,DBL_date_mesc,DBL_date_rdv_d,DBL_nd_code,DBL_zone,DBL_annee1_cir,DBL_STATUS])
		#print("DBL LAYER: "+str(LayersInfoDic["DBL_Layer"]))
		LayersInfoDic["DBL_Layer"].dataProvider().addFeature(NewDBLfeat)
		LayersInfoDic["DBL_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["DBL_Layer"])
def addADDUCFeature(LayersInfoDic):
	lastId = -1
	for ThistZpboFeat in LayersInfoDic["Eif_Zones_Layer"].getFeatures():
		if(ThistZpboFeat.attributes()[LayersInfoDic["EifZONE_Typ_PfAttr"]] != 'PB'): continue
		thisBOPosition = None
		for thisBO in LayersInfoDic["Eif_PF_Layer"].getFeatures():
			if(thisBO.attributes()[LayersInfoDic["EifPF_TypPfAttr"]] != 'PB'): continue
			if(thisBO.geometry().intersects(ThistZpboFeat.geometry())):
				thisBOPosition = thisBO.geometry().asPoint()
				break
		for thisDBL in LayersInfoDic["DBL_Layer"].getFeatures():
			if(thisDBL.geometry().intersects(ThistZpboFeat.geometry())):
				if(thisBOPosition == None):
					print("\a\nBO Position not found\n\a")
					continue
				NewADDfeat = QgsFeature()
				NewADDfeat.setGeometry(QgsGeometry.fromPolyline([QgsPoint(thisBOPosition.x(),thisBOPosition.y()), QgsPoint(thisDBL.geometry().asPoint().x(),thisDBL.geometry().asPoint().y())]))
				
				ADD_id = lastId +1
				lastId += 1
				ADD_typestruc = 'AERIEN'
				ADD_isole = '0'
				ADD_lg = int(NewADDfeat.geometry().length())
				ADD_comment = ''


				#NewADDfeat.setAttributes([str(thisDBL.id()),'',0,'',''])
				NewADDfeat.setAttributes([ADD_id, ADD_typestruc, ADD_isole, ADD_lg, ADD_comment])
				LayersInfoDic["ADDUC_Layer"].dataProvider().addFeature(NewADDfeat)
				LayersInfoDic["ADDUC_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ADDUC_Layer"])
def OldaddADDUCFeature(LayersInfoDic):
	lastId = -1
	for ThistAdducFeat in LayersInfoDic["tdf_Cable_Layer"].getFeatures():
		if(str(ThistAdducFeat.attributes()[LayersInfoDic["tdfCB_TypLogAttr"]]) != "RACCORDEMENT"): continue
		NewADDfeat = QgsFeature()
		NewADDfeat.setGeometry(ThistAdducFeat.geometry())				
		ADD_id = 0
		ADD_typestruc = ''
		ADD_isole = '0'
		ADD_lg = int(NewADDfeat.geometry().length())
		ADD_comment = ''
		#NewADDfeat.setAttributes([str(thisDBL.id()),'',0,'',''])
		NewADDfeat.setAttributes([ADD_id, ADD_typestruc, ADD_isole, ADD_lg, ADD_comment])
		LayersInfoDic["ADDUC_Layer"].dataProvider().addFeature(NewADDfeat)
		LayersInfoDic["ADDUC_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ADDUC_Layer"])

def AddZACableFeature(LayersInfoDic):
	# add a feature
	ZCablefeat = QgsFeature()
	for tZcable_Feat in LayersInfoDic["Eif_Zones_Layer"].getFeatures():
		if(tZcable_Feat.attributes()[LayersInfoDic["EifZONE_Typ_PfAttr"]] != 'PA'): continue
		zCableGeometry = tZcable_Feat.geometry()
		ZCablefeat.setGeometry(zCableGeometry)

		ZACABLE_nom = str(tZcable_Feat.attributes()[LayersInfoDic["EifZONE_CodeAttr"]])[-5:]
		ZACABLE_nbprise = 0#int(str(tZcable_Feat.attributes()[LayersInfoDic["tdfZB_NbPrisAttr"]]))
		ZACABLE_comment = ''
		ZCablefeat.setAttributes([ZACABLE_nom, ZACABLE_nbprise, ZACABLE_comment])
		LayersInfoDic["ZACABLE_Layer"].dataProvider().addFeature(ZCablefeat)
		LayersInfoDic["ZACABLE_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ZACABLE_Layer"])

def RefindCoInfos(LayersInfoDic):
	CurrentLayer = LayersInfoDic["CO_Layer"]
	CurrentLayer.startEditing()
	for ThisCo in LayersInfoDic["CO_Layer"].getFeatures():
		print("Refining Co: "+str(ThisCo.attributes()[LayersInfoDic["CO_NomAttr"]]))
		#Originie
		if(str(ThisCo.attributes()[LayersInfoDic["CO_OrigAttr"]]) == '' or str(ThisCo.attributes()[LayersInfoDic["CO_OrigAttr"]]) == 'NULL'):
			print("Origine inconu")
			CurrentLayer = LayersInfoDic["CO_Layer"]
			CurrentLayer.startEditing()
			CurrentLayer.changeAttributeValue(ThisCo.id(),LayersInfoDic["CO_OrigAttr"],'UnKnow')
			CurrentLayer.commitChanges()
		#Extremite
		possibleExtr = []
		print("ext: "+str(ThisCo.attributes()[LayersInfoDic["CO_ExtrAttr"]]))
		if(str(ThisCo.attributes()[LayersInfoDic["CO_ExtrAttr"]]) == '' or str(ThisCo.attributes()[LayersInfoDic["CO_ExtrAttr"]]) == 'NULL'):
			print("Extremite inconu")
			for ThisBo in LayersInfoDic["BO_Layer"].getFeatures():
				if(ThisBo.geometry().asPoint() in ThisCo.geometry().asMultiPolyline()[0][::len(ThisCo.geometry().asMultiPolyline()[0])-1]
					and str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]])[:14] not in str(ThisCo.attributes()[LayersInfoDic["CO_OrigAttr"]])):
					possibleExtr.append(str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]]))
					print("extremite Found: "+str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]]))
			if(len(possibleExtr) < 1):
				print("No Extr Found")
				possibleExtrField = "SRip_UnKnow"
			elif(len(possibleExtr) >= 1):
				possibleExtrField = 'SRip_'+str(possibleExtr[0])
			if(len(possibleExtr) > 1):
				for thisExtr in possibleExtr[1:]:
					possibleExtrField += ' or SRip_'+ str(thisExtr)
			CurrentLayer.changeAttributeValue(ThisCo.id(),LayersInfoDic["CO_ExtrAttr"],possibleExtrField)
		print('Found lgr: '+str(ThisCo.geometry().length()))
		CurrentLayer.changeAttributeValue(ThisCo.id(),LayersInfoDic["CO_LgrCartAttr"],ThisCo.geometry().length())
	CurrentLayer.commitChanges()

def refindBoInfos(LayersInfoDic):
	print("adding nbAbbo in Bo")
	for ThisZpbo in LayersInfoDic["ZAPBO_Layer"].getFeatures():
		isZpbInPatch = False
		for ThisZpboPatch in LayersInfoDic["tZpboPatch_Layer"].getFeatures():
			if(ThisZpbo.attributes()[LayersInfoDic["ZAPBO_NomAttr"]] == ThisZpboPatch.attributes()[LayersInfoDic["ZPpat_CodeAttr"]]):
				isZpbInPatch = True
				isBoInPatch = False 
				for ThisBo in LayersInfoDic["BO_Layer"].getFeatures():
					if(ThisBo.attributes()[LayersInfoDic["BO_CodeAttr"]] == ThisZpboPatch.attributes()[LayersInfoDic["ZPPBpat_CodeAttr"]]):
						isBoInPatch = True
						nbAbboinThisBo = 0
						for thisDBL in LayersInfoDic["DBL_Layer"].getFeatures():
							if(thisDBL.geometry().intersects(ThisZpbo.geometry())): nbAbboinThisBo += int(thisDBL.attributes()[LayersInfoDic["DBL_NbPriseAttr"]])
						CurrentLayer = LayersInfoDic["BO_Layer"]
						CurrentLayer.startEditing()
						CurrentLayer.changeAttributeValue(ThisBo.id(),LayersInfoDic["BO_NbAbboAttr"],nbAbboinThisBo)
						CurrentLayer.commitChanges()
						break
				if(not isBoInPatch):
					print('can\'t found Bo: '+str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]])+" in ZpbPatch")
				break
		if(not isZpbInPatch):
			print('can\'t found zpb: '+str(ThisZpbo.attributes()[LayersInfoDic["ZAPBO_NomAttr"]])+" in ZpbPatch")

#Defs For Refind Geom--------------------------------------------------
def FixSITEconnexions(LayersInfoDic, SearchRange):
	#Reconnect COs
	print("\nFixing \'CABLE OPTIQUE\' on \'SITE\'")
	LayersInfoDic["CO_Layer"].startEditing()
	for ThisCableOptique in LayersInfoDic["CO_Layer"].getFeatures():
		if(LayersInfoDic["SRO_GEO"].asPoint().distance(ThisCableOptique.geometry().asMultiPolyline()[0][0]) <= SearchRange):
			if(LayersInfoDic["SRO_GEO"]!=ThisCableOptique.geometry().asMultiPolyline()[0][0]):
				#print("\nDoing "+str(ThisCableOptique.id()))
				#print(str(LayersInfoDic["SRO_GEO"])+' =/= '+str(ThisCableOptique.geometry().asMultiPolyline()[0][0]))
				#print('Target '+str(LayersInfoDic["SRO_GEO"]))
				#print('From '+str(ThisCableOptique.geometry().asMultiPolyline()[0][0]))
				NewGeo = ThisCableOptique.geometry()
				NewGeo.moveVertex(LayersInfoDic["SRO_GEO"].asPoint().x(),LayersInfoDic["SRO_GEO"].asPoint().y(),0)
				LayersInfoDic["CO_Layer"].changeGeometry(ThisCableOptique.id(),NewGeo)

		elif(LayersInfoDic["SRO_GEO"].asPoint().distance(ThisCableOptique.geometry().asMultiPolyline()[0][len(ThisCableOptique.geometry().asMultiPolyline()[0])-1]) <= SearchRange):
			if(LayersInfoDic["SRO_GEO"]!=ThisCableOptique.geometry().asMultiPolyline()[0][len(ThisCableOptique.geometry().asMultiPolyline()[0])-1]):
				#print("\nDoing "+str(ThisCableOptique.id()))
				#print(str(LayersInfoDic["SRO_GEO"])+' =/= '+str(ThisCableOptique.geometry().asMultiPolyline()[0][-1]))
				#print('Target'+str(LayersInfoDic["SRO_GEO"]))
				#print('From '+str(ThisCableOptique.geometry().asMultiPolyline()[0][len(ThisCableOptique.geometry().asMultiPolyline()[0])-1]))
				NewGeo = ThisCableOptique.geometry()
				NewGeo.moveVertex(LayersInfoDic["SRO_GEO"].asPoint().x(),LayersInfoDic["SRO_GEO"].asPoint().y(),len(ThisCableOptique.geometry().asMultiPolyline()[0])-1)
				LayersInfoDic["CO_Layer"].changeGeometry(ThisCableOptique.id(),NewGeo)
	LayersInfoDic["CO_Layer"].commitChanges()

	print("\nFixing \'SUPPORT\' on \'SITE\'")
	LayersInfoDic["SI_Layer"].startEditing()
	for ThisSupport in LayersInfoDic["SI_Layer"].getFeatures():
		if(LayersInfoDic["SRO_GEO"].asPoint().distance(ThisSupport.geometry().asMultiPolyline()[0][0]) <= SearchRange):
			if(LayersInfoDic["SRO_GEO"]!=ThisSupport.geometry().asMultiPolyline()[0][0]):
				#print("\nDoing "+str(ThisSupport.id()))
				#print(str(LayersInfoDic["SRO_GEO"])+' =/= '+str(ThisSupport.geometry().asMultiPolyline()[0][0]))
				#print('Target '+str(LayersInfoDic["SRO_GEO"]))
				#print('From '+str(ThisSupport.geometry().asMultiPolyline()[0][0]))
				NewGeo = ThisSupport.geometry()
				NewGeo.moveVertex(LayersInfoDic["SRO_GEO"].asPoint().x(),LayersInfoDic["SRO_GEO"].asPoint().y(),0)
				LayersInfoDic["SI_Layer"].changeGeometry(ThisSupport.id(),NewGeo)

		elif(LayersInfoDic["SRO_GEO"].asPoint().distance(ThisSupport.geometry().asMultiPolyline()[0][len(ThisSupport.geometry().asMultiPolyline()[0])-1]) <= SearchRange):
			if(LayersInfoDic["SRO_GEO"]!=ThisSupport.geometry().asMultiPolyline()[0][len(ThisSupport.geometry().asMultiPolyline()[0])-1]):
				#print("\nDoing "+str(ThisSupport.id()))
				#print(str(LayersInfoDic["SRO_GEO"])+' =/= '+str(ThisSupport.geometry().asMultiPolyline()[0][-1]))
				#print('Target'+str(LayersInfoDic["SRO_GEO"]))
				#print('From '+str(ThisSupport.geometry().asMultiPolyline()[0][len(ThisSupport.geometry().asMultiPolyline()[0])-1]))
				NewGeo = ThisSupport.geometry()
				NewGeo.moveVertex(LayersInfoDic["SRO_GEO"].asPoint().x(),LayersInfoDic["SRO_GEO"].asPoint().y(),len(ThisSupport.geometry().asMultiPolyline()[0])-1)
				LayersInfoDic["SI_Layer"].changeGeometry(ThisSupport.id(),NewGeo)
	LayersInfoDic["SI_Layer"].commitChanges()

def FixBoAdduc(LayersInfoDic, SearchRange):
	#LayersInfoDic["ADDUC_Layer"].startEditing()
	BO_Cnt =0
	MyPBar = QProgressDialog("\nFixing \'ADDUCTION\' extremities on \'BOITE OPTIQUE\'", 'Annuler', 0, len(list(LayersInfoDic["BO_Layer"].getFeatures())))
	MyPBar.setWindowModality(QtCore.Qt.WindowModal)
	print("\nFixing \'ADDUCTION\' extremities on \'BOITE OPTIQUE\'")
	LayersInfoDic["ADDUC_Layer"].startEditing()
	for ThisBoiteOptique in LayersInfoDic["BO_Layer"].getFeatures():
		MyPBar.setValue(BO_Cnt)
		BO_Cnt +=1
		if(MyPBar.wasCanceled()):
			break
		for ThisAdduc in LayersInfoDic["ADDUC_Layer"].getFeatures():
			if(ThisBoiteOptique.geometry().asPoint().distance(ThisAdduc.geometry().asMultiPolyline()[0][0]) <= SearchRange):
				if(ThisBoiteOptique.geometry().asPoint()!=ThisAdduc.geometry().asMultiPolyline()[0][0]):
					print("\nDoing "+str(ThisAdduc.id()))
					print(str(ThisBoiteOptique.geometry().asPoint())+' =/= '+str(ThisAdduc.geometry().asMultiPolyline()[0][0]))
					print('Target '+str(ThisBoiteOptique.geometry().asPoint()))
					print('From '+str(ThisAdduc.geometry().asMultiPolyline()[0][0]))
					#LayersInfoDic["ADDUC_Layer"].startEditing()
					NewGeo = ThisAdduc.geometry()
					NewGeo.moveVertex(ThisBoiteOptique.geometry().asPoint().x(),ThisBoiteOptique.geometry().asPoint().y(),0)
					LayersInfoDic["ADDUC_Layer"].changeGeometry(ThisAdduc.id(),NewGeo)
					#LayersInfoDic["ADDUC_Layer"].commitChanges()
					#print('To '+str(ThisAdduc.geometry().asMultiPolyline()[0][0]))

			elif(ThisBoiteOptique.geometry().asPoint().distance(ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]) <= SearchRange):
				if(ThisBoiteOptique.geometry().asPoint()!=ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]):
					print("\nDoing "+str(ThisAdduc.id()))
					print(str(ThisBoiteOptique.geometry().asPoint())+' =/= '+str(ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]))
					print('Target '+str(ThisBoiteOptique.geometry().asPoint()))
					print('From '+str(ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]))
					#LayersInfoDic["ADDUC_Layer"].startEditing()
					NewGeo = ThisAdduc.geometry()
					NewGeo.moveVertex(ThisBoiteOptique.geometry().asPoint().x(),ThisBoiteOptique.geometry().asPoint().y(),len(ThisAdduc.geometry().asMultiPolyline()[0])-1)
					LayersInfoDic["ADDUC_Layer"].changeGeometry(ThisAdduc.id(),NewGeo)
					#bLayersInfoDic["ADDUC_Layer"].commitChanges()
					#print('To '+str(ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]))
	LayersInfoDic["ADDUC_Layer"].commitChanges()

def FixDblAdduc(LayersInfoDic, SearchRange):
	#LayersInfoDic["ADDUC_Layer"].startEditing()
	DBL_Cnt =0
	MyPBar = QProgressDialog("\nFixing \'ADDUCTION\' extremities on \'DBL\'", 'Annuler', 0, len(list(LayersInfoDic["DBL_Layer"].getFeatures())))
	MyPBar.setWindowModality(QtCore.Qt.WindowModal)
	print("\nFixing \'ADDUCTION\' extremities on \'DBL\'")
	LayersInfoDic["ADDUC_Layer"].startEditing()
	for ThisDbl in LayersInfoDic["DBL_Layer"].getFeatures():
		MyPBar.setValue(DBL_Cnt)
		DBL_Cnt +=1
		if(MyPBar.wasCanceled()):
			break
		print('FIXING DBL id: '+str(ThisDbl.id()))
		for ThisAdduc in LayersInfoDic["ADDUC_Layer"].getFeatures():
			if(ThisDbl.geometry().asPoint().distance(ThisAdduc.geometry().asMultiPolyline()[0][0]) <= SearchRange):
				if(ThisDbl.geometry().asPoint()!=ThisAdduc.geometry().asMultiPolyline()[0][0]):
					print("\nDoing "+str(ThisAdduc.id()))
					print(str(ThisDbl.geometry().asPoint())+' =/= '+str(ThisAdduc.geometry().asMultiPolyline()[0][0]))
					print('Target '+str(ThisDbl.geometry().asPoint()))
					print('From '+str(ThisAdduc.geometry().asMultiPolyline()[0][0]))
					#LayersInfoDic["ADDUC_Layer"].startEditing()
					NewGeo = ThisAdduc.geometry()
					NewGeo.moveVertex(ThisDbl.geometry().asPoint().x(),ThisDbl.geometry().asPoint().y(),0)
					LayersInfoDic["ADDUC_Layer"].changeGeometry(ThisAdduc.id(),NewGeo)
					#LayersInfoDic["ADDUC_Layer"].commitChanges()
					#print('To '+str(ThisAdduc.geometry().asMultiPolyline()[0][0]))

			elif(ThisDbl.geometry().asPoint().distance(ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]) <= SearchRange):
				if(ThisDbl.geometry().asPoint()!=ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]):
					print("\nDoing "+str(ThisAdduc.id()))
					print(str(ThisDbl.geometry().asPoint())+' =/= '+str(ThisAdduc.geometry().asMultiPolyline()[0][-1]))
					print('Target'+str(ThisDbl.geometry().asPoint()))
					print('From '+str(ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]))
					#LayersInfoDic["ADDUC_Layer"].startEditing()
					NewGeo = ThisAdduc.geometry()
					NewGeo.moveVertex(ThisDbl.geometry().asPoint().x(),ThisDbl.geometry().asPoint().y(),len(ThisAdduc.geometry().asMultiPolyline()[0])-1)
					LayersInfoDic["ADDUC_Layer"].changeGeometry(ThisAdduc.id(),NewGeo)
					#bLayersInfoDic["ADDUC_Layer"].commitChanges()
					#print('To '+str(ThisAdduc.geometry().asMultiPolyline()[0][len(ThisAdduc.geometry().asMultiPolyline()[0])-1]))
	LayersInfoDic["ADDUC_Layer"].commitChanges()

def FixBoCo(LayersInfoDic, SearchRange):
	BO_Cnt =0
	MyPBar = QProgressDialog("\nFixing \'CABLE OPTIQUE\' extremities on \'BOITE OPTIQUE\'", 'Annuler', 0, len(list(LayersInfoDic["BO_Layer"].getFeatures())))
	MyPBar.setWindowModality(QtCore.Qt.WindowModal)
	print("\nFixing \'CABLE OPTIQUE\' extremities on \'BOITE OPTIQUE\'")
	LayersInfoDic["CO_Layer"].startEditing()
	for ThisBoiteOptique in LayersInfoDic["BO_Layer"].getFeatures():
		MyPBar.setValue(BO_Cnt)
		BO_Cnt +=1
		if(MyPBar.wasCanceled()):
			break
		for ThisCableOptique in LayersInfoDic["CO_Layer"].getFeatures():
			if(ThisBoiteOptique.geometry().asPoint().distance(ThisCableOptique.geometry().asMultiPolyline()[0][0]) <= SearchRange):
				if(ThisBoiteOptique.geometry().asPoint()!=ThisCableOptique.geometry().asMultiPolyline()[0][0]):
					print("\nDoing "+str(ThisCableOptique.id()))
					print(str(ThisBoiteOptique.geometry().asPoint())+' =/= '+str(ThisCableOptique.geometry().asMultiPolyline()[0][0]))
					print('Target '+str(ThisBoiteOptique.geometry().asPoint()))
					print('From '+str(ThisCableOptique.geometry().asMultiPolyline()[0][0]))
					NewGeo = ThisCableOptique.geometry()
					NewGeo.moveVertex(ThisBoiteOptique.geometry().asPoint().x(),ThisBoiteOptique.geometry().asPoint().y(),0)
					LayersInfoDic["CO_Layer"].changeGeometry(ThisCableOptique.id(),NewGeo)

			elif(ThisBoiteOptique.geometry().asPoint().distance(ThisCableOptique.geometry().asMultiPolyline()[0][len(ThisCableOptique.geometry().asMultiPolyline()[0])-1]) <= SearchRange):
				if(ThisBoiteOptique.geometry().asPoint()!=ThisCableOptique.geometry().asMultiPolyline()[0][len(ThisCableOptique.geometry().asMultiPolyline()[0])-1]):
					print("\nDoing "+str(ThisCableOptique.id()))
					print(str(ThisBoiteOptique.geometry().asPoint())+' =/= '+str(ThisCableOptique.geometry().asMultiPolyline()[0][-1]))
					print('Target'+str(ThisBoiteOptique.geometry().asPoint()))
					print('From '+str(ThisCableOptique.geometry().asMultiPolyline()[0][len(ThisCableOptique.geometry().asMultiPolyline()[0])-1]))
					NewGeo = ThisCableOptique.geometry()
					NewGeo.moveVertex(ThisBoiteOptique.geometry().asPoint().x(),ThisBoiteOptique.geometry().asPoint().y(),len(ThisCableOptique.geometry().asMultiPolyline()[0])-1)
					LayersInfoDic["CO_Layer"].changeGeometry(ThisCableOptique.id(),NewGeo)
	LayersInfoDic["CO_Layer"].commitChanges()

def FixPTechSupp(LayersInfoDic, SearchRange):
	PT_Cnt =0
	MyPBar = QProgressDialog('"\nFixing \'SUPPORT\' extremities on \'POINT TECHNIQUE\'"', 'Annuler', 0, len(list(LayersInfoDic["PT_Layer"].getFeatures())))
	MyPBar.setWindowModality(QtCore.Qt.WindowModal)
	print("\nFixing \'SUPPORT\' extremities on \'POINT TECHNIQUE\'")
	LayersInfoDic["SI_Layer"].startEditing()
	for ThisPointTechnique in LayersInfoDic["PT_Layer"].getFeatures():
		MyPBar.setValue(PT_Cnt)
		PT_Cnt +=1
		if(MyPBar.wasCanceled()):
			break
		for ThisSupport in LayersInfoDic["SI_Layer"].getFeatures():
			if(ThisPointTechnique.geometry().asPoint().distance(ThisSupport.geometry().asMultiPolyline()[0][0]) <= SearchRange):
				if(ThisPointTechnique.geometry().asPoint()!=ThisSupport.geometry().asMultiPolyline()[0][0]):
					print("\nDoing "+str(ThisSupport.id()))
					print(str(ThisPointTechnique.geometry().asPoint())+' =/= '+str(ThisSupport.geometry().asMultiPolyline()[0][0]))
					print('Target '+str(ThisPointTechnique.geometry().asPoint()))
					print('From '+str(ThisSupport.geometry().asMultiPolyline()[0][0]))
					NewGeo = ThisSupport.geometry()
					NewGeo.moveVertex(ThisPointTechnique.geometry().asPoint().x(),ThisPointTechnique.geometry().asPoint().y(),0)
					LayersInfoDic["SI_Layer"].changeGeometry(ThisSupport.id(),NewGeo)

			elif(ThisPointTechnique.geometry().asPoint().distance(ThisSupport.geometry().asMultiPolyline()[0][len(ThisSupport.geometry().asMultiPolyline()[0])-1]) <= SearchRange):
				if(ThisPointTechnique.geometry().asPoint()!=ThisSupport.geometry().asMultiPolyline()[0][len(ThisSupport.geometry().asMultiPolyline()[0])-1]):
					print("\nDoing "+str(ThisSupport.id()))
					print(str(ThisPointTechnique.geometry().asPoint())+' =/= '+str(ThisSupport.geometry().asMultiPolyline()[0][-1]))
					print('Target'+str(ThisPointTechnique.geometry().asPoint()))
					print('From '+str(ThisSupport.geometry().asMultiPolyline()[0][len(ThisSupport.geometry().asMultiPolyline()[0])-1]))
					NewGeo = ThisSupport.geometry()
					NewGeo.moveVertex(ThisPointTechnique.geometry().asPoint().x(),ThisPointTechnique.geometry().asPoint().y(),len(ThisSupport.geometry().asMultiPolyline()[0])-1)
					LayersInfoDic["SI_Layer"].changeGeometry(ThisSupport.id(),NewGeo)
	LayersInfoDic["SI_Layer"].commitChanges()

def FixPtBo(LayersInfoDic, SearchRange):
	#LayersInfoDic["ADDUC_Layer"].startEditing()
	PT_Cnt =0
	MyPBar = QProgressDialog("\nFixing \'BOITE OPTIQUE\' on \'POINT TECHNIQUE\'", 'Annuler', 0, len(list(LayersInfoDic["PT_Layer"].getFeatures())))
	MyPBar.setWindowModality(QtCore.Qt.WindowModal)
	print("\nFixing \'BOITE OPTIQUE\' on \'POINT TECHNIQUE\'")
	LayersInfoDic["BO_Layer"].startEditing()
	for ThisPointTechnique in LayersInfoDic["PT_Layer"].getFeatures():
		MyPBar.setValue(PT_Cnt)
		PT_Cnt +=1
		if(MyPBar.wasCanceled()):
			break
		for ThisBoiteOptique in LayersInfoDic["BO_Layer"].getFeatures():
			if(ThisBoiteOptique.geometry().asPoint().distance(ThisPointTechnique.geometry().asPoint()) <= SearchRange):
				if(ThisBoiteOptique.geometry().asPoint()!=ThisPointTechnique.geometry().asPoint()):
					print("\nDoing BO: "+str(ThisBoiteOptique.id()))
					print(str(ThisBoiteOptique.geometry().asPoint())+' =/= '+str(ThisPointTechnique.geometry().asPoint()))
					print('Target '+str(ThisPointTechnique.geometry().asPoint()))
					print('From '+str(ThisBoiteOptique.geometry().asPoint()))
					NewGeo = ThisPointTechnique.geometry()
					LayersInfoDic["BO_Layer"].changeGeometry(ThisBoiteOptique.id(),NewGeo)
	LayersInfoDic["BO_Layer"].commitChanges()
def refindGeneralGeom(LayersInfoDic):
	#adding PT on support's extremitys when out of Zasro
	doneSupports = []
	for ptInd, thisSupp in enumerate(LayersInfoDic["SI_Layer"].getFeatures()):
		pointToCreate = None

		origpoint = QgsGeometry.fromPointXY(QgsPointXY(thisSupp.geometry().asMultiPolyline()[0][0]))
		extrpoint = QgsGeometry.fromPointXY(QgsPointXY(thisSupp.geometry().asMultiPolyline()[0][-1]))
		if(not origpoint.intersects(LayersInfoDic["ZSRO_GEOM"])):
			print("Supp Orig foud ooz: "+str(origpoint))
			for thisSuppBis in LayersInfoDic["SI_Layer"].getFeatures():
				if(thisSupp.id() == thisSuppBis.id()): continue
				#print('supportbisid1: '+str(thisSuppBis.id()))
				origpointBis = QgsGeometry.fromPointXY(QgsPointXY(thisSuppBis.geometry().asMultiPolyline()[0][0]))
				extrpointBis = QgsGeometry.fromPointXY(QgsPointXY(thisSuppBis.geometry().asMultiPolyline()[0][-1]))
				if(not (thisSupp.id() in doneSupports or thisSuppBis.id() in doneSupports) and ((origpointBis.distance(origpoint) < 0.1 or extrpointBis.distance(origpoint) < 0.1))):
					print(" or ChbCreateWith id: "+str(thisSupp.id())+" and "+str(thisSuppBis.id()))
					pointToCreate = origpoint.asPoint()
					break;
		elif(not extrpoint.intersects(LayersInfoDic["ZSRO_GEOM"])):
			print("Supp Ext foud ooz: "+str(extrpoint))
			for thisSuppBis in LayersInfoDic["SI_Layer"].getFeatures():
				if(thisSupp.id() == thisSuppBis.id()): continue
				#print('supportbisid2: '+str(thisSuppBis.id()))
				origpointBis = QgsGeometry.fromPointXY(QgsPointXY(thisSuppBis.geometry().asMultiPolyline()[0][0]))
				extrpointBis = QgsGeometry.fromPointXY(QgsPointXY(thisSuppBis.geometry().asMultiPolyline()[0][-1]))
				if(not (thisSupp.id() in doneSupports or thisSuppBis.id() in doneSupports) and ((origpointBis.distance(extrpoint) < 0.1 or extrpointBis.distance(extrpoint) < 0.1))):
					print(" ex ChbCreateWith id: "+str(thisSupp.id())+" and "+str(thisSuppBis.id()))
					pointToCreate = extrpoint.asPoint()
					break;
		if(pointToCreate != None):
			doneSupports.append(thisSupp.id())
			print(str(doneSupports))
			NewPTfeat = QgsFeature()
			NewPTfeat.setGeometry(QgsGeometry.fromPointXY(pointToCreate))

			PT_nom = 'HorsZoneSRO-'+str(ptInd)
			PT_code = 'HorsZoneSRO-'+str(ptInd)
			PT_gestionnai = 'EIFFAGE'
			PT_type_struc = 'CHAMBRE'

			#NewPTfeat.setAttributes([str(thisPTech.attributes()[LayersInfoDic["tPT_CodeAttr"]]),'','ORANGE',str(thisPTech.attributes()[LayersInfoDic["tPT_TypeLogAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_TypePhyAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_StatutAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_PropTypAttr"]]),0.0,0.0,PtAppuiHauteur,'','#SRO',str(thisPTech.attributes()[LayersInfoDic["tPT_NatureAttr"]]),'',0,'ORANGE','','',str(ThisttNoeud.id())])
			NewPTfeat.setAttributes([PT_nom, PT_code, PT_gestionnai,'', PT_type_struc])
			LayersInfoDic["PT_Layer"].dataProvider().addFeature(NewPTfeat)
			LayersInfoDic["PT_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["PT_Layer"])
	#---------------------------------------------------------------------------------------
	#Fix Connections issues
	FixSITEconnexions(LayersInfoDic,0.150)
	FixPtBo(LayersInfoDic,0.150)
	FixPTechSupp(LayersInfoDic,0.150)
	FixBoCo(LayersInfoDic,0.150)
	FixBoAdduc(LayersInfoDic,0.150)
	FixDblAdduc(LayersInfoDic,0.150)

#OLD DEF's -------------------------------------------------------------------------------------------------------------
def SaveInfos(LayersInfoDic,SaveDataDic):
	Saves_Cnt =0
	MyPBar = QProgressDialog('Enregistrement des informations', 'Annuler', 0, len(SaveDataDic.ToChangeDic["PT_Layer"])+len(SaveDataDic.ToChangeDic["CO_Layer"])+len(SaveDataDic.ToChangeDic["SI_Layer"])+len(SaveDataDic.ToChangeDic["ADDUC_Layer"])+len(SaveDataDic.ToChangeDic["ZACABLE_Layer"])+len(SaveDataDic.ToChangeDic["ZASRO_Layer"])+len(SaveDataDic.ToChangeDic["BO_Layer"])+len(SaveDataDic.ToChangeDic["ZAPBO_Layer"]))
	MyPBar.setWindowModality(QtCore.Qt.WindowModal)
	print('Enregistrement des informations')
	for SavingLayer in SaveDataDic.ToChangeDic:
		MyPBar.setLabelText("Enregistrement des informations ("+str(SavingLayer)+")")
		CurrentLayer = LayersInfoDic[SavingLayer]
		CurrentLayer.startEditing()
		SavingTimer = datetime.datetime.now()
		print(str(CurrentLayer.name())+".StartEditing")
		for ChangeRow in SaveDataDic.ToChangeDic[SavingLayer]:
			MyPBar.setValue(Saves_Cnt)
			Saves_Cnt +=1
			if(MyPBar.wasCanceled()):
				break
			for AttrToChange in ChangeRow["Attrs"]:
				if(ChangeRow["Attrs"][AttrToChange] == ''):
					continue
				CurrentLayer.changeAttributeValue(ChangeRow["Id"],LayersInfoDic[AttrToChange],ChangeRow["Attrs"][AttrToChange])
				#print("\n\aAttribut changé: "+str(AttrToChange))
		CurrentLayer.commitChanges()
		print("\nSaving"+str(CurrentLayer.name())+" = "+str(datetime.datetime.now()-SavingTimer))

#Defs____________________________________________________________________________________________________________________________________________________________
def ControlInit():
	try:
		i=0
		# consoleWidget = iface.mainWindow().findChild( QDockWidget, 'PythonConsole' )
		global NowIs
		NowIs = datetime.datetime.now()
	except:
		NowIs = 0
		print('Erreurs lors de l\'initialisation')
		return -1;
	try:
		pass
		# del AllErrors
	except:
		pass
	return 0;

def CheckFilePresence(LayersInfoDic):
	try:
		LayersInfoDic["PT_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES POINT TECHNIQUE N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["BO_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES BOITE OPTIQUE N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["ADDUC_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES ADDUCTION N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["DBL_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES DBL N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["ZAPBO_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES ZAPBO N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["ZACABLE_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES ZACABLE N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["CO_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES CABLE OPTIQUE N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["SI_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES SUPPORT N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;

def GetLayersInfo(LayersInfoDic):
	#Recuperation des information des couches----------------
	#LayersInfoDic = {}
	try:
		for CurrentLayer in QgsProject.instance().mapLayers().values():
			if("BOITE_OPTIQUE" in CurrentLayer.name()):
				print("DEBUG: Get BO_Layer Info")
				LayersInfoDic["BO_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["BO_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["BO_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "SUPPORT" or CurrentLayer.attributeDisplayName(AttrId) == "support"):
						LayersInfoDic["BO_SupportAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "FA_FS" or CurrentLayer.attributeDisplayName(AttrId) == "fa_fs"):
						LayersInfoDic["BO_FafsAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NBABONNES" or CurrentLayer.attributeDisplayName(AttrId) == "nbabonnes"):
						LayersInfoDic["BO_NbAbboAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NBEPISSURE" or CurrentLayer.attributeDisplayName(AttrId) == "nbepissure"):
						LayersInfoDic["BO_NbEpiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NBFUTILE" or CurrentLayer.attributeDisplayName(AttrId) == "nbfutile"):
						LayersInfoDic["BO_NbFutAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "FONCTION" or CurrentLayer.attributeDisplayName(AttrId) == "fonction"):
						LayersInfoDic["BO_FonctionAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_FONC" or CurrentLayer.attributeDisplayName(AttrId) == "type_fonc"):
						LayersInfoDic["BO_TypeFoncAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "FABRICANT" or CurrentLayer.attributeDisplayName(AttrId) == "fabricant"):
						LayersInfoDic["BO_FabriAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROPRIETAI" or CurrentLayer.attributeDisplayName(AttrId) == "proprietai"):
						LayersInfoDic["BO_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["BO_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ID_PARENT" or CurrentLayer.attributeDisplayName(AttrId) == "id_parent"):
						LayersInfoDic["BO_IdParAttr"] = AttrId
					#if(CurrentLayer.attributeDisplayName(AttrId) == "POINT TECH" or CurrentLayer.attributeDisplayName(AttrId) == "point tech"):#INTROUVABLE
						#LayersInfoDic["BO_PtechAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REFERENCE" or CurrentLayer.attributeDisplayName(AttrId) == "reference"):
						LayersInfoDic["BO_RefAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "AMONT" or CurrentLayer.attributeDisplayName(AttrId) == "amont"):
						LayersInfoDic["BO_amonAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "INTERCO" or CurrentLayer.attributeDisplayName(AttrId) == "interco"):
						LayersInfoDic["BO_intcoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["BO_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ETAT" or CurrentLayer.attributeDisplayName(AttrId) == "etat"):
						LayersInfoDic["BO_EtatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EMPRISE" or CurrentLayer.attributeDisplayName(AttrId) == "emprise"):
						LayersInfoDic["BO_EmpAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "DIMENSION" or CurrentLayer.attributeDisplayName(AttrId) == "dimension"):
						LayersInfoDic["BO_DimAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "HAUTEUR" or CurrentLayer.attributeDisplayName(AttrId) == "hauteur"):
						LayersInfoDic["BO_HautAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "VOLUME" or CurrentLayer.attributeDisplayName(AttrId) == "volume"):
						LayersInfoDic["BO_VolAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["BO_tyStAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_EQUIP" or CurrentLayer.attributeDisplayName(AttrId) == "type_equip"):
						LayersInfoDic["BO_TyEqAttr"] = AttrId
					#TEST
					if(CurrentLayer.attributeDisplayName(AttrId) == "ThdCodeExt"):
						LayersInfoDic["BO_ThdCodeExtAttr"] = AttrId
			#Couche PT
			if("POINT_TECHNIQUE" in CurrentLayer.name() and CurrentLayer.wkbType() == 1):
				print("DEBUG: Get PT_Layer Info")
				LayersInfoDic["PT_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["PT_TypeStrucAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_FONC" or CurrentLayer.attributeDisplayName(AttrId) == "type_fonc"):
						LayersInfoDic["PT_TypeFoncAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["PT_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["PT_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "MODELE" or CurrentLayer.attributeDisplayName(AttrId) == "modele"):
						LayersInfoDic["PT_ModAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LONGUEUR" or CurrentLayer.attributeDisplayName(AttrId) == "longueur"):
						LayersInfoDic["PT_LonAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LARGEUR" or CurrentLayer.attributeDisplayName(AttrId) == "largeur"):
						LayersInfoDic["PT_LarAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROF_HAUT" or CurrentLayer.attributeDisplayName(AttrId) == "prof_haut"):
						LayersInfoDic["PT_HautAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "SECTEUR" or CurrentLayer.attributeDisplayName(AttrId) == "secteur"):
						LayersInfoDic["PT_SecAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROPRIETAI" or CurrentLayer.attributeDisplayName(AttrId) == "proprietai"):
						LayersInfoDic["PT_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["PT_GesAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "RATTACH" or CurrentLayer.attributeDisplayName(AttrId) == "rattach"):
						LayersInfoDic["PT_RatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["PT_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ETAT" or CurrentLayer.attributeDisplayName(AttrId) == "etat"):
						LayersInfoDic["PT_EtatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EMPRISE" or CurrentLayer.attributeDisplayName(AttrId) == "emprise"):
						LayersInfoDic["PT_EmpAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "IDPARENT" or CurrentLayer.attributeDisplayName(AttrId) == "idparent"):
						LayersInfoDic["PT_IdParAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REMPLA_APP" or CurrentLayer.attributeDisplayName(AttrId) == "rempla_app"):
						LayersInfoDic["PT_RemApAttr"] = AttrId
					#TEST
					if(CurrentLayer.attributeDisplayName(AttrId) == "ThdCodeExt"):
						LayersInfoDic["PT_ThdCodeExtAttr"] = AttrId
			#Couche DBL
			if("DBL" in CurrentLayer.name() and  "CONTROLE" not in CurrentLayer.name() and "ZASRO" not in CurrentLayer.name()):# and CurrentLayer.wkbType() == 1):
				print("DEBUG: Get DBL_Layer Info")
				LayersInfoDic["DBL_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "REF_IMB" or CurrentLayer.attributeDisplayName(AttrId) == "ref_imb"):
						LayersInfoDic["DBL_RefImbAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ID_BAN" or CurrentLayer.attributeDisplayName(AttrId) == "id_ban"):
						LayersInfoDic["DBL_BanAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "HEXACLE" or CurrentLayer.attributeDisplayName(AttrId) == "hexacle"):
						LayersInfoDic["DBL_HexAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TECHNO" or CurrentLayer.attributeDisplayName(AttrId) == "techno"):
						LayersInfoDic["DBL_TechnoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NUM_ADR" or CurrentLayer.attributeDisplayName(AttrId) == "num_adr"):
						LayersInfoDic["DBL_NumAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_CLI" or CurrentLayer.attributeDisplayName(AttrId) == "type_cli"):
						LayersInfoDic["DBL_TCliAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_BAT" or CurrentLayer.attributeDisplayName(AttrId) == "type_bat"):
						LayersInfoDic["DBL_TBatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "RACCO_BAT" or CurrentLayer.attributeDisplayName(AttrId) == "racco_bat"):
						LayersInfoDic["DBL_RacBatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NB_PRISE" or CurrentLayer.attributeDisplayName(AttrId) == "nb_prise"):
						LayersInfoDic["DBL_NbPriseAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "RAC_LONG" or CurrentLayer.attributeDisplayName(AttrId) == "rac_long"):
						LayersInfoDic["DBL_RacLngAttr"] = AttrId
			
			#Couche ZANRO
			if("ZANRO" in CurrentLayer.name()):
				print("DEBUG: Get ZANRO_Layer Info")
				LayersInfoDic["ZANRO_Layer"] = CurrentLayer
			#Couche ZAPBO
			if("ZAPBO" in CurrentLayer.name() ):#and CurrentLayer.wkbType() == 3):
				print("DEBUG: Get ZAPBO_Layer Info")
				LayersInfoDic["ZAPBO_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["ZAPBO_CodeAttr"] = AttrId
						LayersInfoDic["ZAPBO_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["ZAPBO_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NB_PRISES" or CurrentLayer.attributeDisplayName(AttrId) == "nb_prises"):
						LayersInfoDic["ZAPBO_NbPrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "COMMENTAIR" or CurrentLayer.attributeDisplayName(AttrId) == "commentair"):
						LayersInfoDic["ZAPBO_CommAttr"] = AttrId
			#Couche ZACABLE
			if("ZACABLE" in CurrentLayer.name() ):# and CurrentLayer.wkbType() == 3):
				print("DEBUG: Get ZACABLE_Layer Info")
				LayersInfoDic["ZACABLE_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["ZACABLE_NumeroAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NB_PRISES" or CurrentLayer.attributeDisplayName(AttrId) == "nb_prises"):
						LayersInfoDic["ZACABLE_NbPrAttr"] = AttrId
			#Couche ZASRO
			if("ZASRO" in CurrentLayer.name() ):# and CurrentLayer.wkbType() == 3):
				print("DEBUG: Get ZASRO_Layer Info")
				LayersInfoDic["ZASRO_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					#print("DEBUG: Get ZASRO_Layer Info222222")
					if(CurrentLayer.attributeDisplayName(AttrId) == "SST" or CurrentLayer.attributeDisplayName(AttrId) == "sst"):
						LayersInfoDic["ZASRO_SSTAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NB_PRISE" or CurrentLayer.attributeDisplayName(AttrId) == "nb_prise"):
						LayersInfoDic["ZASRO_NbPrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REF_SRO" or CurrentLayer.attributeDisplayName(AttrId) == "ref_sro"):
						LayersInfoDic["ZASRO_RefSAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REF_NRO" or CurrentLayer.attributeDisplayName(AttrId) == "ref_nro"):
						LayersInfoDic["ZASRO_RefNAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EPCI" or CurrentLayer.attributeDisplayName(AttrId) == "epci"):
						LayersInfoDic["ZASRO_EPCIAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PLANNING" or CurrentLayer.attributeDisplayName(AttrId) == "planning"):
						LayersInfoDic["ZASRO_PlanAttr"] = AttrId
			#Couche ADDUCTION
			if("ADDUCTION" in CurrentLayer.name()):# and CurrentLayer.wkbType() == 2):
				print("DEBUG: Get ADDUC_Layer Info")
				LayersInfoDic["ADDUC_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["ADDUC_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["ADDUC_TyStrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ID" or CurrentLayer.attributeDisplayName(AttrId) == "id"):
						LayersInfoDic["ADDUC_IdAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LG" or CurrentLayer.attributeDisplayName(AttrId) == "lg"):
						LayersInfoDic["ADDUC_LgAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "COMMENT" or CurrentLayer.attributeDisplayName(AttrId) == "comment"):
						LayersInfoDic["ADDUC_CommAttr"] = AttrId
			#Couche CABLE OPTIQUE
			if("CABLE_OPTIQUE" in CurrentLayer.name() ):#and CurrentLayer.wkbType() == 2):
				print("DEBUG: Get CO_Layer Info")
				LayersInfoDic["CO_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["CO_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["CO_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CAPACITE" or CurrentLayer.attributeDisplayName(AttrId) == "capacite"):
						LayersInfoDic["CO_CapaAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ORIGINE" or CurrentLayer.attributeDisplayName(AttrId) == "origine"):
						LayersInfoDic["CO_OrigAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EXTREMITE" or CurrentLayer.attributeDisplayName(AttrId) == "extremite"):
						LayersInfoDic["CO_ExtrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["CO_TyStAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["CO_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_FONC" or CurrentLayer.attributeDisplayName(AttrId) == "type_fonc"):
						LayersInfoDic["CO_TyfonAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EMPRISE" or CurrentLayer.attributeDisplayName(AttrId) == "emprise"):
						LayersInfoDic["CO_EmpAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["CO_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROPRIETAI" or CurrentLayer.attributeDisplayName(AttrId) == "proprietai"):
						LayersInfoDic["CO_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ETAT" or CurrentLayer.attributeDisplayName(AttrId) == "etat"):
						LayersInfoDic["CO_EtatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "FABRICANT" or CurrentLayer.attributeDisplayName(AttrId) == "fabricant"):
						LayersInfoDic["CO_FabAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REFERENCE" or CurrentLayer.attributeDisplayName(AttrId) == "reference"):
						LayersInfoDic["CO_RefAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LGR_CARTO" or CurrentLayer.attributeDisplayName(AttrId) == "lgr_carto"):
						LayersInfoDic["CO_LgrCartAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "DIAMETRE" or CurrentLayer.attributeDisplayName(AttrId) == "diametre"):
						LayersInfoDic["CO_DiamAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPEFIBRE" or CurrentLayer.attributeDisplayName(AttrId) == "typefibre"):
						LayersInfoDic["CO_TyFibreAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "RATTACH" or CurrentLayer.attributeDisplayName(AttrId) == "rattach"):
						LayersInfoDic["CO_RatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "SECTION" or CurrentLayer.attributeDisplayName(AttrId) == "section"):
						LayersInfoDic["CO_SectAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "COMMENT" or CurrentLayer.attributeDisplayName(AttrId) == "comment"):
						LayersInfoDic["CO_CommentAttr"] = AttrId

			#Couche SUPPORT INFRA
			if("SUPPORT" in CurrentLayer.name() ):#and CurrentLayer.wkbType() == 2):
				print("DEBUG: Get SUPP_Layer Info")
				LayersInfoDic["SI_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["SI_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["SI_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["SI_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "AMONT" or CurrentLayer.attributeDisplayName(AttrId) == "amont"):
						LayersInfoDic["SI_AmontAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "AVAL" or CurrentLayer.attributeDisplayName(AttrId) == "aval"):
						LayersInfoDic["SI_AvalAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "STRUCTURE" or CurrentLayer.attributeDisplayName(AttrId) == "structure"):
						LayersInfoDic["SI_StrucAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["SI_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROPRIETAI" or CurrentLayer.attributeDisplayName(AttrId) == "proprietai"):
						LayersInfoDic["SI_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "MODE_POSE" or CurrentLayer.attributeDisplayName(AttrId) == "mode_pose"):
						LayersInfoDic["SI_MPoseAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["SI_TyStAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LG_REELLE" or CurrentLayer.attributeDisplayName(AttrId) == "le_reelle"):
						LayersInfoDic["SI_LGreAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LARGEUR" or CurrentLayer.attributeDisplayName(AttrId) == "largeur"):
						LayersInfoDic["SI_LarAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "HAUTEUR" or CurrentLayer.attributeDisplayName(AttrId) == "hauteur"):
						LayersInfoDic["SI_HautAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "INSEE" or CurrentLayer.attributeDisplayName(AttrId) == "insee"):
						LayersInfoDic["SI_INSEEAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "COMPO" or CurrentLayer.attributeDisplayName(AttrId) == "compo"):
						LayersInfoDic["SI_CompAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "UTILISATIO" or CurrentLayer.attributeDisplayName(AttrId) == "utilisatio"):
						LayersInfoDic["SI_UtiAttr"] = AttrId
			#Couche SITE
			if("_SITE" in CurrentLayer.name()):
				print("DEBUG: Get SITE_Layer Info")
				LayersInfoDic["SNRO_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["SNRO_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ST_CODE" or CurrentLayer.attributeDisplayName(AttrId) == "st_code"):
						LayersInfoDic["SNRO_StCodAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "HEXACLE" or CurrentLayer.attributeDisplayName(AttrId) == "hexcale"):
						LayersInfoDic["SNRO_HexAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "INSEE" or CurrentLayer.attributeDisplayName(AttrId) == "insee"):
						LayersInfoDic["SNRO_InseeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["SNRO_GesAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NBPRISES" or CurrentLayer.attributeDisplayName(AttrId) == "nbprises"):
						LayersInfoDic["SNRO_NbPrisesAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EXTRMETHOD"):
						LayersInfoDic["SNRO_ExtrMethodAttr"] = AttrId

		#GET MORE PRECISE INFOS
		for SNRO_Infos in LayersInfoDic["SNRO_Layer"].getFeatures():
			LayersInfoDic["SNRO_Feature"] = SNRO_Infos
			LayersInfoDic["SNRO_Id"] = str(SNRO_Infos.attributes()[LayersInfoDic["SNRO_CodeAttr"]])
			LayersInfoDic["SNRO_Gestion"] = str(SNRO_Infos.attributes()[LayersInfoDic["SNRO_GesAttr"]])
			LayersInfoDic["SNRO_INSEE"] = str(SNRO_Infos.attributes()[LayersInfoDic["SNRO_InseeAttr"]])
			#LayersInfoDic["SRO_GEO"] = SNRO_Infos.geometry().asPoint()

		'''
		for tZnro_Feat in LayersInfoDic["tZnro_Layer"].getFeatures():
			LayersInfoDic["ZNRO_REF"] = str(tZnro_Feat.attributes()[LayersInfoDic["ZN_NroRefAttr"]])

		for tZsro_Feat in LayersInfoDic["tZsro_Layer"].getFeatures():
			LayersInfoDic["ZSRO_REF"] = str(tZsro_Feat.attributes()[LayersInfoDic["ZS_RefPmAttr"]])


		LayersInfoDic["INSEE"] = str(tZnro_Feat.attributes()[LayersInfoDic["ZN_NroRefAttr"]])[:5]
		'''
		return LayersInfoDic;
	except:
		return -1;

def GetLayersInfo_EiffageShapes(LayersInfoDic):
	#Recuperation des information des couches----------------
	#LayersInfoDic = {}
	try:
		print('0')
	except:
		pass
	if(1):
		for CurrentLayer in QgsProject.instance().mapLayers().values():
			#EIFFAGE
			if(CurrentLayer.name() == "ftth_pf"):
				print("DEBUG: Get Eif_PF_Layer Info")
				LayersInfoDic["Eif_PF_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier_"):
						LayersInfoDic["EifPF_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier1"):
						LayersInfoDic["EifPF_CodeSd1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_site"):
						LayersInfoDic["EifPF_TypSitAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metie_1"):
						LayersInfoDic["EifPF_CodeMetAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "statut_ftt"):
						LayersInfoDic["EifPF_CodeStatuAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nom_nro"):
						LayersInfoDic["EifPF_NomNroAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "num_ordre"):
						LayersInfoDic["EifPF_NumOrdrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_pf"):
						LayersInfoDic["EifPF_TypPfAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_peo"):
						LayersInfoDic["EifPF_TypPeoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_sortie_"):
						LayersInfoDic["EifPF_NbSortiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "operateur"):
						LayersInfoDic["EifPF_OperAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_com"):
						LayersInfoDic["EifPF_CodeComAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "commentair"):
						LayersInfoDic["EifPF_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_coupleu"):
						LayersInfoDic["EifPF_NbCoupAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_coupl_1"):
						LayersInfoDic["EifPF_NbCoup1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "taille_cab"):
						LayersInfoDic["EifPF_TaiCabAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth_si"):
						LayersInfoDic["EifPF_idFtthSiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth_pf"):
						LayersInfoDic["EifPF_idFtthPfAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_pf_pe"):
						LayersInfoDic["EifPF_TypPfPeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "num_ordre_"):
						LayersInfoDic["EifPF_NumOrdAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "vers_ingen"):
						LayersInfoDic["EifPF_VersIngAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "migration"):
						LayersInfoDic["EifPF_MigrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ipon_id"):
						LayersInfoDic["EifPF_IponIdAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "dept"):
						LayersInfoDic["EifPF_DeptAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "auteur"):
						LayersInfoDic["EifPF_AuteurAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "symbologie"):
						LayersInfoDic["EifPF_SymboAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_proje"):
						LayersInfoDic["EifPF_CodProjAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "forcage"):
						LayersInfoDic["EifPF_ForcAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "rome_ok"):
						LayersInfoDic["EifPF_RomOkAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifPF_IdFtthAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_regleme"):
						LayersInfoDic["EifPF_IdReglAttr"] = AttrId

			if(CurrentLayer.name() == "ftth_site_chambre"):
				print("DEBUG: Get Eif_SiteCH_Layer Info")
				LayersInfoDic["Eif_SiteCH_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSitCH_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_com"):
						LayersInfoDic["EifSitCH_CodeComAttr"] = AttrId	
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_voie"):
						LayersInfoDic["EifSitCH_CodeVoiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "implant"):
						LayersInfoDic["EifSitCH_ImplAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nature_cha"):
						LayersInfoDic["EifSitCH_NaturChAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ref_chambr"):
						LayersInfoDic["EifSitCH_RefChAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ref_note"):
						LayersInfoDic["EifSitCH_RefNotAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "num_voie"):
						LayersInfoDic["EifSitCH_NumVoiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_proprie"):
						LayersInfoDic["EifSitCH_IdPropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_gestion"):
						LayersInfoDic["EifSitCH_IdGestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_trapp"):
						LayersInfoDic["EifSitCH_TypTrapAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_ch1_c"):
						LayersInfoDic["EifSitCH_Codech1cAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_ch2_p"):
						LayersInfoDic["EifSitCH_Codech2pAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "auteur"):
						LayersInfoDic["EifSitCH_AuteurAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier_"):
						LayersInfoDic["EifSitCH_IdMetAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "statut_ftt"):
						LayersInfoDic["EifSitCH_StatFttAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "num_ordre"):
						LayersInfoDic["EifSitCH_NumOrdAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ref_pt"):
						LayersInfoDic["EifSitCH_RefPtAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "lib_num_cp"):
						LayersInfoDic["EifSitCH_LibNumCpAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nom_com"):
						LayersInfoDic["EifSitCH_NomComAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "commentair"):
						LayersInfoDic["EifSitCH_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_ui"):
						LayersInfoDic["EifSitCH_CodeUiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_nra"):
						LayersInfoDic["EifSitCH_CodeNraAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "infra_gc"):
						LayersInfoDic["EifSitCH_InfraGcAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "rotation"):
						LayersInfoDic["EifSitCH_RotaAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_ch1"):
						LayersInfoDic["EifSitCH_CodeCh1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_ch2"):
						LayersInfoDic["EifSitCH_CodeCh2Attr"] = AttrId
					'''
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSitCH_CodeAttr"] = AttrId
					coord_x
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSitCH_CodeAttr"] = AttrId
					coord_y
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSitCH_CodeAttr"] = AttrId
					date_modif
					'''
					if(CurrentLayer.attributeDisplayName(AttrId) == "abs_docume"):
						LayersInfoDic["EifSitCH_AbsDocAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "tag_ipon"):
						LayersInfoDic["EifSitCH_TagIponAttr"] = AttrId
					'''
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSitCH_CodeAttr"] = AttrId
					coord_x2
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSitCH_CodeAttr"] = AttrId
					coord_y2
					'''
					if(CurrentLayer.attributeDisplayName(AttrId) == "modif_geo"):
						LayersInfoDic["EifSitCH_ModifGeoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "deployeur"):
						LayersInfoDic["EifSitCH_DeployAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "dept"):
						LayersInfoDic["EifSitCH_DeptAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nom_voie"):
						LayersInfoDic["EifSitCH_NomVoiAttr"] = AttrId
					

			if(CurrentLayer.name() == "ftth_parcours"):
				print("DEBUG: Get Eif_Parcour_Layer Info")
				LayersInfoDic["Eif_Parcour_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSI_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier_"):
						LayersInfoDic["EifSI_IdMetAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth_si"):
						LayersInfoDic["EifSI_IdFttSiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_site_"):
						LayersInfoDic["EifSI_TypSitAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier1"):
						LayersInfoDic["EifSI_IdMet1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_site1"):
						LayersInfoDic["EifSI_TypSit1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth__1"):
						LayersInfoDic["EifSI_Code1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_parco"):
						LayersInfoDic["EifSI_TypParcAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "commentair"):
						LayersInfoDic["EifSI_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth_pa"):
						LayersInfoDic["EifSI_IdFttPaAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_com"):
						LayersInfoDic["EifSI_CodeComAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "dept"):
						LayersInfoDic["EifSI_DeptAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "auteur"):
						LayersInfoDic["EifSI_AuteurAttr"] = AttrId
					'''
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSI_CodeAttr"] = AttrId
					date_creat
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifSI_CodeAttr"] = AttrId
					date_modif
					'''
					if(CurrentLayer.attributeDisplayName(AttrId) == "longueur"):
						LayersInfoDic["EifSI_LongAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nom_affich"):
						LayersInfoDic["EifSI_NomAffichAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_noeud_a"):
						LayersInfoDic["EifSI_IdNdaAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_noeud_b"):
						LayersInfoDic["EifSI_IdNdbAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_cable"):
						LayersInfoDic["EifSI_TypCablAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "compositio"):
						LayersInfoDic["EifSI_CompoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_proprie"):
						LayersInfoDic["EifSI_IdPropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "operateur"):
						LayersInfoDic["EifSI_OperAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "shape_Leng"):
						LayersInfoDic["EifSI_ShpLenAttr"] = AttrId

			if(CurrentLayer.name() == "ftth_cable"):
				print("DEBUG: Get Eif_Cable_Layer Info")
				LayersInfoDic["Eif_Cable_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifCB_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_com"):
						LayersInfoDic["EifCB_CodeComAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ref_cable"):
						LayersInfoDic["EifCB_RefCbAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "statut_ftt"):
						LayersInfoDic["EifCB_StatuFttAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cable_id"):
						LayersInfoDic["EifCB_CbIdAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_longu"):
						LayersInfoDic["EifCB_TypLongAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "longueur"):
						LayersInfoDic["EifCB_LongAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "diametre"):
						LayersInfoDic["EifCB_DiamAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_fibre"):
						LayersInfoDic["EifCB_NbFibrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_cable"):
						LayersInfoDic["EifCB_TypCbAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier_"):
						LayersInfoDic["EifCB_IdMetAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth_si"):
						LayersInfoDic["EifCB_IdFttSiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_site_"):
						LayersInfoDic["EifCB_TypSitAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier1"):
						LayersInfoDic["EifCB_IdMet1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth__1"):
						LayersInfoDic["EifCB_IdFtt1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_site1"):
						LayersInfoDic["EifCB_TypSit1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "commentair"):
						LayersInfoDic["EifCB_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_nra"):
						LayersInfoDic["EifCB_CodeNraAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "operateur"):
						LayersInfoDic["EifCB_OperAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_proje"):
						LayersInfoDic["EifCB_CodeProjAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "dept"):
						LayersInfoDic["EifCB_DeptAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nom_affich"):
						LayersInfoDic["EifCB_NomAffiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "symbologie"):
						LayersInfoDic["EifCB_SymbAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "auteur"):
						LayersInfoDic["EifCB_AuteurAttr"] = AttrId
					'''
						if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
							LayersInfoDic["EifCB_CodeAttr"] = AttrId
					date_creat
						if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
							LayersInfoDic["EifCB_CodeAttr"] = AttrId
					
					'''
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ipon"):
						LayersInfoDic["EifCB_IdIponAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "usage_ftth"):
						LayersInfoDic["EifCB_UsagFttAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "usage_rome"):
						LayersInfoDic["EifCB_UsagRomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "shape_Leng"):
						LayersInfoDic["EifCB_ShpLenAttr"] = AttrId
				

			if(CurrentLayer.name() == "ftth_zone_eligibilite"):
				print("DEBUG: Get Eif_Zones_Layer Info")
				LayersInfoDic["Eif_Zones_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier_"):
						LayersInfoDic["EifZONE_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "code_com"):
						LayersInfoDic["EifZONE_CodeComAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_metier1"):
						LayersInfoDic["EifZONE_Code1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "statut_ftt"):
						LayersInfoDic["EifZONE_StatuFttAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "num_ordre"):
						LayersInfoDic["EifZONE_NumOrderAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth_pf"):
						LayersInfoDic["EifZONE_IdFtthPfAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_pf"):
						LayersInfoDic["EifZONE_Typ_PfAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_el"):
						LayersInfoDic["EifZONE_Nb_ElAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "commentair"):
						LayersInfoDic["EifZONE_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nom_nro"):
						LayersInfoDic["EifZONE_NomNroAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "dept"):
						LayersInfoDic["EifZONE_DeptAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "auteur"):
						LayersInfoDic["EifZONE_AuteurAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "date_creat"):
						LayersInfoDic["EifZONE_DatCreatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "date_modif"):
						LayersInfoDic["EifZONE_DatModifAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "type_calcu"):
						LayersInfoDic["EifZONE_TypCalcAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_module"):
						LayersInfoDic["EifZONE_NbModuleAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "old_id_met"):
						LayersInfoDic["EifZONE_OldIdMetAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_potenti"):
						LayersInfoDic["EifZONE_NbPotenAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_module_"):
						LayersInfoDic["EifZONE_NbModulAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "rome_ok"):
						LayersInfoDic["EifZONE_RomeOkAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "id_ftth"):
						LayersInfoDic["EifZONE_IdFttAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "shape_Leng"):
						LayersInfoDic["EifZONE_ShapeLenAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "shape_Area"):
						LayersInfoDic["EifZONE_ShapeAreaAttr"] = AttrId
					

		for tZnro_Feat in LayersInfoDic["Eif_Zones_Layer"].getFeatures():
			if(tZnro_Feat.attributes()[LayersInfoDic["EifZONE_Typ_PfAttr"]] == 'NRO'):
				LayersInfoDic["ZNRO_REF"] = str(tZnro_Feat.attributes()[LayersInfoDic["EifZONE_CodeAttr"]])
				break

		for thisSTech in LayersInfoDic["Eif_PF_Layer"].getFeatures():
			if(thisSTech.attributes()[LayersInfoDic["EifPF_TypSitAttr"]] == 'Armoire'):
				print("site armoir found")
				for Zsro_Feat in LayersInfoDic["Eif_Zones_Layer"].getFeatures():
					if(Zsro_Feat.attributes()[LayersInfoDic["EifZONE_Typ_PfAttr"]] == 'PMZ'):
						print("pmzFound")
						if(Zsro_Feat.geometry().intersects(thisSTech.geometry())):
							LayersInfoDic["ZSRO_FEAT"] = Zsro_Feat
							zSroGeometry = Zsro_Feat.geometry()
							LayersInfoDic["ZSRO_GEOM"] = zSroGeometry
							LayersInfoDic["ZSRO_REF"] = str(Zsro_Feat.attributes()[LayersInfoDic["EifZONE_CodeAttr"]]).replace('/','.')
							break
				break
		# LayersInfoDic["ZSRO_GEOM"] = zSroGeometry
		for thisSTech in LayersInfoDic["Eif_PF_Layer"].getFeatures():
			if(thisSTech.attributes()[LayersInfoDic["EifPF_TypSitAttr"]] == 'Armoire'):
				#CodeSRO = thisSTech.attributes()[LayersInfoDic["ST_CodeExtAttr"]]
				SroFeat = thisSTech
				SITEgeometry = thisSTech.geometry()
				inseeCode = str(thisSTech.attributes()[LayersInfoDic["EifPF_CodeComAttr"]])
				LayersInfoDic["SRO_FEAT"] = SroFeat
				LayersInfoDic["SRO_GEO"] = SITEgeometry
				LayersInfoDic["INSEE"] = inseeCode
				break
		# LayersInfoDic["SRO_FEAT"] = SroFeat
		# LayersInfoDic["SRO_GEO"] = SITEgeometry
		# LayersInfoDic["INSEE"] = inseeCode
		'''
		for thisDblInseeTemplate in LayersInfoDic["tdf_DBL_Layer"].getFeatures():
			LayersInfoDic["INSEE"] = str(thisDblInseeTemplate.attributes()[LayersInfoDic["tdfAD_InseeAttr"]])
			break
		'''
		return LayersInfoDic;
		#except:
		#return -1;

def GetXlsxInfos(DoeFilePath):
	import pandas as pd
	from pandas import read_excel
	import re
	
	def intfromstring(x):
		return re.findall(r'\d+', x)[0]

	def fill_dict(columns_name, row):
		dico = {}
		i = 1
		for col in columns_name:
			dico[col] = row[i]
			i+=1
		return dico

	def from_df_to_dict(refNro, id_PMZ, df_PA, df_PB, list_pa):
		output = {}
		output["refNro"]=refNro
		output["id_PMZ"]=id_PMZ

		for pa in list_pa:
			new_dict={}
			output[pa]=new_dict

		for index, row in df_PA.iterrows():
			new_dict = fill_dict(df_PA.columns.tolist()[1:], row)
			output[row[0]] = new_dict
		
		i=0
		for pa in df_PB:
			for index, row in pa.iterrows():
				new_dict = fill_dict(df_PA.columns.tolist()[1:], row)
				output[list_pa[i]][row[0]] = new_dict
			i+=1
		return output

	def extract_data(df):
		#retire les lignes vides et reindexe la dataframe
		df = df[df['ID_PA'].notna()].reset_index()
		df = df.drop('index', axis=1)
		#recupère les index des lignes contenant ID_PB/PRE pour separer les differents tableaux
		indexes = df.index[df['ID_PA'] == 'ID_PB/PRE'].tolist()
		current_index=0
		print("*DEBUG\nCurrentIndex: "+str(current_index)+"\nindexes: "+str(indexes)+" [0]-1")
		print("df_PA = "+str(df[current_index:indexes[0]-1]))
		df_PA=df[current_index:indexes[0]-1]
		current_index=indexes[0]+1
		indexes=indexes[1:len(indexes)]
		df_PA = df_PA.drop('µ\nréserve', axis=1)
		
		#récup info sur les PB
		dfs_PB = []
		for index in indexes:
			dfs_PB += [df[current_index:index-1]]
			current_index = index+1
		dfs_PB+=[df[current_index:]]
		
		#transforme l'information de cable pour recuperer seulement l'int
		df_PA['Cap. Câble']=df_PA['Cap. Câble'].apply(intfromstring)
		
		#reformatage de la liste pour avoir les bons termes et garder les bonnes colonnes
		df_PB = []
		for pb in dfs_PB:
			pb['Cap. Câble']=pb['Cap. Câble'].apply(intfromstring)
			pb = pb.rename(columns={'µ\nréserve':'site support'})
			pb = pb.drop('Nbre FTTE', axis=1)
			df_PB+=[pb]
		
		#récup les noms des PA
		indexes = df.index[df['ID_PA'] == 'ID_PB/PRE'].tolist()  
		list_pa = []
		for index in indexes:
			list_pa += [df['ID_CABLE'].iloc[index-1]]
		list_pa = [x.replace('_','-') for x in list_pa]
		
		#recup des infos pas qui ne sont pas directement dans le tableau
		entire_excel = read_excel(file_name, sheet_name=3)
		refNro = entire_excel.loc[entire_excel['Unnamed: 0'] == 'Référence\nCMTHD','Unnamed: 4'].tolist()[0].replace('\t', ' ')
		id_PMZ = entire_excel['Unnamed: 4'][0]
		return refNro, id_PMZ, df_PA, df_PB, list_pa

	#Get Excel Infos
	pd.options.mode.chained_assignment = None
	#colonnes à extraire
	colonnes=['ID_PA', 'ID_CABLE','Cap. Câble', 'Nbre EL', 'Nbre FTTE', 'µ\nréserve']
	sheet = 'DETAIL DISTRI'
	#nombre de lignes à skip dans le cas ou le fichier commence pas ligne 0
	skiprows=2
	sheet_number=3

	#lecture de l'excel
	file_name = DoeFilePath
	df = read_excel(file_name, sheet_name=sheet_number, usecols=colonnes, skiprows=skiprows)
	print("-DEBUG -1\n"+str(df))
	#print(df["ID_PA"])
	for ind, x in enumerate(df['ID_PA']):
		print(str(x))
		if(str(x)[0:2] == 'PA' or str(x)[0:2] == 'PB'):
			df['ID_PA'][ind] = str(x).replace('_','-')
	#print(df["ID_PA"])
	print("-DEBUG\n"+str(df))
	#extraction data
	refNro, id_PMZ, df_PA, df_PB, list_pa = extract_data(df)

	#remplissage dico
	output = from_df_to_dict(refNro, id_PMZ, df_PA, df_PB, list_pa)
	for outKey in output.keys():
		print("------------\n"+str(outKey)+":\n")
		if(type(output[outKey]) == type({})):
			for outPBKey in output[outKey].keys():
				print(str(outPBKey)+" => "+str(output[outKey][outPBKey]))
		else:
			print("*"+str(output[outKey]))
	return output
#____________________________________________________________________________________Start of the Script____________________________________________________________________________________#
print("-"*50+"\nDEBUT DU SCRIPT SRIP_ConverTDF_V0.0.3\n"+"-"*50+"\n"*2)
StartingTime = datetime.datetime.now()
LayersInfoDicGlob = {}
#Open Action choosing window

qgis_path = f"{sys.argv[1]}"

for f in os.listdir(f"{qgis_path}\\shp"):
	if f.endswith(".shp"):
		name = os.path.splitext(f)[0]
		layer = QgsVectorLayer(os.path.join(qgis_path, f), name)
		print(os.path.join(qgis_path, f))
		print("add layer: {} to QgsProject".format(layer.name()))
		QgsProject.instance().addMapLayer(layer)

while(1):
	LayersInfoDicGlob = GetLayersInfo_EiffageShapes(LayersInfoDicGlob)
	if(LayersInfoDicGlob == -1):
		print("ERREUR lors de la récupérations des couches EIFFAGE")
		break
	print("DEBUG STD NAME: "+str(LayersInfoDicGlob["ZSRO_REF"]))
	GetCircetForatShapesLoaded(str(LayersInfoDicGlob["ZSRO_REF"]))
	
	LayersInfoDicGlob = GetLayersInfo(LayersInfoDicGlob)

	#OpenCSV
	homePath = str(QgsProject.instance().homePath())
	print('homePath found: '+str(homePath))
	cmsrbalFile = str(homePath)+"/cms rbal.csv"
	print('cmsrbalFile found: '+str(cmsrbalFile))

	codeNRO = str(LayersInfoDicGlob["ZNRO_REF"]).split('/')[-1]
	for ThisFile in os.listdir(homePath):
		if(str(ThisFile[:len(codeNRO)+10]) == str(codeNRO)+"-DOE-DIST-" and str(ThisFile[-5:]) == '.xlsx'):
			DoeFile = ThisFile
			print("DOE File:"+str(ThisFile))
	doeCompletFilePath = str(homePath)+"/"+str(DoeFile)
	EifxlsxInfos = GetXlsxInfos(doeCompletFilePath)
	'''
	csvGlobalFolder = homePath
	allElePosBoit = []
	PTcsvInfos = []
	with open(str(csvFolder+r"\\pivot_PointTechnique_to_netgeo.csv"), newline='', encoding='utf-8') as f:
		reader = csv.reader(f,delimiter=';')
		for myid ,row in enumerate(reader):
			#print('LIGE: '+str(myid)+" == "+str(row))
			PTcsvInfos.append(row)
	#print("\n\nPTcsvInfos: "+str(PTcsvInfos))
	
	#
	uri = "file:///"+cmsrbalFile+"?type=regexp&delimiter=;&detectTypes=yes&xField=CoordX&yField=CoordY&crs=EPSG:2154&spatialIndex=no&subsetIndex=no&watchFile=no"
	vlayer = QgsVectorLayer(uri, "cms rbal", "delimitedtext")
	QgsProject.instance().addMapLayer(vlayer)
	if not vlayer.isValid():
		print("vlayer failed to load!")
	'''
	AddZANroFeature(LayersInfoDicGlob)
	AddZASroFeature(LayersInfoDicGlob)
	AddZACableFeature(LayersInfoDicGlob)
	AddZAPboFeature(LayersInfoDicGlob)
	AddSIFeature(LayersInfoDicGlob)
	#AddCOFeature(LayersInfoDicGlob) - Old
	AddSITEFeature(LayersInfoDicGlob)
	AddPTFeature(LayersInfoDicGlob)
	AddBOFeature(LayersInfoDicGlob,EifxlsxInfos)
	#Set Co infosafeternaming BO's
	AddCOFeature(LayersInfoDicGlob)
	#break
	AddDBLFeature(LayersInfoDicGlob, cmsrbalFile)
	addADDUCFeature(LayersInfoDicGlob)
	print('\n-end of layer creation\nRefining Layers:')
	refindGeneralGeom(LayersInfoDicGlob)
	'''
	RefindCoInfos(LayersInfoDicGlob)
	refindBoInfos(LayersInfoDicGlob)
	'''


	break;

#PassageNbErreur = NbErreurTotal
print("\nEND " + str(datetime.datetime.now()))
print("Script took "+str(datetime.datetime.now()-StartingTime)+" To achieve")
print("V0.0.3")
