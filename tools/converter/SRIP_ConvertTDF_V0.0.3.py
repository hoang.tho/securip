
#-*- coding: utf-8 -*-

#DATE DE FIN DE VALIDITE: NA

###################################################################
#Script Name	:NommageSIDFibre31
#Version       	:1.0.0
#Description  	:Rempli automatiquement certaines informations en partant des informations de couches requises
#                       Voir ReadMe.txt Pour plus d'informations
#                       Version FlowChart Disponible, demmander à l'auteur du script.
#Date DMAJ  	:derniere mise a jour le 03/12/2019
#Author        	:EL BASRI   Tristan
#Email          :tristan.el-basri@cfitech.fr
###################################################################

###################################################################
#
#Ce script est la propriete exclusive de CFItech.
#
###################################################################



#Imports
import sys
import os
from math import *
import datetime
import getpass

from PyQt5 import QtCore
from PyQt5.QtGui import  *
from PyQt5 import *
from PyQt5.QtWidgets import QProgressDialog, QInputDialog, QLineEdit
from qgis.core import *
import qgis.utils
import copy
import shutil
import csv
#Initialisations -------------------------------------------------
# consoleWidget = iface.mainWindow().findChild( QDockWidget, 'PythonConsole' )
NowIs = datetime.datetime.now()
GetDistance = QgsDistanceArea()
def showError():
	pass

# def DisplayWarning(WarningText):
# 	widget = iface.messageBar().createMessage(WarningText)#, "Show Me")
# 	#button = QPushButton(widget)
# 	#button.setText("Show Me")
# 	#button.pressed.connect(showError)
# 	#widget.layout().addWidget(button)
# 	iface.messageBar().pushWidget(widget, Qgis.Warning)

def GetCircetForatShapesLoaded(studyName):
	#saveLocation = r'C:\\Users\\tristan.el-basri\\Desktop\\Temporaire\\SecuRip\\SecurRIP_ERREUR lors de la récupérations des couches TDFConvertOrange\\savingFolderTest'
	#baseFilesFolder = r'C:\\Users\\tristan.el-basri\\Desktop\\Temporaire\\SecuRip\\SecurRIP_ConvertOrange\\BaseShapeFolder'
	saveLocation = r'C:\\Users\\wirewind\\Documents\\teletravail\\SecurRIP_TDF\\savingFolderTest'
	baseFilesFolder = r'C:\\Users\\wirewind\\Documents\\teletravail\\SecurRIP_TDF\\BaseShapeFolder'
	print("SAVE LOCATION: "+str(saveLocation))
	print("BaseFolder Location: "+str(baseFilesFolder))
	#ZANOR
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.prj',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.qml',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.shp',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZANRO.shx',str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.shx')
	
	ZANROlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ZANRO.shp', str(studyName)+"_ZANRO", "ogr")
	QgsProject.instance().addMapLayer(ZANROlayer)
	if not ZANROlayer.isValid():
		print("ZANRO_layer failed to load!")

	#ZASOR
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.prj',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.qml',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.shp',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZASRO.shx',str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.shx')
	
	ZASROlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ZASRO.shp', str(studyName)+"_ZASRO", "ogr")
	QgsProject.instance().addMapLayer(ZASROlayer)
	if not ZASROlayer.isValid():
		print("ZASRO_layer failed to load!")

	#ZACABLE
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.prj',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.qml',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.shp',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZACABLE.shx',str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.shx')
	
	ZACABLElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ZACABLE.shp', str(studyName)+"_ZACABLE", "ogr")
	QgsProject.instance().addMapLayer(ZACABLElayer)
	if not ZACABLElayer.isValid():
		print("ZACABLE_layer failed to load!")

	#ZAPBO
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.prj',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.qml',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.shp',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ZAPBO.shx',str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.shx')
	
	ZAPBOlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ZAPBO.shp', str(studyName)+"_ZAPBO", "ogr")
	QgsProject.instance().addMapLayer(ZAPBOlayer)
	if not ZAPBOlayer.isValid():
		print("ZAPBO_layer failed to load!")

	#SUPPORT
	#shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.cpg',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.dbf',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.prj',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.qml',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.shp',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SUPPORT.shx',str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.shx')
	
	SUPPORTlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_SUPPORT.shp', str(studyName)+"_SUPPORT", "ogr")
	QgsProject.instance().addMapLayer(SUPPORTlayer)
	if not SUPPORTlayer.isValid():
		print("SUPPORT_layer failed to load!")

	#CABLE_OPTIQUE
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.prj',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.qml',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.shp',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_CABLE_OPTIQUE.shx',str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.shx')
	
	CABLE_OPTIQUElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_CABLE_OPTIQUE.shp', str(studyName)+"_CABLE_OPTIQUE", "ogr")
	QgsProject.instance().addMapLayer(CABLE_OPTIQUElayer)
	if not CABLE_OPTIQUElayer.isValid():
		print("CABLE_OPTIQUE_layer failed to load!")

	#ADDUCTION
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.cpg',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.dbf',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.prj',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.qml',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.shp',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_ADDUCTION.shx',str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.shx')
	
	ADDUCTIONlayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_ADDUCTION.shp', str(studyName)+"_ADDUCTION", "ogr")
	QgsProject.instance().addMapLayer(ADDUCTIONlayer)
	if not ADDUCTIONlayer.isValid():
		print("ADDUCTION_layer failed to load!")

	#SITE
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_SITE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_SITE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.prj',str(saveLocation)+"\\"+str(studyName)+r'_SITE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.qml',str(saveLocation)+"\\"+str(studyName)+r'_SITE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.shp',str(saveLocation)+"\\"+str(studyName)+r'_SITE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_SITE.shx',str(saveLocation)+"\\"+str(studyName)+r'_SITE.shx')
	
	SITElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_SITE.shp', str(studyName)+"_SITE", "ogr")
	QgsProject.instance().addMapLayer(SITElayer)
	if not SITElayer.isValid():
		print("SITE_Layer failed to load!")

	#POINT_TECHNIQUE
	#shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.prj',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.qml',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.shp',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_POINT_TECHNIQUE.shx',str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.shx')
	
	POINT_TECHNIQUElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_POINT_TECHNIQUE.shp', str(studyName)+"_POINT_TECHNIQUE", "ogr")
	QgsProject.instance().addMapLayer(POINT_TECHNIQUElayer)
	if not POINT_TECHNIQUElayer.isValid():
		print("POINT_TECHNIQUE_layer failed to load!")

	#BOITE_OPTIQUE
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.cpg',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.dbf',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.prj',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.qml',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.qml')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.shp',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_BOITE_OPTIQUE.shx',str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.shx')
	
	BOITE_OPTIQUElayer = QgsVectorLayer(str(saveLocation)+"\\"+str(studyName)+r'_BOITE_OPTIQUE.shp', str(studyName)+"_BOITE_OPTIQUE", "ogr")
	QgsProject.instance().addMapLayer(BOITE_OPTIQUElayer)
	if not BOITE_OPTIQUElayer.isValid():
		print("BOITE_OPTIQUE_layer failed to load!")

	#DBL_COMPLEMENT
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.cpg',str(saveLocation)+"\\"+r'DBL COMPLET.cpg')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.dbf',str(saveLocation)+"\\"+r'DBL COMPLET.dbf')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.prj',str(saveLocation)+"\\"+r'DBL COMPLET.prj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.qpj',str(saveLocation)+"\\"+r'DBL COMPLET.qpj')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.shp',str(saveLocation)+"\\"+r'DBL COMPLET.shp')
	shutil.copyfile(str(baseFilesFolder)+'\\EMPTY_DBL_COMPLET.shx',str(saveLocation)+"\\"+r'DBL COMPLET.shx')
	
	DBL_COMPLEMENTlayer = QgsVectorLayer(str(saveLocation)+"\\"+r'DBL COMPLET.shp', "DBL_COMPLET", "ogr")
	QgsProject.instance().addMapLayer(DBL_COMPLEMENTlayer)
	if not DBL_COMPLEMENTlayer.isValid():
		print("DBL_COMPLET_layer failed to load!")


def AddZANroFeature(LayersInfoDic):
	# add a feature
	ZNrofeat = QgsFeature()
	for tdf_Znro_Feat in LayersInfoDic["tdf_Znro_Layer"].getFeatures():
		zNroGeometry = tdf_Znro_Feat.geometry()
		break

	ZNrofeat.setGeometry(zNroGeometry)

	ZANRO_sid = 0 #id voir ce qu'on veux mettre
	ZANRO_ref_nro = LayersInfoDic["ZNRO_REF"]
	ZANRO_ref_nra = ''
	ZANRO_degroup = 0
	ZANRO_bt_sfr = 0
	ZANRO_nb_prise = 0
	ZANRO_planning = 1
	ZANRO_sst = 'TDF'
	ZANRO_zone = ''
	ZANRO_zn_code = ''
	ZANRO_nb_prise_r = 0
	ZANRO_sst_rbal = ''
	ZANRO_date_rbal = ''
	ZANRO_com_tvx = ''
	ZANRO_com_sst = ''
	ZANRO_sst_rang2 = ''

	ZNrofeat.setAttributes([ZANRO_sid, ZANRO_ref_nro, ZANRO_ref_nra, ZANRO_degroup, ZANRO_bt_sfr, ZANRO_nb_prise, ZANRO_planning, ZANRO_sst, ZANRO_zone, ZANRO_zn_code, ZANRO_nb_prise_r, ZANRO_sst_rbal, ZANRO_date_rbal, ZANRO_com_tvx, ZANRO_com_sst, ZANRO_sst_rang2])
	LayersInfoDic["ZANRO_Layer"].dataProvider().addFeature(ZNrofeat)
	LayersInfoDic["ZANRO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ZANRO_Layer"])

def AddZASroFeature(LayersInfoDic):
	# add a feature
	ZSrofeat = QgsFeature()
	for tdf_Zsro_Feat in LayersInfoDic["tdf_Zsro_Layer"].getFeatures():
		zSroGeometry = tdf_Zsro_Feat.geometry()
		break

	ZSrofeat.setGeometry(zSroGeometry)

	ZASRO_ref_sro = LayersInfoDic["ZSRO_REF"]
	ZASRO_ref_nro = LayersInfoDic["ZNRO_REF"]
	ZASRO_nb_prises = 0
	ZASRO_epci = 'NR'
	ZASRO_planning = 0
	ZASRO_sst = 'TDF'

	#ZSrofeat.setAttributes([str(tZsro_Feat.attributes()[LayersInfoDic["ZS_RefPmAttr"]])])
	ZSrofeat.setAttributes([ZASRO_ref_sro, ZASRO_ref_nro, ZASRO_nb_prises, ZASRO_epci, ZASRO_planning, ZASRO_sst])
	LayersInfoDic["ZASRO_Layer"].dataProvider().addFeature(ZSrofeat)
	LayersInfoDic["ZASRO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ZASRO_Layer"])

def AddZAPboFeature(LayersInfoDic):
	# add a feature
	for ThistZpboFeat in LayersInfoDic["tdf_Zpbo_Layer"].getFeatures():
		NewZPbofeat = QgsFeature()
		NewZPbofeat.setGeometry(ThistZpboFeat.geometry())

		ZAPBO_nom = ThistZpboFeat.attributes()[LayersInfoDic["tdfZP_CodeAttr"]]
		ZAPBO_nb_prise = ThistZpboFeat.attributes()[LayersInfoDic["tdfZP_NbPriseAttr"]]
		ZAPBO_commentair = ThistZpboFeat.attributes()[LayersInfoDic["tdfZP_CommentAttr"]]
		ZAPBO_isole = 0

		#NewZPbofeat.setAttributes([str(ThistZpboFeat.attributes()[LayersInfoDic["ZP_CodeAttr"]]),0, ThistZpboFeat.id(),0])
		NewZPbofeat.setAttributes([ZAPBO_nom, ZAPBO_nb_prise, ZAPBO_commentair, ZAPBO_isole])
		LayersInfoDic["ZAPBO_Layer"].dataProvider().addFeature(NewZPbofeat)
		LayersInfoDic["ZAPBO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ZAPBO_Layer"])

def AddSIFeature(LayersInfoDic):
	# add a feature
	for ThistCheminFeat in LayersInfoDic["tdf_Chemin_Layer"].getFeatures():
		NewSIfeat = QgsFeature()
		NewSIfeat.setGeometry(ThistCheminFeat.geometry())

		SUPP_nom = str(ThistCheminFeat.attributes()[LayersInfoDic["tdfCM_CodeAttr"]])
		SUPP_code = str(ThistCheminFeat.attributes()[LayersInfoDic["tdfCM_CodeAttr"]])
		SUPP_propri = str(ThistCheminFeat.attributes()[LayersInfoDic["tdfCM_proAttr"]])
		SUPP_gestio = str(ThistCheminFeat.attributes()[LayersInfoDic["tdfCM_GestAttr"]])
		SUPP_type_str = 'TELECOM'
		SUPP_lg_reel = int(NewSIfeat.geometry().length())
		SUPP_largeur = 0
		SUPP_hauteur = 0
		
		SUPP_amont = str(ThistCheminFeat.attributes()[LayersInfoDic["tdfCM_NdC1Attr"]])
		SUPP_aval = str(ThistCheminFeat.attributes()[LayersInfoDic["tdfCM_NdC2Attr"]])
		SUPP_insee = LayersInfoDic["INSEE"]
		SUPP_comment = ''
		SUPP_structure = ''
		if('AERIEN' in str(ThistCheminFeat.attributes()[LayersInfoDic["tdfCM_TypImpAttr"]])):
			SUPP_structure = 'AER'
		#elif(str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypImpAttr"]]) == '9'):
		#SUPP_structure = 'TRA'
		else:
			SUPP_structure = 'EXI'
		SUPP_isole = 0
		SUPP_compo = ''#str(ThistCheminFeat.attributes()[LayersInfoDic["CM_CompoAttr"]])
		SUPP_disponible = ''#str(ThistCheminFeat.attributes()[LayersInfoDic["CM_EtatAttr"]])
		SUPP_uttilisation = ''
		'''
		if(str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypLogAttr"]]) == 'TR'):
			SUPP_uttilisation = 'T'
		elif(str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypLogAttr"]]) == 'TD'):
			SUPP_uttilisation = 'TD'
		elif(str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypLogAttr"]]) == 'DI'):
			SUPP_uttilisation = 'D'
		'''
		SUPP_casse_bloc = ''
		SUPP_ref_fiche = ''
		SUPP_mode_pose = ''#str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypImpAttr"]])

		#NewSIfeat.setAttributes([str(ThistCheminFeat.attributes()[LayersInfoDic["CM_CodeAttr"]]), str(ThistCheminFeat.attributes()[LayersInfoDic["CM_CodeAttr"]]),'','','TELECOM',0,0,0,'ND_Amont','ND_Aval','Insee',ThistCheminFeat.id(),str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypImpAttr"]]), 0,'compo','',str(ThistCheminFeat.attributes()[LayersInfoDic["CM_TypLogAttr"]])])
		NewSIfeat.setAttributes([SUPP_nom, SUPP_code, SUPP_propri, SUPP_gestio, SUPP_type_str, SUPP_lg_reel, SUPP_largeur, SUPP_hauteur, SUPP_amont, SUPP_aval, SUPP_insee, SUPP_comment, SUPP_structure, SUPP_isole, SUPP_compo, SUPP_disponible, SUPP_uttilisation, SUPP_casse_bloc, SUPP_ref_fiche, SUPP_mode_pose])
		LayersInfoDic["SI_Layer"].dataProvider().addFeature(NewSIfeat)
		LayersInfoDic["SI_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["SI_Layer"])

def AddCOFeature(LayersInfoDic):
	# add a feature
	for ThistCableLineFeat in LayersInfoDic["tdf_Cable_Layer"].getFeatures():
		if(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_TypLogAttr"]]) == "RACCORDEMENT"): continue
		NewCOfeat = QgsFeature()
		NewCOfeat.setGeometry(ThistCableLineFeat.geometry())
		CO_nom = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CodeAttr"]])
		CO_code = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CodeAttr"]])
		CO_type_fonc = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_TypLogAttr"]])
		'''
		if(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_TypeLogAttr"]]) == 'DI'):
			CO_type_fonc = 'DISTRIBUTION'
		'''
		CO_type_struct = ''
		if('AERIEN' in str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CommentAttr"]])):# DOUTE
			CO_type_struct = 'AERIEN'
		elif('SOUTERRAIN' in str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CommentAttr"]])):
			CO_type_struct = 'CONDUITE'
		CO_emprise = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_PropTypAttr"]])
		'''
		if(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'CST'):
			CO_emprise = 'CONSTRUCTION'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'RAC'):
			CO_emprise = 'RACHAT'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'CES'):
			CO_emprise = 'CESSION'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'IRU'):
			CO_emprise = 'IRU'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'LOC'):
			CO_emprise = 'LOCATION'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_PropTypAttr"]]) == 'OCC'):
			CO_emprise = 'OCCUPATION'
		'''
		CO_propri = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_PropAttr"]])
		CO_gestio = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_GestAttr"]])
		CO_etat = ''
		if(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'PRE'):
			CO_etat = 'ETUDE PRELIMINAIRE'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'DIA'):
			CO_etat = 'ETUDE DE DIAGNOSTIC'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'AVP'):
			CO_etat = 'AVANT-PROJET'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'PRO'):
			CO_etat = 'PROJET'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'ACT'):
			CO_etat = 'PASSATION DES MARCHES DE TRAVAUX'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'EXE'):
			CO_etat = 'ETUDE D EXECUTION'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'TVX'):
			CO_etat = 'TRAVAUX'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'REC'):
			CO_etat = 'RECOLLEMENT'
		elif(str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_StatutAttr"]]) == 'MCO'):
			CO_etat = 'MAINTIEN EN CONDITIONS OPERATIONNELLES'
		CO_fabricant = ''
		CO_reference = ''
		CO_lg_reel = int(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_LgReelAttr"]])
		CO_lg_carto = ThistCableLineFeat.geometry().length()
		CO_diametre = float(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_DiamAttr"]])
		CO_typefibre = ''
		CO_comment = str(ThistCableLineFeat.id())
		CO_rattach = LayersInfoDic["ZSRO_REF"]
		CO_capacite = float(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CapaAttr"]])
		#print("\n\nOrig-Extr co: "+str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_CodeAttr"]]))
		BoName = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_Nd1Attr"]]).split('-')
		if(str(BoName[-1]).isdigit()):
			BoOrigName = str(BoName[-2]+'-'+BoName[-1])
		else:
			BoOrigName = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_Nd1Attr"]])
		CO_origine = BoOrigName
		BoName = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_Nd2Attr"]]).split('-')
		if(str(BoName[-1]).isdigit()):
			BoExtrName = str(BoName[-2]+'-'+BoName[-1])
		else:
			BoExtrName = str(ThistCableLineFeat.attributes()[LayersInfoDic["tdfCB_Nd2Attr"]])
		CO_extremite = BoExtrName
		'''
		extFound = False
		origFound = False
		for ThisRow in allElePosBoit:
			#print(str(ThisRow))
			#print("IF: "+str(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_CodeAttr"]]))+ " in in "+str(ThisRow[4]))
			#print("elIF: "+str(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_CodeAttr"]]))+ " in in "+str(ThisRow[12]))
			if(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_CodeAttr"]]) in ThisRow[4] and not origFound):#Orig
				print(str(ThisRow))
				print("CO_Orig = "+str(ThisRow[0]))
				CO_extremite = str(ThisRow[0])
				origFound = True
			if(str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_CodeAttr"]]) in ThisRow[12] and not extFound):#Extremite
				print(str(ThisRow))
				print("CO_Extr = "+str(ThisRow[0]))
				CO_origine = str(ThisRow[0])
				extFound = True
			if(extFound and origFound): break
		'''
		CO_section = ''
		CO_isole = 0

		#NewCOfeat.setAttributes([str(ThistCableLineFeat.attributes()[LayersInfoDic["CB_CodeAttr"]]),'',str(ThisCableInfo.attributes()[LayersInfoDic["CB_TypeLogAttr"]]),'tCalbeLine Type','CONSTRUCTION','??','??',str(ThisCableInfo.attributes()[LayersInfoDic["CB_PropTypAttr"]]),'??','??',0.0,0.0,float(ThisCableInfo.attributes()[LayersInfoDic["CB_DiamAttr"]]),'??','','SRO',float(ThisCableInfo.attributes()[LayersInfoDic["CB_CapaFoAttr"]]),'','','', 0])
		NewCOfeat.setAttributes([CO_nom, CO_code, CO_type_fonc, CO_type_struct, CO_emprise, CO_propri, CO_gestio, CO_etat, CO_fabricant, CO_reference, CO_lg_reel, CO_lg_carto, CO_diametre, CO_typefibre, CO_comment, CO_rattach, CO_capacite, CO_origine, CO_extremite, CO_section, CO_isole])
		LayersInfoDic["CO_Layer"].dataProvider().addFeature(NewCOfeat)
		LayersInfoDic["CO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["CO_Layer"])

def AddSITEFeature(LayersInfoDic):
	# add a feature
	NewSITEfeat = QgsFeature()
	NewSITEfeat.setGeometry(LayersInfoDic["SRO_GEO"])

	SITE_code = LayersInfoDicGlob["ZSRO_REF"]#str(SroFeat.attributes()[LayersInfoDic["ST_CodeAttr"]])
	SITE_nom = LayersInfoDicGlob["ZSRO_REF"]#str(SroFeat.attributes()[LayersInfoDic["ST_CodeAttr"]])
	SITE_propri = str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_PropAttr"]])
	SITE_gestio = str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_GestAttr"]])
	SITE_type_fonc = 'SOUS-REPARTITEUR OPTIQUE'
	SITE_etat = ''
	if(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'PRE'):
		SITE_etat = 'ETUDE PRELIMINAIRE'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'DIA'):
		SITE_etat = 'ETUDE DE DIAGNOSTIC'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'AVP'):
		SITE_etat = 'AVANT-PROJET'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'PRO'):
		SITE_etat = 'PROJET'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'ACT'):
		SITE_etat = 'PASSATION DES MARCHES DE TRAVAUX'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'EXE'):
		SITE_etat = 'ETUDE D EXECUTION'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'TVX'):
		SITE_etat = 'TRAVAUX'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'REC'):
		SITE_etat = 'RECOLLEMENT'
	elif(str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StStatAttr"]]) == 'MCO'):
		SITE_etat = 'MAINTIEN EN CONDITIONS OPERATIONNELLES'
	SITE_type_struc = str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_StTypPhyAttr"]])
	SITE_emprise = str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_PropTypAttr"]])
	SITE_typerac = ''
	SITE_hexacle = str(LayersInfoDic["SRO_FEAT"].attributes()[LayersInfoDic["tdfAD_HexAttr"]])
	SITE_nbprise = 0
	SITE_comment = ''
	SITE_rattach = LayersInfoDic["ZNRO_REF"]
	SITE_insee = LayersInfoDic["INSEE"]
	SITE_st_code = 'TDF'
	SITE_date_pose = ''
	SITE_EXTRMETHOD = 'SRIP_TDF'

	#NewSITEfeat.setAttributes([str(SroFeat.attributes()[LayersInfoDic["ST_CodeAttr"]]),'Nom','??','??','SOUS-REPARTIREUR OPTIQUE','etat?','TypStruc',SroFeat.attributes()[LayersInfoDic["ST_PropTypAttr"]],'??','HAXc',0,str(ThisttNoeud.id()),'Nro','Insee','STcode',''])
	NewSITEfeat.setAttributes([SITE_code, SITE_nom, SITE_propri, SITE_gestio, SITE_type_fonc, SITE_etat, SITE_type_struc, SITE_emprise, SITE_typerac, SITE_hexacle, SITE_nbprise, SITE_comment, SITE_rattach, SITE_insee, SITE_st_code, SITE_date_pose, SITE_EXTRMETHOD])
	LayersInfoDic["SNRO_Layer"].dataProvider().addFeature(NewSITEfeat)
	LayersInfoDic["SNRO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["SNRO_Layer"])

def AddPTFeature(LayersInfoDic, csvFolder):
	# add a feature
	PTcsvInfos = []
	with open(str(csvFolder+r"\\pivot_PointTechnique_to_netgeo.csv"), newline='', encoding='utf-8') as f:
		reader = csv.reader(f,delimiter=';')
		for myid ,row in enumerate(reader):
			#print('LIGE: '+str(myid)+" == "+str(row))
			PTcsvInfos.append(row)
	'''
	for item in PTcsvInfos:
		print(item)
	'''
	for thisPTech in LayersInfoDic["tdf_PT_Layer"].getFeatures():	
		roundedCoordAttr = (str(str(str(str(str(thisPTech.attributes()[LayersInfoDic["tdfPT_GeomAttr"]]).replace('(','')).replace('POINT ','')).replace('POINT','')).replace(')','')).split(' '))
		#print("DEBUG_ ATTR COORD: "+str(roundedCoordAttr))
		#print("RONDED ATTR COORD: "+str(round(float(roundedCoordAttr[0]),3))+" -;- "+str(round(float(roundedCoordAttr[1]),3)))
		ptFoundInCsv = False
		for RowId, thisRow in enumerate(PTcsvInfos[1:]):
			roundedCoordCsv = (str(str(str(str(thisRow[32]).replace('(','')).replace('POINT','')).replace(')','')).split(' '))
			#print("Rounded Qgs coord: "+str(round(thisPTech.geometry().asPoint().x(),3))+" -;- "+str(round(thisPTech.geometry().asPoint().y(),3)))
			#print("RONDED CSV COORD: "+str(round(float(roundedCoordCsv[0]),3))+" -;- "+str(round(float(roundedCoordCsv[1]),3)))
			#print("Dist x: "+str(abs(round(float(roundedCoordAttr[0]),3) - round(float(roundedCoordCsv[0]),3)))+"\nDist y: "+str(abs(round(float(roundedCoordAttr[1]),3) - round(float(roundedCoordCsv[1]),3))))
			if(abs(round(float(roundedCoordAttr[0]),3) - round(float(roundedCoordCsv[0]),3)) > 0.1 or abs(round(float(roundedCoordAttr[1]),3) - round(float(roundedCoordCsv[1]),3)) > 0.1): continue
			'''
			if(ptFoundInCsv):
				print("\n\aATTENTION! Le point technique correspond à plusieurs Ligne sur le CSV!\n")
			'''
			ptFoundInCsv = True
			break
		if(ptFoundInCsv):
			print("PT INFOS found in csv row: "+str(RowId))
			print("Dist x: "+str(abs(round(float(roundedCoordAttr[0]),3) - round(float(roundedCoordCsv[0]),3)))+"\nDist y: "+str(abs(round(float(roundedCoordAttr[1]),3) - round(float(roundedCoordCsv[1]),3))))
		else:
			print("PT not found in CSV")
		NewPTfeat = QgsFeature()
		NewPTfeat.setGeometry(thisPTech.geometry())

		PT_nom = str(thisPTech.attributes()[LayersInfoDic["tdfPT_CodeAttr"]])
		PT_code = str(thisPTech.attributes()[LayersInfoDic["tdfPT_CodeAttr"]])
		PT_gestionnai = str(thisPTech.attributes()[LayersInfoDic["tdfPT_GestAttr"]])
		PT_type_fonc = ''
		if(ptFoundInCsv):
			PT_type_fonc = str(thisRow[11])
		'''
		if(str(thisPTech.attributes()[LayersInfoDic["tPT_TypeLogAttr"]]) == 'T'):
			PT_type_fonc = 'TIRAGE'
		elif(str(thisPTech.attributes()[LayersInfoDic["tPT_TypeLogAttr"]]) == 'R'):
			PT_type_fonc = 'RACCORDEMENT'
		'''
		PT_type_struc = str(thisPTech.attributes()[LayersInfoDic["tdfPT_TypPhyAttr"]])
		PT_etat = ''
		if(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'PRE'):
			PT_etat = 'ETUDE PRELIMINAIRE'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'DIA'):
			PT_etat = 'ETUDE DE DIAGNOSTIC'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'AVP'):
			PT_etat = 'AVANT-PROJET'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'PRO'):
			PT_etat = 'PROJET'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'ACT'):
			PT_etat = 'PASSATION DES MARCHES DE TRAVAUX'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'EXE'):
			PT_etat = 'ETUDE D EXECUTION'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'TVX'):
			PT_etat = 'TRAVAUX'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'REC'):
			PT_etat = 'RECOLLEMENT'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_StatuAttr"]]) == 'MCO'):
			PT_etat = 'MAINTIEN EN CONDITIONS OPERATIONNELLES'
		if(PT_etat == '' and ptFoundInCsv):
			PT_etat = str(thisRow[8])
		PT_emprise = ''
		if(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'CST'):
			PT_emprise = 'CONSTRUCTION'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'RAC'):
			PT_emprise = 'RACHAT'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'CES'):
			PT_emprise = 'CESSION'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'IRU'):
			PT_emprise = 'IRU'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'LOC'):
			PT_emprise = 'LOCATION'
		elif(str(thisPTech.attributes()[LayersInfoDic["tdfPT_PropTypAttr"]]) == 'OCC'):
			PT_emprise = 'OCCUPATION'
		
		PT_longeur = ''
		PT_largeur = ''
		PT_prof_haut = ''
		if(ptFoundInCsv):
			PT_longeur = float(str(thisRow[47]).replace(',','.'))
			PT_largeur = float(str(thisRow[48]).replace(',','.'))
			PT_prof_haut = float(str(thisRow[49]).replace(',','.'))
		PT_secteur = ''#str(LayersInfoDic["ZSRO_REF"])[-3:]
		PT_rattach = str(LayersInfoDic["ZSRO_REF"])
		PT_modele = str(thisPTech.attributes()[LayersInfoDic["tdfPT_NatuAttr"]])
		PT_comment = str(thisPTech.attributes()[LayersInfoDic["tdfPT_CommentAttr"]])
		PT_isole = 0
		PT_proprietai = str(thisPTech.attributes()[LayersInfoDic["tdfPT_GestAttr"]])
		PT_idparent = ''
		PT_remplaceapp = ''
		PT_ThdCodeExt = ''#str(thisPTech.attributes()[LayersInfoDic["tPT_CodeAttr"]])

		#NewPTfeat.setAttributes([str(thisPTech.attributes()[LayersInfoDic["tPT_CodeAttr"]]),'','ORANGE',str(thisPTech.attributes()[LayersInfoDic["tPT_TypeLogAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_TypePhyAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_StatutAttr"]]),str(thisPTech.attributes()[LayersInfoDic["tPT_PropTypAttr"]]),0.0,0.0,PtAppuiHauteur,'','#SRO',str(thisPTech.attributes()[LayersInfoDic["tPT_NatureAttr"]]),'',0,'ORANGE','','',str(ThisttNoeud.id())])
		NewPTfeat.setAttributes([PT_nom, PT_code, PT_gestionnai, PT_type_fonc, PT_type_struc, PT_etat, PT_emprise, PT_longeur, PT_largeur, PT_prof_haut, PT_secteur, PT_rattach, PT_modele, PT_comment, PT_isole, PT_proprietai, PT_idparent, PT_remplaceapp,PT_ThdCodeExt])
		LayersInfoDic["PT_Layer"].dataProvider().addFeature(NewPTfeat)
		LayersInfoDic["PT_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["PT_Layer"])

def AddBOFeature(LayersInfoDic, csvFolder):
	# add a feature
	BOcsvInfos = []
	with open(str(csvFolder+r"\\pivot_Bpe_to_netgeo.csv"), newline='', encoding='utf-8') as f:
		reader = csv.reader(f,delimiter=';')
		for row in reader:
			BOcsvInfos.append(row)

	for item in BOcsvInfos:
		print(str(item))

	COcsvInfos = []
	with open(str(csvFolder+r"\\pivot_Cable_to_netgeo.csv"), newline='', encoding='utf-8') as f:
		reader = csv.reader(f,delimiter=';')
		for row in reader:
			COcsvInfos.append(row)
	#for item in COcsvInfos:
	#print(item)	
		
	for thisBO in LayersInfoDic["tdf_Noeud_Layer"].getFeatures():
		if(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]] == 'PM'): continue
		boFoundInCsv = False
		for RowId, thisRow in enumerate(BOcsvInfos):
			if(thisRow[0] != thisBO.attributes()[LayersInfoDic["tdfBP_CodeAttr"]]): continue
			boFoundInCsv = True
			break
		if(boFoundInCsv):
			print("BO( "+str(thisRow[0])+" ) INFOS found in csv row: "+str(RowId))
		else:
			print("BO can't be found in CSV")
		NewBOfeat = QgsFeature()
		NewBOfeat.setGeometry(thisBO.geometry())

		BoName = str(thisBO.attributes()[LayersInfoDic["tdfBP_CodeAttr"]]).split('-')
		if(str(BoName[-1]).isdigit()):
			BoNewName = str(BoName[-2]+'-'+BoName[-1])
		else:
			BoNewName = str(thisBO.attributes()[LayersInfoDic["tdfBP_CodeAttr"]])
		BO_nom = BoNewName#str(thisBO.attributes()[LayersInfoDic["tdfBP_CodeAttr"]])
		BO_code = BoNewName#str(thisBO.attributes()[LayersInfoDic["tdfBP_CodeAttr"]])
		BO_typefonc = ''
		
		if(str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]]) == 'BPE'):
			BO_typefonc = 'BOITIER PROTECTION EPISSURE'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]]) == 'DTI'):
			BO_typefonc = 'DISPOSITIF DE TERMINAISON INTERIEUR OPTIQUE'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]]) == 'PBO'):
			BO_typefonc = 'POINT DE BRANCHEMENT OPTIQUE'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]]) == 'PTO'):
			BO_typefonc = 'POINT DE TERMINAISON OPTIQUE'
			#ATTENTION, ACRONYME INCONU
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]]) == 'BPI'):
			BO_typefonc = 'BPI?'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]]) == 'BDI'):
			BO_typefonc = 'BDI?'
		
		BO_etat = ''
		if(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'PRE'):
			BO_etat = 'ETUDE PRELIMINAIRE'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'DIA'):
			BO_etat = 'ETUDE DE DIAGNOSTIC'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'AVP'):
			BO_etat = 'AVANT-PROJET'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'PRO'):
			BO_etat = 'PROJET'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'ACT'):
			BO_etat = 'PASSATION DES MARCHES DE TRAVAUX'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'EXE'):
			BO_etat = 'ETUDE D EXECUTION'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'TVX'):
			BO_etat = 'TRAVAUX'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'REC'):
			BO_etat = 'RECOLLEMENT'
		elif(str(thisBO.attributes()[LayersInfoDic["tdfBP_StatuAttr"]]) == 'MCO'):
			BO_etat = 'MAINTIEN EN CONDITIONS OPERATIONNELLES'
		BO_reference = ''
		BO_proprietai = ''
		BO_gestionnai = ''
		if(boFoundInCsv):
			BO_reference = str(thisRow[30]) #(ae)
			BO_proprietai = str(thisRow[11]) #(l)
			BO_gestionnai = str(thisRow[12]) #(m)
		BO_typestruc = str(thisBO.attributes()[LayersInfoDic["tdfBP_TpyPhyAttr"]])
		BO_emprise = ''
		'''
		if(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'CST'):
			BO_emprise = 'CONSTRUCTION'
		elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'RAC'):
			BO_emprise = 'RACHAT'
		elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'CES'):
			BO_emprise = 'CESSION'
		elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'IRU'):
			BO_emprise = 'IRU'
		elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'LOC'):
			BO_emprise = 'LOCATION'
		elif(str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]) == 'OCC'):
			BO_emprise = 'OCCUPATION'
		'''
		BO_fabricant = ''
		if(boFoundInCsv):
			BO_fabricant = str(thisRow[29]) #(ad)
		BO_dimension = ''
		BO_typeequip = ''
		BO_interco = str(thisBO.attributes()[LayersInfoDic["tdfBP_FoncAttr"]])
		BO_amont = 'NR'
		for thisrowCO in COcsvInfos:
			if(thisBO.attributes()[LayersInfoDic["tdfBP_CodeAttr"]] != thisrowCO[4]): continue# (e)
			BO_amont = str(thisrowCO[0])
			break
		BO_volume = 0.0
		BO_comment = ''
		BO_hauteur = 0.0
		BO_nbepissure = int(thisBO.attributes()[LayersInfoDic["tdfBP_NbEpiAttr"]])
		BO_nbabbo = int(thisBO.attributes()[LayersInfoDic["tdfBP_NbPtoAttr"]])
		BO_nbfafs = 0
		BO_nbfu = 0
		BO_support = ''
		if(boFoundInCsv):
			if(str(thisRow[2])[5:8] == 'CHA'):#2(c)
				BO_support = 'CHAMBRE'
			if(str(thisRow[2])[5:8] == 'POT'):#2(c)
				BO_support = 'APPUI'
			if(str(thisRow[2])[5:8] == 'PFA'):#2(c)
				BO_support = 'ANCRAGE FACADE'
			if(str(thisRow[2])[5:8] == 'SCL'):#2(c)
				BO_support = 'COLONNE MONTANTE?'
		SuppFound = False
		for thisPt in LayersInfoDic["PT_Layer"].getFeatures():
			if(NewBOfeat.geometry().asPoint() == thisPt.geometry().asPoint()):
				SuppFound = True
				break
		if(not SuppFound): print("Can't found Bo's support TypePhy.")
		BO_fonction = str(thisBO.attributes()[LayersInfoDic["tdfBP_TypLogAttr"]])
		BO_isole = '0'
		BO_idparent = ''
		BO_ThdCodeExt = ''#str(thisBO.attributes()[LayersInfoDic["tBP_CodeAttr"]])
		#NewBOfeat.setAttributes([str(thisBO.attributes()[LayersInfoDic["tBP_CodeAttr"]]),'','PointBranchement?',str(thisBO.attributes()[LayersInfoDic["tBP_StatutAttr"]]),'','','',str(thisBO.attributes()[LayersInfoDic["tBP_TypePhyAttr"]]),str(thisBO.attributes()[LayersInfoDic["tBP_PropTypAttr"]]),'','','','','',0.0,str(ThisttNoeud.id()),0.0,0,0,0,0,'',str(thisBO.attributes()[LayersInfoDic["tBP_TypeLogAttr"]]),'0',''])
		NewBOfeat.setAttributes([BO_nom, BO_code, BO_typefonc, BO_etat, BO_reference, BO_proprietai, BO_gestionnai, BO_typestruc, BO_emprise, BO_fabricant, BO_dimension, BO_typeequip, BO_interco, BO_amont, BO_volume, BO_comment, BO_hauteur, BO_nbepissure, BO_nbabbo, BO_nbfafs, BO_nbfu, BO_support, BO_fonction, BO_isole, BO_idparent, BO_ThdCodeExt])
		LayersInfoDic["BO_Layer"].dataProvider().addFeature(NewBOfeat)
		LayersInfoDic["BO_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["BO_Layer"])

def AddDBLFeature(LayersInfoDic):
	# add a feature
	for thisDBL in LayersInfoDic["tdf_DBL_Layer"].getFeatures():

		NewDBLfeat = QgsFeature()
		NewDBLfeat.setGeometry(thisDBL.geometry())
		#print("NewDBL GEom: "+str(NewDBLfeat.geometry()))
		#break

		DBL_id = str(thisDBL.id())
		DBL_refimb = str(thisDBL.attributes()[LayersInfoDic["tdfAD_OidAttr"]])
		DBL_ad_code = str(thisDBL.attributes()[LayersInfoDic["tdfAD_CodeAttr"]])
		DBL_hexacle = str(thisDBL.attributes()[LayersInfoDic["tdfAD_HexAttr"]])
		DBL_id_ban = str(thisDBL.attributes()[LayersInfoDic["tdfAD_CodeAttr"]])
		DBL_num_adr = 0
		if(str(thisDBL.attributes()[LayersInfoDic["tdfAD_NumAttr"]]).isdigit()):
			DBL_num_adr = int(thisDBL.attributes()[LayersInfoDic["tdfAD_NumAttr"]])
		else:
			DBL_num_adr = -1
		DBL_ex_num_adr = str(thisDBL.attributes()[LayersInfoDic["tdfAD_RepAttr"]])
		DBL_nom_bat = ''#ad_alias?
		DBL_voie_adr = str(thisDBL.attributes()[LayersInfoDic["tdfAD_tVoiAttr"]]) +' '+ str(thisDBL.attributes()[LayersInfoDic["tdfAD_NomVAttr"]])
		DBL_c_postal = str(thisDBL.attributes()[LayersInfoDic["tdfAD_PostAttr"]])
		DBL_c_insee = str(thisDBL.attributes()[LayersInfoDic["tdfAD_InseeAttr"]])
		DBL_lib_com = ''
		DBL_planning = '2'
		DBL_nb_prise = int(thisDBL.attributes()[LayersInfoDic["tdfAD_NbHabAttr"]]) + int(thisDBL.attributes()[LayersInfoDic["tdfAD_NbPROAttr"]])
		DBL_xL93 = NewDBLfeat.geometry().asPoint().x()
		DBL_yL93 = NewDBLfeat.geometry().asPoint().y()
		DBL_ref_nro = str(thisDBL.attributes()[LayersInfoDic["tdfAD_SbpSroAttr"]])
		DBL_ref_sro = str(thisDBL.attributes()[LayersInfoDic["tdfAD_SbpNroAttr"]])
		DBL_ref_pbo = ''
		DBL_mdp_pbo = ''
		DBL_ddp_pbo = ''
		DBL_dpexp_pbo = ''
		DBL_statut = ''#PROJET?
		DBL_type_zone = ''
		'''
		if(str(thisDBL.attributes()[LayersInfoDic["tAD_TypZoneAttr"]]) == '1'):
			DBL_type_zone = 'ZTD HAUTE DENSITE'
		elif(str(thisDBL.attributes()[LayersInfoDic["tAD_TypZoneAttr"]]) == '2'):
			DBL_type_zone = 'ZTD BASSE DENSITE'
		if(str(thisDBL.attributes()[LayersInfoDic["tAD_TypZoneAttr"]]) == '3'):
			DBL_type_zone = 'ZMD'
		'''
		DBL_techno = ''
		DBL_type_bat = str(thisDBL.attributes()[LayersInfoDic["tdfAD_TypImAttr"]])
		DBL_type_cli = ''
		DBL_racco_bat = str(thisDBL.attributes()[LayersInfoDic["tdfAD_RaccAttr"]])
		DBL_trav_bat = ''
		DBL_rac_long = '0'
		DBL_acc_gest = ''#thisDBL.attributes()[LayersInfoDic["tAD_iAccGstAttr"]]
		DBL_rais_gest = ''
		DBL_cont_gest =''
		DBL_t_con_gest = ''
		DBL_type = ''
		DBL_comment = str(thisDBL.attributes()[LayersInfoDic["tdfAD_CommentAttr"]])
		DBL_voie_adr_t = ''
		DBL_id_loc = ''
		DBL_t2_con_ges = ''
		DBL_fonct_gest = ''
		DBL_adr_gest = ''
		DBL_cp_gest = ''
		DBL_mail_gest = ''
		DBL_ville_gest = ''
		DBL_date_ag = ''
		DBL_date_envoie = ''
		DBL_date_relan = ''
		DBL_date_conv = ''
		DBL_besoin_dta = ''
		DBL_dta = ''
		DBL_date_prev = ''
		DBL_date_env_1 = ''
		DBL_date_rel_1 = ''
		DBL_date_signa = ''
		DBL_date_sig_1 = ''
		DBL_equipe = ''
		DBL_date_previ = ''
		DBL_date_fin_t = ''
		DBL_commentaire = ''#thisDBL.attributes()[LayersInfoDic["tAD_CommentAttr"]]
		DBL_orig_adr = 'TDF'
		DBL_phase = ''
		DBL_com_fib_31 = ''
		DBL_nbprhab = str(thisDBL.attributes()[LayersInfoDic["tdfAD_NbHabAttr"]])
		DBL_nbprpro = str(thisDBL.attributes()[LayersInfoDic["tdfAD_NbPROAttr"]])
		DBL_nbprtte = 0
		DBL_nbprgfu = 0
		DBL_nbprfon = 0
		DBL_sst = 'TDF'
		DBL_proj_futur = ''
		DBL_date_proj = ''
		DBL_date_refus = ''
		DBL_sst_nego = ''
		DBL_imb_prefib = ''
		DBL_edl_debut = ''
		DBL_edl_fin_tv = ''
		DBL_etapes_con = ''
		DBL_com_sst = ''
		DBL_date_prise = ''
		DBL_date_pri_1 = ''
		DBL_date_pri_2 = ''
		DBL_date_mesc = ''
		DBL_date_rdv_d = ''
		DBL_nd_code = ''
		DBL_zone = 'TDF'
		DBL_annee1_cir = 0
		DBL_STATUS = 'nOK?'


		#TRUE!!! NewDBLfeat.setAttributes([str(thisDBL.attributes()[LayersInfoDic["tAD_BatCodeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_CodeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_HexAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_BanIdAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_NumAttr"]]),"??","??", str(thisDBL.attributes()[LayersInfoDic["tAD_NomVoieAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_PostalAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_InseeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_CommuAttr"]]), (int(str(thisDBL.attributes()[LayersInfoDic["tAD_NblHabAttr"]]))+int(str(thisDBL.attributes()[LayersInfoDic["tAD_NblProAttr"]]))),thisDBL.geometry().asPoint().x(),thisDBL.geometry().asPoint().y(),"NRO","SRO",None,"PBO CODE", "PBO PT",None,None, str(thisDBL.attributes()[LayersInfoDic["tAD_iEtatAttr"]]), None, "Z?D", "FTTH", str(thisDBL.attributes()[LayersInfoDic["tAD_iTypeImAttr"]]),"??","??",0])
		#NewDBLfeat.setAttributes([str(thisDBL.attributes()[LayersInfoDic["tAD_BatCodeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_CodeAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_NumAttr"]]), str(thisDBL.attributes()[LayersInfoDic["tAD_iTypeImAttr"]]),"FTTH", (int(str(thisDBL.attributes()[LayersInfoDic["tAD_NblHabAttr"]]))+int(str(thisDBL.attributes()[LayersInfoDic["tAD_NblProAttr"]]))),0])
		NewDBLfeat.setAttributes([DBL_id,DBL_refimb,DBL_ad_code,DBL_hexacle,DBL_id_ban,DBL_num_adr,DBL_ex_num_adr,DBL_nom_bat,DBL_voie_adr,DBL_c_postal,DBL_c_insee,DBL_lib_com,DBL_planning,DBL_nb_prise,DBL_xL93,DBL_yL93,DBL_ref_nro,DBL_ref_sro,DBL_ref_pbo,DBL_mdp_pbo,DBL_ddp_pbo,DBL_dpexp_pbo,DBL_statut,DBL_type_zone,DBL_techno,DBL_type_bat,DBL_type_cli,DBL_racco_bat,DBL_trav_bat,DBL_rac_long,DBL_acc_gest,DBL_rais_gest,DBL_cont_gest,DBL_t_con_gest,DBL_type,DBL_comment,DBL_voie_adr_t,DBL_id_loc,DBL_t2_con_ges,DBL_fonct_gest,DBL_adr_gest,DBL_cp_gest,DBL_mail_gest,DBL_ville_gest,DBL_date_ag,DBL_date_envoie,DBL_date_relan,DBL_date_conv,DBL_besoin_dta,DBL_dta,DBL_date_prev,DBL_date_env_1,DBL_date_rel_1,DBL_date_signa,DBL_date_sig_1,DBL_equipe,DBL_date_previ,DBL_date_fin_t,DBL_commentaire,DBL_orig_adr,DBL_phase,DBL_com_fib_31,DBL_nbprhab,DBL_nbprpro,DBL_nbprtte,DBL_nbprgfu,DBL_nbprfon,DBL_sst,DBL_proj_futur,DBL_date_proj,DBL_date_refus,DBL_sst_nego,DBL_imb_prefib,DBL_edl_debut,DBL_edl_fin_tv,DBL_etapes_con,DBL_com_sst,DBL_date_prise,DBL_date_pri_1,DBL_date_pri_2,DBL_date_mesc,DBL_date_rdv_d,DBL_nd_code,DBL_zone,DBL_annee1_cir,DBL_STATUS])
		#print("DBL LAYER: "+str(LayersInfoDic["DBL_Layer"]))
		LayersInfoDic["DBL_Layer"].dataProvider().addFeature(NewDBLfeat)
		LayersInfoDic["DBL_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["DBL_Layer"])

def addADDUCFeature(LayersInfoDic):
	lastId = -1
	for ThistAdducFeat in LayersInfoDic["tdf_Cable_Layer"].getFeatures():
		if(str(ThistAdducFeat.attributes()[LayersInfoDic["tdfCB_TypLogAttr"]]) != "RACCORDEMENT"): continue
		NewADDfeat = QgsFeature()
		NewADDfeat.setGeometry(ThistAdducFeat.geometry())				
		ADD_id = 0
		ADD_typestruc = ''
		ADD_isole = '0'
		ADD_lg = int(NewADDfeat.geometry().length())
		ADD_comment = ''
		#NewADDfeat.setAttributes([str(thisDBL.id()),'',0,'',''])
		NewADDfeat.setAttributes([ADD_id, ADD_typestruc, ADD_isole, ADD_lg, ADD_comment])
		LayersInfoDic["ADDUC_Layer"].dataProvider().addFeature(NewADDfeat)
		LayersInfoDic["ADDUC_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ADDUC_Layer"])

def AddZACableFeature(LayersInfoDic):
	# add a feature
	ZCablefeat = QgsFeature()
	for tZcable_Feat in LayersInfoDic["tdf_Zbdi_Layer"].getFeatures():
		zCableGeometry = tZcable_Feat.geometry()
		ZCablefeat.setGeometry(zCableGeometry)

		ZACABLE_nom = str(tZcable_Feat.attributes()[LayersInfoDic["tdfZB_CodeAttr"]])[-2:]
		ZACABLE_nbprise = 0
		if(str(tZcable_Feat.attributes()[LayersInfoDic["tdfZB_NbPrisAttr"]]).isdigit()):
			ZACABLE_nbprise = int(str(tZcable_Feat.attributes()[LayersInfoDic["tdfZB_NbPrisAttr"]]))
		ZACABLE_comment = ''
		ZCablefeat.setAttributes([ZACABLE_nom, ZACABLE_nbprise, ZACABLE_comment])
		LayersInfoDic["ZACABLE_Layer"].dataProvider().addFeature(ZCablefeat)
		LayersInfoDic["ZACABLE_Layer"].updateExtents()
	QgsProject.instance().addMapLayer(LayersInfoDic["ZACABLE_Layer"])

def RefindCoInfos(LayersInfoDic):
	CurrentLayer = LayersInfoDic["CO_Layer"]
	CurrentLayer.startEditing()
	for ThisCo in LayersInfoDic["CO_Layer"].getFeatures():
		print("Refining Co: "+str(ThisCo.attributes()[LayersInfoDic["CO_NomAttr"]]))
		#Originie
		if(str(ThisCo.attributes()[LayersInfoDic["CO_OrigAttr"]]) == '' or str(ThisCo.attributes()[LayersInfoDic["CO_OrigAttr"]]) == 'NULL'):
			print("Origine inconu")
			CurrentLayer = LayersInfoDic["CO_Layer"]
			CurrentLayer.startEditing()
			CurrentLayer.changeAttributeValue(ThisCo.id(),LayersInfoDic["CO_OrigAttr"],'UnKnow')
			CurrentLayer.commitChanges()
		#Extremite
		possibleExtr = []
		print("ext: "+str(ThisCo.attributes()[LayersInfoDic["CO_ExtrAttr"]]))
		if(str(ThisCo.attributes()[LayersInfoDic["CO_ExtrAttr"]]) == '' or str(ThisCo.attributes()[LayersInfoDic["CO_ExtrAttr"]]) == 'NULL'):
			print("Extremite inconu")
			for ThisBo in LayersInfoDic["BO_Layer"].getFeatures():
				if(ThisBo.geometry().asPoint() in ThisCo.geometry().asMultiPolyline()[0][::len(ThisCo.geometry().asMultiPolyline()[0])-1]
					and str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]])[:14] not in str(ThisCo.attributes()[LayersInfoDic["CO_OrigAttr"]])):
					possibleExtr.append(str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]]))
					print("extremite Found: "+str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]]))
			if(len(possibleExtr) < 1):
				print("No Extr Found")
				possibleExtrField = "SRip_UnKnow"
			elif(len(possibleExtr) >= 1):
				possibleExtrField = 'SRip_'+str(possibleExtr[0])
			if(len(possibleExtr) > 1):
				for thisExtr in possibleExtr[1:]:
					possibleExtrField += ' or SRip_'+ str(thisExtr)
			CurrentLayer.changeAttributeValue(ThisCo.id(),LayersInfoDic["CO_ExtrAttr"],possibleExtrField)
		print('Found lgr: '+str(ThisCo.geometry().length()))
		CurrentLayer.changeAttributeValue(ThisCo.id(),LayersInfoDic["CO_LgrCartAttr"],ThisCo.geometry().length())
	CurrentLayer.commitChanges()

def refindBoInfos(LayersInfoDic):
	print("adding nbAbbo in Bo")
	for ThisZpbo in LayersInfoDic["ZAPBO_Layer"].getFeatures():
		isZpbInPatch = False
		for ThisZpboPatch in LayersInfoDic["tZpboPatch_Layer"].getFeatures():
			if(ThisZpbo.attributes()[LayersInfoDic["ZAPBO_NomAttr"]] == ThisZpboPatch.attributes()[LayersInfoDic["ZPpat_CodeAttr"]]):
				isZpbInPatch = True
				isBoInPatch = False 
				for ThisBo in LayersInfoDic["BO_Layer"].getFeatures():
					if(ThisBo.attributes()[LayersInfoDic["BO_CodeAttr"]] == ThisZpboPatch.attributes()[LayersInfoDic["ZPPBpat_CodeAttr"]]):
						isBoInPatch = True
						nbAbboinThisBo = 0
						for thisDBL in LayersInfoDic["DBL_Layer"].getFeatures():
							if(thisDBL.geometry().intersects(ThisZpbo.geometry())): nbAbboinThisBo += int(thisDBL.attributes()[LayersInfoDic["DBL_NbPriseAttr"]])
						CurrentLayer = LayersInfoDic["BO_Layer"]
						CurrentLayer.startEditing()
						CurrentLayer.changeAttributeValue(ThisBo.id(),LayersInfoDic["BO_NbAbboAttr"],nbAbboinThisBo)
						CurrentLayer.commitChanges()
						break
				if(not isBoInPatch):
					print('can\'t found Bo: '+str(ThisBo.attributes()[LayersInfoDic["BO_NomAttr"]])+" in ZpbPatch")
				break
		if(not isZpbInPatch):
			print('can\'t found zpb: '+str(ThisZpbo.attributes()[LayersInfoDic["ZAPBO_NomAttr"]])+" in ZpbPatch")

#OLD DEF's -------------------------------------------------------------------------------------------------------------
def SaveInfos(LayersInfoDic,SaveDataDic):
	Saves_Cnt =0
	MyPBar = QProgressDialog('Enregistrement des informations', 'Annuler', 0, len(SaveDataDic.ToChangeDic["PT_Layer"])+len(SaveDataDic.ToChangeDic["CO_Layer"])+len(SaveDataDic.ToChangeDic["SI_Layer"])+len(SaveDataDic.ToChangeDic["ADDUC_Layer"])+len(SaveDataDic.ToChangeDic["ZACABLE_Layer"])+len(SaveDataDic.ToChangeDic["ZASRO_Layer"])+len(SaveDataDic.ToChangeDic["BO_Layer"])+len(SaveDataDic.ToChangeDic["ZAPBO_Layer"]))
	MyPBar.setWindowModality(QtCore.Qt.WindowModal)
	print('Enregistrement des informations')
	for SavingLayer in SaveDataDic.ToChangeDic:
		MyPBar.setLabelText("Enregistrement des informations ("+str(SavingLayer)+")")
		CurrentLayer = LayersInfoDic[SavingLayer]
		CurrentLayer.startEditing()
		SavingTimer = datetime.datetime.now()
		print(str(CurrentLayer.name())+".StartEditing")
		for ChangeRow in SaveDataDic.ToChangeDic[SavingLayer]:
			MyPBar.setValue(Saves_Cnt)
			Saves_Cnt +=1
			if(MyPBar.wasCanceled()):
				break
			for AttrToChange in ChangeRow["Attrs"]:
				if(ChangeRow["Attrs"][AttrToChange] == ''):
					continue
				CurrentLayer.changeAttributeValue(ChangeRow["Id"],LayersInfoDic[AttrToChange],ChangeRow["Attrs"][AttrToChange])
				#print("\n\aAttribut changé: "+str(AttrToChange))
		CurrentLayer.commitChanges()
		print("\nSaving"+str(CurrentLayer.name())+" = "+str(datetime.datetime.now()-SavingTimer))

#Defs____________________________________________________________________________________________________________________________________________________________
def ControlInit():
	try:
		i=0
		# consoleWidget = iface.mainWindow().findChild( QDockWidget, 'PythonConsole' )
		global NowIs
		NowIs = datetime.datetime.now()
	except:
		NowIs = 0
		print('Erreurs lors de l\'initialisation')
		return -1;
	try:
		pass
		# del AllErrors
	except:
		pass
	return 0

def CheckFilePresence(LayersInfoDic):
	try:
		LayersInfoDic["PT_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES POINT TECHNIQUE N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["BO_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES BOITE OPTIQUE N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["ADDUC_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES ADDUCTION N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["DBL_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES DBL N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["ZAPBO_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES ZAPBO N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["ZACABLE_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES ZACABLE N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["CO_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES CABLE OPTIQUE N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;
	try:
		LayersInfoDic["SI_Layer"]
	except:
		print("\a"+"-"*50+"\nLA COUCHES SUPPORT N AS PAS ETE TROUVE\n"+"-"*50)
		return -1;

def GetLayersInfo(LayersInfoDic):
	#Recuperation des information des couches----------------
	#LayersInfoDic = {}
	try:
		for CurrentLayer in QgsProject.instance().mapLayers().values():
			if("BOITE_OPTIQUE" in CurrentLayer.name()):
				print("DEBUG: Get BO_Layer Info")
				LayersInfoDic["BO_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["BO_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["BO_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "SUPPORT" or CurrentLayer.attributeDisplayName(AttrId) == "support"):
						LayersInfoDic["BO_SupportAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "FA_FS" or CurrentLayer.attributeDisplayName(AttrId) == "fa_fs"):
						LayersInfoDic["BO_FafsAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NBABONNES" or CurrentLayer.attributeDisplayName(AttrId) == "nbabonnes"):
						LayersInfoDic["BO_NbAbboAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NBEPISSURE" or CurrentLayer.attributeDisplayName(AttrId) == "nbepissure"):
						LayersInfoDic["BO_NbEpiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NBFUTILE" or CurrentLayer.attributeDisplayName(AttrId) == "nbfutile"):
						LayersInfoDic["BO_NbFutAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "FONCTION" or CurrentLayer.attributeDisplayName(AttrId) == "fonction"):
						LayersInfoDic["BO_FonctionAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_FONC" or CurrentLayer.attributeDisplayName(AttrId) == "type_fonc"):
						LayersInfoDic["BO_TypeFoncAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "FABRICANT" or CurrentLayer.attributeDisplayName(AttrId) == "fabricant"):
						LayersInfoDic["BO_FabriAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROPRIETAI" or CurrentLayer.attributeDisplayName(AttrId) == "proprietai"):
						LayersInfoDic["BO_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["BO_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ID_PARENT" or CurrentLayer.attributeDisplayName(AttrId) == "id_parent"):
						LayersInfoDic["BO_IdParAttr"] = AttrId
					#if(CurrentLayer.attributeDisplayName(AttrId) == "POINT TECH" or CurrentLayer.attributeDisplayName(AttrId) == "point tech"):#INTROUVABLE
						#LayersInfoDic["BO_PtechAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REFERENCE" or CurrentLayer.attributeDisplayName(AttrId) == "reference"):
						LayersInfoDic["BO_RefAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "AMONT" or CurrentLayer.attributeDisplayName(AttrId) == "amont"):
						LayersInfoDic["BO_amonAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "INTERCO" or CurrentLayer.attributeDisplayName(AttrId) == "interco"):
						LayersInfoDic["BO_intcoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["BO_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ETAT" or CurrentLayer.attributeDisplayName(AttrId) == "etat"):
						LayersInfoDic["BO_EtatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EMPRISE" or CurrentLayer.attributeDisplayName(AttrId) == "emprise"):
						LayersInfoDic["BO_EmpAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "DIMENSION" or CurrentLayer.attributeDisplayName(AttrId) == "dimension"):
						LayersInfoDic["BO_DimAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "HAUTEUR" or CurrentLayer.attributeDisplayName(AttrId) == "hauteur"):
						LayersInfoDic["BO_HautAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "VOLUME" or CurrentLayer.attributeDisplayName(AttrId) == "volume"):
						LayersInfoDic["BO_VolAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["BO_tyStAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_EQUIP" or CurrentLayer.attributeDisplayName(AttrId) == "type_equip"):
						LayersInfoDic["BO_TyEqAttr"] = AttrId
					#TEST
					if(CurrentLayer.attributeDisplayName(AttrId) == "ThdCodeExt"):
						LayersInfoDic["BO_ThdCodeExtAttr"] = AttrId
			#Couche PT
			if("POINT_TECHNIQUE" in CurrentLayer.name() and CurrentLayer.wkbType() == 1):
				print("DEBUG: Get PT_Layer Info")
				LayersInfoDic["PT_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["PT_TypeStrucAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_FONC" or CurrentLayer.attributeDisplayName(AttrId) == "type_fonc"):
						LayersInfoDic["PT_TypeFoncAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["PT_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["PT_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "MODELE" or CurrentLayer.attributeDisplayName(AttrId) == "modele"):
						LayersInfoDic["PT_ModAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LONGUEUR" or CurrentLayer.attributeDisplayName(AttrId) == "longueur"):
						LayersInfoDic["PT_LonAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LARGEUR" or CurrentLayer.attributeDisplayName(AttrId) == "largeur"):
						LayersInfoDic["PT_LarAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROF_HAUT" or CurrentLayer.attributeDisplayName(AttrId) == "prof_haut"):
						LayersInfoDic["PT_HautAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "SECTEUR" or CurrentLayer.attributeDisplayName(AttrId) == "secteur"):
						LayersInfoDic["PT_SecAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROPRIETAI" or CurrentLayer.attributeDisplayName(AttrId) == "proprietai"):
						LayersInfoDic["PT_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["PT_GesAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "RATTACH" or CurrentLayer.attributeDisplayName(AttrId) == "rattach"):
						LayersInfoDic["PT_RatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["PT_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ETAT" or CurrentLayer.attributeDisplayName(AttrId) == "etat"):
						LayersInfoDic["PT_EtatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EMPRISE" or CurrentLayer.attributeDisplayName(AttrId) == "emprise"):
						LayersInfoDic["PT_EmpAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "IDPARENT" or CurrentLayer.attributeDisplayName(AttrId) == "idparent"):
						LayersInfoDic["PT_IdParAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REMPLA_APP" or CurrentLayer.attributeDisplayName(AttrId) == "rempla_app"):
						LayersInfoDic["PT_RemApAttr"] = AttrId
					#TEST
					if(CurrentLayer.attributeDisplayName(AttrId) == "ThdCodeExt"):
						LayersInfoDic["PT_ThdCodeExtAttr"] = AttrId
			#Couche DBL
			if("DBL" in CurrentLayer.name() and  "CONTROLE" not in CurrentLayer.name() and "ZASRO" not in CurrentLayer.name()):# and CurrentLayer.wkbType() == 1):
				print("DEBUG: Get DBL_Layer Info")
				LayersInfoDic["DBL_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "REF_IMB" or CurrentLayer.attributeDisplayName(AttrId) == "ref_imb"):
						LayersInfoDic["DBL_RefImbAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ID_BAN" or CurrentLayer.attributeDisplayName(AttrId) == "id_ban"):
						LayersInfoDic["DBL_BanAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "HEXACLE" or CurrentLayer.attributeDisplayName(AttrId) == "hexacle"):
						LayersInfoDic["DBL_HexAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TECHNO" or CurrentLayer.attributeDisplayName(AttrId) == "techno"):
						LayersInfoDic["DBL_TechnoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NUM_ADR" or CurrentLayer.attributeDisplayName(AttrId) == "num_adr"):
						LayersInfoDic["DBL_NumAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_CLI" or CurrentLayer.attributeDisplayName(AttrId) == "type_cli"):
						LayersInfoDic["DBL_TCliAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_BAT" or CurrentLayer.attributeDisplayName(AttrId) == "type_bat"):
						LayersInfoDic["DBL_TBatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "RACCO_BAT" or CurrentLayer.attributeDisplayName(AttrId) == "racco_bat"):
						LayersInfoDic["DBL_RacBatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NB_PRISE" or CurrentLayer.attributeDisplayName(AttrId) == "nb_prise"):
						LayersInfoDic["DBL_NbPriseAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "RAC_LONG" or CurrentLayer.attributeDisplayName(AttrId) == "rac_long"):
						LayersInfoDic["DBL_RacLngAttr"] = AttrId
			
			#Couche ZANRO
			if("ZANRO" in CurrentLayer.name()):
				print("DEBUG: Get ZANRO_Layer Info")
				LayersInfoDic["ZANRO_Layer"] = CurrentLayer
			#Couche ZAPBO
			if("ZAPBO" in CurrentLayer.name() ):#and CurrentLayer.wkbType() == 3):
				print("DEBUG: Get ZAPBO_Layer Info")
				LayersInfoDic["ZAPBO_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["ZAPBO_CodeAttr"] = AttrId
						LayersInfoDic["ZAPBO_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["ZAPBO_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NB_PRISES" or CurrentLayer.attributeDisplayName(AttrId) == "nb_prises"):
						LayersInfoDic["ZAPBO_NbPrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "COMMENTAIR" or CurrentLayer.attributeDisplayName(AttrId) == "commentair"):
						LayersInfoDic["ZAPBO_CommAttr"] = AttrId
			#Couche ZACABLE
			if("ZACABLE" in CurrentLayer.name() ):# and CurrentLayer.wkbType() == 3):
				print("DEBUG: Get ZACABLE_Layer Info")
				LayersInfoDic["ZACABLE_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["ZACABLE_NumeroAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NB_PRISES" or CurrentLayer.attributeDisplayName(AttrId) == "nb_prises"):
						LayersInfoDic["ZACABLE_NbPrAttr"] = AttrId
			#Couche ZASRO
			if("ZASRO" in CurrentLayer.name() ):# and CurrentLayer.wkbType() == 3):
				print("DEBUG: Get ZASRO_Layer Info")
				LayersInfoDic["ZASRO_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					#print("DEBUG: Get ZASRO_Layer Info222222")
					if(CurrentLayer.attributeDisplayName(AttrId) == "SST" or CurrentLayer.attributeDisplayName(AttrId) == "sst"):
						LayersInfoDic["ZASRO_SSTAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NB_PRISE" or CurrentLayer.attributeDisplayName(AttrId) == "nb_prise"):
						LayersInfoDic["ZASRO_NbPrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REF_SRO" or CurrentLayer.attributeDisplayName(AttrId) == "ref_sro"):
						LayersInfoDic["ZASRO_RefSAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REF_NRO" or CurrentLayer.attributeDisplayName(AttrId) == "ref_nro"):
						LayersInfoDic["ZASRO_RefNAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EPCI" or CurrentLayer.attributeDisplayName(AttrId) == "epci"):
						LayersInfoDic["ZASRO_EPCIAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PLANNING" or CurrentLayer.attributeDisplayName(AttrId) == "planning"):
						LayersInfoDic["ZASRO_PlanAttr"] = AttrId
			#Couche ADDUCTION
			if("ADDUCTION" in CurrentLayer.name()):# and CurrentLayer.wkbType() == 2):
				print("DEBUG: Get ADDUC_Layer Info")
				LayersInfoDic["ADDUC_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["ADDUC_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["ADDUC_TyStrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ID" or CurrentLayer.attributeDisplayName(AttrId) == "id"):
						LayersInfoDic["ADDUC_IdAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LG" or CurrentLayer.attributeDisplayName(AttrId) == "lg"):
						LayersInfoDic["ADDUC_LgAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "COMMENT" or CurrentLayer.attributeDisplayName(AttrId) == "comment"):
						LayersInfoDic["ADDUC_CommAttr"] = AttrId
			#Couche CABLE OPTIQUE
			if("CABLE_OPTIQUE" in CurrentLayer.name() ):#and CurrentLayer.wkbType() == 2):
				print("DEBUG: Get CO_Layer Info")
				LayersInfoDic["CO_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["CO_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["CO_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CAPACITE" or CurrentLayer.attributeDisplayName(AttrId) == "capacite"):
						LayersInfoDic["CO_CapaAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ORIGINE" or CurrentLayer.attributeDisplayName(AttrId) == "origine"):
						LayersInfoDic["CO_OrigAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EXTREMITE" or CurrentLayer.attributeDisplayName(AttrId) == "extremite"):
						LayersInfoDic["CO_ExtrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["CO_TyStAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["CO_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_FONC" or CurrentLayer.attributeDisplayName(AttrId) == "type_fonc"):
						LayersInfoDic["CO_TyfonAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EMPRISE" or CurrentLayer.attributeDisplayName(AttrId) == "emprise"):
						LayersInfoDic["CO_EmpAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["CO_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROPRIETAI" or CurrentLayer.attributeDisplayName(AttrId) == "proprietai"):
						LayersInfoDic["CO_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ETAT" or CurrentLayer.attributeDisplayName(AttrId) == "etat"):
						LayersInfoDic["CO_EtatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "FABRICANT" or CurrentLayer.attributeDisplayName(AttrId) == "fabricant"):
						LayersInfoDic["CO_FabAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "REFERENCE" or CurrentLayer.attributeDisplayName(AttrId) == "reference"):
						LayersInfoDic["CO_RefAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LGR_CARTO" or CurrentLayer.attributeDisplayName(AttrId) == "lgr_carto"):
						LayersInfoDic["CO_LgrCartAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "DIAMETRE" or CurrentLayer.attributeDisplayName(AttrId) == "diametre"):
						LayersInfoDic["CO_DiamAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPEFIBRE" or CurrentLayer.attributeDisplayName(AttrId) == "typefibre"):
						LayersInfoDic["CO_TyFibreAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "RATTACH" or CurrentLayer.attributeDisplayName(AttrId) == "rattach"):
						LayersInfoDic["CO_RatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "SECTION" or CurrentLayer.attributeDisplayName(AttrId) == "section"):
						LayersInfoDic["CO_SectAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "COMMENT" or CurrentLayer.attributeDisplayName(AttrId) == "comment"):
						LayersInfoDic["CO_CommentAttr"] = AttrId

			#Couche SUPPORT INFRA
			if("SUPPORT" in CurrentLayer.name() ):#and CurrentLayer.wkbType() == 2):
				print("DEBUG: Get SUPP_Layer Info")
				LayersInfoDic["SI_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "NOM" or CurrentLayer.attributeDisplayName(AttrId) == "nom"):
						LayersInfoDic["SI_NomAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["SI_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ISOLE" or CurrentLayer.attributeDisplayName(AttrId) == "isole"):
						LayersInfoDic["SI_IsoAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "AMONT" or CurrentLayer.attributeDisplayName(AttrId) == "amont"):
						LayersInfoDic["SI_AmontAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "AVAL" or CurrentLayer.attributeDisplayName(AttrId) == "aval"):
						LayersInfoDic["SI_AvalAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "STRUCTURE" or CurrentLayer.attributeDisplayName(AttrId) == "structure"):
						LayersInfoDic["SI_StrucAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["SI_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "PROPRIETAI" or CurrentLayer.attributeDisplayName(AttrId) == "proprietai"):
						LayersInfoDic["SI_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "MODE_POSE" or CurrentLayer.attributeDisplayName(AttrId) == "mode_pose"):
						LayersInfoDic["SI_MPoseAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "TYPE_STRUC" or CurrentLayer.attributeDisplayName(AttrId) == "type_struc"):
						LayersInfoDic["SI_TyStAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LG_REELLE" or CurrentLayer.attributeDisplayName(AttrId) == "le_reelle"):
						LayersInfoDic["SI_LGreAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "LARGEUR" or CurrentLayer.attributeDisplayName(AttrId) == "largeur"):
						LayersInfoDic["SI_LarAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "HAUTEUR" or CurrentLayer.attributeDisplayName(AttrId) == "hauteur"):
						LayersInfoDic["SI_HautAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "INSEE" or CurrentLayer.attributeDisplayName(AttrId) == "insee"):
						LayersInfoDic["SI_INSEEAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "COMPO" or CurrentLayer.attributeDisplayName(AttrId) == "compo"):
						LayersInfoDic["SI_CompAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "UTILISATIO" or CurrentLayer.attributeDisplayName(AttrId) == "utilisatio"):
						LayersInfoDic["SI_UtiAttr"] = AttrId
			#Couche SITE
			if("_SITE" in CurrentLayer.name()):
				print("DEBUG: Get SITE_Layer Info")
				LayersInfoDic["SNRO_Layer"] = CurrentLayer
				for AttrId in range(0,len(CurrentLayer.attributeList())):
					if(CurrentLayer.attributeDisplayName(AttrId) == "CODE" or CurrentLayer.attributeDisplayName(AttrId) == "code"):
						LayersInfoDic["SNRO_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ST_CODE" or CurrentLayer.attributeDisplayName(AttrId) == "st_code"):
						LayersInfoDic["SNRO_StCodAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "HEXACLE" or CurrentLayer.attributeDisplayName(AttrId) == "hexcale"):
						LayersInfoDic["SNRO_HexAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "INSEE" or CurrentLayer.attributeDisplayName(AttrId) == "insee"):
						LayersInfoDic["SNRO_InseeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "GESTIONNAI" or CurrentLayer.attributeDisplayName(AttrId) == "gestionnai"):
						LayersInfoDic["SNRO_GesAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "NBPRISES" or CurrentLayer.attributeDisplayName(AttrId) == "nbprises"):
						LayersInfoDic["SNRO_NbPrisesAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "EXTRMETHOD"):
						LayersInfoDic["SNRO_ExtrMethodAttr"] = AttrId

		#GET MORE PRECISE INFOS
		for SNRO_Infos in LayersInfoDic["SNRO_Layer"].getFeatures():
			LayersInfoDic["SNRO_Feature"] = SNRO_Infos
			LayersInfoDic["SNRO_Id"] = str(SNRO_Infos.attributes()[LayersInfoDic["SNRO_CodeAttr"]])
			LayersInfoDic["SNRO_Gestion"] = str(SNRO_Infos.attributes()[LayersInfoDic["SNRO_GesAttr"]])
			LayersInfoDic["SNRO_INSEE"] = str(SNRO_Infos.attributes()[LayersInfoDic["SNRO_InseeAttr"]])
			LayersInfoDic["SNRO_PointPos"] = SNRO_Infos.geometry().asPoint()

		'''
		for tZnro_Feat in LayersInfoDic["tZnro_Layer"].getFeatures():
			LayersInfoDic["ZNRO_REF"] = str(tZnro_Feat.attributes()[LayersInfoDic["ZN_NroRefAttr"]])

		for tZsro_Feat in LayersInfoDic["tZsro_Layer"].getFeatures():
			LayersInfoDic["ZSRO_REF"] = str(tZsro_Feat.attributes()[LayersInfoDic["ZS_RefPmAttr"]])


		LayersInfoDic["INSEE"] = str(tZnro_Feat.attributes()[LayersInfoDic["ZN_NroRefAttr"]])[:5]
		'''
		return LayersInfoDic;
	except:
		return -1;

def GetLayersInfo_TDFShapes(LayersInfoDic):
	#Recuperation des information des couches----------------
	#LayersInfoDic = {}
	try:
		for CurrentLayer in QgsProject.instance().mapLayers().values():
			#TDF
			if(CurrentLayer.name() == "sites"):
				print("DEBUG: Get tdf_DBL_Layer Info")
				LayersInfoDic["tdf_DBL_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "oid"):
						LayersInfoDic["tdfAD_OidAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_code"):
						LayersInfoDic["tdfAD_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_hexacle"):
						LayersInfoDic["tdfAD_HexAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_tvoie"):
						LayersInfoDic["tdfAD_tVoiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_nomvoie"):
						LayersInfoDic["tdfAD_NomVAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_numero"):
						LayersInfoDic["tdfAD_NumAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_rep"):
						LayersInfoDic["tdfAD_RepAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_insee"):
						LayersInfoDic["tdfAD_InseeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_postal"):
						LayersInfoDic["tdfAD_PostAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_commune"):
						LayersInfoDic["tdfAD_ComAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_nblhab"):
						LayersInfoDic["tdfAD_NbHabAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_nblpro"):
						LayersInfoDic["tdfAD_NbPROAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_isole"):
						LayersInfoDic["tdfAD_IsoleAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_racc"):
						LayersInfoDic["tdfAD_RaccAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_itypeim"):
						LayersInfoDic["tdfAD_TypImAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_imneuf"):
						LayersInfoDic["tdfAD_ImNeufAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "ad_comment"):
						LayersInfoDic["tdfAD_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "sf_code"):
						LayersInfoDic["tdfAD_SfCodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "sf_type"):
						LayersInfoDic["tdfAD_SfType"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "st_genre"):
						LayersInfoDic["tdfAD_StGenrAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "st_statut"):
						LayersInfoDic["tdfAD_StStatAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "st_prop"):
						LayersInfoDic["tdfAD_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "st_gest"):
						LayersInfoDic["tdfAD_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "st_proptyp"):
						LayersInfoDic["tdfAD_PropTypAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "st_typephy"):
						LayersInfoDic["tdfAD_StTypPhyAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "s_nbprise"):
						LayersInfoDic["tdfAD_SnbPriseAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "s_bpsro"):
						LayersInfoDic["tdfAD_SbpSroAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "s_bpnro"):
						LayersInfoDic["tdfAD_SbpNroAttr"] = AttrId

			if(CurrentLayer.name() == "pointstechnique"):
				print("DEBUG: Get tdf_PT_Layer Info")
				LayersInfoDic["tdf_PT_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_code"):
						LayersInfoDic["tdfPT_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_codeext"):
						LayersInfoDic["tdfPT_CodeExtAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_nboites"):
						LayersInfoDic["tdfPT_NbBoitAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_ptop"):
						LayersInfoDic["tdfPT_PropriAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_gest"):
						LayersInfoDic["tdfPT_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_proptyp"):
						LayersInfoDic["tdfPT_PropTypAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_statut"):
						LayersInfoDic["tdfPT_StatuAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_typephy"):
						LayersInfoDic["tdfPT_TypPhyAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_nature"):
						LayersInfoDic["tdfPT_NatuAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_comment"):
						LayersInfoDic["tdfPT_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_code"):
						LayersInfoDic["tdfPT_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "pt_geom"):
						LayersInfoDic["tdfPT_GeomAttr"] = AttrId

			if(CurrentLayer.name() == "noeud"):
				print("DEBUG: Get tdf_Noeud_Layer Info")
				LayersInfoDic["tdf_Noeud_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_code"):
						LayersInfoDic["tdfBP_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_support"):
						LayersInfoDic["tdfBP_SuppAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_nbcas"):
						LayersInfoDic["tdfBP_NbCasAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_casdisp"):
						LayersInfoDic["tdfBP_CasDispAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_nbepi"):
						LayersInfoDic["tdfBP_NbEpiAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_statut"):
						LayersInfoDic["tdfBP_StatuAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_typephy"):
						LayersInfoDic["tdfBP_TpyPhyAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_capa"):
						LayersInfoDic["tdfBP_CapaAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_typelog"):
						LayersInfoDic["tdfBP_TypLogAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_fonc"):
						LayersInfoDic["tdfBP_FoncAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_rf_code"):
						LayersInfoDic["tdfBP_RfCodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "bp_comment"):
						LayersInfoDic["tdfBP_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "nb_nbpto"):
						LayersInfoDic["tdfBP_NbPtoAttr"] = AttrId

			if(CurrentLayer.name() == "cheminement"):
				print("DEBUG: Get tdf_Chemin_Layer Info")
				LayersInfoDic["tdf_Chemin_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_code"):
						LayersInfoDic["tdfCM_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_ndcode1"):
						LayersInfoDic["tdfCM_NdC1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_ndcode2"):
						LayersInfoDic["tdfCM_NdC2Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_pro_do"):
						LayersInfoDic["tdfCM_proAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_gest_do"):
						LayersInfoDic["tdfCM_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_typ_imp"):
						LayersInfoDic["tdfCM_TypImpAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_statut"):
						LayersInfoDic["tdfCM_statutAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_mod_pos"):
						LayersInfoDic["tdfCM_ModPosAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_comment"):
						LayersInfoDic["tdfCM_CommentAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cm_long"):
						LayersInfoDic["tdfCM_LongAttr"] = AttrId

			if(CurrentLayer.name() == "cable"):
				print("DEBUG: Get tdf_Cable_Layer Info")
				LayersInfoDic["tdf_Cable_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_code"):
						LayersInfoDic["tdfCB_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_nd1"):
						LayersInfoDic["tdfCB_Nd1Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_nd2"):
						LayersInfoDic["tdfCB_Nd2Attr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_prop"):
						LayersInfoDic["tdfCB_PropAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_gest"):
						LayersInfoDic["tdfCB_GestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_proptyp"):
						LayersInfoDic["tdfCB_PropTypAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_statut"):
						LayersInfoDic["tdfCB_StatutAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_typelog"):
						LayersInfoDic["tdfCB_TypLogAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_rf_code"):
						LayersInfoDic["tdfCB_RfCodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_capafo"):
						LayersInfoDic["tdfCB_CapaAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_fo_disp"):
						LayersInfoDic["tdfCB_FoDispAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_fo_util"):
						LayersInfoDic["tdfCB_FoUtilAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_modulo"):
						LayersInfoDic["tdfCB_ModuAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_diam"):
						LayersInfoDic["tdfCB_DiamAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_lgreel"):
						LayersInfoDic["tdfCB_LgReelAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "cb_comment"):
						LayersInfoDic["tdfCB_CommentAttr"] = AttrId

			if(CurrentLayer.name() == "zpbo"):
				print("DEBUG: Get tdf_Zpbo_Layer Info")
				LayersInfoDic["tdf_Zpbo_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "zp_code"):
						LayersInfoDic["tdfZP_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zp_nbprise"):
						LayersInfoDic["tdfZP_NbPriseAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zp_capamax"):
						LayersInfoDic["tdfZP_CapaMaxAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zp_statut"):
						LayersInfoDic["tdfZP_StatutAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zp_comment"):
						LayersInfoDic["tdfZP_CommentAttr"] = AttrId

			if(CurrentLayer.name() == "zbdi"):
				print("DEBUG: Get tdf_Zbdi_Layer Info")
				LayersInfoDic["tdf_Zbdi_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "zb_code"):
						LayersInfoDic["tdfZB_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zb_nbprise"):
						LayersInfoDic["tdfZB_NbPrisAttr"] = AttrId

			if(CurrentLayer.name() == "zsro"):
				print("DEBUG: Get tdf_Zsro_Layer Info")
				LayersInfoDic["tdf_Zsro_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "zs_code"):
						LayersInfoDic["tdfZS_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zs_zn_code"):
						LayersInfoDic["tdfZS_ZnCodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zs_capamax"):
						LayersInfoDic["tdfZS_CapaMaxAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zs_accgest"):
						LayersInfoDic["tdfZS_accGestAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zs_comment"):
						LayersInfoDic["tdfZS_CommentAttr"] = AttrId

			if(CurrentLayer.name() == "znro"):
				print("DEBUG: Get tdf_Znro_Layer Info")
				LayersInfoDic["tdf_Znro_Layer"] = CurrentLayer
				for AttrId in CurrentLayer.attributeList():
					if(CurrentLayer.attributeDisplayName(AttrId) == "zn_code"):
						LayersInfoDic["tdfZN_CodeAttr"] = AttrId
					if(CurrentLayer.attributeDisplayName(AttrId) == "zn_codeext"):
						LayersInfoDic["tdfZN_CodeExtAttr"] = AttrId

		# for tZnro_Feat in LayersInfoDic["tdf_Znro_Layer"].getFeatures():
		# 	LayersInfoDic["ZNRO_REF"] = str(tZnro_Feat.attributes()[LayersInfoDic["tdfZN_CodeAttr"]])

		for tZsro_Feat in LayersInfoDic["tdf_Zsro_Layer"].getFeatures():
			LayersInfoDic["ZSRO_REF"] = str(tZsro_Feat.attributes()[LayersInfoDic["tdfZS_CodeAttr"]])
		print("ZSRO_REF = "+str(LayersInfoDic["ZSRO_REF"]))
		
		for thisSTech in LayersInfoDic["tdf_DBL_Layer"].getFeatures():
			if(thisSTech.attributes()[LayersInfoDic["tdfAD_StGenrAttr"]] == 'PM'):
				#CodeSRO = thisSTech.attributes()[LayersInfoDic["ST_CodeExtAttr"]]
				SroFeat = thisSTech
				SITEgeometry = thisSTech.geometry()
				break
		# LayersInfoDic["SRO_FEAT"] = SroFeat
		# LayersInfoDic["SRO_GEO"] = SITEgeometry

		for thisDblInseeTemplate in LayersInfoDic["tdf_DBL_Layer"].getFeatures():
			LayersInfoDic["INSEE"] = str(thisDblInseeTemplate.attributes()[LayersInfoDic["tdfAD_InseeAttr"]])
			break

		return LayersInfoDic;
	except:
		return -1;
#____________________________________________________________________________________Start of the Script____________________________________________________________________________________#
print("-"*50+"\nDEBUT DU SCRIPT SRIP_ConverTDF_V0.0.3\n"+"-"*50+"\n"*2)
StartingTime = datetime.datetime.now()
LayersInfoDicGlob = {}
#Open Action choosing window

qgis_path = f"{sys.argv[1]}"

for f in os.listdir(f"{qgis_path}\\shp"):
	if f.endswith(".shp"):
		name = os.path.splitext(f)[0]
		layer = QgsVectorLayer(os.path.join(qgis_path, f), name)
		print(os.path.join(qgis_path, f))
		print("add layer: {} to QgsProject".format(layer.name()))
		QgsProject.instance().addMapLayer(layer)

while(1):
	LayersInfoDicGlob = GetLayersInfo_TDFShapes(LayersInfoDicGlob)
	if(LayersInfoDicGlob == -1):
		print("ERREUR lors de la récupérations des couches TDF")
		break

	GetCircetForatShapesLoaded(str(LayersInfoDicGlob["ZSRO_REF"]))
	
	LayersInfoDicGlob = GetLayersInfo(LayersInfoDicGlob)

	#OpenCSV
	homePath = str(QgsProject.instance().homePath())
	if(str(homePath) == ''):
		csvGlobalFolder, okPressed = QInputDialog.getText(None, "Get text","csv folder:", QLineEdit.Normal, "")
		if okPressed and csvGlobalFolder != '':
			print("CsvFolder set to: "+str(csvGlobalFolder))
	else:
		print('homePath found: '+str(homePath))
		csvGlobalFolder = homePath+r"\\csv"
	'''
	allElePosBoit = []
	with open(str(vElemPosXls+r"\\csv\\pivot_Bpe_to_netgeo.csv"), newline='', encoding='utf-8') as f:
		reader = csv.reader(f)
		for row in reader:
			allElePosBoit.append(row[0].split(';'))
	#print("\n\nallElePosBoit: "+str(allElePosBoit))
	'''
	AddZANroFeature(LayersInfoDicGlob)
	AddZASroFeature(LayersInfoDicGlob)
	AddZACableFeature(LayersInfoDicGlob)
	AddZAPboFeature(LayersInfoDicGlob)
	AddSIFeature(LayersInfoDicGlob)
	#AddCOFeature(LayersInfoDicGlob) - Old
	AddSITEFeature(LayersInfoDicGlob)
	AddPTFeature(LayersInfoDicGlob, csvGlobalFolder)
	AddBOFeature(LayersInfoDicGlob, csvGlobalFolder)
	#Set Co infosafeternaming BO's
	AddCOFeature(LayersInfoDicGlob)
	#break
	AddDBLFeature(LayersInfoDicGlob)
	addADDUCFeature(LayersInfoDicGlob)
	print('\n-end of layer creation\nRefining Layers:')
	'''
	RefindCoInfos(LayersInfoDicGlob)
	refindBoInfos(LayersInfoDicGlob)
	'''


	break;

#PassageNbErreur = NbErreurTotal
print("\nEND " + str(datetime.datetime.now()))
print("Script took "+str(datetime.datetime.now()-StartingTime)+" To achieve")
print("V0.0.3")
