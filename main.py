# This Python file uses the following encoding: utf-8

# if __name__ == "__main__":
#     pass

import sys
import os
import datetime
from PyQt5 import QtWidgets, QtCore, uic, QtGui
from PyQt5.Qt import QPushButton, QMessageBox, QLabel, QImage
from PyQt5.QtGui import QPen, QColor, QPainter, QBrush, QFont
from PyQt5.QtCore import Qt, QSettings, QUrl
from qsripproject import QSRIPProject
import const

from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget


sys.path.append(".")
from import_tab import ImportTab
from sys_tab import SysnoptiqueTab
from directory_tab import DirectoryTab
from reporting_tab import ReportingTab
from field_tab import FieldTab
from tool_tab import ToolTab
from logindialog import LoginDialog
from user import QSRIPUser

from qgis.core import QgsApplication, QgsVectorLayer, QgsProject
from qgis.gui import QgsMapCanvas
from qgis.utils import iface

from pyOTDR import sorparse
import resources_rc
import qsriputils


# os.environ["QT_FATAL_WARNINGS"] = "1"

# def trap_exc_during_debug(*args):
#     # when app raises uncaught exception, print info
#     qsriputils.print_rip(args)
#
#
# # install exception hook: without this, uncaught exception would cause application to exit
# sys.excepthook = trap_exc_during_debug
def qt_message_handler(mode, context, message):
    if mode == QtCore.QtInfoMsg:
        mode = 'INFO'
    elif mode == QtCore.QtWarningMsg:
        mode = 'WARNING'
    elif mode == QtCore.QtCriticalMsg:
        mode = 'CRITICAL'
    elif mode == QtCore.QtFatalMsg:
        mode = 'FATAL'
    else:
        mode = 'DEBUG'
    qsriputils.print_rip('qt_message_handler: line: %d, func: %s(), file: %s' % (
          context.line, context.function, context.file))
    qsriputils.print_rip('  %s: %s\n' % (mode, message))

QtCore.qInstallMessageHandler(qt_message_handler)

class UI(QtWidgets.QMainWindow):

    projs = None
    user: QSRIPUser = None
    tabs = []
    boiteLayer = None
    cableLayer = None
    activeId = 0
    activeQgsProject: QgsProject = None

    def __init__(self, *args, obj=None, **kwargs):
        super(UI, self).__init__(*args, **kwargs)
        uic.loadUi("ui/mainwindow.ui", self)
        conn = qsriputils.db_connect(os.path.join(const.dbpath, "qsrip.sqlite"))
        last_login = qsriputils.get_config(conn, const.key_loggined)
        qsriputils.print_rip(last_login)
        if last_login and len(last_login) > 0:
            qsriputils.print_rip("last login:", last_login)
            self.user = qsriputils.get_user(conn, last_login)
        if conn:
            conn.close()
        if not self.user:
            self.login()

        self.projs = []
        self.comboBoxSelectProject.currentIndexChanged.connect(self.on_change_project)
        self.updateUser(self.user)
        self.buttonLogin.clicked.connect(self.login_Slot)
        self.buttonSetting.clicked.connect(self.setting_Slot)
        self.buttonImport.clicked.connect(self.importTab_Slot)
        self.buttonSysnoptique.clicked.connect(self.sysnoptiqueTab_Slot)
        self.buttonDirectory.clicked.connect(self.directoryTab_Slot)
        self.buttonReporting.clicked.connect(self.reportingTab_Slot)
        self.buttonField.clicked.connect(self.fieldTab_Slot)
        self.buttonTool.clicked.connect(self.toolTab_Slot)
        self.treeOfFeatures = {}
        self.qgisProjectPath = ""
        self.sorfilesPath = ""
        self.sroName = ""
        self.features = []
        self.boiteData = ["REFERENCE","FONCTION", "SUPPORT"]
        self.sorpbo = []

    def login(self):
        login = LoginDialog()
        self.user = QSRIPUser()
        login.user = self.user
        accept = login.exec()
        if accept:
            self.user.last_loggined = datetime.datetime.now()
            conn = qsriputils.db_connect(os.path.join(const.dbpath, "qsrip.sqlite"))
            if conn:
                u = qsriputils.get_user(conn, self.user.userId)
                if u:
                    qsriputils.update_last_login(conn, self.user.userId, self.user.last_loggined)
                else:
                    qsriputils.create_user(conn, (self.user.displayName, self.user.userId, self.user.email, self.user.last_loggined))
                qsriputils.set_config(conn, const.key_loggined, self.user.userId)
                conn.close()
            if self.isHidden():
                self.updateUser(self.user)
                self.setHidden(False)

        else:
            sys.exit()

    def on_change_project(self, value):
        qsriputils.print_rip("change project to: ", value)
        self.activeId = self.projs[value]._id
        self.loadActiveProjectLayer()
        conn = qsriputils.db_connect(os.path.join(const.dbpath, "qsrip.sqlite"))
        if conn:
            qsriputils.set_config(conn, "{}_{}".format(const.key_last_opened_project, self.user.userId),
                              self.activeId)
            conn.close()
        for t in self.tabs:
            if hasattr(t, "update_active_project"):
                t.update_active_project(self.projs[value])

    def updateProjects(self, _projs, activeProj=None):
        if len(_projs) > 0:
            self.projs.clear()
            self.projs.extend(_projs)
            self.activeId = activeProj._id if activeProj else self.projs[0]._id
            self.comboBoxSelectProject.clear()
            self.comboBoxSelectProject.addItems(["{} {} V{}".format(p.project, p.type, p.ver) for p in self.projs])
            if activeProj:
                index = self.comboBoxSelectProject.findText("{} {} V{}".format(activeProj.project, activeProj.type, activeProj.ver), QtCore.Qt.MatchFixedString)
                if index >= 0:
                    self.comboBoxSelectProject.setCurrentIndex(index)

    def activeProject(self):
        if self.activeId:
            for p in self.projs:
                if p._id == self.activeId:
                    return p
        return None

    def loadActiveProjectLayer(self):
        for p in self.projs:
            if p._id == self.activeId:
                qsriputils.print_rip("sro: {}, loading layers".format(p.sro))
                self.cableLayer = QgsVectorLayer(os.path.join(p.qgis_path,p.cable_layer))
                QgsProject.instance().addMapLayer(self.cableLayer)
                qsriputils.print_rip("load layer:", p.cable_layer)
                self.boiteLayer = QgsVectorLayer(os.path.join(p.qgis_path,p.boite_layer))
                QgsProject.instance().addMapLayer(self.boiteLayer)
                qsriputils.print_rip("load layer:", p.boite_layer)
                break

    def updateUser(self, u):
        self.user = u
        if self.user:
            qsriputils.clearLayout(self.contentLayout)
            self.userDisplayNameLabel.setText(self.user.displayName)
            self.userDisplayNameLabel.setHidden(False)
            self.buttonLogin.setText("Logout")
            self.buttonSetting.setHidden(False)
            self.labelSelectProject.setHidden(False)
            self.comboBoxSelectProject.setHidden(False)
            importTab = ImportTab(self)
            self.tabs.append(importTab)
            self.contentLayout.addWidget(importTab)
        else:
            self.buttonLogin.setText("Login")
            self.userDisplayNameLabel.setHidden(True)
            self.labelSelectProject.setHidden(True)
            self.comboBoxSelectProject.setHidden(True)
            self.buttonSetting.setHidden(True)

    def updateProject(self, p):
        self.proj = p
        self.tabs[0].proj = self.proj

    def __selectTab(self, tab):
        for i in range(self.tabLayout.count()):
            w = self.tabLayout.itemAt(i).widget()
            if isinstance(w, QPushButton) and w != tab:
                w.setChecked(False)
            else:
                w.setChecked(True)

    @QtCore.pyqtSlot()
    def login_Slot(self):
        qsriputils.print_rip(self.user)
        conn = qsriputils.db_connect(os.path.join(const.dbpath, "qsrip.sqlite"))
        if conn:
            qsriputils.remove_config(conn, const.key_loggined)
            conn.close()
        self.user = None
        self.updateUser(None)
        self.setHidden(True)
        self.login()

    @QtCore.pyqtSlot()
    def setting_Slot(self):
        pass

    @QtCore.pyqtSlot()
    def importTab_Slot(self):
        if self.user:
            tab = self.sender()
            self.__selectTab(tab)
            for t in self.tabs:
                if isinstance(t, ImportTab):
                    if t.isHidden():
                        t.show()
                else:
                    t.hide()

    @QtCore.pyqtSlot()
    def sysnoptiqueTab_Slot(self):
        if self.user:
            tab = self.sender()
            qsriputils.print_rip(self.projs)
            if len(self.projs) == 0:
                tab.setChecked(False)
                qsriputils.showMessage("Please load a project to view sysnoptique")
            else:
                self.__selectTab(tab)
                foundTab = False
                for t in self.tabs:
                    if isinstance(t, SysnoptiqueTab):
                        foundTab = True
                        if t.isHidden():
                            t.show()
                    else:
                        t.hide()
                if not foundTab:
                    qsriputils.print_rip("not found tab - add first time")
                    sysTab = SysnoptiqueTab(self)
                    self.tabs.append(sysTab)
                    self.contentLayout.addWidget(sysTab)

    @QtCore.pyqtSlot()
    def reportingTab_Slot(self):
        if self.user:
            tab = self.sender()
            if len(self.projs) == 0:
                tab.setChecked(False)
                qsriputils.showMessage("Please load a project to view report")
            else:
                self.__selectTab(tab)
                foundTab = False
                for t in self.tabs:
                    if isinstance(t, ReportingTab):
                        foundTab = True
                        if t.isHidden():
                            t.show()
                    else:
                        t.hide()
                if not foundTab:
                    qsriputils.print_rip("not found tab - add first time")
                    ttab = ReportingTab(self)
                    self.tabs.append(ttab)
                    self.contentLayout.addWidget(ttab)

    @QtCore.pyqtSlot()
    def fieldTab_Slot(self):
        if self.user:
            tab = self.sender()
            self.__selectTab(tab)
            foundTab = False
            for t in self.tabs:
                if isinstance(t, FieldTab):
                    foundTab = True
                    if t.isHidden():
                        t.show()
                else:
                    t.hide()
            if not foundTab:
                qsriputils.print_rip("not found tab - add first time")
                ttab = FieldTab()
                self.tabs.append(ttab)
                self.contentLayout.addWidget(ttab)

    @QtCore.pyqtSlot()
    def directoryTab_Slot(self):
        if self.user:
            tab = self.sender()
            self.__selectTab(tab)
            foundTab = False
            for t in self.tabs:
                if isinstance(t, DirectoryTab):
                    foundTab = True
                    if t.isHidden():
                        t.show()
                else:
                    t.hide()
            if not foundTab:
                qsriputils.print_rip("not found tab - add first time")
                ttab = DirectoryTab()
                self.tabs.append(ttab)
                self.contentLayout.addWidget(ttab)

    @QtCore.pyqtSlot()
    def toolTab_Slot(self):
        if self.user:
            tab = self.sender()
            self.__selectTab(tab)
            foundTab = False
            for t in self.tabs:
                if isinstance(t, ToolTab):
                    foundTab = True
                    if t.isHidden():
                        t.show()
                else:
                    t.hide()
            if not foundTab:
                qsriputils.print_rip("not found tab - add first time")
                ttab = ToolTab(self)
                self.tabs.append(ttab)
                self.contentLayout.addWidget(ttab)


app = QtWidgets.QApplication(sys.argv)
QtGui.QFontDatabase.addApplicationFont("sfuitext-font/SFUIText-Regular.ttf")
QtGui.QFontDatabase.addApplicationFont("sfuitext-font/SFUIText-Medium.ttf")
QtGui.QFontDatabase.addApplicationFont("sfuitext-font/SFUIText-Bold.ttf")
# qsriputils.print_rip(QtGui.QFontDatabase.applicationFontFamilies(idf))
# qsriputils.print_rip(QtGui.QFontDatabase.applicationFontFamilies(idfm))
user: QSRIPUser = None
path = os.getcwd()
const.dbpath = os.path.join(path, "db")
if not os.path.exists(const.dbpath):
    os.mkdir(const.dbpath)
qsriputils.print_rip("path:", const.dbpath)

conn = qsriputils.db_connect(os.path.join(const.dbpath, "qsrip.sqlite"))
if conn:
    qsriputils.create_table(conn, const.sql_create_projects_table)
    qsriputils.create_table(conn, const.sql_create_cable_table)
    qsriputils.create_table(conn, const.sql_create_pbo_table)
    qsriputils.create_table(conn, const.sql_create_user_table)
    qsriputils.create_table(conn, const.sql_create_config_table)
    qsriputils.create_table(conn, const.sql_create_issues_table)
    conn.close()

window = UI()
window.show()

# qgs = QgsApplication([], False)
# qgs.setPrefixPath('C:/OSGeo4W64/apps/qgis')
# qgs.initQgis()
# qsriputils.print_rip(qgs.showSettings())
# qgs.exitQgis()
# sorfile = "C:/Qt/QS-RIP/Reflectometrie - SRO 544 Distri/SRO 544 Distri\pbo 31 191 544 1015/sro 31 191 544 pbo 31 191 544 1015_001_1310_OE.sor"
# a,meta,rawtrace = sorparse(sorfile)
# qsriputils.print_rip(str(meta))
# qsriputils.print_rip(str(a))

app.exec()
