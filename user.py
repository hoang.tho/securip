# This Python file uses the following encoding: utf-8

# if __name__ == "__main__":
#     pass
from dataclasses import dataclass
from datetime import datetime
@dataclass
class QSRIPUser:
    userId: str = ''
    displayName: str = ""
    email: str = ""
    last_loggined: datetime = datetime.now()
    isOnline = False
    role: str = "user"
    password: str = ''
    project_ids = []

