from PyQt5 import QtWidgets, QtCore, uic, QtGui, QtPrintSupport
from PyQt5.QtWidgets import QDialog, QTableWidgetItem, QLabel, QTableWidget, QVBoxLayout, QTreeWidget, QTreeWidgetItem
from qsripproject import QSRIPProject, PBO, Cable
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QPixmap
from matplotlib.figure import Figure
from matplotlib.figure import figaspect
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import numpy as np
import os
import const
from qgis.core import QgsVectorLayer
import qsriputils
from pyOTDR import sorparse
import datetime
import pyqtgraph as pg
import dateutil.relativedelta
f = open("lang.txt", 'r')
from multilang import english, french
if f.read() == 'en':
    ll = english
else:
    ll = french
f.close()

class ReportingTab(QtWidgets.QFrame):
    proj: QSRIPProject
    window: None
    months = ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jui", "Aou", "Sep", "Oct", "Nov", "Dec"]

    def on_project_changed(self):
        self.proj = self.window.activeProject()
        qsriputils.print_rip(f"report tab is: {self.isHidden()}")
        self.update_filter()
        self.updateUI()

    def view_detail_script_issue(self):
        qsriputils.print_rip("view_detail_script_issue")
        dlg = QDialog()
        dlg.resize(400, 600)
        dlg.setWindowTitle(ll['Studies_issues'])
        tableWidget = QTableWidget()
        box = QVBoxLayout()
        box.addWidget(tableWidget)
        tableWidget.setColumnCount(2)
        tableWidget.verticalHeader().setVisible(False)
        tableWidget.setHorizontalHeaderLabels([ll['Point_tech'], "Type"])
        tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        i = 0
        issues = qsriputils.get_issues_with_type(self.list_client_checked, self.lst_project_checked, self.list_sro_checked, 'script')
        if len(issues) > 100:
            issues = issues[:100].copy()
        tableWidget.setRowCount(len(issues))
        for issue in issues:
            tableWidget.setItem(i, 0, QTableWidgetItem(issue.pbo))
            tableWidget.setItem(i, 1, QTableWidgetItem(
                const.errCode[issue.code] if issue.code in const.errCode else issue.code))
            i += 1

        dlg.setLayout(box)
        dlg.exec()

    def view_detail_sor_issue(self):
        qsriputils.print_rip("view_detail_sor_issue")
        dlg = QDialog()
        dlg.resize(400, 600)
        dlg.setWindowTitle(ll['Work_issues'])
        tableWidget = QTableWidget()
        box = QVBoxLayout()
        box.addWidget(tableWidget)
        tableWidget.setColumnCount(2)
        tableWidget.verticalHeader().setVisible(False)
        tableWidget.setHorizontalHeaderLabels([ll['Point_tech'], "Type"])
        tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        i = 0
        issues = qsriputils.get_issues_with_type(self.list_client_checked, self.lst_project_checked, self.list_sro_checked, "sor")
        if len(issues) > 100:
            issues = issues[:100].copy()
        tableWidget.setRowCount(len(issues))
        for issue in issues:
            tableWidget.setItem(i, 0, QTableWidgetItem(issue.pbo))
            tableWidget.setItem(i, 1, QTableWidgetItem(issue.code))
            i += 1

        dlg.setLayout(box)

        dlg.exec()

    def on_filter_project(self):
        qsriputils.print_rip("on_filter_project")
        dlg = QDialog()
        dlg.resize(400, 600)
        dlg.setWindowTitle(ll['Project_Filter'])
        w = QTreeWidget()
        box = QVBoxLayout()
        box.addWidget(w)
        w.setColumnCount(1)
        w.setHeaderHidden(True)
        projs = self.window.projs
        clients = []
        projects = []
        sro = []
        nro = []

        def on_project_select(it, col):
            qsriputils.print_rip(f"select: {it}, {col}")
            text = it.text(0)
            if text.startswith("APD V") or text.startswith("DOE V"):
                qsriputils.print_rip(f"select project: {text}, sro: {it.parent().text(0)}")
                sro = it.parent().text(0)
                for p_ in projs:
                    if p_.sro == sro and text == f"{p_.type} V{p_.ver}":
                        self.proj = p_
                        self.updateUI()
                        break
                dlg.accept()
            elif it.childCount() == 0:
                if text in projects:
                    sro = []
                    for p_ in projs:
                        if p_.project == text and p_.sro not in sro:
                            sro.append(p_.sro)
                    sro.sort()
                    for s in sro:
                        item = QTreeWidgetItem(it, [s])
                        item.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)

                elif text in self.proj.sro:
                    apds = []
                    for p_ in projs:
                        apd_ = f"{p_.type} V{p_.ver}"
                        if p_.sro == text and apd_ not in apds:
                            apds.append(apd_)
                    apds.sort()
                    for a in apds:
                        QTreeWidgetItem(it, [a])

        w.itemClicked.connect(on_project_select)

        for p in projs:
            if p.client not in clients:
                clients.append(p.client)
            if p.project not in projects:
                projects.append(p.project)
            if p.sro not in sro:
                sro.append(p.sro)

        for c in clients:
            root = QTreeWidgetItem([c])
            w.addTopLevelItem(root)
            projs_ = []
            for p in projs:
                if p.client == c and p.project not in projs_:
                    pitem = QTreeWidgetItem(root, [p.project])
                    pitem.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
                    projs_.append(p.project)

        dlg.setLayout(box)
        dlg.setModal(True)
        dlg.exec()

    def __init__(self, w=None):
        super(ReportingTab, self).__init__()
        uic.loadUi('ui/reporting.ui', self)

        self.label.setText(ll['Quality_Score'])
        self.label_2.setText(ll['Comparative_Technical_Points'])
        self.label_3.setText(ll['Conformes'])
        self.label_4.setText(ll['Errors'])
        self.label_5.setText(ll['warnings'])
        self.label_13.setText(ll['Errors'])
        self.label_14.setText(ll['warnings'])
        self.label_15.setText(ll['Conformes'])
        self.label_10.setText(ll['Thematic_score'])
        self.buttonDetailBO.setText(ll['View_Details'])
        self.buttonDetailLigne.setText(ll['View_Details'])
        self.label_16.setText(ll['Studies'])
        self.label_17.setText(ll['Works'])
        self.label_6.setText(ll['Main_issues'])

        self.issueLayout = QVBoxLayout()
        self.scrollAreaWidgetContents.setLayout(self.issueLayout)
        self.canvas = None
        self.window = w
        self.proj = None
        self.mode = ""
        self.key = ""
        self.pwe = None
        self.pwo = None
        self.pww = None
        self.selectedClient = ""
        path = os.getcwd()
        self.labelBlue.setPixmap(QPixmap(os.path.join(path, "images",'blueshield.png')))
        self.labelGreen.setPixmap(QPixmap(os.path.join(path, "images",'greenshield.png')))
        self.labelRed.setPixmap(QPixmap(os.path.join(path, "images",'redshield.png')))
        self.buttonDetailBO.clicked.connect(self.view_detail_script_issue)
        self.buttonDetailLigne.clicked.connect(self.view_detail_sor_issue)
        # self.buttonFilterClient.clicked.connect(self.on_filter_project)
        # self.buttonFilterProject.clicked.connect(self.on_filter_project)
        # self.buttonFilteNRO.clicked.connect(self.on_filter_project)
        self.report = {}
        self.load_report()
        current = datetime.datetime.now().strftime("%Y-%m")
        # self.report = qsriputils.get_issue_report(current)
        self.list_client_checked = list(set([i.client for i in self.window.projs]))
        self.lst_project_checked = list(set([i.project for i in self.window.projs]))
        self.list_sro_checked = list(set([i.sro for i in self.window.projs]))

        self.create_filter()
        self.update_filter()
    def updateText(self, lang):
        global ll
        ll = lang

        self.label.setText(ll['Quality_Score'])
        self.label_2.setText(ll['Comparative_Technical_Points'])
        self.label_3.setText(ll['Conformes'])
        self.label_4.setText(ll['Errors'])
        self.label_5.setText(ll['warnings'])
        self.label_13.setText(ll['Errors'])
        self.label_14.setText(ll['warnings'])
        self.label_15.setText(ll['Conformes'])
        self.label_10.setText(ll['Thematic_score'])
        self.buttonDetailBO.setText(ll['View_Details'])
        self.buttonDetailLigne.setText(ll['View_Details'])
        self.label_16.setText(ll['Studies'])
        self.label_17.setText(ll['Works'])
        self.label_6.setText(ll['Main_issues'])

        self.clientRootItem.setText(0, ll['sociReport'])
        self.projRootItem.setText(0, ll['projectReport'])
        self._draw_issues()

    def load_report(self):
        now = datetime.datetime.now()
        datestr = [now.strftime("%Y-%m")]
        for i in range(1, 7):
            other = now + dateutil.relativedelta.relativedelta(months=-i)
            datestr.append(other.strftime("%Y-%m"))
        qsriputils.print_rip(f"datestr: {datestr}")
        for d in datestr:
            self.report[d] = qsriputils.get_issue_report(d)

    def create_filter(self):
        self.filters = {ll['sociReport']: 0, ll['projectReport']: 1, 'SRO': 2}
        self.clientRootItem = QTreeWidgetItem([ll['sociReport']])
        self.clientRootItem.setForeground(0, QtGui.QBrush(QtGui.QColor(18, 67, 105)))
        # projectRootItem.setChildIndicatorPolicy(QTreeWidgetItem.DontShowIndicator)
        self.filterTreeWidget.addTopLevelItem(self.clientRootItem)
        self.clientRootItem.setExpanded(True)
        self.projRootItem = QTreeWidgetItem([ll['projectReport']])
        self.projRootItem.setForeground(0, QtGui.QBrush(QtGui.QColor(18, 67, 105)))
        # nroRootItem.setChildIndicatorPolicy(QTreeWidgetItem.DontShowIndicator)
        self.filterTreeWidget.addTopLevelItem(self.projRootItem)
        self.projRootItem.setExpanded(True)

        sroRootItem = QTreeWidgetItem(["SRO"])
        sroRootItem.setForeground(0, QtGui.QBrush(QtGui.QColor(18, 67, 105)))
        # entRootItem.setChildIndicatorPolicy(QTreeWidgetItem.DontShowIndicator)
        self.filterTreeWidget.addTopLevelItem(sroRootItem)
        sroRootItem.setExpanded(True)

        self.filterTreeWidget.itemClicked.connect(self.__on_filter_changed)

    def tree_item_clear_child(self, parent):
        children = []
        for i in range(parent.childCount()):
            children.append(parent.child(i))
        for child in children:
            parent.removeChild(child)

    def update_filter(self):
        self.clients = list(set([p.client for p in self.window.projs]))
        self.projects = list(set([p.project for p in self.window.projs]))
        self.sro = list(set([p.sro for p in self.window.projs]))
        self.tree_item_clear_child(self.filterTreeWidget.topLevelItem(0))
        self.tree_item_clear_child(self.filterTreeWidget.topLevelItem(1))
        self.tree_item_clear_child(self.filterTreeWidget.topLevelItem(2))
        for c in self.clients:
            self.__addTreeItem(self.filterTreeWidget.topLevelItem(0), c, checked=True)
        for p in self.projects:
            self.__addTreeItem(self.filterTreeWidget.topLevelItem(1), p, checked=True)
        for s in self.sro:
            self.__addTreeItem(self.filterTreeWidget.topLevelItem(2), s, checked=True)
        self.list_client_checked = self.clients.copy()
        self.lst_project_checked = self.projects.copy()
        self.list_sro_checked = self.sro.copy()

    def export_pdf(self):
        # qsriputils.print_widget(self.scrollAreaWidgetContents, "test.pdf")
        printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        # printer.setPageSize(QtPrintSupport.QPrinter.PageSize)
        dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        dialog.paintRequested.connect(self.print_preview)
        dialog.exec_()

    @QtCore.pyqtSlot(QtPrintSupport.QPrinter)
    def print_preview(self, printer):
        painter = QtGui.QPainter(printer)

        # start scale
        xscale = printer.pageRect().width() * 1.0 / self.reportFrame.width()
        yscale = printer.pageRect().height() * 1.0 / self.reportFrame.height()
        scale = min(xscale, yscale)
        painter.translate(printer.paperRect().center())
        painter.scale(scale, scale)
        painter.translate(-self.reportFrame.width() / 2, -self.reportFrame.height() / 2)
        # end scale

        self.reportFrame.render(painter)
        painter.end()

    @QtCore.pyqtSlot(QtWidgets.QTreeWidgetItem, int)
    def on_filter_changed(self, it, col):
        if it.parent() is None:
            qsriputils.print_rip(f"filter: {it.text(col)}: child:{it.childCount()}")
            it.setExpanded(not it.isExpanded())
        else:
            text = it.text(col)
            if text in self.clients and it.parent().text(0) == ll['sociReport']:
                for i in range(len(self.clients)):
                    client_check = self.clients[i]
                    if self.filterTreeWidget.topLevelItem(0).child(i).checkState(0) == 2 and client_check not in self.list_client_checked:
                        self.list_client_checked.append(client_check)
                    elif self.filterTreeWidget.topLevelItem(0).child(i).checkState(0) != 2 and client_check in self.list_client_checked:
                        self.list_client_checked.remove(client_check)
                        for p in self.window.projs:
                            if p.client == client_check and p.project in self.lst_project_checked and p.sro in self.list_sro_checked:
                                self.list_sro_checked.remove(p.sro)
                        for p in self.window.projs:
                            if p.client == client_check and p.project in self.lst_project_checked:
                                self.lst_project_checked.remove(p.project)
                self.reload_projects(self.list_client_checked)
                self.reload_sro(self.lst_project_checked)
                if self.filterTreeWidget.topLevelItem(1).childCount() > 0:
                    self.filterTreeWidget.topLevelItem(1).setExpanded(True)
            elif text in self.projects and it.parent().text(0) == ll['project']:
                for i in range(len(self.projects)):
                    project_check = self.projects[i]
                    if self.filterTreeWidget.topLevelItem(1).child(i).checkState(0) == 2 and project_check not in self.lst_project_checked:
                        self.lst_project_checked.append(project_check)
                    elif project_check in self.lst_project_checked and self.filterTreeWidget.topLevelItem(1).child(i).checkState(0) == 0:
                        self.lst_project_checked.remove(project_check)
                        for p in self.window.projs:
                            if p.project == project_check and p.sro in self.list_sro_checked:
                                self.list_sro_checked.remove(p.sro)
                self.reload_sro(self.lst_project_checked)
                if self.filterTreeWidget.topLevelItem(2).childCount() > 0:
                    self.filterTreeWidget.topLevelItem(2).setExpanded(True)
            elif text in self.sro and it.parent().text(0) == 'SRO':
                for i in range(len(self.sro)):
                    sro_check = self.sro[i]
                    if self.filterTreeWidget.topLevelItem(2).child(i).checkState(0) == 2 and sro_check not in self.list_sro_checked:
                        self.list_sro_checked.append(sro_check)
                    elif sro_check in self.list_sro_checked and self.filterTreeWidget.topLevelItem(2).child(i).checkState(0) == 0:
                        self.list_sro_checked.remove(sro_check)
                # self.update_sro(self.list_sro_checked)
            self.updateUI()
        qsriputils.print_rip(self.list_client_checked, self.lst_project_checked, self.list_sro_checked)
        # qsriputils.print_rip(f"filter: {it.text(col)} {it.parent()}")

    def __addTreeItem(self, parent, text, checked=False):
        item = QTreeWidgetItem(parent, [text])
        item.setForeground(0, QtGui.QBrush(QtGui.QColor(18, 67, 105)))
        item.setCheckState(0, Qt.Checked if checked else Qt.Unchecked)

    def showEvent(self, a0: QtGui.QShowEvent) -> None:
        qsriputils.print_rip("report now show")
        if self.proj is None or self.proj != self.window.activeProject():
            self.proj = self.window.activeProject()
        self.update_filter()
        self.updateUI()

    # def updateUI(self, mode="all"):
    #     now = datetime.datetime.now()
    #     current = now.strftime("%Y-%m")
    #     filtered_projects = []
    #     if mode == "all":
    #         self.mode = mode
    #         filtered_projects = self.window.projs
    #         errKeys = [f"{client}_error" for client in self.clients]
    #         sor_errKeys = [f"{client}_sor_error" for client in self.clients]
    #         warnKeys = [f"{client}_warn" for client in self.clients]
    #         sor_warnKeys = [f"{client}_sor_warn" for client in self.clients]
    #         testedKeys = [f"{client}_tested" for client in self.clients]
    #         sor_testedKeys = [f"{client}_sor_tested" for client in self.clients]
    #         okKeys = [f"{client}_ok" for client in self.clients]
    #         sor_okKeys = [f"{client}_sor_ok" for client in self.clients]
    #     else:
    #         t = mode.split("|")
    #         key = t[1]
    #         self.mode = t[0]
    #         self.key = key
    #         if t[0] in ["client", "project"]:
    #             self.key = t[1]
    #             if t[0] == "project":
    #                 client = ""
    #                 filtered_projects = [p for p in self.window.projs if p.project == key]
    #
    #                 for p in self.window.projs:
    #                     if p.project == key:
    #                         client = p.client
    #                         break
    #                 key = f"{client}_{key}"
    #             else:
    #                 filtered_projects = [p for p in self.window.projs if p.client == key]
    #
    #             errKeys = [f"{key}_error"]
    #             sor_errKeys = [f"{key}_sor_error"]
    #             warnKeys = [f"{key}_warn"]
    #             sor_warnKeys = [f"{key}_sor_warn"]
    #             testedKeys = [f"{key}_tested"]
    #             sor_testedKeys = [f"{key}_sor_tested"]
    #             okKeys = [f"{key}_ok"]
    #             sor_okKeys = [f"{key}_sor_ok"]
    #         elif t[0] == "sro":
    #             keys = self.report[current].keys()
    #             filtered_projects = [p for p in self.window.projs if key in p.sro]
    #
    #             subkeys = [k for k in keys if key in k]
    #             sor_errKeys = [k for k in subkeys if k.endswith("sor_error")]
    #             errKeys = [k for k in subkeys if k.endswith("error") and not k.endswith("sor_error")]
    #             qsriputils.print_rip(f"sor_error: {sor_errKeys}")
    #             qsriputils.print_rip(f"error: {errKeys}")
    #             sor_warnKeys = [k for k in subkeys if k.endswith("sor_warn")]
    #             warnKeys = [k for k in subkeys if k.endswith("warn") and not k.endswith("sor_warn")]
    #             sor_testedKeys = [k for k in subkeys if k.endswith("sor_tested")]
    #             testedKeys = [k for k in subkeys if k.endswith("tested") and not k.endswith("sor_tested")]
    #             sor_okKeys = [k for k in subkeys if k.endswith("sor_ok")]
    #             okKeys = [k for k in subkeys if k.endswith("ok") and not k.endswith("sor_ok")]
    #     error = 0
    #     warn = 0
    #     ok = 0
    #     sor_error = 0
    #     sor_warn = 0
    #     sor_ok = 0
    #     tested = 0
    #     sor_tested = 0
    #
    #     for key in errKeys:
    #         error += self.report[current][key] if key in self.report[current] else 0
    #     for key in warnKeys:
    #         warn += self.report[current][key] if key in self.report[current] else 0
    #     for key in testedKeys:
    #         tested += self.report[current][key] if key in self.report[current] else 0
    #     for key in okKeys:
    #         ok += self.report[current][key] if key in self.report[current] else 0
    #     for key in sor_errKeys:
    #         sor_error += self.report[current][key] if key in self.report[current] else 0
    #     for key in sor_warnKeys:
    #         sor_warn += self.report[current][key] if key in self.report[current] else 0
    #     for key in sor_testedKeys:
    #         sor_tested += self.report[current][key] if key in self.report[current] else 0
    #     for key in sor_okKeys:
    #         sor_ok += self.report[current][key] if key in self.report[current] else 0
    #     qsriputils.print_rip(f"{error} {warn} {ok}, {sor_error} {sor_warn} {sor_ok}")
    #     if self.proj:
    #         self.labelProject.setText(
    #             f"{self.proj.client} {self.proj.project} {self.proj.sro} {self.proj.type} V{self.proj.ver}")
    #         # time = datetime.datetime.now()
    #         # self.tested = self.proj.sor_tested
    #         # self.error = self.proj.sor_error
    #         # self.warn = self.proj.sor_warn
    #         # qsriputils.print_rip("loading time:", datetime.datetime.now() - time)
    #     if tested > 0:
    #         self.labelScriptRatio.setText(
    #             f"{round(ok * 100 / tested)}%")
    #     else:
    #         self.labelScriptRatio.setText("N/A")
    #
    #     if sor_tested > 0:
    #         self.labelSorRatio.setText(f"{round(sor_ok * 100 / sor_tested)}%")
    #     else:
    #         self.labelSorRatio.setText("N/A")
    #     totalTested = tested + sor_tested
    #     totalWarn = warn + sor_warn
    #     totalError = error + sor_error
    #     totalOk = ok + sor_ok
    #     vqse2 = sum((p.vqse1 + p.vqse2 + p.vqse3) for p in filtered_projects)
    #     vqse1 = sum((p.vqse1 + p.vqse3) for p in filtered_projects)
    #     vqseA = sum(p.vqse3 for p in filtered_projects)
    #     vqseB = sum((p.vqse2 + p.vqse3) for p in filtered_projects)
    #     totalOk += vqse1
    #     totalTested += vqse1
    #     self.labelTotal.setText(str(totalTested))
    #     self.labelNumOk.setText(str(totalOk))
    #     self.labelNumWarn.setText(str(totalWarn))
    #     self.labelNumError.setText(str(totalError))
    #     self.labelOkCount.setText(str(totalOk))
    #     self.labelWarningCount.setText(str(totalWarn))
    #     self.labelTotalError.setText(str(totalError))
    #     if totalTested > 0:
    #         self.barLayour.setStretch(0, round(totalWarn * 100 / totalTested))
    #         self.barLayour.setStretch(1, round(totalOk * 100 / totalTested))
    #         self.barLayour.setStretch(2, round(totalError * 100 / totalTested))
    #
    #     self._draw(totalTested=totalTested, totalOk=totalOk)
    #
    #     # error graph
    #     if self.pwe:
    #         self.errorLayout.removeWidget(self.pwe)
    #         self.pwe.setParent(None)
    #     self.pwe = pg.PlotWidget()
    #     self.errorLayout.addWidget(self.pwe)
    #     datestr = [now.strftime("%Y-%m")]
    #     for i in range(1, 7):
    #         other = now + dateutil.relativedelta.relativedelta(months=-i)
    #         datestr.append(other.strftime("%Y-%m"))
    #     datestr = list(reversed(datestr))
    #     err = []
    #     www = []
    #     okk = []
    #     for ss in datestr:
    #         _error = 0
    #         _warn = 0
    #         _ok = 0
    #         for key in errKeys:
    #             _error += self.report[ss][key] if key in self.report[ss] else 0
    #         for key in warnKeys:
    #             _warn += self.report[ss][key] if key in self.report[ss] else 0
    #         for key in okKeys:
    #             _ok += self.report[ss][key] if key in self.report[ss] else 0
    #         for key in sor_errKeys:
    #             _error += self.report[ss][key] if key in self.report[ss] else 0
    #         for key in sor_warnKeys:
    #             _warn += self.report[ss][key] if key in self.report[ss] else 0
    #         for key in sor_okKeys:
    #             _ok += self.report[ss][key] if key in self.report[ss] else 0
    #         err.append(_error)
    #         www.append(_warn)
    #         okk.append(_ok)
    #     self.labelVQSERatio.setText("NA" if vqseB == 0 else f"{(vqseA*100/vqseB):.0f}%")
    #     self.labelVQSE2.setText(f"{vqse2}/{totalTested} Points techniques audites")
    #     self.pwe.setBackground('w')
    #     xax = self.pwe.getAxis('bottom')
    #     month = now.month
    #     xm = []
    #     if month >= 7:
    #         xm = self.months[month - 7:month]
    #     ticks = [list(zip(range(7), xm))]
    #     xax.setTicks(ticks)
    #     pen = pg.mkPen(color=(206, 8, 20), width=4)
    #     self.pwe.plot(range(7), err, pen=pen, symbol="o", symbolSize=10, symbolBrush=(206, 8, 20))
    #
    #     # warning graph
    #     if self.pww:
    #         self.warningLayout.removeWidget(self.pww)
    #         self.pww.setParent(None)
    #
    #     self.pww = pg.PlotWidget()
    #     self.warningLayout.addWidget(self.pww)
    #     # www = [0, 0, 0, 0, 0, 0, totalWarn]
    #     self.pww.setBackground('w')
    #     xax = self.pww.getAxis('bottom')
    #     xax.setTicks(ticks)
    #     pen = pg.mkPen(color=(253, 127, 35), width=4)
    #     self.pww.plot(range(7), www, pen=pen, symbol="o", symbolSize=10, symbolBrush=(253, 127, 35))
    #
    #     # ok graph
    #     if self.pwo:
    #         self.okLayout.removeWidget(self.pwo)
    #         self.pwo.setParent(None)
    #
    #     self.pwo = pg.PlotWidget()
    #     self.okLayout.addWidget(self.pwo)
    #     # okk = [0, 0, 0, 0, 0, 0, totalOk]
    #     self.pwo.setBackground('w')
    #     xax = self.pwo.getAxis('bottom')
    #     xax.setTicks(ticks)
    #     pen = pg.mkPen(color=(50, 166, 27), width=4)
    #     self.pwo.plot(range(7), okk, pen=pen, symbol="o", symbolSize=10, symbolBrush=(50, 166, 27))
    #     # issues
    #     qsriputils.clearLayout(self.issueLayout)
    #     thismonth = datetime.datetime.now().strftime("%Y-%m")
    #
    #     issuesCode = qsriputils.get_issue_report(f"{thismonth}_code")
    #     # issues = qsriputils.get_all_issues(self.window.user.azureId, admin=self.window.user.role == "admin")
    #     code = []
    #     allKeys = list(issuesCode.keys())
    #     for key in allKeys:
    #         index = key.rindex("]")
    #         c = key[index + 1:]
    #         if c not in code:
    #             code.append(c)
    #     codeKeys = {}
    #     totalCode = 0
    #     qsriputils.print_rip(f"code: {code}")
    #     if mode == "all":
    #         for c in code:
    #             for client in self.clients:
    #                 k = f"[{client}]{c}"
    #                 if k in issuesCode:
    #                     v = 0
    #                     if c in codeKeys:
    #                         v = codeKeys[c]
    #                     codeKeys[c] = v + issuesCode[k]
    #                     totalCode += issuesCode[k]
    #     else:
    #         t = mode.split("|")
    #         key = t[1]
    #         if t[0] in ["client", "project"]:
    #             if t[0] == "client":
    #                 projects = list(set([p.project for p in self.window.projs if p.client == key]))
    #
    #                 qsriputils.print_rip(f"project: {projects}")
    #                 for c in code:
    #                     for p in projects:
    #                         k = f"[{key}][{p}]{c}"
    #                         if k in issuesCode:
    #                             v = 0
    #                             if c in codeKeys:
    #                                 v = codeKeys[c]
    #                             codeKeys[c] = v + issuesCode[k]
    #                             totalCode += issuesCode[k]
    #             else:
    #                 client = ""
    #                 for p in self.window.projs:
    #                     if p.project == key:
    #                         client = p.client
    #                         break
    #                 for c in code:
    #                     k = f"[{client}][{key}]{c}"
    #                     if k in issuesCode:
    #                         v = 0
    #                         if c in codeKeys:
    #                             v = codeKeys[c]
    #                         codeKeys[c] = v + issuesCode[k]
    #                         totalCode += issuesCode[k]
    #         elif t[0] == "sro":
    #             for c in code:
    #                 for k in allKeys:
    #                     if k.startswith(f"[SRO-{key}") and k.endswith(c):
    #                         v = 0
    #                         if c in codeKeys:
    #                             v = codeKeys[c]
    #                         codeKeys[c] = v + issuesCode[k]
    #                         totalCode += issuesCode[k]
    #
    #     qsriputils.print_rip(mode)
    #     qsriputils.print_rip(codeKeys)
    #     qsriputils.print_rip(totalCode)
    #     newCode = sorted(codeKeys, key=codeKeys.get, reverse=True)[:5]
    #     qsriputils.print_rip(f"newcode: {newCode}")
    #     for k in newCode:
    #         vbox = QtWidgets.QVBoxLayout()
    #         hbox = QtWidgets.QHBoxLayout()
    #         hbox2 = QtWidgets.QHBoxLayout()
    #         nLabel = QLabel(f"{codeKeys[k]}")
    #         nLabel.setFont(QFont('SF UI Text-Semibold', 14))
    #         nLabel.setStyleSheet("color: rgb(68,114,196)")
    #         eLabel = QLabel("Erreur")
    #         eLabel.setFont(QFont('SF UI Text-Semibold', 12))
    #         eLabel.setStyleSheet("color:red")
    #         ilabel = QLabel(const.errCode[k] if k in const.errCode else k)
    #         hbox.addWidget(nLabel)
    #         hbox.addWidget(eLabel)
    #         hbox.addWidget(ilabel)
    #         hbox.setStretch(0, 50)
    #         hbox.setStretch(1, 200)
    #         hbox.setStretch(2, 1000)
    #         label1 = QLabel()
    #         label2 = QLabel()
    #         hbox2.addWidget(label1)
    #         hbox2.addWidget(label2)
    #         label1.setStyleSheet("border: 1px solid rgb(68,114,196); background-color: rgb(68,114,196)")
    #         label2.setStyleSheet("border: 1px solid rgb(68,114,196);")
    #         rate = float(codeKeys[k]) * 100 / float(totalCode)
    #         hbox2.setStretch(0, rate)
    #         hbox2.setStretch(1, 100 - rate)
    #         hbox2.setSpacing(0)
    #         vbox.addLayout(hbox)
    #         vbox.addLayout(hbox2)
    #         vbox.setSpacing(10)
    #         w = QtWidgets.QWidget(self)
    #         w.setLayout(vbox)
    #         vbox.setContentsMargins(0, 0, 0, 0)
    #         self.issueLayout.addWidget(w)
    #     # co_in = qsriputils.get_issues("#CO_IN", self.proj.sro)
    #     # co_ot = qsriputils.get_issues("#CO_OT", self.proj.sro)
    #     # inlabel = QLabel()
    #     # inlabel.setTextFormat(Qt.RichText)
    #     # inlabel.setText(f"<b>{len(co_in)}</b>: {const.errCode['#CO_IN']}")
    #     # self.issueLayout.addWidget(inlabel)
    #     # otlabel = QLabel()
    #     # otlabel.setTextFormat(Qt.RichText)
    #     # otlabel.setText(f"<b>{len(co_ot)}</b>: {const.errCode['#CO_OT']}")
    #     # self.issueLayout.addWidget(otlabel)
    #     self.issueLayout.insertStretch(-1, 500)

    def _draw(self, totalTested, totalOk):
        '''(re)draw the plot with the latest data'''
        # totalTested = self.tested + self.proj.tested
        # totalWarn = self.warn + self.proj.warn
        # totalError = self.error + self.proj.error
        w, h = figaspect(1)
        qsriputils.print_rip((w, h))
        fig = Figure(figsize=(w, h))
        fig.set_facecolor("none")
        plt = fig.add_subplot(1, 1, 1)
        plt.set_aspect("equal")
        x = np.array([totalOk, totalTested - totalOk])
        plt.pie(x,
                colors=[(27.0 / 255.0, 114.0 / 255.0, 179.0 / 255.0), "white"],
                startangle=90)
        y = np.array([100])
        plt.pie(y, colors=[(245.0 / 255.0, 245.0 / 255.0, 245.0 / 255.0)], radius=0.8)
        diff = totalOk * 100 / totalTested if totalTested > 0 else 0
        plt.text(0, 0, "{}%".format(round(diff)), ha="center")
        if self.canvas:
            self.canvas.close()

        self.canvas = FigureCanvas(fig)
        qsriputils.clearLayout(self.chartLayout)
        self.chartLayout.addWidget(self.canvas)

    def reload_projects(self, client_checked):
        while self.filterTreeWidget.topLevelItem(1).childCount() > 0:
            self.filterTreeWidget.topLevelItem(1).removeChild(self.filterTreeWidget.topLevelItem(1).child(0))
        projects = []
        for p in self.window.projs:
            if p.client in client_checked:
                projects.append(p.project)
        projects = set(projects)
        self.projects = list(projects)
        for p in projects:
            if p in self.lst_project_checked:
                self.__addTreeItem(self.filterTreeWidget.topLevelItem(1), p, checked=True)
            else:
                self.__addTreeItem(self.filterTreeWidget.topLevelItem(1), p)

    def reload_sro(self, lst_project_checked):
        while self.filterTreeWidget.topLevelItem(2).childCount() > 0:
            self.filterTreeWidget.topLevelItem(2).removeChild(self.filterTreeWidget.topLevelItem(2).child(0))
        sro = []
        for p in self.window.projs:
            if p.project in lst_project_checked:
                sro.append(p.sro)
        sro = set(sro)
        self.sro = list(sro)
        for p in sro:
            if p in self.list_sro_checked:
                self.__addTreeItem(self.filterTreeWidget.topLevelItem(2), p, checked=True)
            else:
                self.__addTreeItem(self.filterTreeWidget.topLevelItem(2), p)


    def updateUI(self):
        date = datetime.datetime.now()
        self.labelProject.setText(date.strftime('%m/%d/%Y'))
        month_list = []
        import dateutil.relativedelta
        for i in range(7):
            month_list.append(self._date_calculate(date))
            date = date + dateutil.relativedelta.relativedelta(months=-1)
        date = date + dateutil.relativedelta.relativedelta(months=7)

        total = month_list[0][0]
        error_count = month_list[0][1]
        warn_count = month_list[0][2]
        ok_count = month_list[0][3]
        ok_script_percent = month_list[0][4]
        ok_sor_percent = month_list[0][5]
        ok_vqse_percent = month_list[0][6]
        month_list.reverse()

        self._drawGraph('error', date, [x[1] for x in month_list], (206, 8, 20))
        self._drawGraph('warn', date, [x[2] for x in month_list], (253, 127, 35))
        self._drawGraph('ok', date, [x[3] for x in month_list], (50, 166, 27))

        self.labelScriptRatio.setText(ok_script_percent)
        self.labelSorRatio.setText(ok_sor_percent)
        self.labelVQSERatio.setText(ok_vqse_percent)

        self.labelVQSE2.setText('')

        self.labelTotal.setText(str(total))
        self.labelNumOk.setText(str(ok_count))
        self.labelNumWarn.setText(str(warn_count))
        self.labelNumError.setText(str(error_count))
        self.labelOkCount.setText(str(ok_count))
        self.labelWarningCount.setText(str(warn_count))
        self.labelTotalError.setText(str(error_count))

        self._draw_issues()

        if total > 0:
            self.barLayour.setStretch(0, round(warn_count * 100 / total))
            self.barLayour.setStretch(1, round(ok_count * 100 / total))
            self.barLayour.setStretch(2, round(error_count * 100 / total))

        self._draw(total, error_count + warn_count)


    def _drawGraph(self, type, now, counts, color):
        if type == 'error' and self.pwe:
            self.errorLayout.removeWidget(self.pwe)
            self.pwe.setParent(None)
        elif type == 'warn' and self.pww:
            self.warningLayout.removeWidget(self.pww)
            self.pww.setParent(None)
        elif type == 'ok' and self.pwo:
            self.okLayout.removeWidget(self.pwo)
            self.pwo.setParent(None)
        pw = pg.PlotWidget()
        if type == 'error':
            self.errorLayout.addWidget(pw)
        elif type == 'warn':
            self.warningLayout.addWidget(pw)
        elif type == 'ok':
            self.okLayout.addWidget(pw)
        datestr = [now.strftime("%Y-%m")]
        for i in range(1, 7):
            other = now + dateutil.relativedelta.relativedelta(months=-i)
            datestr.append(other.strftime("%Y-%m"))
        pw.setBackground('w')
        pw.setLimits(yMin=0)
        xax = pw.getAxis('bottom')
        month = now.month
        xm = []
        if month >= 7:
            xm = self.months[month - 7:month]
        else:
            xm = self.months[11 - (7 - month) + 1:] + self.months[:month]
        ticks = [list(zip(range(7), xm))]
        xax.setTicks(ticks)
        pen = pg.mkPen(color=color, width=4)
        pw.plot(range(7), counts, pen=pen, symbol="o", symbolSize=10, symbolBrush=color)
        if type == 'error':
            self.pwe = pw
        elif type == 'warn':
            self.pww = pw
        elif type == 'ok':
            self.pwo = pw

    def _date_calculate(self, date):
        firstday = date.replace(day=1, hour=0, minute=0, second=0)
        import dateutil.relativedelta
        nextmonth = firstday + dateutil.relativedelta.relativedelta(months=1)
        proj_in_month = []
        for proj in self.window.projs:
            if firstday.replace(tzinfo=None) <= proj.create_date.replace(tzinfo=None) and proj.create_date.replace(tzinfo=None) <= nextmonth.replace(tzinfo=None):
                proj_in_month.append(proj)

        sro_had_set = {}

        for proj in proj_in_month:
            proj = vars(proj)
            if proj['client'] in self.list_client_checked and proj['project'] in self.lst_project_checked and proj['sro'] in self.list_sro_checked:
                if ((proj['sro'] not in sro_had_set) or
                        (sro_had_set[proj['sro']][0] == 'APD' and proj['type'] == 'DOE') or
                        (sro_had_set[proj['sro']][0] == proj['type'] and sro_had_set[proj['sro']][1] < proj['ver'])):
                    sro_error_count = []
                    sro_warn_count = []
                    sro_ok_count = []
                    if 'error' in proj:
                        sro_error_count.append(proj['error'])
                    else:
                        sro_error_count.append(0)

                    if 'sor_error' in proj:
                        sro_error_count.append(proj['sor_error'])
                    else:
                        sro_error_count.append(0)

                    if 'vqse_error' in proj:
                        sro_error_count.append(proj['vqse_error'])
                    else:
                        sro_error_count.append(0)

                    if 'warn' in proj:
                        sro_warn_count.append(proj['warn'])
                    else:
                        sro_warn_count.append(0)

                    if 'sor_warn' in proj:
                        sro_warn_count.append(proj['sor_warn'])
                    else:
                        sro_warn_count.append(0)

                    if 'vqse_warn' in proj:
                        sro_warn_count.append(proj['vqse_warn'])
                    else:
                        sro_warn_count.append(0)

                    if 'ok' in proj:
                        sro_ok_count.append(proj['ok'])
                    else:
                        sro_ok_count.append(0)

                    if 'sor_ok' in proj:
                        sro_ok_count.append(proj['sor_ok'])
                    else:
                        sro_ok_count.append(0)

                    if 'vqse_ok' in proj:
                        sro_ok_count.append(proj['vqse_ok'])
                    else:
                        sro_ok_count.append(0)
                    if 'vqse2' in proj:
                        vqse2_count = proj['vqse2']
                    else:
                        vqse2_count = 0
                    if 'vqse3' in proj:
                        vqse3_count = proj['vqse3']
                    else:
                        vqse3_count = 0
                    sro_had_set[proj['sro']] = [proj['type'], proj['ver'], sro_error_count, sro_warn_count,  sro_ok_count, vqse2_count, vqse3_count]

        error_count = 0
        warn_count = 0
        ok_count = 0

        ok_script_count = 0
        script_count = 0
        ok_sor_count = 0
        sor_count = 0
        vqse2_count = 0
        vqse3_count = 0

        for i in sro_had_set:
            error_count += sum(sro_had_set[i][2])
            warn_count += sum(sro_had_set[i][3])
            ok_count += sum(sro_had_set[i][4])
            script_count += sro_had_set[i][2][0] + sro_had_set[i][3][0] + sro_had_set[i][4][0]
            ok_script_count += sro_had_set[i][4][0]
            sor_count += sro_had_set[i][2][1] + sro_had_set[i][3][1] + sro_had_set[i][4][1]
            ok_sor_count += sro_had_set[i][4][1]
            vqse2_count += sro_had_set[i][5]
            vqse3_count += sro_had_set[i][6]

        total = error_count + warn_count + ok_count
        if script_count > 0:
            script_percent = str((script_count - ok_script_count) * 100 // script_count) + '%'
        else:
            script_percent = 'N/A'
        if sor_count > 0:
            sor_percent = str((sor_count - ok_sor_count) * 100 // sor_count) + '%'
        else:
            sor_percent = 'N/A'
        if vqse2_count + vqse3_count > 0:
            vqse_percent = str(vqse2_count * 100 // (vqse2_count + vqse3_count)) + '%'
        else:
            vqse_percent = 'N/A'
        return [total, error_count, warn_count, ok_count, script_percent, sor_percent, vqse_percent]


    def _draw_issues(self):
        qsriputils.clearLayout(self.issueLayout)

        # issuesCode = qsriputils.get_issue_report(f"{thismonth}_code")
        # issues = qsriputils.get_all_issues(self.window.user.azureId, admin=self.window.user.role == "admin")
        # issues = qsriputils.get_issues_with_type(self.list_client_checked, self.lst_project_checked, "all", self.list_sro_checked)
        code = []
        # allKeys = list(issuesCode.keys())
        # for key in allKeys:
        #     index = key.rindex("]")
        #     c = key[index + 1:]
        #     if c not in code:
        #         code.append(c)
        codeKeys = {}
        totalCode = 0
        # qsriputils.print_rip(f"code: {code}")
        # for c in code:
        #     for client in self.clients:
        #         k = f"[{client}]{c}"
        #         if k in issuesCode:
        #             v = 0
        #             if c in codeKeys:
        #                 v = codeKeys[c]
        #             codeKeys[c] = v + issuesCode[k]
        #             totalCode += issuesCode[k]

        date = datetime.datetime.now()
        firstday = date.replace(day=1, hour=0, minute=0, second=0)
        import dateutil.relativedelta
        nextmonth = firstday + dateutil.relativedelta.relativedelta(months=1)
        proj_in_month = []
        for proj in self.window.projs:
            if firstday.replace(tzinfo=None) <= proj.create_date.replace(tzinfo=None) and proj.create_date.replace(
                    tzinfo=None) <= nextmonth.replace(tzinfo=None):
                proj_in_month.append(proj)

        sro_had_set = {}
        for proj in proj_in_month:
            if proj.client in self.list_client_checked and proj.project in self.lst_project_checked and proj.sro in self.list_sro_checked:
                if ((proj.sro not in sro_had_set) or
                        (sro_had_set[proj.sro].type == 'APD' and proj.type == 'DOE') or
                        (sro_had_set[proj.sro] == proj.type and sro_had_set[proj.sro].ver < proj.ver)):

                    sro_had_set[proj.sro] = proj

        for sro in sro_had_set:
            for k in sro_had_set[sro].code:
                if k not in codeKeys:
                    codeKeys[k] = sro_had_set[sro].code[k]
                else:
                    codeKeys[k] += sro_had_set[sro].code[k]

        for i in codeKeys:
            totalCode += codeKeys[i]
        newCode = sorted(codeKeys, key=codeKeys.get, reverse=True)
        qsriputils.print_rip(f"newcode: {newCode}")
        for k in newCode:
            vbox = QtWidgets.QVBoxLayout()
            hbox = QtWidgets.QHBoxLayout()
            hbox2 = QtWidgets.QHBoxLayout()
            nLabel = QLabel(f"{codeKeys[k]}")
            nLabel.setFont(QFont('SF UI Text-Semibold', 14))
            nLabel.setStyleSheet("color: rgb(68,114,196)")
            self.eLabel = QLabel(ll['Error'])
            self.eLabel.setFont(QFont('SF UI Text-Semibold', 12))
            self.eLabel.setStyleSheet("color:red")
            ilabel = QLabel(const.errCode[k] if k in const.errCode else k)
            hbox.addWidget(nLabel)
            hbox.addWidget(self.eLabel)
            hbox.addWidget(ilabel)
            hbox.setStretch(0, 50)
            hbox.setStretch(1, 200)
            hbox.setStretch(2, 1000)
            label1 = QLabel()
            label2 = QLabel()
            hbox2.addWidget(label1)
            hbox2.addWidget(label2)
            label1.setStyleSheet("border: 1px solid rgb(68,114,196); background-color: rgb(68,114,196)")
            label2.setStyleSheet("border: 1px solid rgb(68,114,196);")
            rate = float(codeKeys[k]) * 100 / float(totalCode)
            hbox2.setStretch(0, rate)
            hbox2.setStretch(1, 100 - rate)
            hbox2.setSpacing(0)
            vbox.addLayout(hbox)
            vbox.addLayout(hbox2)
            vbox.setSpacing(10)
            w = QtWidgets.QWidget(self)
            w.setLayout(vbox)
            vbox.setContentsMargins(0, 0, 0, 0)
            self.issueLayout.addWidget(w)
        # co_in = qsriputils.get_issues("#CO_IN", self.proj.sro)
        # co_ot = qsriputils.get_issues("#CO_OT", self.proj.sro)
        # inlabel = QLabel()
        # inlabel.setTextFormat(Qt.RichText)
        # inlabel.setText(f"<b>{len(co_in)}</b>: {const.errCode['#CO_IN']}")
        # self.issueLayout.addWidget(inlabel)
        # otlabel = QLabel()
        # otlabel.setTextFormat(Qt.RichText)
        # otlabel.setText(f"<b>{len(co_ot)}</b>: {const.errCode['#CO_OT']}")
        # self.issueLayout.addWidget(otlabel)
        self.issueLayout.insertStretch(-1, 500)

    def __on_filter_changed(self, it, col):
        self.list_client_checked = []
        self.lst_project_checked = []
        self.list_sro_checked = []
        if it.parent() is None:
            qsriputils.print_rip(f"filter: {it.text(col)}: child:{it.childCount()}")
            it.setExpanded(not it.isExpanded())
        else:
            if it.parent().text(0) == 'SRO' and it.checkState(0) == Qt.Checked:
                for proj in self.window.projs:
                    if proj.sro == it.text(0):
                        for i in range(self.filterTreeWidget.topLevelItem(1).childCount()):
                            if self.filterTreeWidget.topLevelItem(1).child(i).text(0) == proj.project:
                                self.filterTreeWidget.topLevelItem(1).child(i).setCheckState(0, Qt.Checked)
                                break
                        for i in range(self.filterTreeWidget.topLevelItem(0).childCount()):
                            if self.filterTreeWidget.topLevelItem(0).child(i).text(0) == proj.client:
                                self.filterTreeWidget.topLevelItem(0).child(i).setCheckState(0, Qt.Checked)
                                break
                        break
            if it.parent().text(0) == ll['project'] and it.checkState(0) == Qt.Checked:
                for proj in self.window.projs:
                    if proj.project == it.text(0):
                        for i in range(self.filterTreeWidget.topLevelItem(0).childCount()):
                            if self.filterTreeWidget.topLevelItem(0).child(i).text(0) == proj.client:
                                self.filterTreeWidget.topLevelItem(0).child(i).setCheckState(0, Qt.Checked)
                                break
                        break

            for i in range(self.filterTreeWidget.topLevelItem(0).childCount()):
                if self.filterTreeWidget.topLevelItem(0).child(i).checkState(0) == Qt.Checked:
                    self.list_client_checked.append(self.filterTreeWidget.topLevelItem(0).child(i).text(0))
            for i in range(self.filterTreeWidget.topLevelItem(1).childCount()):
                if self.filterTreeWidget.topLevelItem(1).child(i).checkState(0) == Qt.Checked:
                    uncheck = False
                    for proj in self.window.projs:
                        if proj.client not in self.list_client_checked and proj.project == self.filterTreeWidget.topLevelItem(1).child(i).text(0):
                            self.filterTreeWidget.topLevelItem(1).child(i).setCheckState(0, Qt.Unchecked)
                            uncheck = True
                            break
                    if not uncheck:
                        self.lst_project_checked.append(self.filterTreeWidget.topLevelItem(1).child(i).text(0))
            for i in range(self.filterTreeWidget.topLevelItem(2).childCount()):
                if self.filterTreeWidget.topLevelItem(2).child(i).checkState(0) == Qt.Checked:
                    uncheck = False
                    for proj in self.window.projs:
                        if proj.project not in self.lst_project_checked and proj.sro == self.filterTreeWidget.topLevelItem(2).child(i).text(0):
                            self.filterTreeWidget.topLevelItem(2).child(i).setCheckState(0, Qt.Unchecked)
                            uncheck = True
                            break
                    if not uncheck:
                        self.list_sro_checked.append(self.filterTreeWidget.topLevelItem(2).child(i).text(0))
            qsriputils.print_rip(self.list_client_checked, self.lst_project_checked, self.list_client_checked)
            self.updateUI()