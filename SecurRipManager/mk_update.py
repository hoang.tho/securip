import os
import shutil


def mk_update():
    root_path = os.path.abspath("../")
    update_path = os.path.join(root_path, "SecurRipManager", "SecurRip")
    folders_path = {
        "cert": os.path.join(root_path, "cert"),
        "fonts": os.path.join(root_path, "fonts"),
        "images": os.path.join(root_path, "images"),
        "ui": os.path.join(root_path, "ui"),
        "videos": os.path.join(root_path, "videos"),
        "tools": os.path.join(root_path, "tools"),
        "box": os.path.join(root_path, "box")
    }
    files_path = {
        "lang.txt": os.path.join(root_path, "lang.txt"),
        "opening.mp4": os.path.join(root_path, "opening.mp4"),
        "opening.wmv": os.path.join(root_path, "opening.wmv"),
        "config.json": os.path.join(root_path, "config.json"),
        "opening.gif": os.path.join(root_path, "opening.gif"),
        # "update.py": os.path.join(root_path, "update.py")
    }
    pyc_path = {
        "": os.path.join(root_path, "__pycache__"),
        "RIPModels": os.path.join(root_path, "RIPModels", "__pycache__"),
        "RIPUtils": os.path.join(root_path, "RIPUtils", "__pycache__")
    }
    print("Copy static folders ...")
    if os.path.exists(update_path):
        shutil.rmtree(update_path)
    os.mkdir(update_path)
    for path in folders_path:
        shutil.copytree(folders_path[path], os.path.join(update_path, path))
    print("Copy static files ...")
    for path in files_path:
        shutil.copyfile(files_path[path], os.path.join(update_path, path))
    print("Compile rip.py ...")
    os.system(f"%PYTHONHOME%\\python -m compileall {os.path.join(root_path, 'rip.py')}")
    # print("Compile update.py ...")
    # os.system(f"%PYTHONHOME%\\python -m compileall {os.path.join(root_path, 'update.py')}")
    print("Copy pyc files ...")
    for path in pyc_path:
        if not os.path.exists(pyc_path[path]):
            print(f"{pyc_path[path]} not found.")
            return
        if path:
            os.mkdir(os.path.join(update_path, path))
        for filename in os.listdir(pyc_path[path]):
            filename_part = filename.split(".")
            shutil.copyfile(
                os.path.join(pyc_path[path], filename),
                os.path.join(update_path, path, f"{filename_part[0]}.{filename_part[2]}")
            )
    shutil.make_archive("SecurRip", "zip", update_path)
    print("Make update done.")


if __name__ == "__main__":
    mk_update()
