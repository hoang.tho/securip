import os
import shutil


def rm_pyc():
    print("Remove '__pycache__' folders ...")
    root_path = os.path.abspath("../")
    pyc_paths = [
        os.path.join(root_path, "__pycache__"),
        os.path.join(root_path, "RIPModels", "__pycache__"),
        os.path.join(root_path, "RIPUtils", "__pycache__")
    ]
    for path in pyc_paths:
        if os.path.exists(path):
            shutil.rmtree(path)
    print("Remove done.")


if __name__ == "__main__":
    rm_pyc()
