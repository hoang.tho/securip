from dataclasses import dataclass
from datetime import datetime
import const
from qsriputils import print_rip


@dataclass
class SORMeta:
    date: datetime = datetime.now()
    pbo: str = ""
    sections = []

    def from_meta(self, meta):
        fix_params = meta["FxdParams"]
        date_time = fix_params["date/time"][:-17]
        self.date = datetime.strptime(date_time, const.sor_date_format)
        self.sections = []
        key_events = meta["KeyEvents"]
        print_rip(f"key: {key_events}")
        num = key_events["num events"]
        ii = 2
        start = float(key_events["event 1"]["distance"])
        if start == 0.0:
            ii = 3
            start = float(key_events["event 2"]["distance"])
        for i in range(ii, num + 1):
            k = "event {}".format(i)
            e = key_events[k]
            d = float(e["distance"])
            loss = float(e["splice loss"])
            s = SORSection(length=round(1000 * (d - start)), loss=loss)
            self.sections.append(s)
            start = d


@dataclass
class SORSection:
    length: int = 0
    loss: float = 0.0
