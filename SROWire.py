# This Python file uses the following encoding: utf-8
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPen, QColor, QPainter, QBrush, QPixmap
from PyQt5.QtCore import Qt, QRect, QSize
from PyQt5.QtWidgets import QWidget
import os


class SROWire(QWidget):
    clicked = QtCore.pyqtSignal()
    selected = False
    editted = False
    icons = ["blueshield.png", "redshield.png", "greenshield.png"]

    def __init__(self, text="224FO", cable="cable", status=-1, left_border=False, last_node=False, first_node=False,
                 vqse=0, cable_info=dict(),
                 *args, **kwargs):
        super(SROWire, self).__init__(*args, **kwargs)
        self.cable_info = cable_info

        self.text = text
        self.cable = cable
        self.status = status
        self.left_border = left_border
        self.last_node = last_node
        self.vqse = vqse
        self.first_node = first_node
        self.selected = False
        # print('wire widget with text', self.text)
        self.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)

    def sizeHint(self):
        return QtCore.QSize(80, 30)

    def paintEvent(self, e):
        painter = QPainter(self)
        color = QColor(176, 97, 255)  # not tested
        if self.status == 2:
            color = QColor("red")
        elif self.status == 0:
            color = QColor("green")
        elif self.status == 1:
            color = QColor("orange")
        if self.editted:
            color = QColor("green")
        pen = QPen(color, 5 if self.selected else 3, Qt.DashLine if self.cable_info['type'] == 'AERIEN' else Qt.SolidLine)
        painter.setPen(pen)
        painter.drawLine(0, painter.device().height() / 2, painter.device().width(), painter.device().height() / 2)
        pen = QPen(color, 5 if self.selected else 3)
        painter.setPen(pen)
        if self.left_border:
            x1 = 0
            y1 = 0
            x2 = 0
            y2 = painter.device().height()
            if self.first_node:
                y1 = painter.device().height() / 2
            elif self.last_node:
                y2 = painter.device().height() / 2
            painter.drawLine(x1, y1, x2, y2)

        rect = QtCore.QRect(0, 0, painter.device().width(), painter.device().height())
        # print("wire rect:", str(rect))
        font = QtGui.QFont("Segoe MDL2 Assets", 9)
        painter.setFont(font)
        fm = QtGui.QFontMetrics(font)
        w = fm.width(self.text)
        if self.vqse > 0:
            path = os.getcwd()
            pixpath = os.path.join(path, "images", self.icons[self.vqse - 1])
            pixmap = QPixmap(pixpath)
            painter.drawPixmap(QRect(0, 2, 16, 16), pixmap)
        painter.fillRect(QtCore.QRect((painter.device().width() - w) / 2 - 5, 0, w + 10, painter.device().height()),
                         QBrush(Qt.white))
        painter.drawText(rect, Qt.AlignCenter, self.text)

        painter.end()

    def mousePressEvent(self, e):
        self.clicked.emit()
        # self.selected = True
        # self.update()
        e.accept()

    def setSelected(self, value):
        if value != self.selected:
            self.selected = value
            self.update()

    def setVQSE(self, v):
        self.vqse = v
        self.update()

    def setEditted(self, e):
        self.editted = e
        if e:
            if not self.text.endswith(" \uE70F"):
                self.text += " \uE70F"
        else:
            if self.text.endswith(" \uE70F"):
                self.text = self.text.replace(" \uE70F", "")
        self.update()


class LeftWire(QWidget):
    selected = False

    def __init__(self, *args, **kwargs):
        super(LeftWire, self).__init__(*args, **kwargs)
        self.selected = False
        self.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.MinimumExpanding)

    def sizeHint(self):
        return QtCore.QSize(100, 30)

    def paintEvent(self, e):
        painter = QPainter(self)
        pen = QPen(QColor(176, 97, 255), 5 if self.selected else 3)
        painter.setPen(pen)
        painter.drawLine(0, 0, 0, painter.device().height())
        painter.end()

    def setSelected(self, value):
        if value != self.selected:
            self.selected = value
            self.update()
